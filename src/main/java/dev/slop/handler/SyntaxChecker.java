package dev.slop.handler;

import dev.slop.exception.LexerException;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenGroup;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.grammar.GrammarExpression;
import dev.slop.tokens.grammar.GrammarGroup;
import dev.slop.tokens.grammar.GrammarReference;
import dev.slop.tokens.grammar.GrammarValue;

import java.util.*;
import java.util.stream.Collectors;

/**
 * The syntax checker performs basic checks to ensure that a token is not left expecting a token. This can cause
 * major issues which are difficult to track down by the time it reaches the Parser, so this is used to help
 * developers by providing easier to read and diagnose messages.
 */
public class SyntaxChecker implements ErrorHandler {

    /**
     * Performs a check to see if there are still items left on the stack, context or resulting token store. Typically
     * this is done by checking the top-most stack token (if one exists) to determine if it is still expecting a token
     * or whether it is technically complete (like in the case of optionals or multiples). An exception is thrown if
     * an expected token is missing or required empty token groups to prevent errors from occurring later in the Parser.
     * Guidance is provided by deferring responsibility to each token to highlight the likely error in the expression.
     * @param expression The original expression tokenized by this Lexer class
     * @param stack The current state of the stack post-tokenization of the expression
     * @param context The state of the short term store for tokens post-tokenization
     * @param evaluated The long term store for completed tokens post-tokenization
     */
    @Override
    public void performCheck(String expression, Stack<Token<?>> stack, List<Token<?>> context, List<Token<?>> evaluated) {
        if (!stack.isEmpty()) {
            Optional<Token<?>> expected = Optional.empty();
            Stack<Token<?>> backUp = new Stack<>();
            while (!stack.isEmpty()) {
                Token<?> topmost = stack.peek();
                //Skip for single repeatable groups which revert to 0 even after being successful e.g. FieldToken
                if (!(topmost.getTokenPosition() == 0 && topmost.getPatternTokens().size() == 1 &&
                        topmost.getPatternTokens().get(0) instanceof GrammarGroup &&
                        ((GrammarGroup)topmost.getPatternTokens().get(0)).isMultiple()) &&
                        !grammarRefComplete(topmost))
                    expected = topmost.getExpectedToken(context);
                List<Integer> emptyGroups = getEmptyTokenGroups(topmost);
                if (!emptyGroups.isEmpty()) {
                    /* If the active token is a grammar value capture and a single item exists in the context or
                     * up the stack then remove these empty groups for validation. */
                    Token<?> activeToken = topmost.getActiveToken(true);
                    if ((activeToken instanceof GrammarValue) &&
                            (!context.isEmpty() || !backUp.isEmpty()) && topmost.getCurrentGroup().intValue() != 0) {
                        emptyGroups.remove(topmost.getCurrentGroup().intValue() - 1);
                    }
                }
                if (expected.isPresent()) {
                    reportError(expression, topmost, expected.get(), emptyGroups, context);
                } else if (!emptyGroups.isEmpty() && backUp.isEmpty()) {
                    reportError(expression, topmost, new TokenValue(""), emptyGroups, context);
                } else {
                    backUp.push(stack.pop());
                }
            }
            while (!backUp.isEmpty()) stack.push(backUp.pop());
        }
    }

    /**
     * Determines whether a grammar reference is complete. This differs to the traditional method
     * of knowing whether a token is complete as those use the pattern tokens and token position.
     * In the case of a reference, it is used as a template but the token groups determine whether
     * the pattern is complete.
     * @param topmost The token containing grammar references
     * @return Returns true if the token reference is complete
     */
    private boolean grammarRefComplete(Token<?> topmost) {
        return topmost.getPatternTokens().get(topmost.getTokenPosition()) instanceof GrammarReference &&
                topmost.getTokenGroups().size() >= topmost.getCurrentGroup().intValue() &&
               topmost.getTokenGroups().get(Math.max(0, topmost.getCurrentGroup().intValue() - 1))
                .getFlatTokens().get(0).isComplete();
    }

    /**
     * Searches the list of generated token groups for any which don't contain at least one valid token. This is
     * likely if the definition has been left out, or alternatively if there is an earlier syntax error and this
     * can act as a fallback.
     * @param token The token on which to find empty token groups
     * @return Returns the list of indexes of found empty token groups
     */
    private List<Integer> getEmptyTokenGroups(Token<?> token) {
        ArrayList<Integer> emptyGroups = new ArrayList<>();
        for (int i = 0;i < token.getTokenGroups().size();i++) {
            TokenGroup tg = token.getTokenGroups().get(i);
            if (tg.getFlatTokens().isEmpty()) emptyGroups.add(i);
        }
        return emptyGroups;
    }

    /**
     * Reports currently two different types of errors which are
     *      1) An expected token missing
     *      2) A required token group existing and is found to be empty
     * Either of these are fail errors and so this information is sent to the related token class so that a
     * human readale error in the form of 'guidance' can be returned and displayed.
     * @param expression The original expression being evaluated
     * @param topmost The topmost token on the stack where an error has been found
     * @param expected The next expected (but missing) token value
     * @param emptyGroups A list of any required empty token groups found
     * @param context The context to print a list of last read tokens
     */
    private void reportError(String expression, Token<?> topmost, Token<?> expected, List<Integer> emptyGroups,
                             List<Token<?>> context) {
        Optional<TokenGroup> lastPopulatedGroup = findLastPopulatedTokenGroup(topmost);
        String lastTokens = context.isEmpty() ?
                !lastPopulatedGroup.isPresent() ? "" :
                    lastPopulatedGroup.get().getFlatTokens().stream()
                            .map(Token::toSimpleString)
                            .map(s -> String.format("'%s'", s.replace("'", "\'")))
                            .collect(Collectors.joining(",")) :
                context.stream()
                        .map(Token::getValue)
                        .filter(Objects::nonNull)
                        .map(Object::toString)
                        .map(s -> s.replace("'", ""))
                        .map(s -> String.format("'%s'", s))
                        .collect(Collectors.joining(","));

        String guidance = Objects.isNull(expected.getValue()) ? "None Available" :
                topmost.getGuidance(expected.getValue().toString(), emptyGroups).orElse("None Available");
        if (expected instanceof GrammarGroup || expected.getValue().equals("")) {
            String error = "The minimum statement requirements for %s were not met.";
            throw new LexerException(String.format(error +
                            System.lineSeparator() + "----------------------------------------------------------------" +
                            System.lineSeparator() + "- Guidance: %s" + System.lineSeparator() +
                            "- Last Read Tokens: %s" + System.lineSeparator() + "- Expression: %s" +
                            System.lineSeparator() + "----------------------------------------------------------------",
                    topmost.getClass().getSimpleName(), guidance, lastTokens, expression));
        } else {
            String error = "Expected token '%s' in %s but expression ended prematurely. ";
            throw new LexerException(String.format(error +
                            System.lineSeparator() + "----------------------------------------------------------------" +
                            System.lineSeparator() + "- Guidance: %s" + System.lineSeparator() +
                            "- Last Read Tokens: %s" + System.lineSeparator() + "- Expression: %s" +
                            System.lineSeparator() + "----------------------------------------------------------------",
                    Objects.isNull(expected.getValue()) ? "???" : expected.getValue().toString(),
                    topmost.getClass().getSimpleName(), guidance, lastTokens, expression));
        }
    }

    /**
     * As issues can happen with the lexing at any stage, we don't know if the last token group which was added had any
     * tokens. As such, read through the groups backwards until one with tokens is found.
     * @param topmost The token to scan for contained tokens
     * @return Returns the populated token group
     */
    private Optional<TokenGroup> findLastPopulatedTokenGroup(Token<?> topmost) {
        for (int i = topmost.getTokenGroups().size() - 1;i > -1;i--) {
            List<Token<?>> groupTokens = topmost.getTokenGroups().get(i).getFlatTokens();
            if (!groupTokens.isEmpty()) return Optional.of(topmost.getTokenGroups().get(i));
        }
        return Optional.empty();
    }
}
