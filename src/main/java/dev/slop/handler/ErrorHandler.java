package dev.slop.handler;

import dev.slop.tokens.Token;

import java.util.List;
import java.util.Stack;

/**
 * Error handlers can be configured in the config object and are used as a check post-tokenization. One or more
 * of these handlers can be used and triggered so long as they are added using the SLOPConfig.addErrorHandler(...)
 * and implement this interface.
 */
public interface ErrorHandler {

    /**
     * Performs a check on the state of the stack, short and long term tokens to determine whether any token is
     * in an incomplete state and whether to show an error.
     * @param expression A string version of the expression that was processed by the Lexer to include in the error
     * @param stack The stack, which if not empty then something has likely gone wrong
     * @param context The short term memory store, which again if not empty will likely mean an issue
     * @param evaluated The final store for any resulting tokens from the expression. These can be validated by
     *                  checking for empty token groups.
     */
    void performCheck(String expression, Stack<Token<?>> stack, List<Token<?>> context, List<Token<?>> evaluated);
}
