package dev.slop.callback;

import dev.slop.tokens.Token;

import java.util.Optional;

/**
 * Provides a way to perform a callback at a specific stage during a tokens processing. For example,
 * this allows a token to separate out its component parts such as a loop. This allows a token class
 * to be defined for the parent token, the loop and finally the body which gets executed on each
 * iteration. This lambda callback would be used when calling the loop portion so that a callback
 * event can initiate the body on each iteration.
 */
@FunctionalInterface
public interface ReferenceCallback {
    Optional<Token<?>> call();
}
