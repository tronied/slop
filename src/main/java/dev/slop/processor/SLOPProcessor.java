package dev.slop.processor;

import com.fasterxml.jackson.databind.type.TypeFactory;
import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.exception.ParserException;
import dev.slop.exception.SLOPException;
import dev.slop.lexer.SLOPLexer;
import dev.slop.lexer.SLOPGrammarLexer;
import dev.slop.lexer.Lexer;
import dev.slop.model.ExpressionResult;
import dev.slop.model.LexerResult;
import dev.slop.parser.SLOPParser;
import dev.slop.parser.Parser;
import dev.slop.tokens.Token;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The processor manages the other SLOP components and is the main entrypoint for expression evaluation. It provides
 * several services including the ability to setup SLOP with custom config, lexer and parsers as well as serialization
 * of tokens and static processing.
 */
public class SLOPProcessor {

    private SLOPContext context;
    private SLOPConfig config;
    private Lexer<?> grammarLexer;
    private Lexer<?> languageLexer;
    private Parser languageParser;
    private ObjectMapper mapper;

    /**
     * Constructs a new processor instance and initializes the config, grammar lexer, language lexer and parser
     * with these instances. This allows a potential custom implementation for one or more of these.
     * @param config The configuration class containing references to token handlers and properties.
     * @param grammarLexer The grammar lexer which processes the grammar on the token to create the pattern tokens
     * @param languageLexer The language lexer which transforms the expression String into tokens
     * @param languageParser The language parser which evaluates the tokens into a result
     */
    public SLOPProcessor(SLOPConfig config, Lexer<?> grammarLexer, Lexer<?> languageLexer, Parser languageParser) {
        this.config = config;
        this.grammarLexer = grammarLexer;
        //Initializes each token with a Grammar String to pattern tokens
        config.mapGrammarTokens(grammarLexer);
        this.languageLexer = languageLexer;
        this.languageParser = languageParser;
        this.mapper = new ObjectMapper();
    }

    /**
     * Constructs a new processor instance but accept a custom config class. This would be the most common option to
     * extend SLOP with extra tokens, statements or token operations.
     * @param config The custom / extended configuration class
     */
    public SLOPProcessor(SLOPConfig config) {
        this(config, new SLOPGrammarLexer(config), new SLOPLexer(config), new SLOPParser(config));
    }

    public SLOPProcessor(SLOPConfig config, SLOPContext context) {
        this(config);
        this.languageParser.addTokenNotifier(context);
        this.context = context;
        context.setDebug(config.isDebugMode());
    }

    /**
     * Constructs a new processor instance with a context instance used for passing referenced objects in the
     * expression String. This is used where the context stays the same across multiple different expressions.
     * @param context The context is an object / value store used in conjunction with the expression String
     */
    public SLOPProcessor(SLOPContext context) {
        //Initialises the default config, lexer and parser
        this();
        this.languageParser.addTokenNotifier(context);
        this.context = context;
    }

    /**
     * Constructs a new processor instance without any custom parameters. This is used when there is no need for
     * context objects and any additional customization.
     */
    public SLOPProcessor() {
        this(new SLOPConfig());
    }

    /**
     * A static method allowing an expression to be evaluated without the need to construct the processor instance.
     * This is useful if no additional customization is needed. Note: This should only be used for single expression
     * evaluation since there is an additional overhead to initialize the lexer, grammar and parser that would
     * be mitigated by creating a separate instance and processing separately.
     * @param expression The expression for which to evaluate and return a result
     * @return Returns a result for the passed expression String
     */
    public static ExpressionResult<?> processStatic(String expression) {
        return processStatic(expression, null);
    }

    /**
     * A static method allowing an expression to be evaluated without the need to construct the processor instance.
     * This method also accepts a context for references to objects contained within. Note: This should only be used
     * for single expression evaluation since there is an additional overhead to initialize the lexer, grammar and
     * parser that would be mitigated by creating a separate instance and processing separately.
     * @param expression The expression for which to evaluate and return a result
     * @param context The context is an object / value store used in conjunction with the expression String
     * @return Returns a result for the passed expression String
     */
    public static ExpressionResult<?> processStatic(String expression, SLOPContext context) {
        SLOPConfig config = new SLOPConfig();
        SLOPGrammarLexer grammarLexer = new SLOPGrammarLexer(config);
        config.mapGrammarTokens(grammarLexer);
        SLOPLexer languageLexer = new SLOPLexer(config);
        SLOPParser languageParser = new SLOPParser(config);
        languageParser.addTokenNotifier(context);
        LexerResult<Token<?>> lexedResult = languageLexer.tokenize(expression);
        return languageParser.process(lexedResult, Objects.isNull(context) ? new SLOPContext() : context);
    }

    /**
     * Processes a String expression and returns an evaluated result
     * @param expression The expression for which to evaluate and return a result
     * @return Returns a result for the passed expression String
     */
    public ExpressionResult<?> process(String expression) {
        LexerResult<Token<?>> lexedResult = languageLexer.tokenize(expression);
        return languageParser.process(lexedResult, getContext());
    }

    /**
     * Processes a String expression, accepts an additional parameter for a context for referenced objects and
     * returns an evaluated result.
     * @param expression The expression for which to evaluate and return a result
     * @param context The context is an object / value store used in conjunction with the expression String
     * @return Returns a result for the passed expression String
     */
    public ExpressionResult<?> process(String expression, SLOPContext context) {
        context.setDebug(config.isDebugMode());
        languageParser.addTokenNotifier(context);
        LexerResult<Token<?>> lexedResult = languageLexer.tokenize(expression);
        return languageParser.process(lexedResult, context);
    }

    /**
     * Processes a String expression using one or more objects using index references. For example, the following would
     * be valid 'process("0.myIntField + 1.myOtherIntField", objectA, objectB)' if objectA had a field called myIntField
     * and objectB had a field called myOtherIntField. Indexes refer to the position in which they added (0 indexed)
     * @param expression The expression for which to evaluate and return a result
     * @param contextObjects One or more referenced objects within the expression String
     * @return Returns a result for the passed expression String
     */
    public ExpressionResult<?> process(String expression, Object... contextObjects) {
        SLOPContext context = new SLOPContext();
        languageParser.addTokenNotifier(context);
        context.addAll(Arrays.asList(contextObjects));
        LexerResult<Token<?>> lexedResult = languageLexer.tokenize(expression);
        return languageParser.process(lexedResult, context);
    }

    /**
     * Processes one or more expression Strings sequentially. This allows chained sequences to be supported where
     * the result of one expression can be passed / referenced in the next. You can do this using 'myVar = ...'
     * and then reference that using {?myVar} within another expression. Alternatively you can use {?} to reference
     * the result of the last evaluated Expression.
     * @param expressions A list of one or more expressions which may contain references to the others result
     * @return Returns a result for chain of expression Strings
     */
    public ExpressionResult<?> process(List<String> expressions) {
        SLOPContext localContext = Objects.isNull(context) ? new SLOPContext() : context;
        List<ExpressionResult<?>> results = expressions.stream()
                .map(languageLexer::tokenize)
                .map(r -> languageParser.process(r, localContext))
                .collect(Collectors.toList());
        return results.get(results.size() - 1);
    }

    /**
     * Converts an expression into a Token result. This can be useful if needing to evaluate an expression whilst
     * processing another expression. This can be used when evaluating dynamically loaded logic from file with
     * Functions.
     * @param expression The expression to evaluate a result and return in Token form
     * @return Returns the resulting Token from the evaluated expression
     */
    public Token<?> processToToken(String expression) {
        LexerResult<Token<?>> lexedResult = languageLexer.tokenize(expression);
        return languageParser.processExpression(lexedResult.getResult(), context);
    }

    /**
     * Lexes an expression into a LexerResult series of tokens. This can be persisted itself or saved against an
     * object for later resolution or re-use. This should improve performance as lexing is computationally expensive.
     * @param expression The expression to evaluate a result and return in LexerResult form
     * @return Returns a lexer result for the expression
     */
    public LexerResult<Token<?>> processToLexerResult(String expression) {
        return languageLexer.tokenize(expression);
    }

    /**
     * Processes a previously lexed result which is a LexerResult&lt;Token&lt;?&gt;&gt;. This saves a lot of time
     * since the Parser is relatively inexpensive compared to the tokenization phase. This method also supports a
     * context being passed so different objects can be run against the same expression.
     * @param lexedResult The previously tokenized result from the Lexer
     * @return Returns a result for the expression
     */
    public ExpressionResult<?> processFromLexerResult(LexerResult<Token<?>> lexedResult) {
        return languageParser.process(lexedResult, Objects.isNull(context) ? new SLOPContext() : context);
    }

    /**
     * Serializes the tokenized result from the Lexer so that it could either be stored in memory or saved to file.
     * Tokenizing is the most expensive part of processing an expression and so for repeated calls to the same expression
     * but with different context objects, this can be avoided by doing this once and reusing it using the
     * processFromString method.
     * @param expression The expression to tokenize and serialize into a String
     * @return Returns a serialized String of the expression
     */
    public String tokenizeToString(String expression) {
        try {
            LexerResult<Token<?>> lexedResult = languageLexer.tokenize(expression);
            mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE); //turn off everything
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY); // only use fields
            return mapper.writeValueAsString(lexedResult);
        } catch (JsonProcessingException ex) {
            throw new SLOPException("Unable to serialize tokens: " + ex.getMessage());
        }
    }

    /**
     * Serializes the tokenized result from the Lexer to a file for later use. Tokenizing is the most expensive part
     * of processing an expression and so for repeated calls to the same expression but with different context objects,
     * this can be avoided by doing this once and reusing it using the processFromString method.
     * @param expression The expression to tokenize and serialize to a file
     * @param filePath The full path of the file to save to
     */
    public void tokenizeToFile(String expression, String filePath) {
        try (FileOutputStream fileOut = new FileOutputStream(filePath);
             ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
            LexerResult<Token<?>> lexedResult = languageLexer.tokenize(expression);
            out.writeObject(lexedResult);
        } catch (IOException ex) {
            throw new SLOPException("Unable to tokens to file: " + ex.getMessage());
        }
    }

    /**
     * Processes a previously lexed result which was serialized to a String. This saves a lot of time since the
     * Parser is relatively inexpensive compared to the tokenization phase. This method also supports a context
     * being passed so different objects can be run against the same expression.
     * @param input The previously serialized token result from the Lexer
     * @param context The context is an object / value store used in conjunction with the expression String
     * @return Returns a result for the expression
     */
    public ExpressionResult<?> processFromString(String input, SLOPContext context) {
        try {
            //TODO - Replace with ObjectOutputStream / ObjectInputStream
            languageParser.addTokenNotifier(context);
            JavaType type = mapper.getTypeFactory().constructParametricType(LexerResult.class, Token.class);
            mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE); //turn off everything
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY); // only use fields
            LexerResult<Token<?>> lexedResult = mapper.readValue(input, type);
            long before = System.currentTimeMillis();
            ExpressionResult<?> result = languageParser.process(lexedResult, context);
            System.out.println("Execution time: " + (System.currentTimeMillis() - before) + "ms");
            return result;
        } catch (JsonProcessingException ex) {
            throw new SLOPException("Unable to serialize tokens: " + ex.getMessage());
        }
    }

    /**
     * Loads and processes a previously lexed result that was serialized and saved to file. This saves a lot of time
     * since the Parser is relatively inexpensive compared to the tokenization phase. This method also supports a
     * context being passed so different objects can be run against the same expression.
     * @param filePath The full path of the file to load
     * @param context The context is an object / value store used in conjunction with the expression String
     * @return Returns a result for the expression
     */
    @SuppressWarnings("unchecked")
    public ExpressionResult<?> processFromFile(String filePath, SLOPContext context) {
        languageParser.addTokenNotifier(context);
        try (FileInputStream fileIn = new FileInputStream(filePath);
             ObjectInputStream in = new ObjectInputStream(fileIn)) {
            Object lexedResult = in.readObject();
            if (Objects.nonNull(lexedResult)) {
                if (!(lexedResult instanceof LexerResult<?>)) {
                    throw new SLOPException(String.format("Unexpected object '%s' when reading from pre-compiled file",
                            in.getClass().getSimpleName()));
                }
                return languageParser.process((LexerResult<Token<?>>) lexedResult, context);
            }
            throw new SLOPException(String.format("File '%s' had no content", filePath));
        } catch (IOException | ClassNotFoundException ex) {
            throw new SLOPException("Unable to tokens to file: " + ex.getMessage());
        }
    }

    /**
     * Returns either the context set during initialisation and used in evaluated SLOP expressions, or if none has
     * been set then a new instance is returned.
     * @return Returns the SLOPContext object used in expressions
     */
    public SLOPContext getContext() {
        if (Objects.isNull(context)) {
            SLOPContext newContext = new SLOPContext();
            languageParser.addTokenNotifier(newContext);
            return newContext;
        }
        return context;
    }

    /**
     * Returns the Apache Jackson TypeFactory class to construct custom types to case the result returned
     * by the SLOPProcessor instance. The resulting constructed JavaType object can then be used in conjunction
     * with the ExpressionResult.getValue(JavaType).
     * @return Returns the TypeFactory instance
     */
    public TypeFactory getTypeFactory() {
        return mapper.getTypeFactory();
    }
}