package dev.slop.parser.formatter;

import dev.slop.tokens.Token;

import java.util.Optional;

/**
 * Formatters operate when a result has been returned from the Parser but still needs to be converted to it's Java
 * native form. This is because during the Lexing / Parsing process, the internal structure of these collections
 * and values are stored in Token&lt;?&gt; form.
 * @param <T> The type for which the implemented formatter class represents
 */
public abstract class Formatter<T> {
    public abstract boolean isMatch(Object object);
    public abstract Optional<T> format(Object object);

    /**
     * Recursively unwraps any token until it is back to the primitive underlying type(s)
     * @param token The token to be unwrapped
     * @return Returns the raw contained object
     */
    protected Object recursiveUnwrap(Token<?> token) {
        while (token.getValue() instanceof Token)
            return recursiveUnwrap((Token<?>)token.getValue());
        return token.getValue();
    }
}
