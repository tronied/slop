package dev.slop.parser.formatter;

import dev.slop.tokens.Token;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Recursively scans all elements in the map and converts both the Key / Value's from their Token types into
 * Java native types.
 */
public class MapFormatter extends Formatter<Map<?,?>> {
    @Override
    public boolean isMatch(Object object) {
        return object instanceof Map;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Optional<Map<?, ?>> format(Object object) {
        Map<?,?> collection = ((Map<?,?>)object);
        if (collection.entrySet().stream()
                .allMatch(e -> e.getKey() instanceof Token<?> && e.getValue() instanceof Token<?>)) {
            //Suppress warnings as it doesn't recognise the check we've made
            Map<Token<?>, Token<?>> tokenMap = (Map<Token<?>, Token<?>>)collection;
            return Optional.of(tokenMap.entrySet().stream()
                    .collect(Collectors.toMap(e -> e.getKey().getValue(), e -> e.getValue().getValue())));
        }
        return Optional.empty();
    }
}
