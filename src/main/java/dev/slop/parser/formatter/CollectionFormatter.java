package dev.slop.parser.formatter;

import dev.slop.tokens.Token;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Recursively scans all elements in the collection and converts the Token types into Java native types
 */
public class CollectionFormatter extends Formatter<Collection<?>> {

    @Override
    public boolean isMatch(Object object) {
        return object instanceof Collection;
    }

    @Override
    public Optional<Collection<?>> format(Object object) {
        Collection<?> collection = ((Collection<?>)object);
        if (collection.stream().allMatch(v -> v instanceof Token<?>)) {
            return Optional.of(collection.stream()
                    .map(v -> ((Token<?>)v))
                    .map(this::recursiveUnwrap)
                    .collect(Collectors.toList()));
        }
        return Optional.empty();
    }
}
