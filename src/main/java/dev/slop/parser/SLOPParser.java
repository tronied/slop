package dev.slop.parser;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.OperatorType;
import dev.slop.enums.PatternType;
import dev.slop.event.NotificationType;
import dev.slop.event.TokenEventSubscriber;
import dev.slop.exception.ParserException;
import dev.slop.model.ExpressionResult;
import dev.slop.model.LexerResult;
import dev.slop.operations.TypeOperation;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenInstance;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.literals.BooleanToken;
import dev.slop.tokens.operators.LogicOperator;
import dev.slop.tokens.literals.NullToken;
import dev.slop.tokens.operators.OperatorHandler;
import dev.slop.tokens.operators.OperatorToken;
import dev.slop.tokens.specials.ReturnToken;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * The default Parser implementation for SLOP. See Parser class for more details.
 */
public class SLOPParser extends Parser {

    public static final String LAST_EXPRESSION_RESULT = "last";

    private Stack<String> activeTokens = new Stack<>();

    public SLOPParser(SLOPConfig config) {
        super(config);
    }

    /**
     * See {@link Parser#process(LexerResult, SLOPContext) process}
     */
    @Override
    public ExpressionResult<?> process(LexerResult<Token<?>> expression, SLOPContext context) {
        Optional<Token<?>> result = Optional.ofNullable(processExpression(expression.getResult(), context));
        Object normalised = formatResult(result.isPresent() ? result.get().getValue() : "", context);
        //Attempt to resolve the value to value in the context (bypasses the single field value resolution issue
        return ExpressionResult.builder().result(Objects.isNull(normalised) ? normalised :
                Optional.ofNullable(context.getContextObject(normalised.toString()))
                .orElse(normalised)).build();
    }

    /**
     * See {@link Parser#processExpression(List, SLOPContext) processExpression}
     */
    public Token<?> processExpression(List<Token<?>> expression, SLOPContext context) {
        //Resolve all values in the operation without not including type operations
        final List<Token<?>> values = preProcess(expression, context);
        if (stripExpressionsFromValues(expression, values).size() > 3) {
            if (Objects.isNull(config.getOperatorHandler())) {
                throw new ParserException("No handler has been defined to determine operator order. Please ensure at " +
                        "least one Operator SLOPTokenHandler implements OperationOrder");
            }
            //Resolve operations according to PEMDAS (brackets are resolved first prior to this)
            Arrays.stream(config.getOperatorHandler().getOperatorOrder())
                    .forEach(ops -> replaceCalcs(context, expression.get(0), Arrays.asList(ops), values,
                            values.stream().map(Object::toString).collect(Collectors.joining(" "))));
            //Determine if the resolved calculations parts equate to a Boolean condition, if so handle Logic operations
            if (isCondition(values)) {
                meetsCondition(values);
            }
            //The result from the Calculation should be a single value. Throw an error is this is not the case
            if (values.size() != 1) {
                throw new ParserException("Error calculating function with unknown members: " +
                        values.stream().map(Object::toString).collect(Collectors.joining(" ")));
            }
            return values.get(0);
        } else {
            Token<?> result;
            if (values.isEmpty()) return new NullToken();
            if (values.size() == 1 && !values.get(0).getParserFlags().isEmpty()) return values.get(0);
            /* Catch case where 'true and true' wouldn't be evaluated correctly. Catch here and resolve before
             * returning the result in the switch statement. */
            if (isCondition(values)) {
                meetsCondition(values);
            }
            if (areExpressions(expression, values)) {
                if (values.size() == 1) return values.get(0);
                //If one expression has populated the result then return that, otherwise null
                Object resultVal = context.getContextObject("result");
                if (Objects.nonNull(resultVal)) return new TokenValue(resultVal);
                return new NullToken();
            }
            List<Token<?>> calcValues = stripExpressionsFromValues(expression, values);
            switch (calcValues.size()) {
                case 0: result = NullToken.builder().build(); break;
                case 1: result = resolveTokenValue(context, calcValues.get(0)); break;
                //Assume that the 3 values are an operation
                default: result = processOp(calcValues, context, expression.get(0));
            }
            if (config.isDebugMode()) System.out.println(" -> ".concat(result.toString()));
            return result;
        }
    }

    /**
     * Each token that is processed by the parser has a unique ID. These IDs are added / removed from a stack to
     * keep track of which tokens are being run and in which order. This also helps with ensuring variable scope is
     * maintained and that they are cleaned up once complete. The depth here determines how many layers deep the
     * ID occurs within the stack. This is useful to know for recursion so that the context can assign a unique ID
     * for each iteration.
     * @param id The ID for which to get the recursion depth
     * @return Returns the depth of the current token (will be 0 if the first occurrence).
     */
    private int countDepth(String id) {
        int result = 0;
        Stack<String> stackCopy = new Stack<>();
        stackCopy.addAll(activeTokens);
        while (!stackCopy.isEmpty()) {
            String next = stackCopy.pop();
            if (next.equalsIgnoreCase(id)) {
                result++;
            }
        }
        return result;
    }

    /**
     * Triggered when each token begins it's processing by the parser. This is useful to ensure the correct scope
     * for any declared variables / resources. Also notifies other classes in case extra actions are required
     * @param token The token that has started being processed
     */
    public void tokenStart(Token<?> token) {
        if (!token.isScoped() || token.isExpression() || token.getTokenGroups() != null &&
                token.getTokenGroups().isEmpty()) return;
        activeTokens.push(token.getUID());
        int depth = countDepth(activeTokens.peek());
        String tokenId = activeTokens.isEmpty() ? "null" : activeTokens.peek();
        String activeToken = String.format("%s:%d", tokenId, depth);
        notifyAll(tokenId, token, depth, NotificationType.ADDITION);
        if (config.isDebugMode())
            System.out.println(String.format("Started: %s (%s)", token, activeToken));
    }

    /**
     * Triggered when each token has finished processing by the parser. This is useful for associated variable cleanup
     * @param token The token that has ended it's processing
     */
    public void tokenEnd(Token<?> token) {
        if (!token.isScoped() || token.isExpression() || token.getTokenGroups() != null &&
                token.getTokenGroups().isEmpty()) return;
        int depth = countDepth(activeTokens.peek());
        String toRemoveId = activeTokens.isEmpty() ? "null" : activeTokens.peek();
        String tokenToRemove = String.format("%s:%d", toRemoveId, depth);
        activeTokens.pop();
        int fallbackDepth = activeTokens.isEmpty() ? 0 : countDepth(activeTokens.peek());
        String fallbackId = activeTokens.isEmpty() ? "null" : activeTokens.peek();
        String fallbackToken = String.format("%s:%d", fallbackId, depth);
        notifyAll(toRemoveId, token, depth, NotificationType.REMOVAL, fallbackId, fallbackDepth);
        if (config.isDebugMode())
            System.out.println(String.format("Finished: %s (%s), reverted to %s", token, tokenToRemove,
                    fallbackToken));
    }

    /**
     * Determines if the list of values contains a genuine result. This ignores all expressions and null tokens.
     * @param expression The list of tokens which were processed to check whether they are an expression
     * @param values The resulting values from each being processed
     * @return Returns true if a genuine result exists in the list of values
     */
    private boolean areExpressions(List<Token<?>> expression, List<Token<?>> values) {
        for (int i = 0;i < expression.size();i++) {
            if (expression.get(i).isExpression()) continue;
            if (!(values.get(i) instanceof NullToken)) return false;
        }
        return true;
    }

    /**
     * Either finds the first result token value or strips all expression token results from the list of results.
     * @param expression The list of input tokens that were processed
     * @param values The evaluated result values from those tokens
     * @return Returns the list of filtered result tokens
     */
    private List<Token<?>> stripExpressionsFromValues(List<Token<?>> expression, List<Token<?>> values) {
        List<Token<?>> resultValues = new ArrayList<>();
        if (expression.size() != values.size()) {
            List<Token<?>> result = values.stream().filter(t -> !t.getParserFlags().isEmpty()).collect(Collectors.toList());
            if (result.size() == 1) {
                return result;
            } else {
                return values;
            }
        }
        for (int i = 0;i < values.size();i++) {
            if (!expression.get(i).isExpression()) resultValues.add(values.get(i));
        }
        return resultValues;
    }

    /**
     * Loops through the set of tokens in the expression and call the process method on each to resolve their values
     * @param tokens The set of tokens currently being processed
     * @param context The context containing potentially referenced objects in the Token
     * @return Returns a list of resolved token values
     */
    private List<Token<?>> preProcess(List<Token<?>> tokens, SLOPContext context) {
        List<Token<?>> result = new ArrayList<>();
        //Pre-process any tokens which have priority
        List<Token<?>> priorityResults = new ArrayList<>();
        List<String> processed = new ArrayList<>();
        //Loop through parser priority tokens, scan for dependencies and process in that order
        for (int i = 0;i < tokens.size();i++) {
            if (!tokens.get(i).hasParserPriority()) continue;
            Token<?> current = tokens.get(i);
            if (current instanceof TokenInstance) {
                TokenInstance instance = (TokenInstance) current;
                processInstance(instance, i + 1 >= tokens.size() ? new ArrayList<>() : tokens.subList(i + 1, tokens.size() - 1),
                        processed, priorityResults, context);
            } else {
                priorityResults.addAll(current.processNoParams(this, context, config));
            }
        }
        int priorityCounter = 0;
        for (Token<?> current : tokens) {
            if (current.hasParserPriority()) {
                //Add their result to the list if they've been previously evaluated
                result.add(priorityResults.get(priorityCounter++));
            } else {
                List<Token<?>> execResult = current.processNoParams(this, context, config);
                Optional<Token<?>> returnToken = execResult.stream().filter(t -> !t.getParserFlags().isEmpty()).findFirst();
                if (returnToken.isPresent()) {
                    return Collections.singletonList(returnToken.get());
                }
                result.addAll(execResult);
            }
        }
        return result;
    }

    /**
     * Scans through an instances list of dependencies (if any exist) and loads each into memory in the order
     * they are required by each class.
     * @param current The instance with zero or more dependencies to load
     * @param unprocessed The current unprocessed list of tokens
     * @param processed The list of processed tokens so as to avoid loading them again
     * @param priorityResults Each result from when each token was loaded (if any)
     * @param context The current context holding all instances and data
     */
    private void processInstance(TokenInstance current, List<Token<?>> unprocessed, List<String> processed,
                                 List<Token<?>> priorityResults, SLOPContext context) {
        current.getDependencies().stream()
                .filter(d -> !processed.contains(d))
                .forEach(d -> {
                    unprocessed.stream()
                            .filter(TokenInstance.class::isInstance)
                            .map(TokenInstance.class::cast)
                            .filter(t -> Objects.equals(t.getTypeName(), d))
                            .findFirst()
                            .ifPresent(dependency -> processInstance(dependency, unprocessed, processed,
                                    priorityResults, context));
                });
        priorityResults.addAll(((Token<?>)current).processNoParams(this, context, config));
        processed.add(current.getTypeName());
    }

    /**
     * Processes a token operation supposedly containing three values. Checks are made to ensure that the two token
     * values are separated by an OperatorToken and attempts to find a match with one of the defined TypeOperation classes
     * @param values The tokens in the operation to process
     * @param context The context containing potentially referenced objects in the Token
     * @param leftSide The original token in case it was subsequently resolved from context
     * @return Returns a single token result for the current operation
     */
    private Token<?> processOp(List<Token<?>> values, SLOPContext context, Token<?> leftSide) {
        Token<?> first = values.get(0);
        OperatorToken operator;
        //Throw an error if no Operator found between the first and third values
        if (!(values.get(1) instanceof OperatorToken)) {
            throw new ParserException(String.format("Expected operand between two calculation values. Instead found '%s'",
                    values.get(1).getClass().getSimpleName()));
        }
        operator = (OperatorToken)values.get(1);
        OperatorHandler handler = config.getOperatorHandler();
        Token<?> second = values.size() > 2 ? values.get(2) : new NullToken();
        //If either are found to be TokenValue's, see if a value can be resolved from context
        Token<?> finalFirst = resolveTokenValue(context, first);
        Token<?> finalSecond = resolveTokenValue(context, second);
        //Special case for assignment operator
        if (handler.getOpType(operator) == OperatorType.ASSIGN) {
            return leftSide.set(this, context, finalSecond);
        }
        if (config.isDebugMode()) System.out.print(String.format("[%s %s %s]", first, operator, second));
        //Loop through for a TypeOperation handler to handle the first and third types for evaluation
        TypeOperation found = config.getTypeOperations().stream()
                .filter(o -> o.canHandle(finalFirst, operator, finalSecond)).findFirst()
                .orElse(config.getDefaultTypeOperation());
        //Call the process method on the found TypeOperation class
        return found.process(config, context, finalFirst, operator, finalSecond);
    }

    /**
     * Attempts to resolve a TokenValue which is now used to represent objects / values in the context. If the tokens
     * value is not found within the context then the token is returned as normal as it might be used for other purposes.
     * @param context The context containing potentially referenced objects in the Token
     * @param token The token which may exist as a reference to an item in the context
     * @return Returns the resolved value.
     */
    private Token<?> resolveTokenValue(SLOPContext context, Token<?> token) {
        if (Objects.isNull(token.getValue()) && !token.getTokenGroups().isEmpty()) {
            //Token might not have been evaluated, so perform that now
            Token<?> result = processExpression(Collections.singletonList(token), context);
            if (Objects.isNull(result.getValue())) return token;
            token = result;
        }
        //If still no value then return as-is
        if (Objects.isNull(token.getValue())) return token;
        Object varValue = context.getContextObject(token.getValue().toString());
        if (varValue instanceof Stack) varValue = ((Stack<?>) varValue).peek();
        if (token instanceof TokenValue && Objects.nonNull(varValue)) {
            Token<?> result = varValue instanceof Token<?> ?
                    (Token<?>) varValue : new TokenValue(varValue);
            result.setName(token.getValue().toString());
            return result;
        }
        return token;
    }

    /**
     * Resolves operations of the type determined by the ops parameter. This is so that different operations can be
     * resolved before others to ensure that the order of operations is correct.
     * @param context The context containing potentially referenced objects in the Token
     * @param leftSide The original token in case it was subsequently resolved from context
     * @param ops The type of operations to resolve. This order in which this method is called and for what is
     *            determined by the order of operations in the config class.
     * @param values The list of tokens to resolve the operations within
     * @param op Purely for logging purposes, the current name of the operation being executed
     */
    private void replaceCalcs(SLOPContext context, Token<?> leftSide, List<String> ops, List<Token<?>> values, String op) {
        //Order of execution is left-to-right (Same as C / Java)
        for (int i = 0;i < values.size();i++) {
            //Find the operator
            if (!Objects.isNull(values.get(i).getValue()) && ops.contains(values.get(i).getValue().toString())) {
                printValues(values);
                //Verify the position is between two other tokens
                if (i == values.size() - 1 || i == 0) {
                    throw new ParserException("Error calculating op with hanging operator: " +
                            values.stream().map(Object::toString).collect(Collectors.joining(" ")));
                }
                Token<?> first = values.get(i - 1);
                OperatorToken operator;
                if (values.get(i) instanceof OperatorToken) {
                    operator = (OperatorToken)values.get(i);
                } else {
                    throw new ParserException(String.format("Expected operand between two calculation values in '%s'. " +
                            "Instead found '%s'", op, values.get(1).getClass().getSimpleName()));
                }
                Token<?> second = values.get(i + 1);
                if (config.isDebugMode()) System.out.print(" >> ");
                Token<?> result = processOp(Arrays.asList(first, operator, second), context, leftSide);
                //Once the triplet has been resolved, remove the three component parts from the expression array
                for (int j = 0;j < 3;j++) {
                    values.remove(i - 1);
                }
                //Insert the result back into the List
                values.add(--i, result);
                if (config.isDebugMode()) System.out.println(" -> " + result.toString());
                if (values.size() == 1) break;
            }
        }
    }

    /**
     * If debugging is enabled, this method outputs the String values of all passed tokens
     * @param values The list of tokens to print
     */
    private void printValues(List<Token<?>> values) {
        if (config.isDebugMode()) System.out.print(String.format("(%s)", values.stream()
                .map(Token::getValue)
                .map(Object::toString)
                .collect(Collectors.joining(" "))));
    }

    /**
     * Determines whether the current list of resolved tokens equates to condition. For example, an example where
     * this would be true would be 'true and false or true and true'.
     * @param conditionResults The resolved token results from previous operations
     * @return Returns true if the results being evaluated equate to a condition
     */
    private boolean isCondition(List<Token<?>> conditionResults) {
        //Determine if all component parts of the expression make up a condition (Boolean / Logical Operators)
        return conditionResults.stream().allMatch(r -> r.is(Boolean.class) || r instanceof LogicOperator);
    }

    /**
     * Accepts a list of Boolean results and logical operators and resolves a single result. It does this by splitting
     * the conditionResults list at the point where an OR operator is used. It then determines if any of the separated
     * lists evaluate to true and returns that, otherwise it resolves to false.
     * @param conditionResults A list of Boolean results and logic operators
     */
    private void meetsCondition(List<Token<?>> conditionResults) {
        boolean result;
        if (conditionResults.stream().anyMatch(t -> t.getValue().toString().equalsIgnoreCase(
                config.getProperty(DefaultProperty.OR_OPERATOR)))) {
            /* If an OR operator is present then splitOnOr splits into separate lists and determine if any
             * one of those evaluates to true */
            result = splitOnOr(conditionResults).stream()
                    .anyMatch(l -> l.stream()
                            .filter(t -> t.is(Boolean.class))
                            .allMatch(t -> t.getValue(Boolean.class)));
        } else {
            //Alternatively just verify that all Boolean values evaluate to true
            result = conditionResults.stream()
                    .filter(t -> t.is(Boolean.class))
                    .allMatch(t -> t.getValue(Boolean.class));
        }
        //Clear and insert overall result back into the list
        conditionResults.clear();
        conditionResults.add(new BooleanToken(result));
    }

    /**
     * Splits the list of results on the OR operator.
     * @param conditionResults A list of Boolean results and logic operators
     * @return Returns a list of lists containing Boolean results and logic operators (excluding OR)
     */
    private List<List<Token<?>>> splitOnOr(List<Token<?>> conditionResults) {
        //Split the single List into multiple using the OR operator as a split point
        int[] indexes = Stream.of(IntStream.of(-1), IntStream.range(0, conditionResults.size())
                .filter(i -> conditionResults.get(i).getValue().toString().equalsIgnoreCase(
                        config.getProperty(DefaultProperty.OR_OPERATOR))), IntStream.of(conditionResults.size()))
                .flatMapToInt(s -> s).toArray();
        return IntStream.range(0, indexes.length - 1)
                .mapToObj(i -> conditionResults.subList(indexes[i] + 1, indexes[i + 1]))
                .collect(Collectors.toList());
    }

    /**
     * If the result is a collection, this method searches for a formatter class (declared in the config) to unwrap
     * all the contained tokens back to their raw types. This is so that when a result is returned it can be handled
     * as a standard Java collection / Map using primitive types or their wrappers and not Token classes. The result
     * is also added to the context for use in chained operations so that it can be referenced.
     * @param suspect The result from the Parser calculations
     * @param context The context object which is used to store the result for chained operations
     * @return Returns the unwrapped result (if a collection)
     */
    private Object formatResult(Object suspect, SLOPContext context) {
        //Unwrap TokenValue back to their raw types
        final Object object = rootUnwrap(suspect);
        Optional<?> result = config.getParserFormatters().stream()
                .filter(f -> f.isMatch(object))
                .map(f -> f.format(object))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst();
        if (result.isPresent()) return result.get();
        //Store result of last expression to context in case it's referred to in chained result
        if (object instanceof Token<?>) {
            Object unwrapped = ((Token<?>) object).getValue();
            context.set(LAST_EXPRESSION_RESULT, new TokenValue(unwrapped));
            return unwrapped;
        }
        context.set(LAST_EXPRESSION_RESULT, new TokenValue(object));
        return object;
    }

    /**
     * Unwraps a token back to its raw object
     * @param object The token being unwrapped
     * @return The resulting non-token object
     */
    private Object rootUnwrap(Object object) {
        while (object instanceof Token<?>) {
            object = ((Token<?>)object).getValue();
        }
        return object;
    }
}
