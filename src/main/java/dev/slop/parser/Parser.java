package dev.slop.parser;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.event.NotificationType;
import dev.slop.event.TokenEventSubscriber;
import dev.slop.model.ExpressionResult;
import dev.slop.model.LexerResult;
import dev.slop.tokens.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * The Parser class uses the Lexer Result and resolves it to a single result. It does this by recursing through the
 * structure and resolving values using either type operations (@see com.experimental.operations.TypeOperation#process()),
 * deferring to the tokens themselves or to referenced functions.
 */
public abstract class Parser {

    protected SLOPConfig config;
    private List<TokenEventSubscriber> subscribers;

    Parser(SLOPConfig config) {
        this.config = config;
        this.subscribers = new ArrayList<>();
    }

    /**
     * Processes the lexed tokens into a single result. The context contains objects so that references contained in the
     * original expression String can be resolved.
     * @param lexedResult The lexed result containing one or more tokens to resolve
     * @param context The context containing referenced objects for value resolution
     * @return Returns a single result in an ExpressionResult wrapper class
     */
    public abstract ExpressionResult<?> process(LexerResult<Token<?>> lexedResult, SLOPContext context);

    /**
     * Processes the expression to resolve and return a token result.
     * @param expression The expression in the form of an embedded set of tokens
     * @param context The context containing zero or more objects from which to resolve referenced values
     * @return Returns the result in Token form
     */
    public abstract Token<?> processExpression(List<Token<?>> expression, SLOPContext context);

    /**
     * Adds a subscriber to active token change events
     * @param subscriber The subscriber class to be added
     */
    public void addTokenNotifier(TokenEventSubscriber subscriber) {
        subscribers.add(subscriber);
    }

    /**
     * Notifies all subscribers of a change in the active token
     * @param uid The UID of the new active token
     * @param token The token for the event taking place
     * @param depth The depth of the token being processed (&gt; 1 is recursive)
     * @param type The type of event i.e. started or finished processing
     */
    public void notifyAll(String uid, Token<?> token, int depth, NotificationType type) {
        subscribers.forEach(s -> s.notify(uid, token, depth, type));
    }

    /**
     * Notifies all subscribers of a change in the active token. This method provides additional
     * parameters for the token that will replace the current if processing has finished.
     * @param uid The UID of the new active token
     * @param token The token for the event taking place
     * @param depth The depth of the token being processed (&gt; 1 is recursive)
     * @param type The type of event i.e. started or finished processing
     * @param fallback The UID of the token to fallback to
     * @param fallbackDepth The depth (recursive) of the fallback token to fallback to
     */
    public void notifyAll(String uid, Token<?> token, int depth, NotificationType type, String fallback, int fallbackDepth) {
        subscribers.forEach(s -> s.notify(uid, token, depth, type, fallback, fallbackDepth));
    }
}
