package dev.slop.operations;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.exception.ParserException;
import dev.slop.tokens.Token;
import dev.slop.tokens.literals.BooleanToken;
import dev.slop.tokens.literals.NullToken;
import dev.slop.tokens.operators.OperatorHandler;
import dev.slop.tokens.operators.OperatorToken;

import java.util.Objects;

/**
 * Handles Enum operations including equals and not equals.
 */
public class EnumOperation implements TypeOperation {

    /**
     * See {@link TypeOperation#canHandle(Token, OperatorToken, Token) canHandle}
     */
    @Override
    public boolean canHandle(Token<?> first, OperatorToken operator, Token<?> second) {
        if (second instanceof NullToken) return false;
        if (Objects.isNull(first.getValue()) || Objects.isNull(second.getValue())) return false;
        return first.getValue().getClass().isEnum() || second.getValue().getClass().isEnum();
    }

    /**
     * See {@link TypeOperation#handleCustomOperator(Token, OperatorToken, Token) handleCustomOperator}
     */
    @Override
    public <T> T handleCustomOperator(Token<?> first, OperatorToken operator, Token<?> second) {
        throw new ParserException(String.format("Unable to handle Enum operation with operator '%s'",
                operator.getValue()));
    }

    /**
     * See {@link TypeOperation#process(SLOPConfig, SLOPContext, Token, OperatorToken, Token) process}
     */
    @Override
    public Token<?> process(SLOPConfig config, SLOPContext context, Token<?> first, OperatorToken operator,
                            Token<?> second) {
        boolean result;
        String firstValue = first.getValue().toString();
        String secondValue = second.getValue().toString();
        OperatorHandler handler = config.getOperatorHandler();
        switch (handler.getOpType(operator)) {
            case EQUALS: result = firstValue.equalsIgnoreCase(secondValue); break;
            case NOT_EQUALS: result = !firstValue.equalsIgnoreCase(secondValue); break;
            default: result = handleCustomOperator(first, operator, second);
        }
        return new BooleanToken(result);
    }
}
