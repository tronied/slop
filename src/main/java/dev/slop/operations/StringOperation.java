package dev.slop.operations;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.exception.ParserException;
import dev.slop.tokens.Token;
import dev.slop.tokens.literals.BooleanToken;
import dev.slop.tokens.literals.StringToken;
import dev.slop.tokens.operators.OperatorHandler;
import dev.slop.tokens.operators.OperatorToken;

/**
 * Handles String operations including add, equals and not equals.
 */
public class StringOperation implements TypeOperation {

    /**
     * See {@link TypeOperation#canHandle(Token, OperatorToken, Token) canHandle}
     */
    @Override
    public boolean canHandle(Token<?> first, OperatorToken operator, Token<?> second) {
        //Anything else is supported as it's simply a String append
        return first.is(String.class) || second.is(String.class);
    }

    /**
     * See {@link TypeOperation#handleCustomOperator(Token, OperatorToken, Token) handleCustomOperator}
     */
    @Override
    public <T> T handleCustomOperator(Token<?> first, OperatorToken operator, Token<?> second) {
        throw new ParserException(String.format("Unable to handle String operation with operator '%s'",
                operator.getValue()));
    }

    /**
     * See {@link TypeOperation#process(SLOPConfig, SLOPContext, Token, OperatorToken, Token) process}
     */
    @Override
    public Token<?> process(SLOPConfig config, SLOPContext context, Token<?> first, OperatorToken operator,
                            Token<?> second) {
        OperatorHandler handler = config.getOperatorHandler();
        switch (handler.getOpType(operator)) {
            //Has the same result as performing the unsafe operation concat(<string>)
            case ADD: return new StringToken(first.getValue().toString()
                    .concat(second.getValue().toString()));
            case EQUALS: return new BooleanToken(first.getValue(String.class)
                    .equalsIgnoreCase(second.getValue(String.class)));
            case NOT_EQUALS: return new BooleanToken(!first.getValue(String.class)
                    .equalsIgnoreCase(second.getValue(String.class)));
            default: return handleCustomOperator(first, operator, second);
        }
    }
}
