package dev.slop.operations;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.exception.ParserException;
import dev.slop.tokens.Token;
import dev.slop.tokens.literals.BooleanToken;
import dev.slop.tokens.operators.OperatorHandler;
import dev.slop.tokens.operators.OperatorToken;

/**
 * Handles Object operations including equals and not equals. This is used as a fallback
 * if no other type operations classes match first.
 */
public class ObjectOperation implements TypeOperation {

    /**
     * See {@link TypeOperation#canHandle(Token, OperatorToken, Token) canHandle}
     */
    @Override
    public boolean canHandle(Token<?> first, OperatorToken operator, Token<?> second) {
        return true;
    }

    /**
     * See {@link TypeOperation#process(SLOPConfig, SLOPContext, Token, OperatorToken, Token) process}
     */
    @Override
    public Token<?> process(SLOPConfig config, SLOPContext context, Token<?> first, OperatorToken operator,
                            Token<?> second) {
        OperatorHandler handler = config.getOperatorHandler();
        switch (handler.getOpType(operator)) {
            case EQUALS: return new BooleanToken(first.getValue().equals(second.getValue()));
            case NOT_EQUALS: return new BooleanToken(!first.getValue().equals(second.getValue()));
            default: return handleCustomOperator(first, operator, second);
        }
    }

    /**
     * See {@link TypeOperation#handleCustomOperator(Token, OperatorToken, Token) handleCustomOperator}
     */
    @Override
    public <T> T handleCustomOperator(Token<?> first, OperatorToken operator, Token<?> second) {
        throw new ParserException(String.format("Unable to handle Object operation with operator '%s'",
                operator.getValue()));
    }
}
