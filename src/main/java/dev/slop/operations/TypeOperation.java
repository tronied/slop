package dev.slop.operations;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.tokens.Token;
import dev.slop.tokens.operators.OperatorToken;

/**
 * When the Parser comes across two tokens separated by an OperatorToken, it loops through all TypeOperation classes
 * trying to find a match using the canHandle method. If no match is found then a ParserException is thrown. The
 * process method is then called to undertake the operation and the result is returned.
 */
public interface TypeOperation {

    /**
     * Determines whether the current TypeOperation class can handle the current operation.
     * @param first The first token found in the operation
     * @param operator The operator in the operation
     * @param second The second token found in the operation
     * @return Returns true if the class can handle the current operation
     */
    boolean canHandle(Token<?> first, OperatorToken operator, Token<?> second);

    /**
     * If the canHandle method was found to return true for the two tokens, the process method determines which
     * operation needs to be undertaken, calculates the result and returns it.
     * @param config The config class in case type operations require any of the contained resources
     * @param context Provides support for reading and updating the value in context
     * @param first The first token in the operation
     * @param operator The operator in the operation
     * @param second The second token in the operation
     * @return Returns the result in a format determined by the concrete TypeOperation class
     */
    Token<?> process(SLOPConfig config, SLOPContext context, Token<?> first, OperatorToken operator, Token<?> second);

    /**
     * If the process method failed to identify the type of operation with the OperatorToken, this method is called
     * to handle this result. Typically this will be where an exception is thrown stating that the operation cannot
     * be handled but it is left open and left to the developer of the TypeOperation class.
     * @param first The first token in the operation
     * @param operator The operator in the operation
     * @param second The second token in the operation
     * @param <T> The generic type used for the result of the handleCustomOperator method
     * @return Is open to returning an object but typically this method throws an exception
     */
    <T> T handleCustomOperator(Token<?> first, OperatorToken operator, Token<?> second);
}
