package dev.slop.operations;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.OperatorType;
import dev.slop.exception.ParserException;
import dev.slop.tokens.Token;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.literals.BooleanToken;
import dev.slop.tokens.literals.DecimalToken;
import dev.slop.tokens.literals.IntegerToken;
import dev.slop.tokens.literals.NullToken;
import dev.slop.tokens.operators.OperatorHandler;
import dev.slop.tokens.operators.OperatorToken;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Objects;

/**
 * Handles numeric operations of numeric (decimal / non-decimal) values. These include dividing, add, subtract,
 * multiply, mod, greater than (and equal), less than (and equal), equals and not equals. The result is returned
 * in the most suitable format for the resulting value.
 */
public class NumericOperation implements TypeOperation {

    /**
     * See {@link TypeOperation#canHandle(Token, OperatorToken, Token) canHandle}
     */
    @Override
    public boolean canHandle(Token<?> first, OperatorToken operator, Token<?> second) {
        try {
            if (Objects.isNull(first.getValue())) return false;
            new BigDecimal(first.getValue().toString());
            if (second instanceof NullToken && (operator.getOpType(operator) == OperatorType.DECREMENT ||
                    operator.getOpType(operator) == OperatorType.INCREMENT)) {
                return true;
            }
            if (Objects.isNull(second.getValue())) return false;
            new BigDecimal(second.getValue().toString());
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    /**
     * See {@link TypeOperation#handleCustomOperator(Token, OperatorToken, Token) handleCustomOperator}
     */
    @Override
    public <T> T handleCustomOperator(Token<?> first, OperatorToken operator, Token<?> second) {
        throw new ParserException(String.format("Unable to handle numeric operation with operator '%s'",
                operator.getValue()));
    }

    /**
     * See {@link TypeOperation#process(SLOPConfig, SLOPContext, Token, OperatorToken, Token) process}
     */
    @Override
    public Token<?> process(SLOPConfig config, SLOPContext context, Token<?> firstToken, OperatorToken operator,
                            Token<?> secondToken) {
        BigDecimal first = new BigDecimal(firstToken.getValue().toString());
        BigDecimal second = secondToken instanceof NullToken ? null : new BigDecimal(secondToken.getValue().toString());
        OperatorHandler handler = config.getOperatorHandler();
        switch (handler.getOpType(operator)) {
            case DIVIDE: return convert(first.divide(second, 6, RoundingMode.DOWN)
                    .stripTrailingZeros());
            case ADD: return convert(first.add(second));
            case SUBTRACT: return convert(first.subtract(second));
            case MULTIPLY: return convert(first.multiply(second));
            case MOD: return convert(first.remainder(second));
            case GTE: return new BooleanToken(first.compareTo(second) >= 0);
            case LTE: return new BooleanToken(first.compareTo(second) <= 0);
            case NOT_EQUALS: return new BooleanToken(first.compareTo(second) != 0);
            case EQUALS: return new BooleanToken(first.compareTo(second) == 0);
            case GT: return new BooleanToken(first.compareTo(second) > 0);
            case LT: return new BooleanToken(first.compareTo(second) < 0);
            case INCREMENT: return convert(first.add(BigDecimal.ONE));
            case DECREMENT: return convert(first.subtract(BigDecimal.ONE));
            default: return handleCustomOperator(firstToken, operator, secondToken);
        }
    }

    /**
     * Takes a BigDecimal value and attempts to convert it to either a Decimal or Integer token
     * @param result The resulting
     * @return
     */
    private Token<?> convert(BigDecimal result) {
        //TODO - This will lead to an incorrect result if using european decimalisation e.g. 1000,99 (DE) vs 1000.99 (UK)
        if (result.toString().contains(".")) {
            return new DecimalToken(result);
        } else {
            return new IntegerToken(result.toBigInteger());
        }
    }
}
