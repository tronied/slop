package dev.slop.operations;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.OperatorType;
import dev.slop.exception.ParserException;
import dev.slop.tokens.Token;
import dev.slop.tokens.literals.BooleanToken;
import dev.slop.tokens.operators.OperatorHandler;
import dev.slop.tokens.operators.OperatorToken;

import java.util.Objects;

/**
 * Handles operations with one or more null tokens including equals and not equals.
 */
public class NullOperation implements TypeOperation {

    /**
     * See {@link TypeOperation#canHandle(Token, OperatorToken, Token) canHandle}
     */
    @Override
    public boolean canHandle(Token<?> first, OperatorToken operator, Token<?> second) {
        if (operator.getOpType(operator) == OperatorType.DECREMENT ||
                operator.getOpType(operator) == OperatorType.INCREMENT) return false;
        return Objects.isNull(first.getValue()) || Objects.isNull(second.getValue());
    }

    /**
     * See {@link TypeOperation#handleCustomOperator(Token, OperatorToken, Token) handleCustomOperator}
     */
    @Override
    public <T> T handleCustomOperator(Token<?> first, OperatorToken operator, Token<?> second) {
        throw new ParserException(String.format("Attempted an unsupported operation on a null value. Operation: %s %s %s",
                Objects.isNull(first.getValue()) ? "null" : "\"" + first.getValue().toString() + "\"", operator.getValue(),
                Objects.isNull(second.getValue()) ? "null" : "\"" + second.getValue().toString() + "\""));
    }

    /**
     * See {@link TypeOperation#process(SLOPConfig, SLOPContext, Token, OperatorToken, Token) process}
     */
    @Override
    public Token<?> process(SLOPConfig config, SLOPContext context, Token<?> first, OperatorToken operator,
                            Token<?> second) {
        OperatorHandler handler = config.getOperatorHandler();
        switch (handler.getOpType(operator)) {
            case EQUALS: return new BooleanToken(first.getValue() == second.getValue());
            case NOT_EQUALS: return new BooleanToken(first.getValue() != second.getValue());
            default: return handleCustomOperator(first, operator, second);
        }
    }
}
