package dev.slop.operations;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.exception.ParserException;
import dev.slop.tokens.Token;
import dev.slop.tokens.literals.BooleanToken;
import dev.slop.tokens.operators.OperatorHandler;
import dev.slop.tokens.operators.OperatorToken;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Handles Date operations including greater (and equal), less than (and equal), equals and not equals. Currently dates
 * are expected in LocalDateTime format, though this could be extended to include Instant.
 */
public class DateOperation implements TypeOperation {

    /**
     * See {@link TypeOperation#canHandle(Token, OperatorToken, Token) canHandle}
     */
    @Override
    public boolean canHandle(Token<?> first, OperatorToken operator, Token<?> second) {
        return (first.is(LocalDateTime.class) || first.is(Date.class)) &&
                (second.is(LocalDateTime.class) || second.is(Date.class));
    }

    /**
     * See {@link TypeOperation#handleCustomOperator(Token, OperatorToken, Token) handleCustomOperator}
     */
    @Override
    public <T> T handleCustomOperator(Token<?> first, OperatorToken operator, Token<?> second) {
        throw new ParserException(String.format("Unable to handle Date operation with operator '%s'",
                operator.getValue()));
    }

    /**
     * See {@link TypeOperation#process(SLOPConfig, SLOPContext, Token, OperatorToken, Token) process}
     */
    @Override
    public Token<?> process(SLOPConfig config, SLOPContext context, Token<?> first, OperatorToken operator,
                            Token<?> second) {
        long dateA = first.is(LocalDateTime.class) ? first.getValue(LocalDateTime.class)
                .atZone(ZoneId.systemDefault()).toInstant().toEpochMilli() :
                    first.getValue(Date.class).getTime();
        long dateB = second.is(LocalDateTime.class) ? second.getValue(LocalDateTime.class)
                .atZone(ZoneId.systemDefault()).toInstant().toEpochMilli() :
                    second.getValue(Date.class).getTime();
        boolean result;
        OperatorHandler handler = config.getOperatorHandler();
        switch (handler.getOpType(operator)) {
            case GT: result = dateA > dateB; break;
            case LT: result = dateA < dateB; break;
            case GTE: result = dateA >= dateB; break;
            case LTE: result = dateA <= dateB; break;
            case EQUALS: result = dateA == dateB; break;
            case NOT_EQUALS: result = dateA != dateB; break;
            default: result = handleCustomOperator(first, operator, second);
        }
        return new BooleanToken(result);
    }
}
