package dev.slop.operations;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.exception.ParserException;
import dev.slop.tokens.Token;
import dev.slop.tokens.literals.ArrayToken;
import dev.slop.tokens.operators.OperatorHandler;
import dev.slop.tokens.operators.OperatorToken;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Handles Array operations including add, subtract, intersection (divide) and reverse intersection (multiply). The
 * result is returned as an ArrayToken.
 */
public class ArrayOperation implements TypeOperation {

    /**
     * See {@link TypeOperation#canHandle(Token, OperatorToken, Token) canHandle}
     */
    @Override
    public boolean canHandle(Token<?> first, OperatorToken operator, Token<?> second) {
        return first.is(List.class) && second.is(List.class);
    }

    /**
     * See {@link TypeOperation#handleCustomOperator(Token, OperatorToken, Token) handleCustomOperator}
     */
    @Override
    public <T> T handleCustomOperator(Token<?> first, OperatorToken operator, Token<?> second) {
        throw new ParserException(String.format("Unable to handle Array operation with operator '%s'",
                operator.getValue()));
    }

    /**
     * See {@link TypeOperation#process(SLOPConfig, SLOPContext, Token, OperatorToken, Token) process}
     */
    @Override
    @SuppressWarnings("unchecked")
    public Token<?> process(SLOPConfig config, SLOPContext context, Token<?> first, OperatorToken operator,
                            Token<?> second) {
        List<Token<?>> result = new ArrayList<>();
        OperatorHandler handler = config.getOperatorHandler();
        List<Token<?>> original = first.getValue(List.class);
        List<Token<?>> compare = second.getValue(List.class);
        switch (handler.getOpType(operator)) {
            //Adds two arrays together e.g. [1,2] + [3,4] = [1,2,3,4]
            case ADD:
                result = new ArrayList<>(original);
                result.addAll(compare);
                break;
            //Subtracts an array from the other e.g. [1,2,3] - [2,3] = [1]
            case SUBTRACT:
                result = new ArrayList<>(original);
                result.removeAll(compare);
                break;
            //Intersection e.g. [1,2,3] / [2,3,4] = [2,3]
            case DIVIDE:
                result.addAll(original.stream()
                        .distinct()
                        .filter(compare::contains)
                        .collect(Collectors.toSet()));
                break;
            //Reverse Intersection e.g. [1,2,3] / [2,3,4] = [1,4]
            case MULTIPLY:
                reverseIntersection(result, original, compare);
                break;
            case INCREMENT_EQUALS:
                result = new ArrayList<>(original);
                result.addAll(compare);
                context.set(first.getName(), result);
                break;
            default: result = handleCustomOperator(first, operator, second);
        }
        return new ArrayToken(result, true);
    }

    /**
     * Performs a reverse intersection between two lists
     * @param result The list used to hold the result from the operation
     * @param original The first list in the operation
     * @param compare The second list in the operation
     */
    private void reverseIntersection(List<Token<?>> result, List<Token<?>> original, List<Token<?>> compare) {
        result.addAll(original.stream()
                .distinct()
                .filter(i -> !compare.contains(i))
                .collect(Collectors.toSet()));
        result.addAll(compare.stream()
                .distinct()
                .filter(i -> !original.contains(i))
                .collect(Collectors.toSet()));
    }
}
