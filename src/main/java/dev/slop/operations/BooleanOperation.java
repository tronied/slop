package dev.slop.operations;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.exception.ParserException;
import dev.slop.tokens.Token;
import dev.slop.tokens.literals.BooleanToken;
import dev.slop.tokens.operators.OperatorHandler;
import dev.slop.tokens.operators.OperatorToken;

/**
 * Handles Boolean operations including equal and not equals.
 */
public class BooleanOperation implements TypeOperation {

    /**
     * See {@link TypeOperation#canHandle(Token, OperatorToken, Token) canHandle}
     */
    @Override
    public boolean canHandle(Token<?> first, OperatorToken operator, Token<?> second) {
        return first.is(Boolean.class) && second.is(Boolean.class);
    }

    /**
     * See {@link TypeOperation#handleCustomOperator(Token, OperatorToken, Token) handleCustomOperator}
     */
    @Override
    public <T> T handleCustomOperator(Token<?> first, OperatorToken operator, Token<?> second) {
        throw new ParserException(String.format("Unable to handle Boolean operation with operator '%s'",
                operator.getValue()));
    }

    /**
     * See {@link TypeOperation#process(SLOPConfig, SLOPContext, Token, OperatorToken, Token) process}
     */
    @Override
    public Token<?> process(SLOPConfig config, SLOPContext context, Token<?> first, OperatorToken operator, Token<?> second) {
        boolean result;
        OperatorHandler handler = config.getOperatorHandler();
        switch (handler.getOpType(operator)) {
            case EQUALS: result = first.getValue(Boolean.class).equals(second.getValue(Boolean.class)); break;
            case NOT_EQUALS: result = !first.getValue(Boolean.class).equals(second.getValue(Boolean.class)); break;
            default: result = handleCustomOperator(first, operator, second);
        }
        return new BooleanToken(result);
    }
}
