package dev.slop.functions;

import dev.slop.config.SLOPConfig;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * Functions are a special type of Operation that have a name and one or more parameters. The identifying name
 * helps the Parser determine which Function to use and passes the config and parameters to retrieve a result.
 * They provide extra functionality which cannot easily be undertaken using the standard mathematical operations
 * and Statements out of the box. A function could be defined as FUNCTION_NAME(param1,param2...)
 */
public interface Function {

    /**
     * The name of the function for which the handler represents so the Parser knows where to send the parameters.
     * It is recommended to keep each function name unique as there is no notion of operator overloading as there
     * are no fixed types or fixed parameters. It is recommended to use an uppercase function name, though there
     * is no requirement for this.
     * @param name Matches the current function class against the given name
     * @return Returns the name of the function
     */
    boolean isMatch(String name);

    /**
     * Executes the given functions logic on the parameter values. The values themselves are passed as a List of
     * TokenValue's which can contain anything (Primitive, Object, Collections etc). The result is another
     * generic TokenValue which can contain a type (Primitive, Object, Collection ect). You can write restrictions
     * in this method to determine the number of parameters, or leave it open i.e. DATE("23-05-83", "dd-MM-yy") vs
     * SUM(1,2,3,4,5,6...)
     * @param config The config object which may contain properties which affects the functions behaviour
     * @param values The list of TokenValue's
     * @return A single TokenValue which contains the calculated result
     */
    Token<?> execute(SLOPConfig config, List<Token<?>> values);
}
