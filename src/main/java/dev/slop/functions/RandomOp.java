package dev.slop.functions;

import dev.slop.config.SLOPConfig;
import dev.slop.exception.ParserException;
import dev.slop.tokens.Token;
import dev.slop.tokens.base.TokenValue;

import java.util.List;
import java.util.Random;

public class RandomOp implements Function {

    /**
     * See {@link Function#isMatch(String name) isMatch}
     */
    @Override
    public boolean isMatch(String name) {
        return name.equalsIgnoreCase("RAND");
    }

    @Override
    public Token<?> execute(SLOPConfig config, List<Token<?>> values) {
        Random random = new Random();
        if (values.size() < 1 || values.size() > 2) {
            throw new ParserException("Expected two arguments e.g. 'RANDOM(\"integer\", 1000)'");
        }
        if (!(values.get(0).getValue() instanceof String)) {
            throw new ParserException("Expected random type (param 0) to be a String value. Valid values are: " +
                    "\"integer\", \"double\", \"float\", \"long\", \"boolean\"");
        }
        String type = values.get(0).getValue(String.class);
        if (values.size() == 2) {
            if (!type.equalsIgnoreCase("integer")) {
                throw new ParserException("Bound only supported when using the integer type");
            }
            if (!(values.get(1).getValue() instanceof Integer)) {
                throw new ParserException("The bound parameter must be specified as an integer");
            }
        }
        switch (type) {
            case "integer": return new TokenValue(values.size() == 1 ? random.nextInt() :
                random.nextInt(values.get(1).getValue(Integer.class)));
            case "float": return new TokenValue(random.nextFloat());
            case "double": return new TokenValue(random.nextDouble());
            case "boolean": return new TokenValue(random.nextBoolean());
            case "long": return new TokenValue(random.nextLong());
        }
        throw new ParserException(String.format("Unexpected type provided in random function '%s'", type));
    }
}
