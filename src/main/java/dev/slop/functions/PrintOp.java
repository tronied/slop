package dev.slop.functions;

import dev.slop.config.SLOPConfig;
import dev.slop.exception.ParserException;
import dev.slop.tokens.Token;
import dev.slop.tokens.literals.NullToken;

import java.util.List;

/**
 * Provides the ability to output text or content to the console. The first parameter is the
 * content with the optional second to determine whether it should be printed inline, or on
 * a new line.
 */
public class PrintOp implements Function {

    @Override
    public boolean isMatch(String name) {
        return name.equalsIgnoreCase("PRINT");
    }

    @Override
    public Token<?> execute(SLOPConfig config, List<Token<?>> values) {
        if (values.size() > 2) {
            throw new ParserException("Expected only 1 or two parameters for PRINT function. 1) The string to print " +
                    "2) If the text is to be printed inline (not default)");
        }
        if (values.size() == 1) {
            System.out.println(values.get(0).getValue().toString());
            return new NullToken();
        }
        if (!(values.get(1).getValue() instanceof Boolean))
            throw new ParserException("Expected the second parameter to be a boolean type!");
        if (Boolean.TRUE.equals(values.get(1).getValue())) {
            System.out.print(values.get(0).getValue().toString());
        } else {
            System.out.println(values.get(0).getValue().toString());
        }
        return new NullToken();
    }
}
