package dev.slop.functions;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.exception.ParserException;
import dev.slop.tokens.Token;
import dev.slop.tokens.base.TokenValue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

/**
 * A simple function that reads in a date using either the default pattern defined by the DefaultProperty.DATE_FORMAT
 * or by defining a pattern as a second parameter. This function uses the SimpleDateFormat and the documentation
 * for constructing a date can be found here: https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
 */
public class DateOp implements Function {

    /**
     * See {@link Function#isMatch(String name) isMatch}
     */
    @Override
    public boolean isMatch(String name) {
        return name.equalsIgnoreCase("DATE");
    }

    /**
     * See {@link Function#execute(SLOPConfig, List) getName}
     */
    @Override
    public Token<?> execute(SLOPConfig config, List<Token<?>> values) {
        try {
            if (values.size() != 1 && values.size() != 2) {
                throw new ParserException("Invalid date function use. Expected DATE(<dateString>, (Optional)<format>)");
            }
            SimpleDateFormat sdf = (values.size() == 1) ?
                    new SimpleDateFormat(config.getProperty(DefaultProperty.DATE_FORMAT)) :
                    new SimpleDateFormat(values.get(1).getValue(String.class));

            Instant parsedDate = sdf.parse(values.get(0).getValue(String.class)).toInstant();
            return new TokenValue(LocalDateTime.ofInstant(parsedDate, ZoneId.systemDefault()));
        } catch (ParseException ex) {
            throw new ParserException(String.format("Could not parse date value '%s'",
                    values.get(0).getValue(String.class)));
        }
    }
}
