package dev.slop.functions;

import dev.slop.config.SLOPConfig;
import dev.slop.exception.SLOPException;
import dev.slop.tokens.Token;

import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class NewFunction implements Function {

    @Override
    public boolean isMatch(String name) {
        return name.equalsIgnoreCase("NEW");
    }

    @Override
    public Token<?> execute(SLOPConfig config, List<Token<?>> values) {
        String includeId = values.get(0).getValue().toString();
        String classPath = config.getIncludeClasspath(includeId);
        try {
            Class<?> c = Class.forName(classPath);
            List<Constructor<?>> classConstructors = Arrays.asList(c.getDeclaredConstructors());
            StringBuilder sb = new StringBuilder();
            sb.append(String.format("Valid constructors for '%s'%s", classPath, System.lineSeparator()));
            for (Constructor<?> constructor : classConstructors) {
                if (values.size() == 1) {
                    String params = Arrays.stream(constructor.getParameterTypes())
                            .map(Class::getSimpleName)
                            .collect(Collectors.joining(", "));
                    sb = sb.append(String.format("- %s(%s)%s", c.getSimpleName(), params, System.lineSeparator()));
                }
            }
            System.out.println(sb);
        } catch (ClassNotFoundException ex) {
            throw new SLOPException(String.format("Could not find '%s' as it does not exist in the class " +
                    "path", classPath));
        }
        return null;
    }
}
