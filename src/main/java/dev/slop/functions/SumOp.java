package dev.slop.functions;

import dev.slop.config.SLOPConfig;
import dev.slop.exception.ParserException;
import dev.slop.tokens.Token;
import dev.slop.tokens.base.TokenValue;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

/**
 * This function takes one or more parameters and simply adds them together. This includes support for collections or
 * single values. The result is returned as a single token value.
 */
public class SumOp implements Function {

    /**
     * See {@link Function#isMatch(String name) isMatch}
     */
    @Override
    public boolean isMatch(String name) {
        return name.equalsIgnoreCase("SUM");
    }

    /**
     * See {@link Function#execute(SLOPConfig, List) execute} 
     */
    @Override
    public Token<?> execute(SLOPConfig config, List<Token<?>> args) {
        Object current = null;
        BigDecimal sum = BigDecimal.ZERO;
        try {
            for (Token<?> arg : args) {
                if (arg.getValue() instanceof Collection) {
                    for (Object item : (Collection<?>)arg.getValue()) {
                        if (item instanceof Token) {
                            item = ((Token<?>) item).getValue();
                        }
                        current = item;
                        sum = sum.add(new BigDecimal(item.toString()));
                    }
                } else {
                    current = arg.getValue();
                    sum = sum.add(new BigDecimal(arg.getValue().toString()));
                }
            }
        } catch (NumberFormatException ex) {
            throw new ParserException(String.format("Could not sum numerically with value '%s'", current));
        }
        return new TokenValue(sum);
    }
}