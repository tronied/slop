package dev.slop.lexer;

import dev.slop.config.SLOPConfig;
import dev.slop.exception.LexerException;
import dev.slop.model.Expression;
import dev.slop.model.LexerResult;
import dev.slop.tokens.Token;
import dev.slop.tokens.grammar.*;

import java.util.ArrayList;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * This simple lexer scans and tokenizes the grammar pattern of a token. The grammar pattern determines not only
 * the structure of the statement and how it can be defined, but also structure of how the tokens are organised
 * into groups for processing. To start there are two basic grammar tokens which cause other values to be captured.
 * These are 'val' (single) and 'expr' (multiple). Around this you can group tokens together using brackets or define
 * syntax by wrapping values in single quotes. For example, the grammar for a FunctionToken is the following:
 *
 *      val '(' ( expr ','? )+ ')'
 *
 * Breaking this down we get the following:
 *
 *      val   = Captures single value
 *      '('   = Denotes an opening bracket in the syntax
 *      (     = Start of a grammar group
 *      expr  = Captures multiple values
 *      ','?  = An optional comma value. '?' marks the preceding token as optional
 *      )+    = End of the grammar group. The '+' denotes that the contents of this group can be repeated one
 *              or more times
 *      ')'   = A closing bracket in the syntax
 *
 * Using this we can pass the following values in an expression which would be a valid match:
 *
 *      DATE("14-02-1983 15:49:03", "dd-MM-yyyy hh:mm:ss")
 *      SUM(45 * 15 + 1, 2 / 3, 3)
 *
 * The first match would result in the following token output:
 *
 *      FunctionToken( tokenGroups = [
 *          TokenGroup( tokens = [
 *              StringToken( value = "DATE" )
 *          ],
 *          TokenGroup( tokens = [
 *              TokenGroup ( tokens = [ StringToken( value = "14-02-1983 15:49:03" ) ] ),
 *               TokenGroup ( tokens = [ StringToken( value = "dd-MM-yyyy hh:mm:ss" ) ] ),
 *          ] )
 *      ] )
 *
 * For the more complicated second example, it would be the following:
 *
 *      FunctionToken( tokenGroups = [
 *          TokenGroup( tokens = [
 *              StringToken( value = "SUM" )
 *          ],
 *          TokenGroup( tokens = [
 *              TokenGroup ( tokens = [
 *                  IntegerToken( value = 45 ),
 *                  OperatorToken( value = * ),
 *                  IntegerToken( value = 15 ),
 *                  OperatorToken( value = + ),
 *                  IntegerToken( value = 1 )
 *              ] ),
 *              TokenGroup ( tokens = [
 *                  IntegerToken( value = 2 ),
 *                  OperatorToken( value = / ),
 *                  IntegerToken( value = 3 ),
 *              ] ),
 *              TokenGroup ( tokens = [ IntegerToken( value = 3 ) ] )
 *          ] )
 *      ] )
 *
 *  When defining a grammar pattern you should be aware of the structure of other tokens defined within SLOP. Should
 *  one of the existing Tokens conflict with the new Token and the Lexer is unsure of which match to use, it will
 *  throw a LexerException to highlight the issue.
 *
 *  There is one final token which can be used when specifying grammar which is the '&lt;' flag. This can be seen in
 *  the SwitchToken:
 *
 *      ... ( expr ','? )+ ':' expr '!'&lt;? ';'? )+ ...
 *
 *  This can follow a syntax token and tells the Lexer not to discard it but instead add it to the context as it would
 *  with a normal token as a TokenValue or NonToken. This can be useful to pass flags to the token processing code
 *  (triggered by the Parser) to denote a certain situation. In the above example if an exclamation mark is found in a
 *  switch case then it will stop looking at any other cases (similar to how a break works in the Java switch statement).
 */
public class SLOPGrammarLexer extends Lexer<Token<?>> {

    public SLOPGrammarLexer(SLOPConfig config) {
        super(config);
    }

    @Override
    public LexerResult<Token<?>> tokenize(Expression expression) {
        initResources();
        String value = expression.getValue();
        boolean inToken = false;
        boolean typeAssignment = false;
        StringBuilder sb = new StringBuilder();
        for (int i = 0;i < value.length();i++) {
            String current = String.valueOf(value.charAt(i));
            if (nonNameCharacter(current) && typeAssignment) {
                //Handle event where a type for a GrammarValue has been captured
                handleSingleOfType(sb);
                typeAssignment = false;
            }
            switch (current) {
                case "+": if (!inToken) { handleMultiple(); continue; } break;
                case "?": if (!inToken) { handleOptional(); continue; } break;
                case "<": if (!inToken) { handlePersist(); continue; } break;
                case " ": if (!inToken) { continue; }
                case "'": inToken = handleToken(inToken, sb); continue;
                case "(": if (!inToken) { handleGroup(true); continue; } break;
                case ")": if (!inToken) { handleGroup(false); continue; } break;
                case "[": if (!inToken) { handleRef(sb, true); continue; } break;
                case "]": if (!inToken) { handleRef(sb, false); continue; } break;
                case ",": if (!inToken) { handleRefToken(sb); continue; } break;
                case ":": if (!inToken) {
                    typeAssignment = true;
                    continue;
                }
            }
            sb.append(current);
            switch (sb.toString()) {
                case "expr": handleExpression(sb); break;
                case "val": handleSingle(sb); break;
            }
        }
        return LexerResult.<Token<?>>builder().result(context).build();
    }

    private boolean nonNameCharacter(String currentChar) {
        Pattern p = Pattern.compile("^[^a-zA-Z0-9]");
        Matcher m = p.matcher(currentChar);
        return m.find();
    }

    private void handleMultiple() {
        if (stack.isEmpty()) {
            Token<?> token = context.get(context.size() - 1);
            if (token instanceof GrammarGroup) {
                GrammarGroup grammarGroup = (GrammarGroup)token;
                grammarGroup.setMultiple(true);
            }
        } else {
            Token<?> token = stack.peek();
            if (token instanceof GrammarGroup) {
                GrammarGroup grammarGroup = (GrammarGroup)token;
                Optional<GrammarGroup> lastGroup = findLastGroup(grammarGroup);
                if (lastGroup.isPresent()) {
                    lastGroup.get().setMultiple(true);
                } else {
                    grammarGroup.setMultiple(true);
                }
            }
        }
    }

    private Optional<Token<?>> findLastAddedToken(Token<?> token, boolean root) {
        if (token instanceof GrammarGroup) {
            GrammarGroup group = (GrammarGroup) token;
            if (!group.getPatternTokens().isEmpty()) {
                Token<?> lastToken = group.getPatternTokens().get(group.getPatternTokens().size() - 1);
                if (lastToken instanceof GrammarGroup) {
                    if (!root) {
                        Optional<Token<?>> found = findLastAddedToken(lastToken, false);
                        if (!(found.get() instanceof GrammarGroup)) {
                            return Optional.of(lastToken);
                        } else {
                            return found;
                        }
                    }
                }
                return Optional.of(lastToken);
            }
        }
        return Optional.of(token);
    }

    private Optional<GrammarGroup> findLastGroup(GrammarGroup group) {
        Token<?> lastToken = group.getPatternTokens().get(group.getPatternTokens().size() - 1);
        if (lastToken instanceof GrammarGroup && !lastToken.getPatternTokens().isEmpty()) {
            return findLastGroup((GrammarGroup)lastToken);
        }
        return Optional.of(group);
    }

    private void handleOptional() {
        if (stack.isEmpty()) {
            Token<?> token = context.get(context.size() - 1);
            if (token instanceof GrammarToken) {
                ((GrammarToken<?>)token).setOptional(true);
            }
        } else {
            Token<?> token = stack.peek();
            if (token instanceof GrammarGroup) {
                GrammarGroup grammarGroup = (GrammarGroup)token;
                Optional<Token<?>> lastToken = findLastAddedToken(grammarGroup, true);
                if (lastToken.isPresent() && lastToken.get() instanceof GrammarToken) {
                    ((GrammarToken<?>)lastToken.get()).setOptional(true);
                } else {
                    grammarGroup.setOptional(true);
                }
            }
        }
    }

    private boolean handleToken(boolean inToken, StringBuilder sb) {
        if (inToken) {
            GrammarValue tokenValue = new GrammarValue(sb.toString());
            if (stack.isEmpty()) {
                context.add(tokenValue);
            } else {
                Token<?> topmost = stack.peek();
                if (topmost instanceof GrammarGroup) {
                    GrammarGroup group = (GrammarGroup)topmost;
                    group.getPatternTokens().add(tokenValue);
                }
            }
            sb.setLength(0);
            return false;
        } else {
            return true;
        }
    }

    private void handleGroup(boolean start) {
        if (start) {
            stack.push(new GrammarGroup(createId(), new ArrayList<>()));
        } else {
            if (!stack.isEmpty()) {
                Token<?> topmost = stack.pop();
                if (stack.isEmpty() && topmost instanceof GrammarGroup) {
                    GrammarGroup group = (GrammarGroup) topmost;
                    context.add(group);
                } else if (stack.peek() instanceof GrammarGroup) {
                    GrammarGroup group = (GrammarGroup) stack.peek();
                    group.getPatternTokens().add(topmost);
                }
            }
        }
    }

    private void handleSingleOfType(StringBuilder sb) {
        if (!stack.isEmpty()) {
            Token<?> topmost = stack.peek();
            if (topmost instanceof GrammarGroup) {
                GrammarGroup group = (GrammarGroup) topmost;
                Token<?> value = group.getPatternTokens().get(group.getPatternTokens().size() - 1);
                if (value instanceof GrammarValue) {
                    GrammarValue grammarValue = (GrammarValue) value;
                    grammarValue.setValueType(sb.toString());
                    sb.setLength(0);
                }
            }
        } else if (!context.isEmpty()) {
            ((GrammarValue)context.get(context.size() - 1)).setValueType(sb.toString());
            sb.setLength(0);
        }
    }

    private void handleRef(StringBuilder sb, boolean start) {
        if (start) {
            stack.push(new GrammarReference(new ArrayList<>()));
        } else {
            Token<?> topmost = stack.pop();
            if (!(topmost instanceof GrammarReference)) {
                throw new LexerException("Unknown ']' character not part of reference found in " +
                        "grammar string");
            }
            ((GrammarReference)topmost).getValue().add(sb.toString());
            sb.setLength(0);
            if (stack.isEmpty()) {
                context.add(topmost);
            } else if (stack.peek() instanceof GrammarGroup) {
                GrammarGroup group = (GrammarGroup)stack.peek();
                group.getPatternTokens().add(topmost);
            }
        }
    }

    private void handleRefToken(StringBuilder sb) {
        Token<?> topmost = stack.peek();
        if (!(topmost instanceof GrammarReference)) {
            throw new LexerException("Unknown ',' character not part of reference found in grammar string");
        }
        ((GrammarReference)topmost).getValue().add(sb.toString());
        sb.setLength(0);
    }

    private String createId() {
        String idConstruct = Integer.toString(context.size() + 1);
        String constructedStackId = stack.stream()
                .map(gg -> gg.getPatternTokens().size() + 1)
                .map(Object::toString)
                .collect(Collectors.joining("."));
        return !constructedStackId.isEmpty() ? idConstruct.concat(".").concat(constructedStackId) : idConstruct;
    }

    private void handleExpression(StringBuilder sb) {
        if (stack.isEmpty()) {
            context.add(new GrammarExpression());
        } else {
            Token<?> topmost = stack.peek();
            if (topmost instanceof GrammarGroup) {
                GrammarGroup group = (GrammarGroup)topmost;
                group.getPatternTokens().add(new GrammarExpression());
            }
        }
        sb.setLength(0);
    }

    private void handleSingle(StringBuilder sb) {
        GrammarValue value = new GrammarValue(sb.toString());
        value.setCapture(true);
        if (stack.isEmpty()) {
            context.add(value);
        } else {
            Token<?> topmost = stack.peek();
            if (topmost instanceof GrammarGroup) {
                GrammarGroup group = (GrammarGroup)topmost;
                group.getPatternTokens().add(value);
            }
        }
        sb.setLength(0);
    }

    private void handlePersist() {
        if (stack.isEmpty()) {
            Token<?> token = context.get(context.size() - 1);
            if (token instanceof GrammarToken) {
                ((GrammarToken<?>)token).setPersist(true);
            }
        } else {
            Token<?> token = stack.peek();
            if (token instanceof GrammarGroup) {
                GrammarGroup grammarGroup = (GrammarGroup)token;
                Optional<Token<?>> lastToken = findLastAddedToken(grammarGroup, true);
                if (lastToken.isPresent() && lastToken.get() instanceof GrammarToken) {
                    ((GrammarToken<?>)lastToken.get()).setPersist(true);
                } else {
                    grammarGroup.setPersist(true);
                }
            }
        }
    }
}
