package dev.slop.lexer;

import dev.slop.config.SLOPConfig;
import dev.slop.enums.PatternType;
import dev.slop.enums.TokenType;
import dev.slop.exception.LexerException;
import dev.slop.exception.ParserException;
import dev.slop.model.Expression;
import dev.slop.model.LexerResult;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenGroup;
import dev.slop.tokens.base.NonToken;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.grammar.*;
import dev.slop.tokens.operators.LogicOperator;
import dev.slop.tokens.operators.OperatorHandler;
import dev.slop.tokens.operators.OperatorToken;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * The main language lexer takes a String expression and converts it into one or more tokens. Tokens are matched
 * using either a regular expression or grammar string located on the token itself. If the match type is regex,
 * these are typically simple tokens such as literals or arguments and can be created and added to the context
 * immediately. Grammar matches are used on statements and are more complex by nature. They define the syntax
 * structure as well as the groups to which the contained tokens will be added. Upon creation, tokens which use
 * grammar are initialized with a series of pattern tokens. These help the lexer keep track of where in the statement
 * it is so that the tokens being read can be added to the relevant group and handled appropriately.
 */
public class SLOPLexer extends Lexer<Token<?>> {

    /* Stores the last group written to by a token. This is used where a token initializer is expected but tracking
     * down the group to which it was written to is not readily available */
    private TokenGroup lastWriteGroup;

    public SLOPLexer(SLOPConfig config) {
        super(config);
    }

    /**
     * Processes a String expression and converts it into one or more matching tokens
     * @param expression An object containing the String expression to process
     * @return Returns a Result object with the resulting tokens
     */
    @Override
    public LexerResult<Token<?>> tokenize(Expression expression) {
        String originalExpression = expression.getValue();
        Optional<Token<?>> m;
        //Find a match for the token found at the beginning of the expression String
        while ((m = findMatch(config.getTokenHandlers(), expression, context,
                (nonToken) -> !stack.isEmpty() && stack.peek().isNextToken(nonToken))).isPresent()) {
            if (m.get().isIgnored()) continue; //Skip ignored content
            context.add(m.get()); //All tokens get added to short term memory
            if (!analysePatterns(stack.size())) break; //Exit processing is an issue occurs during analysis
        }
        config.getErrorHandlers().forEach(eh -> eh.performCheck(originalExpression, stack, context, evaluated));
        finalizeExpression();
        return new LexerResult<>(evaluated);
    }

    /**
     * Attempts to find a match for the current read token against those using Grammar i.e. statements. If a match
     * is found then a new token representing that statement is created, pattern tokens are cloned from the original
     * token handler (found in the config) and pushed to the stack. Additional checks and actions are also performed
     * should the context not be empty during creation. If no match is found then stack operation checks are made to
     * ensure that the token doesn't belong to a parent and takes action if necessary.
     * @param stackSize The current size of the stack
     * @return Provides the ability to stop execution of the expression is something has gone wrong
     */
    private boolean analysePatterns(int stackSize) {
        Token<?> lastToken = context.remove(context.size() - 1);
        Optional<Token<?>> bestMatch = getBestMatch(lastToken);
        if (bestMatch.isPresent()) {
            Token<?> match = bestMatch.get();
            String idCheck = match.getUID();
            if (!stack.stream().map(Token::getUID).collect(Collectors.toList()).contains(idCheck)) {
                handleTokenIncompatibility(lastToken, match);
                if (!defaultTokenWriteable(match.getActiveToken(true)) && !context.isEmpty()) handleContext();
                handleContextExceedsMatch(match);
                match = match.createToken("");
                initTokenGroups(match, null);
                if (!handleExpressionScenario(match)) return false;
                handleSingleTokenInitializer(match);
                handleMultiTokenInitializer(match);
                checkMatchRestrictions(match);
                stack.push(match);
            }
            /* Using the last read token and context, perform any updates / actions relevant to the found match
             * including progression, adding child tokens or completing the match and removing it from the stack. */
            match.progressToken(lastToken, context, evaluated);
            if (match.isComplete()) handleMatchComplete();
        } else {
            handleNoMatch(lastToken, stackSize);
        }
        return true;
    }

    /**
     * Handles scenarios where the top of the stack is expecting an optional syntax value (else for example), but
     * the new active token doesn't match this. As such, we push to complete prior to processing the new match
     * and token.
     * @param lastToken The current token being processed
     */
    private void handleTokenIncompatibility(Token<?> lastToken, Token<?> match) {
        if (!stack.isEmpty()) {
            Token<?> activeToken = stack.peek().getActiveToken(true, false, true);
            Token<?> activeGroup = stack.peek().getActiveToken(true, true);
            if (Objects.nonNull(activeGroup) && activeGroup instanceof GrammarGroup &&
                    ((GrammarGroup)activeGroup).isOptional() &&
                    stack.peek().getTokenPosition() != stack.peek().getPatternTokens().size() - 1) return;
            if (activeToken instanceof GrammarValue && !((GrammarValue)activeToken).isCapture() &&
                    !((GrammarValue)activeToken).isOptional() && !Objects.equals(activeToken.getValue(), lastToken.getValue())) {
                handleMatchComplete();
            }
            checkTopStackAgainstMatch(activeToken, activeGroup, lastToken, match);
        }
    }

    /**
     * Uses primitive matching against the current top of the stack and the new match. If the existing
     * token is not expecting a token (grammar value type specified) then we are confident that we can
     * kick the topmost off the stack as the two are unrelated.
     * @param activeToken The active token in the top item of the stack
     * @param activeGroup The group containing the active token
     * @param lastToken The last token read from the expression string
     * @param match The match in the process of being added
     */
    private void checkTopStackAgainstMatch(Token<?> activeToken, Token<?> activeGroup, Token<?> lastToken,
                                           Token<?> match) {
        if (!Objects.equals(activeToken.getValue(), lastToken.getValue())) {
            if (Objects.nonNull(activeGroup) && activeGroup.getTokenPosition() - 1 >= 0) {
                Token<?> previous = activeGroup.getPatternTokens().get(activeGroup.getTokenPosition() - 1);
                if (previous instanceof GrammarValue && Objects.nonNull(((GrammarValue) previous).getValueType()) &&
                        !((GrammarValue) previous).getValueType().isEmpty()) {
                    if (match.getPatternType() == PatternType.GRAMMAR) {
                        Token<?> topmost = stack.pop();
                        evaluated.add(topmost);
                    }
                }
            }
        }
    }

    /**
     * Attempts to find a matching handler in the config class for the current token. This is done using both the
     * tokens in the configuration and the one that is currently the head of the stack. A stack match takes precedence
     * over any others and is returned in an Optional wrapper.
     * @param lastToken The last token which has been scanned from the expression String
     * @return Returns a matching token either from the stack or list of configured handlers
     */
    private Optional<Token<?>> getBestMatch(Token<?> lastToken) {
        Optional<Token<?>> defaultMatch = getDefaultBestTokenMatch(lastToken);
        if (!stack.isEmpty()) {
            Optional<Token<?>> found = stack.peek().matchNextToken(lastToken);
            if (found.isPresent() && found.get() instanceof GrammarReference) {
                Optional<Token<?>> handler = getReferenceHandler((GrammarReference)found.get(), lastToken);
                if (handler.isPresent()) {
                    stack.peek().matchNextToken(lastToken, true, new AtomicBoolean(false));
                    return handler;
                }
            }
            if (found.isPresent() && stack.peek().isNextToken(lastToken)) {
                return Optional.of(stack.peek());
            }
            if (tokenExistsUpStack(defaultMatch, lastToken)) {
                Optional<GrammarReference> result = findActiveRefOnToken();
                if (!result.isPresent() || !getReferenceHandler(result.get(), lastToken).isPresent()) {
                    return Optional.empty();
                }
            }
        }
        return defaultMatch;
    }

    /**
     * Searches the stack for any active grammar reference and returns it
     * @return Returns the reference found on an item in the execution stack
     */
    private Optional<GrammarReference> findActiveRefOnToken() {
        Stack<Token<?>> stackItems = new Stack<>();
        stackItems.addAll(stack);
        if (stackItems.size() <= 1) return Optional.empty();
        try {
            while (!stackItems.isEmpty()) {
                Token<?> current = stackItems.peek().getActiveToken(true);
                if (current instanceof GrammarReference) {
                    return Optional.of((GrammarReference) current);
                }
                stackItems.pop();
            }
        } catch (Exception ex) {
            //Ignore
        }
        return Optional.empty();
    }

    /**
     * Searches the list of token handlers for the best match against the token currently being processed.
     * @param lastToken The token currently being processed from the expression String
     * @return Returns an optional token containing the match if one can be found
     */
    private Optional<Token<?>> getDefaultBestTokenMatch(Token<?> lastToken) {
        List<Token<?>> matches;
        if (!stack.isEmpty()) {
            Token<?> activeGroup = stack.peek().getActiveToken(true, true);
            Optional<Token<?>> refToken = getReferenceHandler(lastToken);
            if (!refToken.isPresent() && Objects.nonNull(activeGroup) && activeGroup instanceof GrammarGroup &&
                    ((GrammarGroup) activeGroup).isOptional() && stack.peek().getTokenPosition() < stack.peek().getPatternTokens().size() - 1) {
                //TODO - This only looks to the next one if optional. Need to look further
                stack.peek().setTokenPosition(stack.peek().getTokenPosition() + 1);
                refToken = getReferenceHandler(lastToken);
                if (!refToken.isPresent())
                    stack.peek().setTokenPosition(stack.peek().getTokenPosition() - 1);
            }
            if (refToken.isPresent()) return refToken;
        }
        if (!(lastToken instanceof NonToken || lastToken instanceof TokenValue)) return Optional.empty();
        matches = config.getTokenHandlers().stream()
                .filter(th -> th.getTokenType() == TokenType.PRIMARY)
                .filter(th -> th.getPatternType() == PatternType.GRAMMAR)
                .filter(th -> !Objects.isNull(th.getPatternTokens()))
                .filter(tl -> tl.firstNonCaptureToken(lastToken, context.size(), true))
                .collect(Collectors.toList());
        return getBestMatchToken(lastToken, matches);
    }

    /**
     * Attempts to find a match in the list of token handlers against the topmost stack grammar reference. This is used
     * where another token provides a reference [ tokenTypeA, tokenTypeB ] and using the last token attempts to
     * find the best match.
     * @param lastToken The token currently being processed
     * @return If found, returns an optional containing the match
     */
    private Optional<Token<?>> getReferenceHandler(Token<?> lastToken) {
        if (stack.peek().getActiveToken(true, false, true) instanceof GrammarReference) {
            GrammarReference refToken = (GrammarReference) stack.peek().getActiveToken(true, false, true);
            //Give priority to pattern matches from the names of tokens in grammar
            List<Token<?>> matches = config.getTokenHandlers().stream()
                    .filter(th -> th.getPatternType() == PatternType.GRAMMAR)
                    .filter(th -> !Objects.isNull(th.getPatternTokens()))
                    .filter(th -> refToken.getValue().contains(th.getName()))
                    .filter(tl -> tl.firstNonCaptureToken(lastToken, context.size(), true))
                    .collect(Collectors.toList());
            if (!matches.isEmpty()) return getBestMatchToken(lastToken, matches);
        }
        return Optional.empty();
    }

    /**
     * Attempts to find a match in the list of token handlers against the passed grammar reference. This is used
     * where another token provides a reference [ tokenTypeA, tokenTypeB ] and using the last token attempts to
     * find the best match.
     * @param refToken The reference token referencing other tokens used to match against the token being processed
     * @param lastToken The token currently being processed
     * @return If found, returns an optional containing the match
     */
    private Optional<Token<?>> getReferenceHandler(GrammarReference refToken, Token<?> lastToken) {
        //Give priority to pattern matches from the names of tokens in grammar
        List<Token<?>> matches = config.getTokenHandlers().stream()
                .filter(th -> th.getPatternType() == PatternType.GRAMMAR)
                .filter(th -> !Objects.isNull(th.getPatternTokens()))
                .filter(th -> refToken.getValue().contains(th.getName()))
                .filter(tl -> tl.firstNonCaptureToken(lastToken, context.size(), true))
                .collect(Collectors.toList());
        if (!matches.isEmpty()) return getBestMatchToken(lastToken, matches);
        return Optional.empty();
    }

    /**
     * If more than one token handler has been found for the current token, an attempt is made to filter those down
     * to the one that is most suitable based on the matches initial pattern token and the state of the context (short
     * term memory). If multiple matches are still found after this process an exception is thrown to highlight this
     * ambiguity as we don't know the correct course of action to take.
     * @param lastToken The last read token from the expression which triggered the matches to be found
     * @param tokenMatches The list of handlers which were matched against the last read token
     * @return Returns the best match
     */
    private Optional<Token<?>> getBestMatchToken(Token<?> lastToken, List<Token<?>> tokenMatches) {
        if (tokenMatches.isEmpty()) return Optional.empty();
        if (tokenMatches.size() == 1) return Optional.of(tokenMatches.get(0));
        List<Token<?>> source = context.isEmpty() ? evaluated : context;
        List<Token<?>> bestFits = tokenMatches.stream()
                .filter(t -> !source.isEmpty() && !(source.get(source.size() - 1) instanceof OperatorHandler) ?
                        (t.getActiveToken(true) instanceof GrammarValue && ((GrammarValue) t.getActiveToken(true)).isCapture()) &&
                        checkTypeMatch((GrammarValue) t.getActiveToken(true), source.get(source.size() - 1)):
                        t.getPatternTokens().get(t.getTokenPosition()).getValue().equals(lastToken.getValue()))
                //Prevent a value to be matched against an expression
                .filter(t -> t.getActiveToken(true) instanceof GrammarValue && !source.isEmpty() &&
                        !source.get(source.size() - 1).isExpression())
                .collect(Collectors.toList());
        if (bestFits.isEmpty()) {
            bestFits = tokenMatches.stream()
                    .filter(t -> t.getPatternTokens().get(t.getTokenPosition()).getValue().equals(lastToken.getValue()))
                    .collect(Collectors.toList());
        }
        String conflictNames = bestFits.stream()
                .map(t -> t.getClass().getSimpleName())
                .collect(Collectors.joining(" / "));
        if (bestFits.size() > 1) {
            throw new ParserException(String.format("Found multiple token matches (%s) conflicting with token (%s)",
                    conflictNames, lastToken.getValue().toString()));
        }
        return bestFits.isEmpty() ? Optional.empty() : Optional.of(bestFits.get(0));
    }

    /**
     * If the current active token is a grammar value and has a specific required type (denoted by a colon and class
     * name e.g. val:String), then this ensures that the token being eyed up to be used in a single token initialiser
     * matches that type.
     * @param value The pattern token grammar value to check if it has a required type
     * @param sourceToken The token to check the to see if it matches the requirement to be used
     * @return Returns true if the target token is a match for the grammar value type
     */
    private boolean checkTypeMatch(GrammarValue value, Token<?> sourceToken) {
        //Default to true if no type check set
        if (Objects.isNull(value.getValueType()) || value.getValueType().isEmpty()) return true;
        return Objects.nonNull(sourceToken.getValue()) && Objects.equals(value.getValueType(),
                sourceToken.getValue().getClass().getSimpleName());
    }

    /**
     * Determines whether the pattern token passed accepts other tokens or whether it is a non token (syntax)
     * @param token The pattern token to determine whether it is used to store / capture other tokens
     * @return Returns true if the pattern token is writeable
     */
    private boolean defaultTokenWriteable(Token<?> token) {
        return (token instanceof GrammarValue && ((GrammarValue) token).isCapture()) ||
                token instanceof GrammarExpression;
    }

    /**
     * This used in conjunction with the opposite result of defaultTokenWriteable ensures that when a non token
     * (syntax) is encountered and the context (short term token memory) is not empty, those are either added to the
     * long term token store if the stack is empty or added to the current stack head. This is because we are safe
     * in the knowledge that the recent tokens can now be added because the active token will progress to the next
     * capture token or group.
     */
    private void handleContext() {
        if (stack.isEmpty()) {
            evaluated.addAll(context);
            context.clear();
        } else {
            Token<?> activeToken = stack.peek();
            Token<?> patternToken = activeToken.getPatternTokens().get(activeToken.getTokenPosition());
            if (Objects.nonNull(activeToken.getActiveGroup())) {
                activeToken.addToGroup(patternToken, activeToken.getActiveToken(true, true),
                        context, activeToken, true);
            } else {
                int groupIndex = activeToken.getCurrentGroup().intValue();
                if (activeToken.getTokenGroups().isEmpty() || activeToken.getTokenGroups().size() < groupIndex) {
                    activeToken.getTokenGroups().add(new TokenGroup(context));
                } else {
                    activeToken.getTokenGroups().get(groupIndex - 1).getTokens().addAll(context);
                }
            }
            context.clear();
        }
    }

    /**
     * This scenario handles an event where a new match is being added and it is expecting only a single token but
     * there are multiple tokens in short term memory. In this case it is assumed that the other items belong to a
     * previous token on the stack and therefore those are added to the appropriate token group before the new match
     * is added to the stack.
     * @param match The new token match found based on the current token being processed
     */
    private void handleContextExceedsMatch(Token<?> match) {
        if ((match.getActiveToken(true) instanceof GrammarValue && ((GrammarValue) match.getActiveToken(true)).isCapture())
                && context.size() > 1 && !stack.isEmpty()) {
            Token<?> topmost = stack.peek();
            Token<?> activeToken = topmost.getActiveToken(true, true);
            if (activeToken instanceof GrammarGroup) {
                String groupId = ((GrammarGroup) activeToken).getGroupId();
                TokenGroup activeGroup = topmost.findTokenGroup(groupId);
                List<Token<?>> excessTokens = new ArrayList<>();
                //Leave the most recent token in short term memory to be used for the new matches single capture value
                while (context.size() > 1) {
                    excessTokens.add(context.remove(0));
                }
                if (activeToken.getActiveToken(true) instanceof GrammarExpression) {
                    activeGroup.getTokens().add(new TokenGroup(excessTokens));
                } else {
                    activeGroup.getTokens().addAll(excessTokens);
                }
            } else if (activeToken instanceof GrammarExpression) {
                //If no grammar group with unique ID, then add all other tokens to active group on stack element
                if (topmost.getTokenGroups().isEmpty()) topmost.getTokenGroups().add(new TokenGroup());
                Token<?> group = topmost.getActiveGroup();
                if (Objects.isNull(group)) {
                    if (topmost.getTokenGroups().size() < topmost.getCurrentGroup()) {
                        topmost.getTokenGroups().add(new TokenGroup());
                    }
                    group = topmost.getTokenGroups().get(topmost.getCurrentGroup().intValue() - 1);
                }
                if (!(group instanceof TokenGroup))
                    throw new LexerException(String.format("Unexpected token '%s' found when expecting token group",
                            group.getClass().getSimpleName()));
                TokenGroup tokenGroup = (TokenGroup)group;
                Token<?> valueCapture = context.remove(context.size() - 1);
                tokenGroup.getTokens().addAll(context);
                context.clear();
                context.add(valueCapture);
            }
        }
    }

    /**
     * If the found new match has a single capture token it is expecting and either the short or long term memory is
     * not empty, add the single token to the new match in a group relevant to the capture token. If there are other
     * tokens found either in short / long term memory and the current active group is ahead of the number of active
     * token groups then we can add or update the group accordingly.
     * @param match The new token match found based on the current token being processed
     */
    private void handleSingleTokenInitializer(Token<?> match) {
        Token<?> activeToken = match.getActiveToken(true);
        List<Token<?>> source = context.isEmpty() ? evaluated : context;
        if (activeToken instanceof GrammarValue && ((GrammarValue) activeToken).isCapture()) {
            if (!source.isEmpty()) {
                Token<?> current = match.getPatternTokens().get(match.getTokenPosition());
                if (current instanceof GrammarGroup) {
                    String groupId = ((GrammarGroup) current).getGroupId();
                    TokenGroup activeGroup = match.findTokenGroup(groupId);
                    if (!stack.isEmpty() && Objects.nonNull(lastWriteGroup) && !lastWriteGroup.getTokens().isEmpty()
                            && context.isEmpty()) {
                        Token<?> hangingToken = lastWriteGroup.getTokens().remove(lastWriteGroup.getTokens().size() - 1);
                        TokenGroup currentGroup = match.getTokenGroups().get(match.getTokenPosition());
                        currentGroup.getTokens().add(hangingToken);
                        Token<?> group = match.getActiveGroup();
                        if (group instanceof GrammarGroup) {
                            GrammarGroup grammarGroup = (GrammarGroup) group;
                            grammarGroup.setTokenPosition(grammarGroup.getTokenPosition() + 1);
                        }
                    } else {
                        //Add single token to child tokens of the new matches active group
                        activeGroup.getTokens().add(0, source.remove(source.size() - 1));
                        if (!stack.isEmpty()) {
                            if (stack.peek().getCurrentGroup().intValue() - 1 >= stack.peek().getTokenGroups().size()) {
                                /* If the number of token groups on the token is less than the current group then we can add
                                 * the remaining tokens in the short / long term memory into a new group */
                                stack.peek().getTokenGroups().add(new TokenGroup(source));
                                source.clear();
                            }
                        }
                    }
                } else if (!stack.isEmpty() && context.isEmpty() && stack.peek().isReset(stack.peek())) {
                    TokenGroup currentGroup = match.getTokenGroups().get(match.getTokenPosition());
                    currentGroup.getTokens().add(stack.pop());
                    match.setTokenPosition(match.getTokenPosition() + 1);
                } else if (!source.isEmpty()) {
                    if (match.getTokenGroups().size() <= match.getTokenPosition()) {
                        match.getTokenGroups().add(new TokenGroup());
                    }
                    TokenGroup currentGroup = match.getTokenGroups().get(match.getTokenPosition());
                    //Clear group of any existing as it is a single grammar value and add
                    currentGroup.getTokens().clear();
                    currentGroup.getTokens().add(source.remove(source.size() - 1));
                    //Increment token position so it points at the new group
                    match.setTokenPosition(match.getTokenPosition() + 1);
                }
            } else {
                /* Fixes the issue encountered with the following `myVar = {1->1,2->2,3->3}.remove(2)`. The match is
                 * expecting a value but none can be found in the long / short term memory. As such we need to look at
                 * the last group written for the token */
                if (Objects.nonNull(lastWriteGroup)) {
                    Token<?> lastWritten = lastWriteGroup.getTokens().remove(lastWriteGroup.getTokens().size() - 1);
                    match.getActiveGroup();
                    Token<?> current = match.getPatternTokens().get(match.getTokenPosition());
                    if (current instanceof GrammarGroup) {
                        String groupId = ((GrammarGroup) current).getGroupId();
                        TokenGroup activeGroup = match.findTokenGroup(groupId);
                        //Add single token to child tokens of the new matches active group
                        activeGroup.getTokens().add(lastWritten);
                    }
                    lastWriteGroup = null;
                } else {
                    throw new ParserException("Unable to initialize new match as last write group is null");
                }
            }
        }
    }

    /**
     * If the active token on the new match is an expression (expecting multiple tokens), this means that all tokens
     * either on the short-term or alternatively long term memory need to be added to the current match. This occurs
     * because if a grammar expression is the first or last token in the pattern, it means it is greedy and without
     * a wrapper boundary from another token will consume all other tokens.
     * @param match The new token match found based on the current token being processed
     */
    private void handleMultiTokenInitializer(Token<?> match) {
        Token<?> activeToken = match.getActiveToken(true);
        List<Token<?>> source = context.isEmpty() && stack.isEmpty() ? evaluated : context;
        if (!match.getTokenGroups().isEmpty() && !match.getTokenGroups().get(0).getFlatTokens().isEmpty()) return;
        if (activeToken instanceof GrammarExpression) {
            if (!source.isEmpty()) {
                //Handle scenario caused by variable assignment being hoovered up in greedy token
                if (!match.isExpression() && match.isAssignment(source)) {
                    source = source.subList(2, source.size());
                }
                source = cutToExpression(source, false);
                if (!stack.isEmpty()) {
                    Token<?> suspectGroup = stack.peek().getActiveToken(true, true);
                    if (suspectGroup instanceof GrammarGroup && Objects.nonNull(((GrammarGroup) suspectGroup).getGroupId())) {
                        TokenGroup found = stack.peek().findTokenGroup(((GrammarGroup) suspectGroup).getGroupId());
                        if (Objects.nonNull(found)) {
                            List<Token<?>> postTokens = cutToExpression(found.getFlatTokens(), false);
                            List<Token<?>> preTokens = cutToExpression(found.getFlatTokens(), true);
                            found.getTokens().clear();
                            found.getTokens().addAll(preTokens);
                            postTokens.addAll(source);
                            source.clear();
                            source = postTokens;
                        }
                    }
                }
                match.getTokenGroups().add(new TokenGroup(source));
                source.clear();
            } else if (!stack.isEmpty()) {
                /* If the token is expecting content, but none can be found in short / long term memory. Check to see
                 * if the top of the stack is expecting a token of this type if a grammar reference. If so then just
                 * add a blank group. Alternatively consume the top of the stack and continue processing. */
                GrammarToken<?> grammarToken = (GrammarToken<?>) stack.peek().getActiveToken(true, false);
                if (Objects.nonNull(grammarToken) && grammarToken instanceof GrammarReference &&
                        ((GrammarReference)grammarToken).getValue().contains(match.getName())) {
                    match.getTokenGroups().add(new TokenGroup());
                } else {
                    match.getTokenGroups().add(new TokenGroup(Collections.singletonList(stack.pop())));
                }
            }
        }
    }

    /**
     * Either return the source list up to the point of an expression or everything following it. This is useful for
     * token ingestion by a match to know where to stop if greedy.
     * @param source The source list from which is being consumed (short / long term memory stores)
     * @param pre Determines whether to take everything up the expression or after
     * @return The modified list
     */
    private List<Token<?>> cutToExpression(List<Token<?>> source, boolean pre) {
        for (int i = source.size() - 1;i > -1;i--) {
            if (source.get(i).isExpression()) {
                return pre ? source.subList(0, i + 1) : source.subList(i + 1, source.size());
            }
        }
        return pre ? new ArrayList<>() : source;
    }

    /**
     * Handles scenarios where actions need to be taken if the new match is an expression and how that may be affected
     * by the current state of the stack and context.
     * @param match The match which if to be handled by this method will be an expression
     * @return Returns false if the current match needs to be aborted due. This is likely to the top-stack token
     * containing the exact token found in the match and causing a match to be incorrectly identified
     */
    private boolean handleExpressionScenario(Token<?> match) {
        if (!stack.isEmpty() && !stack.peek().isComplete() && match.isExpression() &&
            stack.peek().getActiveToken(true) instanceof GrammarReference && stack.peek().isExpression()) {
            Token<?> suspectGroup = stack.peek().getActiveGroup();
            TokenGroup group = null;
            if (suspectGroup instanceof GrammarGroup) {
                String groupId = ((GrammarGroup)suspectGroup).getGroupId();
                group = stack.peek().findTokenGroup(groupId);
            }
            if (Objects.isNull(group))
                stack.peek().getTokenGroups().get(Math.max(0, (int)(stack.peek().getCurrentGroup() - 1)));
            List<Token<?>> flatTokens = group.getFlatTokens();
            if (flatTokens.stream().anyMatch(Token::isExpression)) {
                List<Token<?>> nonExpression = new ArrayList<>();
                for (int i = flatTokens.size() - 1;i > -1;i--) {
                    if (flatTokens.get(i).isExpression()) break;
                    nonExpression.add(flatTokens.remove(i));
                }
                if (nonExpression.isEmpty()) return true;
                Collections.reverse(nonExpression);
                TokenGroup newGroup = new TokenGroup(nonExpression);
                if (!context.isEmpty()) newGroup.getTokens().addAll(context);
                group.getTokens().clear();
                group.getTokens().addAll(flatTokens);
                match.getTokenGroups().add(newGroup);
            } else {
                TokenGroup newGroup = new TokenGroup(group.getFlatTokens());
                if (!context.isEmpty()) newGroup.getTokens().addAll(context);
                match.getTokenGroups().add(newGroup);
                context.clear();
                group.getTokens().clear();
            }
        } else if (!stack.isEmpty() && !stack.peek().isComplete() && !context.isEmpty() && match.isExpression() &&
            !(stack.peek().getActiveToken(true) instanceof GrammarReference)) {
            if (stack.peek().getTokenGroups().size() < stack.peek().getCurrentGroup()) {
                while (stack.peek().getTokenGroups().size() < stack.peek().getCurrentGroup()) {
                    stack.peek().getTokenGroups().add(new TokenGroup());
                }
            }
            if (stack.peek().getTokenGroups().isEmpty()) {
                stack.peek().getTokenGroups().add(new TokenGroup());
            }
            TokenGroup group = stack.peek().getTokenGroups().get((int) (Math.max(0, stack.peek().getCurrentGroup() - 1)));
            group.getTokens().addAll(context);
            //If last token not complete then leave it on the stack as something weird has happened
            context.clear();
            if ((stack.peek().isReset(stack.peek()) || isTechnicallyComplete(stack.peek())) && stack.size() > 1 &&
                    !stack.get(stack.size() - 2).isExpression()) {
                Token<?> topmost = stack.pop();
                Token<?> susGroup = stack.peek().getActiveToken(true, true);
                topmost.clean();
                if (stack.peek().getCurrentGroup().intValue() > stack.peek().getTokenGroups().size()) {
                    group = new TokenGroup();
                    stack.peek().getTokenGroups().add(group);
                } else {
                    group = stack.peek().getTokenGroups().get((int) (Math.max(0, stack.peek().getCurrentGroup() - 1)));
                }
                group.getTokens().add(topmost);
            } else if (!stack.peek().isComplete() && !isTechnicallyComplete(stack.peek()) && !stack.peek().isReset(stack.peek()))
                return false;
            context.addAll(evaluated.stream().filter(t -> !t.isExpression() && t.getTokenGroups().stream()
                    .noneMatch(tg -> tg.getFlatTokens().stream().anyMatch(Token::isExpression))).collect(Collectors.toList()));
            evaluated = evaluated.stream().filter(t -> t.isExpression() || t.getTokenGroups().stream()
                    .anyMatch(tg -> tg.getFlatTokens().stream().anyMatch(Token::isExpression))).collect(Collectors.toList());
            context.add(stack.pop());
        } else if (stack.isEmpty() && match.isExpression() && !evaluated.isEmpty()) {
            context.addAll(evaluated.stream().filter(t -> !t.isExpression() && t.getTokenGroups().stream()
                    .noneMatch(tg -> tg.getFlatTokens().stream().anyMatch(Token::isExpression))).collect(Collectors.toList()));
            evaluated = evaluated.stream().filter(t -> t.isExpression() || t.getTokenGroups().stream()
                    .anyMatch(tg -> tg.getFlatTokens().stream().anyMatch(Token::isExpression))).collect(Collectors.toList());
        }
        return true;
    }

    /**
     * Determines whether the current token is complete for all intents and purposes. This resolves the issue where
     * a token may be remaining on the stack because it is stuck on an expression. In this case it checks for a
     * tokens greedy nature and return true if this is to be the case. Other checks are made in addition to this to
     * verify the best course of action to take.
     * @param token The token to check to determine whether it is technically complete even if it is not flagged as
     *              such using the isComplete method.
     * @return Returns true if the token is technically complete
     */
    private boolean isTechnicallyComplete(Token<?> token) {
        return token.getTokenPosition() == token.getPatternTokens().size() - 1 &&
                token.getPatternTokens().get(token.getTokenPosition()) instanceof GrammarExpression;
    }

    /**
     * If a token hasn't triggered a match either within the stack or in a new match, this method performs a series of
     * checks to ensure it is either not a valid token further up the stack or a valid token for the current stack head
     * @param lastToken The token last read and processed from the expression String
     * @param stackSize The current size of the stack at the time this function was called
     */
    private void handleNoMatch(Token<?> lastToken, int stackSize) {
        checkTopStackItem(lastToken);
        if (tokenExistsUpStack(lastToken)) {
            //If the current token exists up the stack as a valid progression token then handle
            handleUpStackAction(lastToken, stackSize);
        } else if (stackSupportsItemSingleValue()) {
            //Handle where current head of the stack has a valid opening for a single token
            Token<?> root = stack.pop();
            root.clean();
            if (root.getTokenGroups().isEmpty()) root.getTokenGroups().add(new TokenGroup());
            TokenGroup suspected = root.getTokenGroups().get(root.getCurrentGroup().intValue() - 1);
            suspected.getTokens().addAll(Collections.singletonList(new TokenGroup(context)));
            context.clear();
            if (stack.isEmpty()) {
                evaluated.addAll(Arrays.asList(root, lastToken));
            } else {
                context.addAll(Arrays.asList(root, lastToken));
            }
        } else if (!stack.isEmpty() && stack.peek().isReset(stack.peek()) && lastToken instanceof LogicOperator) {
            //We should be inside an expression within another token, so
            if (stack.size() > 1) {
                handleOperatorEvent(lastToken);
            } else {
                throw new LexerException(String.format("Unexpected logic operator (%s) found with single token on stack (%s)",
                        lastToken.getValue(), stack.peek().getClass().getSimpleName()));
            }
        } else if (!stack.isEmpty() && stack.peek().isReset(stack.peek()) && context.isEmpty() &&
                lastToken instanceof OperatorToken) {
            //Handling case where stack token is overly greedy when concerning logic tokens
            if (stack.size() > 1) {
                handleOperatorEvent(lastToken);
            } else {
                Token<?> topmost = stack.pop();
                List<Token<?>> source = context.isEmpty() ? evaluated : context;
                source.add(topmost);
                source.add(lastToken);
            }
        } else {
            //Finally add to either short or long term dependent on whether there are any active stack tokens
            if (!evaluated.isEmpty() && stack.isEmpty()) {
                evaluated.add(lastToken);
            } else {
                context.add(lastToken);
            }
        }
    }

    /**
     * If the current top-stack item remains but is stuck on an optional group expecting a fixed syntax grammar token,
     * check that value and if there's no match with the current token, increment the token position and complete if
     * necessary. This avoids tokens unnecessarily remaining on the stack.
     * @param lastToken The token currently being processed
     */
    private void checkTopStackItem(Token<?> lastToken) {
        if (stack.isEmpty()) return;
        //Check and skip if the current grammar token is a static syntax value, is optional and is not met
        GrammarToken<?> active = ((GrammarToken<?>)stack.peek().getActiveToken(true, false, true));
        Token<?> suspect = stack.peek().getActiveToken(true, true, true);
        GrammarGroup group = null;
        if (suspect instanceof GrammarGroup) group = (GrammarGroup) suspect;
        if (Objects.nonNull(active) && active instanceof GrammarValue &&
                !Objects.equals(active.getValue(), lastToken.getValue()) && (active.isOptional() ||
                Objects.nonNull(group) && group.isOptional() && !group.isMultiple())) {
            stack.peek().setTokenPosition(stack.peek().getTokenPosition() + 1);
        }
        if (stack.peek().isComplete())
            handleMatchComplete();
    }

    /**
     * If the token is a logic or operator token and a stack item remains in a reset state, we need to add both that
     * and the top-stack item either to context or to the parent stack item.
     * @param lastToken The token currently being processed
     */
    private void handleOperatorEvent(Token<?> lastToken) {
        Token<?> topmost = stack.pop();
        Token<?> activeToken = stack.peek();
        Token<?> patternGroup = activeToken.getActiveToken(true, true);
        if (patternGroup instanceof GrammarGroup) {
            context.addAll(Arrays.asList(topmost, lastToken));
        } else {
            long groupCount = activeToken.getCurrentGroup();
            if (activeToken.getTokenGroups().size() < groupCount) {
                activeToken.getTokenGroups().add(new TokenGroup(Arrays.asList(topmost, lastToken)));
            } else {
                activeToken.getTokenGroups().get((int)groupCount -1).getTokens()
                        .addAll(Arrays.asList(topmost, lastToken));
            }
        }
    }

    /**
     * Determines if the current token is the next point of progression further up the stack. This usually follows
     * an unmatched token using the initial matching method.
     * @param breakOnToken Avoids scanning any further up the stack if the same token class found to avoid clashes
     * @param lastToken The token last read and processed from the expression String
     * @return Returns true if the token is a logical fit further up the stack
     */
    private boolean tokenExistsUpStack(Optional<Token<?>> breakOnToken, Token<?> lastToken) {
        Stack<Token<?>> stackItems = new Stack<>();
        if (stack.size() <= 1) return false;
        try {
            while (!stack.isEmpty()) {
                if (stack.peek().getTokenPosition() == stack.peek().getLastNonOptionalPosition() &&
                        !stack.peek().isNextToken(lastToken)) {
                    /* If the next item up the stack is capturing tokens and the last group, then we know that it
                     * will not provide a match. As such, add to a temporary stack and remove from the check */
                    stackItems.push(stack.pop());
                    continue;
                } else if (stack.peek().isNextToken(lastToken)) {
                    //If the current head of the stack's next logical token is the current token then return true
                    if (breakOnToken.isPresent() && breakOnToken.get().getClass().equals(stack.peek().getClass())) {
                        break;
                    }
                    return true;
                }
                break;
            }
        } finally {
            /* Once we've found the answer, return the stack to its previous state by re-adding the tokens from the
             * temporary stack. */
            while (!stackItems.isEmpty()) {
                stack.push(stackItems.pop());
            }
        }
        return false;
    }

    /**
     * Determines if the current token is the next point of progression further up the stack. This usually follows
     * an unmatched token using the initial matching method.
     * @param lastToken The token last read and processed from the expression String
     * @return Returns true if the token is a logical fit further up the stack
     */
    private boolean tokenExistsUpStack(Token<?> lastToken) {
        return tokenExistsUpStack(Optional.empty(), lastToken);
    }

    /**
     * If the token has previously been found to be the next logical token up the stack (determined by
     * tokenExistsUpStack), actions such as completing and adding the stack head to the next token are
     * performed.
     * @param lastToken The token which caused the up-stack token to be identified
     * @param stackSize The current stack size at the time of calling
     */
    private void handleUpStackAction(Token<?> lastToken, int stackSize) {
        Token<?> topmost = stack.pop();
        //If tokens exist in the short term memory, add them to the recently removed stack token
        if (!context.isEmpty()) {
            if (!(context.get(0) instanceof OperatorToken &&
                    topmost.getActiveToken(true) instanceof GrammarValue)) {
                int groupIndex = topmost.getCurrentGroup().intValue();
                if (topmost.getTokenGroups().size() < groupIndex) {
                    topmost.getTokenGroups().add(new TokenGroup(context));
                } else {
                    topmost.getTokenGroups().get(groupIndex - 1).getTokens().addAll(context);
                }
                context.clear();
            }
        }
        List<Token<?>> orphanTokens = scanForIncompleteGroups(topmost);
        topmost.clean();
        Token<?> activeToken = stack.peek();
        orphanTokens.add(0, topmost);
        orphanTokens.addAll(context);
        //Add the removed stack item and add it to either the active or new group on the new stack head
        int groupIndex = activeToken.getCurrentGroup().intValue();
        if (activeToken.getTokenGroups().size() < groupIndex) {
            activeToken.getTokenGroups().add(new TokenGroup(orphanTokens));
        } else {
            Token<?> patternToken = activeToken.getActiveToken(true, true);
            if (patternToken instanceof GrammarGroup) {
                String groupId = ((GrammarGroup)patternToken).getGroupId();
                TokenGroup tokenGroup = activeToken.findTokenGroup(groupId);
                checkForAndAddToExpression(orphanTokens, tokenGroup,
                        activeToken.getActiveToken(true) instanceof GrammarExpression);
            } else {
                if (activeToken.getTokenGroups().isEmpty()) activeToken.getTokenGroups().add(new TokenGroup());
                checkForAndAddToExpression(orphanTokens, activeToken.getTokenGroups().get(Math.max(0, groupIndex - 1)),
                        activeToken.getActiveToken(true) instanceof GrammarExpression);
            }
        }
        context.clear();
        /* If the stack size has altered since this method was called, a recursive call to analysePatterns is
         * performed to ensure recent changes haven't completed another token which itself needs to be actioned. */
        if (!topmost.isNextToken(lastToken)) {
            context.add(lastToken);
        }
        if (stack.size() != stackSize && !context.isEmpty()) {
            analysePatterns(stack.size());
        }
    }

    /**
     * If a completed statement is added to another token group which has previous tokens and that initial pattern
     * token is greedy, those existing tokens will need to be added to ensure scope validity. This method checks
     * for this scenario and inserts the existing tokens into the first group of the completed statement. Once complete
     * this is then added to back to the token group.
     * @param topmost The completed statement whose first pattern token is greedy being added to an existing token group
     *                which has existing tokens
     * @param tokenGroup The token group to which the completed statement is being added
     */
    private void checkForAndAddToExpression(List<Token<?>> topmost, TokenGroup tokenGroup, boolean expression) {
        if (!topmost.get(0).getPatternTokens().isEmpty() && topmost.get(0).getPatternTokens().get(0) instanceof GrammarExpression &&
                !tokenGroup.getTokens().isEmpty() && !(tokenGroup.getTokens().get(0) instanceof TokenGroup)) {
            TokenGroup firstGroup = topmost.get(0).getTokenGroups().get(0);
            List<Token<?>> existing = new ArrayList<>(firstGroup.getTokens());
            firstGroup.getTokens().clear();
            firstGroup.getTokens().addAll(tokenGroup.getTokens());
            tokenGroup.getTokens().clear();
            firstGroup.getTokens().addAll(existing);
            tokenGroup.getTokens().addAll(topmost);
        } else {
            //If the statement is not left-side greedy then simply add the token to the group
            boolean addedToGroup = false;
            if (!tokenGroup.getTokens().isEmpty()) {
                Token<?> lastToken = tokenGroup.getTokens().get(tokenGroup.getTokens().size() - 1);
                if (lastToken instanceof TokenGroup) {
                    if (expression) {
                        ((TokenGroup) lastToken).getTokens().add(new TokenGroup(topmost));
                    } else {
                        ((TokenGroup) lastToken).getTokens().addAll(topmost);
                    }
                    addedToGroup = true;
                }
            }
            if (!addedToGroup) tokenGroup.getTokens().add(new TokenGroup(topmost));
        }
    }

    /**
     * Determine whether the current stack head is expecting a single token and a single token is present in the
     * short term memory
     * @return Returns true if the stack head supports adding the short term memory token
     */
    private boolean stackSupportsItemSingleValue() {
        if (stack.isEmpty()) return false;
        Token<?> current = stack.peek().getActiveToken(true);
        return context.size() == 1 && !(context.get(0) instanceof OperatorHandler) &&
                !(context.get(0) instanceof LogicOperator) && !(context.get(0) instanceof NonToken) &&
                current instanceof GrammarValue && ((GrammarValue) current).isCapture();
    }

    /**
     * Copies the pattern tokens from the matched token handler and copies them to the newly created clone. This
     * ensures that the lexer can track it's progress and know when to complete as tokens are scanned / processed.
     * @param token The matched token handler for the last token
     * @param tokenGroup The token group to add the copied pattern tokens
     */
    private void initTokenGroups(Token<?> token, TokenGroup tokenGroup) {
        for (Token<?> patternToken : token.getPatternTokens()) {
            if (patternToken instanceof GrammarGroup) {
                TokenGroup idGroup = new TokenGroup(((GrammarGroup) patternToken).getGroupId(), new ArrayList<>());
                TokenGroup toCreate = new TokenGroup(Collections.singletonList(idGroup));
                if (Objects.isNull(tokenGroup)) {
                    token.getTokenGroups().add(toCreate);
                } else {
                    tokenGroup.getTokens().add(toCreate);
                }
                initTokenGroups(patternToken, idGroup);
            } else if (patternToken instanceof GrammarValue && ((GrammarValue)patternToken).isCapture()) {
                if (Objects.isNull(tokenGroup)) {
                    token.getTokenGroups().add(new TokenGroup());
                } else {
                    tokenGroup.getTokens().add(new TokenGroup());
                }
            }
        }
    }

    /**
     * If the current stack head is found to be complete, this method checks for and adds any remaining tokens and
     * adds the completed token to either the long term memory store or the parent token in the stack.
     */
    private void handleMatchComplete() {
        //Cleans the token of pattern tokens and ensures there are no empty token groups left
        stack.peek().clean();
        if (stack.size() > 1) {
            //If there is more than one token found on the stack then add the token to the next token up the stack
            Token<?> topmost = stack.pop();
            Long currentGroup = stack.peek().getCurrentGroup();
            //Determine whether token can be appended to existing group or added as a new
            if (currentGroup.intValue() != 0 && currentGroup == stack.peek().getTokenGroups().size()) {
                TokenGroup group = stack.peek().getTokenGroups().get(currentGroup.intValue() - 1);
                if (group.getTokens().isEmpty()) group.getTokens().add(new TokenGroup());
                Token<?> last = group.getTokens().get(group.getTokens().size() - 1);
                if (last instanceof TokenGroup) {
                    //If it's an expression, add all existing tokens in the group to it
                    if (topmost.isExpression()) {
                        List<Token<?>> current = ((TokenGroup) last).getTokens();
                        if (!current.isEmpty() && !current.get(current.size() - 1).isExpression()) {
                            TokenGroup mergeGroup = topmost.getTokenGroups().get(topmost.getTokenPosition() - 2);
                            for (int i = current.size() - 1; i > -1; i--) {
                                if (current.get(i).isExpression()) break;
                                mergeGroup.getTokens().add(0, current.remove(i));
                            }
                        }
                    }
                    Optional<TokenGroup> incompleteLast = hasIncompleteTokenGroup((TokenGroup)last);
                    if (incompleteLast.isPresent()) {
                        incompleteLast.get().getTokens().add(topmost);
                    } else {
                        ((TokenGroup) last).getTokens().add(topmost);
                    }
                    lastWriteGroup = (TokenGroup)last;
                } else {
                    group.getTokens().add(new TokenGroup(Collections.singletonList(topmost)));
                    lastWriteGroup = group;
                }
            } else {
                if (stack.peek().getActiveToken(true) instanceof GrammarExpression) {
                    context.add(topmost); //Add topmost to context so it can be added to last TokenGroup for expression
                    Token<?> activeToken = stack.peek();
                    Token<?> patternToken = activeToken.getPatternTokens().get(activeToken.getTokenPosition());
                    if (Objects.nonNull(activeToken.getActiveGroup())) {
                        activeToken.addToGroup(patternToken, activeToken.getActiveGroup(), context, activeToken, true);
                    } else {
                        int groupIndex = activeToken.getCurrentGroup().intValue();
                        if (activeToken.getTokenGroups().size() < groupIndex) {
                            TokenGroup newGroup = new TokenGroup(context);
                            activeToken.getTokenGroups().add(newGroup);
                            lastWriteGroup = newGroup;
                        }
                    }
                    context.clear();
                } else {
                    Token<?> activeToken = stack.peek().getActiveToken(true);
                    if (Objects.nonNull(activeToken) && activeToken instanceof GrammarReference) {
                        if (((GrammarReference)activeToken).getValue().contains(topmost.getName())) {
                            stack.peek().setTokenPosition(stack.peek().getTokenPosition() + 1);
                        }
                    }
                    boolean openEndedScenario = false;
                    if (stack.peek().getTokenPosition() < stack.peek().getPatternTokens().size()) {
                        GrammarToken<?> grammarToken = (GrammarToken<?>) stack.peek().getPatternTokens().get(stack.peek().getTokenPosition());
                        if (grammarToken.isOptional() && stack.peek().getTokenPosition() + 1 == stack.peek().getPatternTokens().size() - 1 &&
                                stack.peek().getPatternTokens().get(stack.peek().getTokenPosition() + 1) instanceof GrammarExpression) {
                            //Open ended ending scenario
                            context.add(topmost);
                            openEndedScenario = true;
                        }
                    }
                    if (!openEndedScenario) {
                        stack.peek().getTokenGroups().add(new TokenGroup(Collections.singletonList(topmost)));
                    }
                }
            }
        } else {
            //If there are no items in the stack then add the completed token to the long term token store
            Token<?> token = stack.pop();
            evaluated.add(token);
        }
        //Handle upstream stack items now be complete if the current one has triggered that change
        while (stack.size() > 1 && checkUpstreamCompleted(stack.peek())) {
            if (!stack.peek().isComplete() && stack.peek().getActiveToken(true, false) instanceof GrammarExpression) break;
            Token<?> topmost = stack.pop();
            Token<?> activeToken = stack.peek().getActiveToken(true, true);
            if (activeToken instanceof GrammarGroup) {
                GrammarGroup found = (GrammarGroup) activeToken;
                TokenGroup group = stack.peek().findTokenGroup(found.getGroupId());
                group.getTokens().add(topmost);
            }
        }
        if (stack.size() == 1 && checkUpstreamCompleted(stack.peek())) {
            if (stack.peek().isComplete() || !(stack.peek().getActiveToken(true, false) instanceof GrammarExpression))
                evaluated.add(stack.pop());
        }
    }

    /**
     * Looks at the current group of the token and determines whether it is complete even if the current pattern
     * token is an expression (open-ended). It does this by looking at the previously written token in the group.
     * If it ends in an operator or is empty then we are fairly confident that the group is incomplete.
     * @param current The token group being scanned
     * @return Returns the incomplete token group from the passed froup
     */
    private Optional<TokenGroup> hasIncompleteTokenGroup(TokenGroup current) {
        if (current.getTokens().isEmpty()) return Optional.empty();
        Token<?> suspectGroup = current.getTokens().get(current.getTokens().size() - 1);
        if (suspectGroup instanceof TokenGroup) {
            List<Token<?>> flatGroupTokens = ((TokenGroup)suspectGroup).getFlatTokens();
            if (flatGroupTokens.isEmpty()) return Optional.empty();
            Token<?> last = flatGroupTokens.get(flatGroupTokens.size() - 1);
            if (last instanceof LogicOperator || last instanceof OperatorToken) {
                return Optional.of((TokenGroup) suspectGroup);
            }
        }
        return Optional.empty();
    }

    /**
     * From handling the closure of the current stack token, the next one upstream might also now be complete. As
     * such we return true if this is found to be the case or false to break out.
     * @param current The current token to determine whether it and it's children are in a completed state
     * @return Return true if it is complete
     */
    private boolean checkUpstreamCompleted(Token<?> current) {
        if (current.getTokenPosition() > current.getPatternTokens().size() - 1) return true;
        if (current.getTokenPosition() == current.getPatternTokens().size() - 1) {
            return checkUpstreamCompleted(current.getPatternTokens().get(current.getTokenPosition()));
        }
        return false;
    }

    /**
     * As not all statements have a distinct closing syntax token, the current state of the stack and context
     * have to be checked in case tokens need to be assigned to a relevant group or stack items need to be
     * cleared. The end result of this is that all tokens should exist in long term memory and that the short
     * term memory and stack should be clear.
     */
    private void finalizeExpression() {
        if (!stack.isEmpty()) {
            Token<?> topmost = stack.pop();
            List<Token<?>> orphanTokens = scanForIncompleteGroups(topmost);
            /* Check to see if there are any tokens left in the short term memory. If so check for the active
             * token on the stack and add to the relevant group */
            if (!context.isEmpty()) {
                if (!(topmost.getActiveToken(true) instanceof GrammarValue) ||
                        context.size() == 1 && topmost.getActiveToken(true) instanceof GrammarValue &&
                                ((GrammarValue) topmost.getActiveToken(true)).isCapture()) {
                    int groupIndex = topmost.getCurrentGroup().intValue();
                    if (topmost.getTokenGroups().size() < groupIndex) {
                        topmost.getTokenGroups().add(new TokenGroup(context));
                    } else {
                        topmost.getTokenGroups().get(Math.max(0, groupIndex - 1)).getTokens().add(new TokenGroup(context));
                    }
                    context.clear();
                }
            }
            topmost.clean(true);
            /* If the stack is still not empty after removing the previous stack head, it may mean that the parent
             * token is also an open-ended statement (ends with grammar expr). As such, we need to add the last
             * stack item to the current stack head and make that the new reference to add to the long term memory */
            boolean appendedToParent = false;
            if (!stack.isEmpty()) {
                Token<?> parent = stack.pop();
                TokenGroup found = null;
                if (!parent.getTokenGroups().isEmpty()) {
                    Token<?> susGroup = parent.getActiveToken(true, true);
                    if (Objects.nonNull(susGroup) && susGroup instanceof GrammarGroup &&
                            !((GrammarGroup)susGroup).getGroupId().isEmpty()) {
                        found = parent.findTokenGroup(((GrammarGroup)susGroup).getGroupId());
                    } else {
                        int groupIndex = parent.getCurrentGroup().intValue();
                        if (parent.getTokenGroups().size() >= groupIndex) {
                            found = parent.getTokenGroups().get(groupIndex - 1);
                        }
                    }
                }
                orphanTokens.add(0, topmost);
                if (Objects.isNull(found)) {
                    parent.getTokenGroups().add(new TokenGroup(orphanTokens));
                } else {
                    found.getTokens().addAll(orphanTokens);
                }
                topmost = parent;
                appendedToParent = true;
            }
            evaluated.add(topmost);
            if (!appendedToParent) {
                evaluated.addAll(orphanTokens);
            }
        } else {
            evaluated.addAll(context);
            evaluated.forEach(t -> t.clean(true));
        }
    }

    /**
     * Scan for tokens added to top-most stack item but may not belong there. This can be detected by looking
     * at the currently active token and see if there are tokens. If all grammar tokens are required but the
     * group has not been closed and not a new one opened then we know they do not belong. We therefore extract
     * them and add them after the stack item being completed. An example where this would happen is in the
     * following statement: repeat(i++,0,<2) result = {?i}; + [2,3]. This is because the repeatable group in
     * the repeat statement is waiting for tokens and as it keeps waiting and then the statement ends, it
     * assumes (incorrectly) that the tokens belong in there. However, since it has not matched the second
     * required token
     * @param topmost
     * @return
     */
    private List<Token<?>> scanForIncompleteGroups(Token<?> topmost) {
        List<Token<?>> incompleteTokens = new ArrayList<>();
        GrammarGroup grammarGroup = (GrammarGroup)topmost.getActiveGroup();
        if (Objects.nonNull(grammarGroup)) {
            TokenGroup active = topmost.findTokenGroup(grammarGroup.getGroupId());
            boolean match = false;
            //Scan for any other tokens in the grammar group which are ahead of the token position and not optional
            for (int i = active.getTokenPosition() + 1;i < grammarGroup.getPatternTokens().size();i++) {
                if (!((GrammarToken<?>)grammarGroup.getPatternTokens().get(i)).isOptional()) {
                    match = true;
                    break;
                }
            }
            //If tokens are present in a repeatable group and a required token is missing then extract tokens
            if (match && !active.getFlatTokens().isEmpty() && grammarGroup.isRepeatable()) {
                incompleteTokens = new ArrayList<>(active.getTokens());
                active.getTokens().clear();
            }
        }
        return incompleteTokens;
    }

    /**
     * If a token has a restriction where it must be the child of another specific class, then these restrictions
     * can be provided in the parentRestrictions list on the token itself. This is where these are checked and
     * errors are thrown if it's found that this requirement is not met.
     * @param match The current token match prior to being added on the stack
     */
    private void checkMatchRestrictions(Token<?> match) {
        if (match.getParentRestrictions().isEmpty()) return;
        String restrictions = match.getParentRestrictions().stream()
                .map(Class::getSimpleName)
                .collect(Collectors.joining(","));
        if (stack.isEmpty()) {
            throw new LexerException(String.format("Token '%s' is required to be a child of one of the following token " +
                    "types [%s]. Instead the stack was empty!", match.getClass().getSimpleName(), restrictions));
        }
        String result = checkParentsToRestrictionDepth(match);
        if (!result.isEmpty()) {
            throw new LexerException(String.format("%s is required to be a child of one of the following token " +
                            "types [%s]. Instead found following class(es) [%s] on the stack", match.getClass().getSimpleName(),
                    restrictions, result));
        }
    }

    /**
     * Checks the parent restrictions on the stack to a specific depth. This depth also declared on the token class
     * determines how deep it can be defined before an error is thrown.
     * @param match The current token match prior to being added on the stack
     * @return Returns a list of parent classes if the required type(s) were not found
     */
    private String checkParentsToRestrictionDepth(Token<?> match) {
        int count = 0;
        Stack<Token<?>> stackCopy = new Stack<>();
        stackCopy.addAll(stack);
        StringBuilder parents = new StringBuilder();
        while (count < match.checkParentDepth() && !stackCopy.isEmpty()) {
            Token<?> current = stackCopy.pop();
            if (!match.getParentRestrictions().contains(current.getClass()))
                parents.append(current.getClass().getSimpleName().concat(","));
            else {
                parents.setLength(0);
                break;
            }
        }
        return parents.length() > 1 ? parents.substring(0, parents.length() - 1) : "";
    }
}
