package dev.slop.lexer;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.enums.TokenType;
import dev.slop.model.Expression;
import dev.slop.model.LexerResult;
import dev.slop.tokens.Token;
import dev.slop.tokens.base.NonToken;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.operators.OperatorToken;

import java.util.*;
import java.util.stream.Collectors;

/**
 * The lexer class takes an expression String and converts it to a set of hierarchical tokens in a single pass.
 * The lexer has three distinct stores which are the context (short term memory), evaluated (long term memory)
 * and finally the stack which keeps track of embedded levels of tokens and statements. When a new token is detected
 * and inserted onto the stack, the preceding tokens which haven't already been resolved are moved from the context
 * and added to the current token. Likewise when either the last pattern token is detected or a matching token is
 * found further up the stack, the context tokens are added, the token closed (removed from stack) and added to the
 * next stack item.
 * @param <T> The type of object to which the Lexer represents. By default for SLOP this will be the Token class.
 */
public abstract class Lexer<T> {

    List<T> context;
    List<T> evaluated;
    Stack<T> stack;
    final SLOPConfig config;

    Lexer(SLOPConfig config) {
        this.config = config;
        initResources();
    }

    /**
     * Takes an expression and converts it to a hierarchical list of tokens. This method firstly initializes the
     * context, evaluated and stack in preparation for the scan before deferring responsibility to the concrete Lexer
     * class for tokenization.
     * @param expression The expression String to be executed
     * @return Returns a result object used to contain the resulting tokens
     */
    public LexerResult<Token<?>> tokenize(String expression) {
        initResources();
        return tokenize(new Expression(expression));
    }

    protected abstract LexerResult<Token<?>> tokenize(Expression expression);

    /**
     * Attempts to find a matching token using a regular expression match. Grammar matches are handled later in the
     * tokenization process after the token has been added to the context.
     * @param tokenHandlers A list of all token handlers
     * @param expression The Expression containing the String value being read
     * @param context The current state of the short term memory
     * @return Returns an optional matching Token
     */
    protected Optional<Token<?>> findMatch(Set<Token<?>> tokenHandlers, Expression expression, List<Token<?>> context,
                                           FallbackMatch fallback) {
        //Attempt to find a match with the defined token handlers in the config class
        Optional<Token<?>> result = findHandlerMatch(tokenHandlers, expression, false);
        if (!result.isPresent()) {
            //If no match has been found, attempt to match against an operator
            result = findHandlerMatch(Collections.singleton(config.getOperatorToken()), expression, false);
        }
        //If all else fails, match the value against one of the base token types so it can be handled correctly
        result = !result.isPresent() ? findHandlerMatch(config.getBaseHandlers(), expression, false) : result;
        if (result.isPresent() && result.get() instanceof OperatorToken) {
            //Check for follow-up operator token which has not triggered an OperatorToken match
            Optional<Token<?>> otherMatch = findHandlerMatch(Collections.singleton(config.getOperatorToken()),
                    expression, true);
            if (otherMatch.isPresent() && !otherMatch.get().getValue(String.class).equalsIgnoreCase("-")) {
                //We must do another match in order to remove the confirmed match from the expression (dryRun = false)
                findHandlerMatch(Collections.singleton(config.getOperatorToken()), expression, false);
                /* This is a limitation of what we can look for. This is because the next token may represent a negative
                 * number and therefore we can't count this toward a custom operator value */
                if (otherMatch.get().getValue(String.class).equalsIgnoreCase("-")) return result;
                return Optional.of(new TokenValue(result.get().getValue(String.class).concat(
                        otherMatch.get().getValue(String.class))));
            }
        }
        return result.map(token -> multiTokenMatch(token, tokenHandlers, expression, context, fallback));
    }

    /**
     * This is not ideal, but this provides support for up to 2 character NonToken chains to be
     * aggregated together. This is because the initial scan is only for those using regular
     * expression pattern type. Since some of these character chains may be present in grammar,
     * this checks the next character using a dry-run scan against the grammar handlers to see
     * if there is a match and return the aggregated version instead. An alternative to this would
     * be to provide a way for the Token classes to match against particular character chains. This
     * can be detected during the grammar lexing phase and added to a list so that if it is found
     * as a match, it is returned as a new TokenValue result.
     * @param token The token which previously had matches and is to be checked
     * @param tokenHandlers The list of token handlers to check
     * @param expression The full expression to look for the next token value
     * @param context The context which is used to identify the iterated token handlers next token
     * @return Returns either an aggregated matched TokenValue of 2 characters, or the original
     * token value is no grammar match was found.
     */
    private Token<?> multiTokenMatch(Token<?> token, Set<Token<?>> tokenHandlers, Expression expression,
                                     List<Token<?>> context, FallbackMatch fallback) {
        if (token instanceof NonToken || token instanceof OperatorToken) {
            Optional<Token<?>> next = findHandlerMatch(config.getBaseHandlers(), expression, true);
            if (next.isPresent() && next.get() instanceof NonToken) {
                NonToken testToken = new NonToken(String.format("%s%s", token.getValue(),
                        ((NonToken)next.get()).getValue()));
                if (tokenHandlers.stream()
                        .filter(th -> th.getTokenType() == TokenType.PRIMARY)
                        .filter(th -> th.getPatternType() == PatternType.GRAMMAR)
                        .filter(th -> !Objects.isNull(th.getPatternTokens()))
                        .anyMatch(tl -> tl.firstNonCaptureToken(testToken, context.size(), true))) {
                    findHandlerMatch(config.getBaseHandlers(), expression, false);
                    return testToken;
                } else {
                    if (fallback.get(testToken)) {
                        findHandlerMatch(config.getBaseHandlers(), expression, false);
                        return testToken;
                    }
                }
            }
        }
        return token;
    }

    /**
     * Uses the passed set of token handlers to find a match in the current expression String
     * @param tokenHandlers The list of token handlers to scan
     * @param expression The Expression String class
     * @return Returns the first token handler which matches its regex pattern against the expression
     */
    @SuppressWarnings("unchecked")
    private Optional<Token<?>> findHandlerMatch(Set<Token<?>> tokenHandlers, Expression expression, boolean dryRun) {
        Optional<? extends Token<?>> result = tokenHandlers.parallelStream()
                .filter(handler -> handler.getPatternType() == PatternType.REGEX)
                .map(handler -> handler.match(expression, dryRun))
                .filter(Optional::isPresent)
                .map(Optional::get)
                //TODO - Collect and throw exception is multiple found. Should not have conflicting regex token handlers
                .findFirst();
        return (Optional<Token<?>>) result;
    }

    protected void initResources() {
        context = new ArrayList<>();
        evaluated = new ArrayList<>();
        stack = new Stack<>();
    }
}
