package dev.slop.lexer;

import dev.slop.tokens.base.NonToken;

/**
 * This is used in the lexer if no match has been found against the current token. In the case of the SLOP lexer
 * as a last ditch effort, it will append two adjacent tokens together and use this to look on the stack for a
 * match. If found, then it will return the NonToken containing both instead of just the single token. This allows
 * SLOP to match against pairs of characters instead of the usual single or regex groups.
 */
@FunctionalInterface
public interface FallbackMatch {
    boolean get(NonToken suspect);
}
