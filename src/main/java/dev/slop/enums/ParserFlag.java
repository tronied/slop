package dev.slop.enums;

/**
 * These flags let the Parser and associated tokens know that a specific event has occurred.
 * Some of these may be acted upon by the Parser itself (such as RETURN to abort token processing)
 * or by the token such as BREAK or CONTINUE where the token will want to know to abort or skip
 * to the next iteration. Each token can set a parser flag, though typically they will be used
 * with one representing their respective flag.
 */
public enum ParserFlag {
    BREAK,
    CONTINUE,
    RETURN,
    RETURN_GROUP,
    EXIT
}
