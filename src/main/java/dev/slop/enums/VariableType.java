package dev.slop.enums;

/**
 * Variable types determine how a variable should be handled by the context and scope. DEFAULT
 * is the normal state for most variables and supports maintaining state through recursion. SELF
 * is for those tokens which are stored on the stack but will be executed at a later stage and
 * not invoked directly. This prevents them from maintaining multiple levels of depth. PARAMETER
 * is a type which gets automatic recursion support, but may be removed as DEFAULT should now
 * cover this use case.
 */
public enum VariableType {
    DEFAULT,
    PARAMETER,
    PROTOTYPE,
    INSTANCE,
    SINGLETON
}
