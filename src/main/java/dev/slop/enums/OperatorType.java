package dev.slop.enums;

/**
 * An enum type containing the standard operator types used in SLOP. These by themselves do not as a new operator but
 * are used in conjunction with the OperatorHandler and type operation classes defined in the config. The types are
 * then used to determine which operators are supported for a given Token / type.
 */
public enum OperatorType {
    ASSIGN,
    EQUALS,
    NOT_EQUALS,
    GT,
    LT,
    GTE,
    LTE,
    MOD,
    ADD,
    SUBTRACT,
    DIVIDE,
    MULTIPLY,
    INCREMENT,
    DECREMENT,
    INCREMENT_EQUALS,
    DECREMENT_EQUALS
}
