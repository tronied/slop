package dev.slop.enums;

/**
 * Tokens can be defined either by using a regular expression pattern (typically for simple literals) or a grammar
 * pattern. Each token defines these within it's Token class to determine how the Lexer will scan, create and
 * handle the given type during expression scanning.
 */
public enum PatternType {
    REGEX,
    GRAMMAR
}
