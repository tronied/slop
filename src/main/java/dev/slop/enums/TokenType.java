package dev.slop.enums;

/**
 * The default type for a token is PRIMARY, but a SECONDARY type can be used for those tokens which
 * are dependent on others. For example, when defining a loop you may use a Grammar Reference where
 * there are two possible options for the loop portion (for / for-each). The SECONDARY tokens would
 * be the implementation for each of these variants.
 */
public enum TokenType {
    PRIMARY,
    SECONDARY
}
