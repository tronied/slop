package dev.slop.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * This class is used to hold the resulting tokens from the Lexer and used as an intermediary format between
 * that and the Parser.
 * @param <T> The type of the result (in SLOP this would be the Token class)
 */
@Data
@Builder
@NoArgsConstructor
public class LexerResult<T> implements Serializable {
    @JsonTypeInfo(use= JsonTypeInfo.Id.CLASS, property="@class")
    List<T> result;

    public LexerResult(List<T> result) {
        this.result = result;
    }
}
