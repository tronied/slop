package dev.slop.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * A class used to contain the String expression that was passed to the Processor and that gets evaluated by the Lexer
 */
@Data
@AllArgsConstructor
public class Expression {
    private String value;

    /**
     * A method that where once a match has been found, removed everything to the index of the end of that match.
     * This is because all token regular expressions match the start of the String. To therefore match the next token
     * it removes / cuts the processed portion in preparation for the next match cycle.
     * @param index The index of the end of the last match
     */
    public void cutTo(int index) {
        this.value = value.substring(index).trim();
    }
}
