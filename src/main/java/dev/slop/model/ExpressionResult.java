package dev.slop.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.slop.exception.ParserException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

/**
 * A wrapper class for the result output from the Parser. This is mainly due to the fact that the result can be of
 * many types and this class provides either direct access or the ability to cast to the expected class type.
 * @param <T> The type of the result (in SLOP this would be the Token class)
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExpressionResult<T> {

    private T result;

    /**
     * Returns the result from the Parser without any type of cast
     * @return The raw result
     */
    public T getValue() {
        return result;
    }

    /**
     * An overloaded method which accepts the expected class type. This will throw a ClassCastException if the
     * raw result does not match the provided target class.
     * @param targetClass The target class type to cast the result
     * @param <L> The generic type used in the target class
     * @return Returns the converted raw result into the target class type
     */
    public <L> L getValue(Class<L> targetClass) {
        return (L)targetClass.cast(result);
    }

    /**
     * Uses the Apache Jackson constructed JavaType to determine the class to map the result to
     * @param targetType The JavaType constructed using the TypeFactory
     * @return Returns a mapped instance of the result
     * @param <L> The mapped result in the type determined by the parameter
     */
    public <L> L getValue(JavaType targetType) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String json = objectMapper.writeValueAsString(result);
            return objectMapper.readValue(json, targetType);
        } catch (JsonProcessingException ex) {
            throw new ParserException(String.format("Could not cast to provided type: %s", ex.getMessage()));
        }
    }

    /**
     * If custom deserialization needs to occur then the object mapper with modules can be passed to this method.
     * Using the constructed Apache Jackson JavaType determines the class to map the result to.
     * @param objectMapper The custom object mapper to deserialize the result
     * @param targetType The JavaType constructed using the TypeFactory
     * @return Returns a mapped instance of the result
     * @param <L> The mapped result in the type determined by the parameter
     */
    public <L> L getValue(ObjectMapper objectMapper, JavaType targetType) {
        try {
            String json = objectMapper.writeValueAsString(result);
            return objectMapper.readValue(json, targetType);
        } catch (JsonProcessingException ex) {
            throw new ParserException(String.format("Could not cast to provided type: %s", ex.getMessage()));
        }
    }

    @Override
    public String toString() {
        return Objects.isNull(result) ? "null" : result.toString();
    }
}
