package dev.slop.tokens;

import dev.slop.callback.ReferenceCallback;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.parser.SLOPParser;

import java.util.List;

public interface TokenCallback {

    /**
     * Adds a method of processing a token with additional callback functionality. This is useful when
     * a token may defer processing to another token to undertake part of the execution of a statement.
     * @param parser The parser used to resolve child token expressions
     * @param context The context for resolving object references
     * @param config The configuration for resolution of property values
     * @param callback Allows a lambda function to be defined when calling this method
     * @return Returns the resulting tokens from executing this token
     */
    List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config,
                           ReferenceCallback callback);
}
