package dev.slop.tokens.grammar;

import dev.slop.enums.PatternType;
import dev.slop.tokens.Token;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Grammar tokens are created from the Grammar Pattern and are used to form the pattern tokens. The Grammar
 * Expression is used to capture one or more tokens. For example, using the Grammar String '(' expr ')'
 * with the expression (1 + 2 * 3), the following tokens would be captured and added to the token group:
 *
 *      IntegerToken (1)
 *      OperatorToken (+)
 *      IntegerToken (2)
 *      OperatorToken (*)
 *      IntegerToken (3)
 *
 * The Non-Tokens specified in the Grammar String and is there to make up the syntax are not persisted unless
 * a special flag is passed.
 */
public class GrammarExpression extends GrammarToken<Void> {

    public GrammarExpression() {
        super("expr", null);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return null;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return null;
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return null;
    }

    /**
     * See {@link Token#isCaptureToken() isCaptureToken}
     */
    @Override
    public boolean isCaptureToken() {
        return true;
    }

    @Override
    public String toString() {
        return "expr";
    }

    @Override
    public boolean equals(Object o) {
        return !Objects.isNull(o) && o instanceof GrammarExpression;
    }
}
