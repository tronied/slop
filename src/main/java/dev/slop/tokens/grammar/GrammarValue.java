package dev.slop.tokens.grammar;

import dev.slop.enums.PatternType;
import dev.slop.enums.TokenType;
import dev.slop.tokens.Token;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Grammar tokens are created from the Grammar Pattern and are used to form the pattern tokens. Grammar Value tokens
 * are used to capture single values (as opposed to Expression which captures many). An example of where this would
 * be used would be if you were defining a structure which contained an identifying name. Two examples of this within
 * SLOP are the FunctionToken and FieldToken. The function token uses the Grammar Pattern: val '(' ( expr ','? )+ ')'.
 * The val in this case represents the name of the function it represents e.g. SUM or DATE.
 */
@NoArgsConstructor
public class GrammarValue extends GrammarToken<String> {

    private boolean capture;
    /* Can restrict the type to a specific class type. This is specified in the grammar string by
     * using a ':<type>'. For example, to restrict to a String token value you can use val:String. */
    private String valueType;

    public GrammarValue(String value) {
        super("GrammarValue", value);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "'(.+?[^\\\\])'(\\??)";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<String> createToken(String value) {
        return new GrammarValue(value);
    }

    public void setCapture(boolean capture) {
        this.capture = capture;
    }

    public boolean isCapture() {
        return this.capture;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    @Override
    public String toString() {
        return getValue();
    }

    @Override
    public boolean equals(Object o) {
        if (!Objects.isNull(o) && o instanceof GrammarValue) {
            GrammarValue grammarValue = (GrammarValue)o;
            return getValue().equalsIgnoreCase(grammarValue.getValue());
        }
        return false;
    }

    /**
     * See {@link Token#isCaptureToken() isCaptureToken}
     */
    @Override
    public boolean isCaptureToken() {
        return capture;
    }
}
