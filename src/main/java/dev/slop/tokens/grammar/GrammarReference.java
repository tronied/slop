package dev.slop.tokens.grammar;

import dev.slop.enums.PatternType;
import dev.slop.tokens.Token;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Represents a token reference in the grammar pattern of another token. This reference may target a
 * single or multiple other tokens, but will be selected during the lexing of the expression. This
 * is a break from the traditional grammar patterns where all syntax had to be laid out in the String.
 * This also allows code reuse for certain tokens.
 */
public class GrammarReference extends GrammarToken<List<String>> {

    public GrammarReference(List<String> refs) {
        super("ref", refs);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return null;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return null;
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<List<String>> createToken(String value) {
        return null;
    }

    /**
     * See {@link Token#isCaptureToken() isCaptureToken}
     */
    @Override
    public boolean isCaptureToken() {
        return true;
    }

    @Override
    public String toString() {
        return "ref";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return ((GrammarReference) o).getValue().equals(getValue());
    }
}
