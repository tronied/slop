package dev.slop.tokens.grammar;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Grammar tokens are created from the Grammar Pattern and are used to form the pattern tokens. The Grammar Token
 * class is used as a parent to all other Grammar Tokens. It implements certain methods which are not necessary
 * and defines certain properties which are only specific to Grammar / Pattern tokens.
 * @param <T> The generic type to which the child token value wraps
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public abstract class GrammarToken<T> extends Token<T> {

    private boolean optional;
    private boolean persist;

    public GrammarToken(String name, T value) {
        super(name, value);
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return null;
    }
}
