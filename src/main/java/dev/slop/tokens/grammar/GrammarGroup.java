package dev.slop.tokens.grammar;

import dev.slop.enums.PatternType;
import dev.slop.tokens.Token;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Grammar tokens are created from the Grammar Pattern and are used to form the pattern tokens. Grammar Group tokens
 * are used to group together other Grammar Tokens and determine the structure of how the child tokens will be written
 * into groups. For example, using the following Grammar String: 'sentence:' ( ( expr ','&lt;? )+ '.'&lt; )+ with the
 * following Expression String:
 *
 *      sentence: There are many types of sentences, all with different structures and complexities.
 *      In its most basic form, a sentence is made up of a subject and predicate, which is the verb and the
 *      words that follow. But no matter how simple or complex, a sentence consists of words. Words in a
 *      sentence are what make it come alive and make sense.
 *
 * You would get the following group output:
 *
 *      TokenGroup( tokens = [
 *          TokenGroup( tokens = [
 *              TokenGroup( tokens = [
 *                  StringToken( value = "There" )
 *                  StringToken( value = "are" )
 *                  StringToken( value = "many" )
 *                  StringToken( value = "types" )
 *                  StringToken( value = "of" )
 *                  StringToken( value = "sentences" )
 *                  StringToken( value = "," )
 *              ],
 *              TokenGroup( tokens = [
 *                  StringToken( value = "all" )
 *                  StringToken( value = "with" )
 *                  StringToken( value = "different" )
 *                  StringToken( value = "structures" )
 *                  StringToken( value = "and" )
 *                  StringToken( value = "complexities" )
 *              ],
 *              StringToken( value = "." )
 *          ],
 *          TokenGroup( tokens = [
 *              TokenGroup( tokens = [
 *                  StringToken( value = "In" )
 *                  StringToken( value = "its" )
 *                  ...
 *              ],
 *              ...
 *          ...
 *     ]
 *
 * This is because in the Grammar Pattern we specified that we're expecting one or more expressions (sets of tokens)
 * separated by an optional comma but always ends with a full stop. This may not always work as text isn't guaranteed
 * to end with full stops as this is down to the writer. In programming we can afford to put those strict rules in there
 * so that we know to look for certain patterns. The above shows how we can layer Grammar groups so that we know to
 * look for certain structures when processing them (@see #process()).
 */
@Data
@NoArgsConstructor
public class GrammarGroup extends GrammarToken<List<GrammarToken<?>>> {

    private String groupId;
    private int groupCounter;
    private boolean multiple;

    public GrammarGroup(String groupId, List<GrammarToken<?>> tokens) {
        super("GrammarGroup", tokens);
        this.groupId = groupId;
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "\\((.+)\\)(\\+?)(\\??)";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    public String getGroupId() {
        return groupId;
    }

    public void incrementCounter() {
        groupCounter++;
    }

    public int getGroupCounter() {
        return groupCounter;
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<List<GrammarToken<?>>> createToken(String value) {
        return null;
    }

    /**
     * See {@link Token#isCaptureToken() isCaptureToken}
     */
    @Override
    public boolean isCaptureToken() {
        return true;
    }

    /**
     * See {@link Token#isRepeatable() isRepeatable}
     */
    @Override
    public boolean isRepeatable() {
        return multiple;
    }

    /**
     * See {@link Token#getId() getId}
     */
    @Override
    public String getId() {
        return groupId;
    }

    public boolean isTechnicallyComplete() {
        for (int i = getTokenPosition() + 1;i < getPatternTokens().size();i++) {
            if (!((GrammarToken<?>)getPatternTokens().get(i)).isOptional()) return false;
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GrammarGroup that = (GrammarGroup) o;
        return groupCounter == that.groupCounter && multiple == that.multiple &&
                Objects.equals(groupId, that.groupId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), groupId, groupCounter, multiple);
    }
}
