package dev.slop.tokens.operators;

import dev.slop.config.SLOPConfig;
import dev.slop.enums.OperatorType;
import dev.slop.exception.ParserException;
import dev.slop.tokens.Token;
import lombok.NoArgsConstructor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * The operator handler class is a special token type declared specifically within the configuration class. It
 * specifies all operators within the implementing classes pattern (@see #getPattern()), the operator order
 * (@see #getOperatorOrder() - BODMAS by default) and utility methods for reverse lookup on operator types.
 */
@NoArgsConstructor
public abstract class OperatorHandler extends Token<String> {
    Map<String, OperatorType> operatorMap;

    public OperatorHandler(String value) {
        super("Operator", value);
        operatorMap = new HashMap<>();
    }

    /**
     * The concrete implementation of this class can add operators to the Map using a String value and the OperatorType
     * mapping. This is typically performed in the constructor.
     * @param value The String value to map to the operator type i.e. '==' -&gt; OperatorType.EQUALS
     * @param opType The Operator type to identify the operator type being added
     */
    protected void addOperator(String value, OperatorType opType) {
        operatorMap.put(value, opType);
    }

    /**
     * Fetches the OperatorType for a given operator token. The operator token contains the read String value e.g. '==',
     * therefore for it to work in operations this method provides the lookup to the associated type to the
     * OperatorHandler classes.
     * @param operator The operator token to identify the type for
     * @return Returns the operator type
     */
    public OperatorType getOpType(OperatorToken operator) {
        return operatorMap.get(operator.getValue());
    }

    /**
     * Fetches the OperatorType for the current token. The operator token contains the read String value e.g. '==',
     * therefore for it to work in operations this method provides the lookup to the associated type to the
     * OperatorHandler classes.
     * @return Returns the operator type
     */
    public OperatorType getOpType() {
        return operatorMap.get(getValue());
    }

    /**
     * This method performs a reverse lookup and instantiates a new token containing the String value based on the
     * Operator Type passed. This information is stored by the implementing concrete class, so to do this we need
     * to fetch the implementing class from the passed config. This is useful when we make assumptions in statements
     * and need to add operators to undertake this operation. For example, in a switch statement case you could just
     * specify '4' as opposed to '&gt; 4'. In this case an assumption is made that this case should only ever be true if
     * the value equals 4. As such, we'll add the EQUALS operator by using this method as we can't make assumptions on
     * what the String value for equals would be as the OperatorToken may be overridden.
     * @param operatorType The operator type for which to create the operator token
     * @param config The config class from which to fetch the implementing operator token
     * @return Returns a new operator token containing the String implementation
     */
    public OperatorHandler createOperator(OperatorType operatorType, SLOPConfig config) {
        String foundOp = operatorMap.entrySet().stream()
                .filter(e -> e.getValue() == operatorType)
                .map(Map.Entry::getKey)
                .findFirst()
                //Should never happen, but handle just in case
                .orElseThrow(() -> new ParserException(
                        String.format("Could not find OperatorType (%s) in Operator Handler class", operatorType.name())));
        Token<?> opToken = config.getOperatorHandler();
        try {
            Constructor<?> tokenConstructor = opToken.getClass().getConstructor(String.class);
            return (OperatorHandler) tokenConstructor.newInstance(foundOp);
        } catch (NoSuchMethodException ex) {
            throw new ParserException("Could not find default constructor accepting an operator String!");
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException exc) {
            throw new ParserException("Could not create new operator instance for '%s'. Error: " + exc.getMessage());
        }
    }

    /**
     * This String array determines the order of execution for operators determined by the implementing class. Each
     * layer is executed in order with the those defined first as the first to be executed. As such, using the
     * default BODMAS OperatorToken we can see the following order defined:
     *
     *         return new String[][] {
     *             {"/", "*", "%"},
     *             {"+", "-"},
     *             {"&gt;=", "&lt;=", "&gt;", "&lt;"},
     *             {"==", "!="}
     *         };
     *
     * @return Returns a 2D string array containing sets of operators and the order in which they execute.
     */
    public abstract String[][] getOperatorOrder();
}
