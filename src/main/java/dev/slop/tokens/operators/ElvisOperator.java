package dev.slop.tokens.operators;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * The elvis operator takes two arguments. If the first is null then it returns the second.
 */
public class ElvisOperator extends Token<Void> {

    public ElvisOperator() {
        super("Elvis", null);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new ElvisOperator());
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "val '?:' val";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        if (getTokenGroups().size() != 2) {
            throw new ParserException(String.format("Expected both a value to check for null and a fallback. " +
                    "Found %s tokens", getTokenGroups().size()));
        }
        Token<?> result = parser.processExpression(getTokenGroups().get(0).getTokens(), context);
        if (Objects.isNull(result.getValue())) {
            return Collections.singletonList(
                    parser.processExpression(getTokenGroups().get(1).getTokens(), context));
        }
        return Collections.singletonList(result);
    }
}
