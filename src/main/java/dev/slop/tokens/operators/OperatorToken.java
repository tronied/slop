package dev.slop.tokens.operators;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.OperatorType;
import dev.slop.enums.PatternType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * This is the default set of operators used within SLOP. They borrow heavily from C / Java but can be customized
 * by setting a custom implementation of the OperatorHandler class. Please see the VerboseOperator example in the
 * tests.
 */
public class OperatorToken extends OperatorHandler {

    public OperatorToken() { super(); }

    public OperatorToken(String value) {
        super(value);
        initOperators();
    }

    /**
     * Creates the mappings between the String operators and their associated operator types used by the
     * TypeOperation classes.
     */
    private void initOperators() {
        addOperator("+", OperatorType.ADD);
        addOperator("-", OperatorType.SUBTRACT);
        addOperator("/", OperatorType.DIVIDE);
        addOperator("*", OperatorType.MULTIPLY);
        addOperator(">=", OperatorType.GTE);
        addOperator("<=", OperatorType.LTE);
        addOperator("<", OperatorType.LT);
        addOperator(">", OperatorType.GT);
        addOperator("%", OperatorType.MOD);
        addOperator("!=", OperatorType.NOT_EQUALS);
        addOperator("==", OperatorType.EQUALS);
        addOperator("++", OperatorType.INCREMENT);
        addOperator("--", OperatorType.DECREMENT);
        addOperator("+=", OperatorType.INCREMENT_EQUALS);
        addOperator("-=", OperatorType.INCREMENT_EQUALS);
        addOperator("=", OperatorType.ASSIGN);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "^(\\+\\+|--|\\+=|-=|\\+|-|\\/|\\*|>=|<=|<|>|%|!=|==|=)";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return Collections.singletonList(this);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<String> createToken(String value) {
        return new OperatorToken(value);
    }

    /**
     * See {@link OperatorHandler#getOperatorOrder() getOperatorOrder}
     */
    @Override
    public String[][] getOperatorOrder() {
        return new String[][] {
                {"/", "*", "%"},
                {"+", "-"},
                {">=", "<=", ">", "<"},
                {"==", "!=", "="}
        };
    }

    @Override
    public String toString() {
        return getValue();
    }
}
