package dev.slop.tokens.operators;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.literals.BooleanToken;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class UnaryOperator extends Token<Void> {

    public UnaryOperator() {
        super("Negate", null);
    }

    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new UnaryOperator());
    }

    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    @Override
    public String getPattern() {
        return "'!' val";
    }

    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        Token<?> result = parser.processExpression(getTokenGroups().get(0).getFlatTokens(), context);
        if (result.getValue() instanceof Boolean) {
            return Collections.singletonList(new BooleanToken(!result.getValue(Boolean.class)));
        }
        throw new ParserException(String.format("Cannot negate a type (%s) which is not a Boolean!",
                ((Boolean) result.getValue()).getClass().getSimpleName()));
    }
}
