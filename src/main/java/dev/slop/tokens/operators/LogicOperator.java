package dev.slop.tokens.operators;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Logic operators are an integral part of boolean conditions and determine how different expressions should
 * evaluate together. The 'and' logic operator specifies that the overall result will only return true if both
 * sides are true themselves. Alternatively 'or' can return true if either side is true. When using a mixture
 * of 'or' / 'and' operators, SLOP splits the expression using the 'or' operator and evaluates individual
 * expressions as if any one of those evaluates to true then the final result will be true.
 */
@NoArgsConstructor
public class LogicOperator extends Token<String> implements LogicOperatorHandler {

    public LogicOperator(String value) {
        super("Logic Operator", value);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "^(and|or)";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return Collections.singletonList(this);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<String> createToken(String value) {
        return new LogicOperator(value);
    }
}
