package dev.slop.tokens;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.parser.SLOPParser;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Token groups are used to store sub-groups or tokens read from the expression String. They are automatically
 * created by the lexer and added to the target token when a grammar group is found in its pattern. Once the Lexer
 * has finished adding all the necessary tokens to the group structure in the target token, these are then used as
 * the source by the process method (@see com.slop.tokens.Token#process()). For example, using the FunctionToken
 * as an example, we may have the following structure output after the Lexer has finished processing:
 *
 *      [FunctionToken Grammar Pattern]
 *      val '(' ( expr ','? )+ ')'
 *
 *      [Lexer Input: Expression]
 *      MY_FUNCTION(3 * 4 + 1, 13.9)
 *
 *      [Lexer Output: FunctionToken.tokenGroups]
 *      [
 *          (TokenGroup)
 *              (StringToken) MY_FUNCTION
 *          (TokenGroup)
 *              (TokenGroup)
 *                  (IntegerToken) 3
 *                  (OperatorToken) *
 *                  (IntegerToken) 4
 *                  (OperatorToken) +
 *                  (IntegerToken) 1
 *              (TokenGroup)
 *                  (DecimalToken) 13.9
 *      ]
 *
 * Looking at the structure above compared to the grammar pattern, there are extra TokenGroup's being added. This is
 * because a new group is created whenever an 'expr' is defined to wrap the contained tokens. The parent group gets
 * created to represent the wrapper grammar group. The token groups and child tokens are stored in the tokens
 * tokenGroups list.
 *
 * In this case, as there is just a single token in the expression (FunctionToken) that is passed to the Parser,
 * the process() method on that is the only thing invoked on the first pass. The process method on the token first
 * extracts the group and assigns the identifying name to a variable. Next it loops through each group representing a
 * parameter and passes the tokens in each to the Parser for resolution. Those are then passed to the target Function
 * as a single array of values [15, 13.9].
 */
public class TokenGroup extends Token<Void> {
    private String groupId;
    private List<Token<?>> tokens;

    public TokenGroup() { this.tokens = new ArrayList<>(); }

    public TokenGroup(List<Token<?>> in) {
        this.tokens = new ArrayList<>(in);
    }

    public TokenGroup(String groupId, List<Token<?>> in) {
        this.groupId = groupId;
        this.tokens = new ArrayList<>(in);
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return null;
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return null;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return null;
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return getTokens();
    }

    /**
     * Returns the tokens contained within this token group
     * @return The contained tokens from this group
     */
    public List<Token<?>> getTokens() {
        return this.tokens;
    }

    /**
     * Unwraps all contained tokens into non-group collection. For example, using the following example:
     *
     *      [
     *          (TokenGroup) &lt;- getFlatTokens called on this
     *              (TokenGroup)
     *                  (StringToken) "Test"
     *              (TokenGroup)
     *                  (IntegerToken) 13
     *              (TokenGroup)
     *                  (TokenGroup)
     *                      (BooleanToken) true
     *      ]
     *
     * Using this method would return the following:
     *
     *      [
     *          (StringToken) "Test"
     *          (IntegerToken) 13
     *          (BooleanToken) true
     *      ]
     *
     * This can be useful when dealing with retrieving values from a hierarchical set of tokens. Care has to be taken
     * though and should only be used on a group where token groupings do not need to be maintained. Otherwise the
     * token order will be lost and the result from this would be likely incorrect.
     * @return Returns an unwrapped set of tokens
     */
    public List<Token<?>> getFlatTokens() {
        return getTokens(this);
    }

    /**
     * Recursively iterates through each token group and adds resulting tokens which are not a group to the result
     * @param group The current token group which is being scanned
     * @return Returns a list of unwrapped tokens for the current group
     */
    private List<Token<?>> getTokens(TokenGroup group) {
        return group.getTokens().stream()
                .flatMap(t -> t instanceof TokenGroup ?
                        getTokens((TokenGroup)t).stream() :
                        Stream.of(t))
                .map(t -> (Token<?>)t)
                .collect(Collectors.toList());
    }

    /**
     * Unwraps the internal token group structure to remove any single entry TokenGroups and unwraps them. Those groups
     * that have multiple child tokens will be kept as groups. The root TokenGroup will always be maintained to avoid
     * unwrapping too far especially in the case of a collection of values.
     * @param token The token to be unwrapped of unnecessary token groups
     * @return Returns a structure free from single entity TokenGroup's.
     */
    public Token<?> unwrapKeepStructure(Token<?> token) {
        if (token instanceof TokenGroup) {
            if (((TokenGroup) token).getTokens().size() > 1) {
                return new TokenGroup(((TokenGroup) token).getTokens().stream()
                        .map(this::unwrapKeepStructure)
                        .collect(Collectors.toList()));
            } else {
                return unwrapKeepStructure(((TokenGroup) token).getTokens().get(0));
            }
        }
        return token;
    }

    /**
     * Loops through each group passing each group of tokens to the parser to resolve a single token result
     * @param parser The parser which resolves a value from a group of tokens
     * @param context The context from which to resolve field references in tokens
     * @return Returns a list of resolved values
     */
    public List<Token<?>> processTokens(SLOPParser parser, SLOPContext context) {
        return getTokens().stream()
            .map(tg -> {
                if (tg instanceof TokenGroup) {
                    return parser.processExpression(((TokenGroup)tg).getFlatTokens(), context);
                } else {
                    return tg;
                }
            })
            .collect(Collectors.toList());
    }

    /**
     * Determines whether the contained tokens within this group contain an instance of the passed class type
     * @param clazz The type to search in the contained tokens
     * @return Returns true if a match was found
     */
    public Optional<Token<?>> containsTokenType(Class<?> clazz) {
        return getTokens().stream()
                .filter(clazz::isInstance)
                .findFirst();
    }

    @Override
    public String toString() {
        return getTokens().stream().map(Token::toString).collect(Collectors.joining(", "));
    }

    /**
     * See {@link Token#toSimpleString() toSimpleString}
     */
    @Override
    public String toSimpleString() {
        return getTokens().stream().map(Token::toSimpleString).collect(Collectors.joining(","));
    }
}
