package dev.slop.tokens;

import dev.slop.callback.ReferenceCallback;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.*;
import dev.slop.exception.LexerException;
import dev.slop.exception.ParserException;
import dev.slop.model.Expression;
import dev.slop.parser.Parser;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.base.NonToken;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.grammar.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import dev.slop.tokens.literals.NullToken;
import dev.slop.tokens.operators.OperatorToken;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * The token forms the basis for all tokens within SLOP. This classes primary function is to store and make available
 * the value to which the token represents for the implementing class. It also provides methods to manage pattern tokens,
 * child tokens and progression during the lexing process.
 * @param <T> The type for the stored value for which this token represents
 */
@Data
@SuperBuilder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public abstract class Token<T> implements Cloneable, Serializable {

    //Used mostly within collections to save the value of the current (iterative) result
    protected static final String RESULT_VARIABLE = "result";

    private String id;
    private T value;
    private String name;
    private TokenType tokenType;
    private List<Token<?>> patternTokens;
    @Setter(AccessLevel.PROTECTED)
    private List<TokenGroup> tokenGroups;
    @Getter
    @Setter(AccessLevel.NONE)
    private Set<ParserFlag> parserFlags;
    protected int tokenPosition;
    private VariableType variableType;
    private String completedGroup;

    public Token() {
        tokenType = TokenType.PRIMARY;
        id = UUID.randomUUID().toString();
        tokenGroups = new ArrayList<>();
        parserFlags = new HashSet<>();
    }

    public Token(String name, T value) {
        this(name, value, TokenType.PRIMARY);
    }

    public Token(String name, T value, TokenType tokenType) {
        this.name = name;
        setValue(value);
        patternTokens = new ArrayList<>();
        tokenGroups = new ArrayList<>();
        parserFlags = new HashSet<>();
        id = UUID.randomUUID().toString();
        this.tokenType = tokenType;
    }

    /**
     * A simple method to determine whether the value contained in the token is of a specific type
     * @param compare The class to determine whether the contained value is of the same type
     * @return Returns true if the value matches the specified type
     */
    public boolean is(Class<?> compare) {
        return compare.isInstance(getValue());
    }

    /**
     * Used in uniquely identifying each token to determine whether the current token already exists on the stack
     * @return Returns the unique ID of the current token
     */
    public String getUID() {
        return this.id;
    }

    @Override
    public String toString() {
        if (Objects.isNull(getValue())) return String.format("%s{null}", getClass().getSimpleName());
        return getValue() instanceof String ? String.format("\"%s\"", getValue()) : getValue().toString();
    }

    /**
     * Clones the pattern tokens and value from the current token. This is because when an object is cloned, by
     * default they either don't copy the contained lists of objects or share them so that modifying one modifies
     * the other. This is obviously problematic when we have multiple statements and tracking tokens of the same type.
     * Copying values from an existing source is quicker than generating them again.
     * @param newToken The new token onto which the pattern tokens and values are copied
     * @return Returns a reference to the new token
     */
    protected Token<T> cloneDefaultProperties(Token<T> newToken) {
        newToken.setPatternTokens(patternTokens.stream().map(Token::clone).map(t -> (Token<?>)t).collect(Collectors.toList()));
        //Reset all positioning values of cloned token
        newToken.getPatternTokens().forEach(t -> t.setTokenPosition(0));
        newToken.getPatternTokens().stream()
                .filter(t -> t instanceof GrammarGroup).map(t -> (GrammarGroup)t)
                .forEach(g -> g.setTokenPosition(0));
        newToken.setValue(value);
        newToken.setName(name);
        return newToken;
    }

    /**
     * Defers creation of a new token to the instance of the implementing class. This is because by default the
     * config has a single set of instances for each token type. From this matches are found by the lexer (current
     * token compared to each set of pattern tokens) and when a match is found this method is then used to
     * create a new one by cloning it's pattern tokens (@see #cloneDefaultProperties()).
     * @param value The value for which the token will hold
     * @return Returns a new token instance
     */
    public abstract Token<T> createToken(String value);

    /**
     * Determines how the current token is matched either by regular expression or a grammar String. Tokens which use
     * regular expressions are typically literals and are matched directly from the expression String. Grammar matches
     * on the other hand match NonToken / TokenValue tokens against their pattern tokens to determine if it matches.
     * @return Returns the pattern type of the current token
     */
    public abstract PatternType getPatternType();

    /**
     * Returns the String pattern either as a regular expression or grammar String
     * @return Returns the pattern String for the current token
     */
    public abstract String getPattern();

    /**
     * If an expected token is missing from the current token in the expression, this method is called so that the
     * token class itself can provide some guidance as to the fix or correct definition approach.
     * @param token The expected token to return the guidance
     * @param groupsCount A list of any empty token groups
     * @return Returns an optional string if guidance is available for that expected token
     */
    public abstract Optional<String> getGuidance(String token, List<Integer> groupsCount);

    /**
     * This method defers resolution of it's resulting value to itself. As such, a token will either return itself
     * if it is a simple literal or perform some extra logic and resolution in the case of a statement. This method
     * is passed the parser, context and config classes so that resolution of child tokens can be referred back to
     * the parser or if a reference needs to be resolved from either the context or config.
     * @param parser The parser used to resolve child token expressions
     * @param context The context for resolving object references
     * @param config The configuration for resolution of property values
     * @return Returns a collection of one or more resolved tokens
     */
    protected abstract List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config);

    /**
     * This is the standard implementation for calling a token. This notifies the parser that the token
     * has begun processing and also when it has finished. The process call is then deferred to the process
     * method on each child token class
     * @param parser The parser used to resolve child token expressions
     * @param context The context for resolving object references
     * @param config The configuration for resolution of property values
     * @return Returns a collection of one or more resolved tokens
     */
    public List<Token<?>> processNoParams(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        parser.tokenStart(this);
        try {
            return process(parser, context, config);
        } finally {
            parser.tokenEnd(this);
        }
    }

    /**
     * This provides a default implementation of the process method for a token which accepts a list of parameter
     * tokens. This is used for example if another token which is being evaluated needs to pass the tokens it
     * has been provided into the target for evaluation. An example of this would be the InvocationToken which
     * references a function token with one or more parameters. That implementation then retrieves the reference
     * for that from context and calls this method passing the parameters.
     * @param parser The parser used to resolve child token expressions
     * @param context The context for resolving object references
     * @param config The configuration for resolution of property values
     * @param params The list of parameters to be passed to this token for execution
     * @return Returns a collection of one or more resolved tokens
     */
    public List<Token<?>> processWithParams(SLOPParser parser, SLOPContext context, SLOPConfig config, List<Token<?>> params) {
        parser.tokenStart(this);
        try {
            if (!(this instanceof TokenParameters)) {
                //Default implementation is not supported for most tokens, but can be overridden by those which require it
                throw new ParserException("This token cannot accept parameters when executing");
            }
            return ((TokenParameters) this).process(parser, context, config, params);
        } finally {
            parser.tokenEnd(this);
        }
    }

    /**
     * The public method to process a token using a callback. This callback can be useful if the
     * current token is a SECONDARY which is part of another token. This is used to notify the
     * parent of some event that has occurred to undertake an action.
     * @param parser The parser used to resolve child token expressions
     * @param context The context for resolving object references
     * @param config The configuration for resolution of property values
     * @param callback The callback method used to notify the parent token of an event
     * @return Returns a collection of one or more resolved tokens
     */
    public List<Token<?>> processWithCallback(SLOPParser parser, SLOPContext context, SLOPConfig config,
                                              ReferenceCallback callback) {
        parser.tokenStart(this);
        try {
            if (!(this instanceof TokenCallback)) {
                //Default implementation is not supported for most tokens, but can be overridden by those which require it
                throw new ParserException("This token cannot accept parameters when executing");
            }
            return ((TokenCallback) this).process(parser, context, config, callback);
        } finally {
            parser.tokenEnd(this);
        }
    }

    /**
     * Creates an instance of the current token if the Expression String has a positive match against the regular
     * expression String (@see #getPattern())
     * @param expression The current state of the expression String being parsed to match against the current token
     * @param dryRun If a match is found, this determines whether to remove it from the expression
     * @return Returns an optional new instance of the current token
     */
    public Optional<Token<T>> match(Expression expression, boolean dryRun) {
        Matcher m = getMatcher(expression.getValue());
        if (m.find()) {
            if (!dryRun) expression.cutTo(m.end(0));
            return Optional.of(createToken(m.group(m.groupCount())));
        }
        return Optional.empty();
    }

    /**
     * Creates a new regular expression Matcher for the tokens regular expression String
     * @param value The expression String for which to create the Matcher
     * @return Returns a Matcher instance
     */
    protected Matcher getMatcher(String value) {
        if (getPatternType() != PatternType.REGEX) {
            throw new LexerException(String.format("The handler '%s' cannot be used to scan for " +
                    "regex matches", name));
        }
        Pattern p = Pattern.compile(getPattern());
        return p.matcher(value);
    }

    /**
     * Tokens use the unique ID and class to determine uniqueness. This is primarily used when a match is found
     * to determine whether it exists on the stack or is in scope already or needs to be added.
     * @param o The object to compare with
     * @return Returns true if the objects are equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token<?> token = (Token<?>) o;
        if (getValue() == null) return false;
        return this.getValue().equals(token.getValue());
    }

    /**
     * Determines whether the current token and it's value are equal
     * @param token The token to compare with
     * @return Returns true if the token has the same value
     */
    public boolean equalsValue(Token<?> token) {
        if (this == token) return true;
        if (token == null) return false;
        if (getValue() == null) return false;
        return this.getValue().equals(token.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.isNull(getValue()) ? Objects.hash(name) : Objects.hash(getValue());
    }

    /**
     * Casts the value contained within this token to the type passed. This method has the possibility of throwing
     * a ClassCastException if the value is not compatible.
     * @param clazz The type to cast the result to
     * @param <L> The generic type to cast the tokens value
     * @return Returns the cast value
     */
    public <L> L getValue(Class<L> clazz) {
        return convert(clazz).orElse(clazz.cast(getValue()));
    }

    /**
     * Provides the ability for a token to convert its value to a given type
     * @param clazz The class type to cast the value to
     * @return Returns the value in the type specified
     * @param <L> The class type to convert to
     */
    protected <L> Optional<L> convert(Class<L> clazz) {
        return Optional.empty();
    }

    /**
     * Determines whether the passed token is the next logical token in the pattern of the current token. This takes
     * into consideration whether the pattern token is optional.
     * @param token The token to search for in the pattern of the current token
     * @return Returns true if the current token is a match
     */
    public boolean isNextToken(Token<?> token) {
        Token<?> currentPos = getPatternTokens().get(getTokenPosition());
        if (currentPos instanceof GrammarValue && !(token instanceof NonToken)) {
            GrammarValue grammarValue = (GrammarValue) currentPos;
            if (grammarValue.isCapture() && Objects.equals(grammarValue.getValueType(),
                    token.getValue().getClass().getSimpleName())) {
                return true;
            }
        }
        if ((currentPos instanceof GrammarExpression || isGrammarCapture(currentPos)) &&
                getTokenPosition() + 1 < getPatternTokens().size()) {
            currentPos = getPatternTokens().get(getTokenPosition() + 1);
        }
        //Look for groups and search within
        if (currentPos instanceof GrammarGroup && !currentPos.isComplete()) {
            boolean result = currentPos.isNextToken(token);
            if (result) return true;
        }
        int tempPosition = getTokenPosition();
        while (true) {
            if (++tempPosition < getPatternTokens().size()) {
                Token<?> nextToken = getPatternTokens().get(tempPosition);
                if (nextToken instanceof GrammarGroup) {
                    boolean result = nextToken.isNextToken(token);
                    if (result) return true;
                    if (((GrammarGroup) nextToken).isOptional()) continue;
                }
                if (nextToken.getValue() != null && nextToken.getValue().equals(token.getValue()))
                    return true;
                if (!(token instanceof NonToken) && nextToken instanceof GrammarValue && ((GrammarValue)nextToken).isCapture() &&
                        Objects.equals(((GrammarValue)nextToken).getValueType(), token.getValue().getClass().getSimpleName())) {
                    return true;
                }
                if (nextToken instanceof GrammarToken && ((GrammarToken<?>) nextToken).isOptional()) continue;
            }
            break;
        }
        //Compare directly to tokens if no groups found
        return currentPos.equalsValue(token) || getTokenPosition() + 1 < getPatternTokens().size() &&
                getPatternTokens().get(getTokenPosition() + 1).equalsValue(token);
    }

    /**
     * A simplified overloaded version of the matchNextToken method. This uses some defaults like not updating the
     * token position when simply scanning for a match. It also initialises the AtomicBoolean used across recursive
     * calls to maintain state about whether the found group is in progress or not.
     * @param token The token to try and match within the pattern token structure
     * @return Returns an optional token if a match has been found
     */
    public Optional<Token<?>> matchNextToken(Token<?> token) {
        return matchNextToken(token, false, new AtomicBoolean(false));
    }

    /**
     * Searches the current token pattern for the provided token parameter. This method supersedes the isNextToken
     * when finding matches in token handlers as the previous implementation had limitations. This method will
     * eventually be used as the primary method for scanning tokens in the list of token handlers and on the stack.
     * @param token The current token that has been scanned and to be matched
     * @param updateToPosition If this is enabled and a match is found, then the token position is updated
     * @param groupInProgress Although not ideal, this retains the state of whether the group matched with the token
     *                        is currently in progress (mid-flow) or false for complete or not started.
     * @return Returns an optional token if a match has been found
     */
    public Optional<Token<?>> matchNextToken(Token<?> token, boolean updateToPosition, AtomicBoolean groupInProgress) {
        if (Objects.isNull(getPatternTokens()) || getPatternTokens().isEmpty()) {
            boolean valueCheck = Objects.equals(getValue(), token.getValue());
            if (!valueCheck && this instanceof GrammarValue && !(token instanceof NonToken)) {
                valueCheck = Objects.equals(token.getValue().getClass().getSimpleName(),
                        ((GrammarValue)this).getValueType());
            }
            return valueCheck ? Optional.of(this) : Optional.empty();
        }
        for (int i = getTokenPosition();i < getPatternTokens().size();i++) {
            Token<?> current = getPatternTokens().get(i);
            if (current instanceof GrammarToken) {
                GrammarToken<?> patternToken = (GrammarToken<?>)current;
                if (current instanceof GrammarReference && !patternToken.isOptional()) {
                    if (updateToPosition) setTokenPosition(i);
                    return Optional.of(current);
                } else {
                    Optional<Token<?>> result = current.matchNextToken(token);
                    if (result.isPresent()) return result;
                }
                if (i < getPatternTokens().size() - 1) {
                    if (!inMiddleGroup(getPatternTokens().get(i))) {
                        Optional<Token<?>> nextImmediate = getPatternTokens().get(i + 1).matchNextToken(token);
                        if (nextImmediate.isPresent()) return nextImmediate;
                    } else {
                        if (!areRestOptional(getPatternTokens().get(i))) {
                            groupInProgress.set(true);
                            return Optional.empty();
                        }
                    }
                }
                if (patternToken.isOptional() && !groupInProgress.get()) continue;
            }
        }
        return Optional.empty();
    }

    /**
     * If the current token is a group, this determines whether that group's progress is not in a reset or
     * at a completed position.
     * @param active The token (if a group) to check
     * @return Returns true if the token is in progress (not reset or complete)
     */
    private boolean inMiddleGroup(Token<?> active) {
        if (active.getPatternTokens().isEmpty() || active.getTokenPosition() == 0) return false;
        return active instanceof GrammarGroup && active.getTokenPosition() > 0 &&
            active.getTokenPosition() < active.getPatternTokens().size() - 1;
    }

    /**
     * Determines whether the rest of the pattern for the provided token is optional. If true, this ensures that
     * we do not just check the current token but others up the stack for the token.
     * @param active The token to check it's pattern
     * @return Returns true if the remaining tokens are optional.
     */
    private boolean areRestOptional(Token<?> active) {
        return active.getPatternTokens().subList(active.getTokenPosition() + 1,
                        active.getPatternTokens().size()).stream()
                .allMatch(t -> t instanceof GrammarValue && ((GrammarValue)t).isOptional());
    }

    /**
     * Determines whether the passed token is a single grammar value capture token
     * @param currentToken The token to check
     * @return Returns true if the token is a match
     */
    private boolean isGrammarCapture(Token<?> currentToken) {
        return (currentToken instanceof GrammarValue && ((GrammarValue) currentToken).isCapture());
    }

    /**
     * This is an overloaded version of the progressToken method. This acts in the same way as the other function
     * but as it is called from the lexer, it resets the completed group. This is because when a repeatable group
     * is completed and reset, we don't want any recursive method to complete that group again and so a variable
     * is set to prevent this from happening. On each new call, this is cleared to allow correct operation again.
     * @param lastToken Previously identified as the next logical token in this tokens pattern
     * @param context The short term store for tokens read from the expression String
     * @param evaluated The long term store for processed tokens
     */
    public void progressToken(Token<?> lastToken, List<Token<?>> context, List<Token<?>> evaluated) {
        /* Once a repeatable group has been completed, this is stored in this variable to prevent other recursive
         * completion events from occurring and performing repeat tasks. In this case, this is the first time that
         * a progression has been requested so we clear this of any value of a previous run. */
        setCompletedGroup("");
        progressToken(lastToken, context, evaluated, this);
    }

    /**
     * This method is called once the current token has been identified as a positive match for the last token read
     * from the expression String. This method recursively loops through the pattern tokens in search for that match
     * and when found, updates relevant positions and completes / adds tokens to groups.
     * @param lastToken Previously identified as the next logical token in this tokens pattern
     * @param context The short term store for tokens read from the expression String
     * @param evaluated The long term store for processed tokens
     * @param root Because this method is recursive, this is a reference to root token object
     */
    public void progressToken(Token<?> lastToken, List<Token<?>> context, List<Token<?>> evaluated, Token<?> root) {
        if (Objects.isNull(getPatternTokens())) return;
        Token<?> patternToken = getPatternTokens().get(getTokenPosition());
        boolean hasContextItems = !context.isEmpty();
        /* Call the recursive findNextPositionAfterMatch method which possibly returns an updated position on the
         * root token depending on the level of the token being matched. */
        int position = findNextPositionAfterMatch(lastToken, context, root, false);
        if (position > getTokenPosition()) {
            //Handles where the token position has been updated and there are still tokens in context
            if (!context.isEmpty()) {
                //Attempt to find the token group for the current pattern token ID
                TokenGroup found = root.findTokenGroup(patternToken.getId());
                if (Objects.nonNull(found)) {
                    if (getActiveToken(true) instanceof GrammarExpression) {
                        //Embed tokens into new embedded group
                        found.getTokens().add(new TokenGroup(context));
                    } else {
                        found.getTokens().addAll(context);
                    }
                    context.clear();
                } else if (patternToken instanceof GrammarValue && ((GrammarValue) patternToken).isCapture()) {
                    //If there is no active token group to add to, it may mean it is a single capture pattern token
                    TokenGroup suspected = root.getTokenGroups().get(root.getCurrentGroup().intValue() - 1);
                    if (context.size() > 1) {
                        suspected.getTokens().add(context.remove(context.size() - 1));
                    } else {
                        suspected.getTokens().addAll(context);
                        context.clear();
                    }
                }
                if (!context.isEmpty()) {
                    if ((patternToken instanceof GrammarValue || isAssignment(context)) && !((GrammarToken<?>)patternToken).isPersist()) {
                        evaluated.addAll(context);
                    } else if (root.getCurrentGroup().intValue() - 1 < root.getTokenGroups().size()) {
                        /* If the context is still not empty then determine whether the current group indexes matches the
                         * number of token groups. If not then add the token or else add the tokens to the previous group. */
                        if (root.getTokenGroups().isEmpty()) root.getTokenGroups().add(new TokenGroup());
                        TokenGroup existing = root.getTokenGroups().get(Math.max(0, root.getCurrentGroup().intValue() - 1));
                        existing.getTokens().addAll(context);
                    } else {
                        root.getTokenGroups().add(new TokenGroup(context));
                    }
                    context.clear();
                }
            }
            //Finally set the updated position on the token
            setTokenPosition(position);
        } else {
            /* Because pattern tokens can be completed internally to the recursion findNextPositionAfterMatch process
             * we perform a manual check on the pattern token fetched prior to the call. If it is now registering that
             * is it complete, we can go ahead and trigger the complete to complete the token. */
            if (patternToken.isComplete()) {
                TokenGroup found = Optional.ofNullable(root.findTokenGroup(patternToken.getId()))
                        .orElse(root.findTokenGroup(getId()));
                if (!(patternToken instanceof GrammarExpression)) {
                    completeToken(found, patternToken, root, context);
                }
            } else {
                if (!(hasContextItems && context.isEmpty())) {
                    /* If no progress has been made and the context is empty, then it is likely that the current
                     * pattern token is complete and progressed. As such, we call the progressToken method on that
                     * and pass it the last token. */
                    Token<?> current = patternToken.getActiveToken(true, true);
                    if (current.isComplete()) {
                        if (context.isEmpty() || !context.get(context.size() - 1).equals(lastToken)) {
                            patternToken.progressToken(lastToken, context, evaluated, root);
                        }
                    }
                }
            }
        }
    }

    /**
     * Determines whether what exists on the source store is an assignment
     * @param source The source list of tokens
     * @return Returns true if it is an assignment operation
     */
    public boolean isAssignment(List<Token<?>> source) {
        return source.size() > 1 && source.get(0) instanceof TokenValue && source.get(1) instanceof OperatorToken &&
                ((OperatorToken)source.get(1)).getOpType().equals(OperatorType.ASSIGN);
    }

    /**
     * To ensure that the correct token group is added to for child tokens (determined by the Grammar pattern),
     * it searches recursively for a token group matching the passed pattern token ID now marked as complete.
     * Once found, it adds those tokens held in short term memory to that group and if the pattern token is marked
     * as repeatable (one or more) then a new empty group with that pattern group ID is created for new tokens to be
     * added to. It may be the case that no new group is needed as the current token can now be added as a child
     * to a token up the stack (this only happens if the token does not use syntax boundaries). In which case the
     * empty groups are cleaned up when the token is removed from the stack and added to another token (@see
     * #clean())
     * @param lastGroup The group matching the pattern tokens group ID
     * @param patternToken The pattern token that has now been deemed complete
     * @param root The root token in which the pattern token resides
     * @param context The short term store for tokens being read from the Expression String
     */
    private void completeToken(TokenGroup lastGroup, Token<?> patternToken, Token<?> root, List<Token<?>> context) {
        //Clone the group structure and IDs for the current pattern token group
        List<Token<?>> childMatches = cloneChildPatternGroups(lastGroup);
        TokenGroup lastGroupCopy = new TokenGroup(lastGroup.getGroupId(), childMatches);
        if (!context.isEmpty()) {
            lastGroup.getTokens().addAll(!childMatches.isEmpty() ?
                    Collections.singletonList(new TokenGroup(context)) : context);
            context.clear();
        }
        //Find the parent token group for the current pattern token group
        TokenGroup parent = findParentOfTokenGroup(root.getTokenGroups(), lastGroup);
        if (!Objects.equals(root.getCompletedGroup(), lastGroup.getGroupId())) {
            if (Objects.nonNull(parent)) {
                root.setCompletedGroup(lastGroup.getGroupId());
                parent.getTokens().add(lastGroupCopy);
                if (Objects.isNull(parent.getGroupId()) || parent.getGroupId().isEmpty())
                    parent.setGroupId(UUID.randomUUID().toString());
                //If the last group with an ID was empty then likely it was not used and can be removed.
                if (lastGroup.getTokens().isEmpty()) {
                    parent.getTokens().remove(lastGroup);
                }
                lastGroup.setGroupId(null);
            } else {
                throw new LexerException(String.format("Could not find the parent group of current TokenGroup (%s)!",
                        lastGroup.getGroupId()));
            }
        }
        //Reset the token position if the current pattern token is repeatable
        if (patternToken.isComplete()) {
            if (!patternToken.isRepeatable()) {
                //Look for repeatable parent tokens in case we need to trigger a completion event upstream
                if (scanForRepeatableParent(root, parent, context))
                    patternToken.resetTokenPosition();
            } else if (patternToken.isRepeatable()) {
                patternToken.resetTokenPosition();
            }
        }
    }

    private boolean scanForRepeatableParent(Token<?> root, TokenGroup parent, List<Token<?>> context) {
        boolean parentRepeatable = parentValidAndRepeatable(root, parent);
        if (parentRepeatable) {
            if (!parent.isRepeatable()) {
                TokenGroup repeatableGroup = findParentOfTokenGroup(root.getTokenGroups(), parent);
                Token<?> repeatablePattern = findPatternTokenById(root, repeatableGroup.getGroupId());
                completeToken(repeatableGroup, repeatablePattern, root, context);
                return true;
            }
        }
        return false;
    }

    private boolean parentValidAndRepeatable(Token<?> root, TokenGroup group) {
        TokenGroup parent;
        do {
            parent = findParentOfTokenGroup(root.getTokenGroups(), group);
            if (Objects.isNull(parent) || Objects.isNull(parent.getGroupId()) ||
                    parent.getGroupId().isEmpty()) break;
            String targetGroupId = parent.getGroupId();
            Token<?> patternToken = findPatternTokenById(root, targetGroupId);
            if (patternToken instanceof GrammarGroup) {
                if (patternToken.getPatternTokens().size() == 1) return true;
                Token<?> lastToken = patternToken.getPatternTokens().get(patternToken.getPatternTokens().size() - 1);
                return (lastToken instanceof GrammarGroup &&
                        ((GrammarGroup)lastToken).getGroupId().equalsIgnoreCase(targetGroupId));
            }
        } while (parent.getGroupId() != null && !parent.getGroupId().isEmpty());
        return false;
    }

    private Token<?> findPatternTokenById(Token<?> group, String groupId) {
        for (Token<?> patternToken : group.getPatternTokens()) {
            if (patternToken instanceof GrammarGroup) {
                if (((GrammarGroup)patternToken).getGroupId().equalsIgnoreCase(groupId)) return patternToken;
                return findPatternTokenById(patternToken, groupId);
            }
        }
        return null;
    }

    /**
     * Using a token group with one or more levels, it searches for groups which have ID's representative of the
     * structure specified in the Grammar match String. Once found, it clones this structure (minus any other child
     * tokens) and returns it.
     * @param original The original token group containing one or more token groups with IDs
     * @return Returns a cloned structure of pattern token groups with IDs
     */
    private List<Token<?>> cloneChildPatternGroups(TokenGroup original) {
        List<Token<?>> foundGroups = new ArrayList<>();
        for (int i = original.getTokens().size() - 1; i > -1; i--) {
            Token<?> subGroup = original.getTokens().get(i);
            if (subGroup instanceof TokenGroup) {
                TokenGroup tokenGroup = (TokenGroup) subGroup;
                TokenGroup newGroup = new TokenGroup(new ArrayList<>());
                newGroup.setGroupId(tokenGroup.getGroupId());
                //Only recursively scan groups with child tokens
                if (Objects.nonNull(tokenGroup.getTokens()) && !tokenGroup.getTokens().isEmpty()) {
                    List<Token<?>> found = cloneChildPatternGroups((TokenGroup)subGroup);
                    if (!found.isEmpty()) {
                        newGroup.getTokens().addAll(found);
                    }
                }
                //Only add groups which either have a group ID or are not empty
                if (Objects.nonNull(newGroup.getGroupId()) && !newGroup.getGroupId().isEmpty() ||
                        !newGroup.getTokens().isEmpty()) {
                    foundGroups.add(newGroup);
                    if (Objects.nonNull(((TokenGroup) subGroup).getGroupId())) {
                        tokenGroup.setGroupId("");
                    }
                }
            }
        }
        return foundGroups;
    }

    /**
     * Recursively scan through the list of token groups for the passed group and returns its parent.
     * @param tokenGroups The list of token groups to scan
     * @param group The group to find the parent group for
     * @return Returns the parent token group
     */
    private TokenGroup findParentOfTokenGroup(List<TokenGroup> tokenGroups, TokenGroup group) {
        for (TokenGroup current : tokenGroups) {
            List<TokenGroup> subGroups = current.getTokens().stream()
                    .filter(t -> t instanceof TokenGroup)
                    .map(t -> (TokenGroup)t)
                    .collect(Collectors.toList());
            if (Objects.nonNull(current.getTokenGroups()))
                subGroups.addAll(current.getTokenGroups());
            for (TokenGroup subGroup : subGroups) {
                if (Objects.nonNull(subGroup.getGroupId()) &&
                        subGroup.equals(group)) return current;
            }
            TokenGroup result = findParentOfTokenGroup(subGroups, group);
            if (Objects.nonNull(result)) return result;
        }
        return null;
    }

    /**
     * Recursively find a token group within the structure matching one with the passed ID
     * @param id The pattern group ID on the token group to find
     * @return Returns a matching token group or null if none found
     */
    public TokenGroup findTokenGroup(String id) {
        return findMatch(this instanceof TokenGroup ?
                ((TokenGroup) this).getTokens() :
                this.getTokenGroups().stream().map(tg -> (Token<?>)tg).collect(Collectors.toList()), id);
    }

    /**
     * Recursively finds a match in the current list of tokens for one with the given ID
     * @param tokens The list of tokens to search
     * @param id The pattern token ID to find
     * @return Returns either a matching token or null
     */
    private TokenGroup findMatch(List<Token<?>> tokens, String id) {
        for (Token<?> current : tokens) {
            if (current instanceof TokenGroup) {
                if (Objects.nonNull(((TokenGroup) current).getGroupId()) &&
                        ((TokenGroup) current).getGroupId().equalsIgnoreCase(id))
                    return (TokenGroup) current;
            }
            TokenGroup result = current.findTokenGroup(id);
            if (Objects.nonNull(result)) return result;
        }
        return null;
    }

    /**
     * Determines whether the current token is used to capture other tokens e.g. GrammarValue or GrammarExpression.
     * This method is overridden by child classes.
     * @return Returns true if it is used to capture tokens
     */
    protected boolean isCaptureToken() {
        return false;
    }

    /**
     * Determines whether the current token is repeatable. This is used in the context of grammar and especially groups
     * where certain patterns can be repeated and capture tokens into groups. This method is overridden by child classes.
     * @return Returns true if the current token is repeatable.
     */
    protected boolean isRepeatable() {
        return false;
    }

    /**
     * A generic method used to store the ID of the pattern group which this token represents. This method is
     * overridden by child classes.
     * @return Returns the String ID of the pattern group
     */
    protected String getId() {
        return null;
    }

    /**
     * Using the current token position, it calculates what the current active token group should be. For example, if
     * you had "expr '?' expr ':' expr" and the token position was 4 then the group count would be 3. This is because
     * there are 3 token capture groups inclusive of the current one. This can be used to verify whether tokens can
     * be added to an existing group (if the current group matches the current count) or added as a new one.
     * @return Returns the expected current token group count
     */
    public Long getCurrentGroup() {
        return getPatternTokens().subList(0, getTokenPosition() + 1).stream()
                .filter(pt -> pt instanceof GrammarGroup ||
                        pt instanceof GrammarExpression ||
                        pt instanceof GrammarReference ||
                        (pt instanceof GrammarValue && ((GrammarValue)pt).isCapture())).count();
    }

    /**
     * Scans the current tokens pattern for the last read token. As each pattern token is checked, a call to this
     * method on each respective token is made until the token is found. This is denoted by a non -1 value being
     * returned which signifies an updated position in the pattern.
     * @param lastToken The last scanned token read from the Expression String
     * @param context The short term memory storing the last read tokens
     * @param root The root token which holds the top level of pattern tokens being scanned
     * @param dryRun If true, this determines whether it is just a scan or whether it makes position level changes
     * @return Returns the new position if the token was found or -1 if none match
     */
    public int findNextPositionAfterMatch(Token<?> lastToken, List<Token<?>> context, Token<?> root, boolean dryRun) {
        if (Objects.isNull(getPatternTokens()) || getPatternTokens().isEmpty()) return -1;
        for (int i = Math.min(getTokenPosition(), getPatternTokens().size() - 1);i < getPatternTokens().size();i++) {
            Token<?> token = getPatternTokens().get(i);
            int originalPos = token.getTokenPosition();
            //Call to this method to scan child patten tokens of the one being scanned
            int pos = token.findNextPositionAfterMatch(lastToken, context, root, dryRun);
            if (pos != -1) {
                /* If the position returned is not -1 then update the token pattern position and add any tokens in
                 * short term memory to the current token. */
                token.setTokenPosition(pos);
                if (!dryRun && originalPos < token.getPatternTokens().size()) {
                    addToGroup(token, token.getPatternTokens().get(originalPos), context, root, false);
                }
                return i;
            } else if (tokenMatchesType(token, lastToken)) {
                if (!(lastToken instanceof NonToken))
                    context.add(lastToken);
                //Bail if it's a non-token as won't be what we're looking for
                else return -1;
                return i + 1;
            } else if (token.equalsValue(lastToken)) {
                /* If the current token is equal to the current pattern token then optionally persist it to short
                 * term memory and increment the position (pos + 1 as we can skip the found token) */
                if (token instanceof GrammarValue && ((GrammarValue) getPatternTokens().get(i)).isPersist()) {
                    context.add(lastToken);
                }
                return i + 1;
            }
        }
        //If none match then return -1
        return -1;
    }

    /**
     * Determines whether the pattern token is a grammar value which has a type restriction. If so, it compares the
     * type of the passed token to determine whether it is a match.
     * @param patternToken The pattern token to scan for a type match
     * @param lastToken The current token being processed
     * @return Returns true if it has a type requirement and the passed token matches that
     */
    private boolean tokenMatchesType(Token<?> patternToken, Token<?> lastToken) {
        if (patternToken instanceof GrammarValue) {
            GrammarValue grammarValue = (GrammarValue) patternToken;
            return grammarValue.isCapture() && Objects.equals(grammarValue.getValueType(),
                    lastToken.getValue().getClass().getSimpleName());
        }
        return false;
    }

    /**
     * Adds the tokens found in short term memory to the passed token. It does this by identifying the token group
     * marked with the active pattern token ID and adds to that group or completes the token.
     * @param token The pattern token which is active and references the relevant token group
     * @param lastGroup If the last found group is known, this is used as a pattern ID reference
     * @param context The short term memory storing the last read tokens
     * @param root The root token which holds the pattern token and token group being added to
     * @param addToLastGroup Flags whether the short term memory tokens can be added to the existing group or whether
     *                       it is required to add to a new group.
     */
    public void addToGroup(Token<?> token, Token<?> lastGroup, List<Token<?>> context, Token<?> root,
                           boolean addToLastGroup) {
        String groupId = token.getId();
        if (Objects.nonNull(lastGroup.getPatternTokens()) && !context.isEmpty() && lastGroup instanceof GrammarGroup) {
            groupId = lastGroup.getId();
        }
        TokenGroup found = Optional.ofNullable(root.findTokenGroup(groupId))
                .orElse(root.findTokenGroup(getId()));
        if (addToLastGroup) {
            found.getTokens().addAll(!context.isEmpty() ?
                    Collections.singletonList(new TokenGroup(context)) : context);
            context.clear();
        } else if (Objects.nonNull(found.getGroupId()) && !context.isEmpty()) {
            completeToken(found, token, root, context);
        }
    }

    /**
     * Determines whether the current token is complete from a progression perspective. If a token is complete it
     * usually signals that it can be removed from the stack and added to the new head in the relevant token group.
     * @return Returns true if the current token is complete and action can be taken
     */
    public boolean isComplete() {
        return (Objects.nonNull(getPatternTokens()) && getTokenPosition() >= getPatternTokens().size());
    }

    /**
     * Scans the current token and attempts to find a match for the passed token value which is a non-capture token.
     * This is because non-capture tokens are the only ones which can be used to when identify with any certainty
     * patterns in tokens.
     * @param token The token to search for in the current tokens pattern
     * @param contextSize The size of the context to avoid comparing tokens when other items remain
     * @param fromStart Determines whether to find a non capturing token from the start of the pattern or use
     *                  the current token position to start from
     * @return Return true if a match is found
     */
    public boolean firstNonCaptureToken(Token<?> token, int contextSize, boolean fromStart) {
        List<Token<?>> tokens = getPatternTokens();
        for (int i = fromStart ? 0 : getTokenPosition();i < tokens.size();i++) {
            Token<?> current = tokens.get(i);
            if (current instanceof GrammarExpression) continue;
            if (current instanceof GrammarValue) {
                if (!fromStart && contextSize != 0 && !current.equalsValue(token))
                    continue;
                GrammarValue value = (GrammarValue) current;
                if (value.isCapture()) continue;
                if (!current.equalsValue(token) && value.isOptional()) continue;
            }
            if (current instanceof GrammarGroup) {
                GrammarGroup grammarGroup = (GrammarGroup)current;
                boolean result = grammarGroup.firstNonCaptureToken(token, contextSize, fromStart);
                if (result || !grammarGroup.isOptional()) return result;
                continue;
            }
            //Determines whether the scanned token has the same value as the one passed
            return current.equalsValue(token);
        }
        return false;
    }

    /**
     * Determines whether the passed token is currently in a reset position. This happens if a token has a pattern
     * which is repeatable. Once the pattern has completed its state will be reset.
     * @param token The token to determine whether it is in the reset (or default) state
     * @return Returns true if the token is in the default state
     */
    public boolean isReset(Token<?> token) {
        if (getTokenPosition() != 0) return false;
        if (!(token instanceof GrammarGroup)) return true;
        return isReset(token.getPatternTokens().get(getTokenPosition()));
    }

    /**
     * This task is usually performed once the token has been fully processed and all child token groups have been
     * read and assigned to. As part of this process, new token groups are added for repeatable grammar groups so
     * that read tokens can be stored. Because it is unknown whether the current repeatable pattern has finished, a
     * new token group is created and is therefore left once the token has completed. This method searches for empty
     * groups and removes all of these groups as well as other cleanup tasks.
     * @param finalClean Determines whether this is the final clean before being returned from the lexer
     */
    public void clean(boolean finalClean) {
        int counter = 0;
        if (finalClean) {
            /* During the final clean, verify that the required capture groups are counted before removal.
             * However, this can lead to groups being removed (even if empty) which are expected in the
             * structure. */
            for (int j = 0; j < getPatternTokens().size(); j++) {
                if (getPatternTokens().get(j) instanceof GrammarToken) {
                    GrammarToken<?> grammarToken = (GrammarToken<?>) getPatternTokens().get(j);
                    if (grammarToken.isCaptureToken()) counter++;
                }
            }
        }
        boolean dontCleanRoot = finalClean && counter == getTokenGroups().size();
        for (int i = getTokenGroups().size() - 1; i > -1; i--) {
            TokenGroup current = getTokenGroups().get(i);
            //Clear pattern tokens as they are no longer required once the current token has completed
            if (Objects.nonNull(current.getPatternTokens())) current.getPatternTokens().clear();
            //Recursively scans for empty groups and removes them
            if (!containsNonTokenGroup(current)) {
                if (!dontCleanRoot) {
                    getTokenGroups().remove(i);
                }
            }
            current.setGroupId("");
            //Clear any remaining token group IDs recursively as they are no longer needed
            clearIds(current);
        }
    }

    /**
     * An overloaded version of the clean method which is used through the lexing process. This ensures that
     * blank groups are not left in the structure when tokenising the expression
     */
    public void clean() {
        clean(false);
    }

    /**
     * Searches recursively through a token group to determine if it has any tokens which aren't token groups
     * themselves. If there are then the caller method can know to leave the current token group as there are
     * valid captured tokens.
     * @param group The group to scan for non-token groups
     * @return Returns true if recursively it has found at least one non-token group token
     */
    private boolean containsNonTokenGroup(TokenGroup group) {
        boolean containsNonToken = false;
        for (int i = group.getTokens().size() - 1; i > -1; i--) {
            Token<?> current = group.getTokens().get(i);
            if (Objects.nonNull(current.getPatternTokens())) current.getPatternTokens().clear();
            if (!(current instanceof TokenGroup)) {
                containsNonToken = true;
                continue;
            }
            boolean removed = false;
            if (((TokenGroup) current).getTokens().isEmpty()) {
                group.getTokens().remove(i);
                removed = true;
            }
            if (!removed && !containsNonTokenGroup((TokenGroup)current)) {
                group.getTokens().remove(i);
            }
        }
        return containsNonToken || !group.getTokens().isEmpty();
    }

    /**
     * Recursively scans through a token group structure clearing any group ID's
     * @param group The group to scan for IDs and clear
     */
    private void clearIds(TokenGroup group) {
        for (Token<?> token : group.getTokens()) {
            if (token instanceof TokenGroup) {
                ((TokenGroup) token).setGroupId("");
                clearIds((TokenGroup) token);
            }
        }
    }

    /**
     * Resets the position of the current token. This is typically used in repeatable grammar groups.
     */
    public void resetTokenPosition() {
        this.tokenPosition = 0;
        if (Objects.isNull(getPatternTokens())) return;
        //Recursively resets the position of all child pattern tokens
        for (Token<?> token : getPatternTokens()) {
            token.resetTokenPosition();
        }
    }

    public Object clone() {
        try {
            Token<?> cloned = (Token<?>)super.clone();
            //Performs a deep clone of any pattern tokens
            if (cloned instanceof GrammarGroup) {
                List<Token<?>> patternTokens = cloned.getPatternTokens();
                cloned.setPatternTokens(patternTokens.stream().map(Token::clone).map(o -> (Token<?>)o).collect(Collectors.toList()));
            }
            return cloned;
        } catch (CloneNotSupportedException ex) {
            throw new LexerException("Cloning wasn't supported on token: " + this);
        }
    }

    /**
     * This overloaded version of the getActiveToken method omits the groupOnly and instead returns the current
     * active token.
     * @param nonComplete Determines whether to return the active complete at the lowest level, or if this option is
     *                    enabled return further up the tree to the pattern token which is not complete but holds the
     *                    current complete token.
     * @return Returns the current active token in the pattern
     */
    public Token<?> getActiveToken(boolean nonComplete) {
        return getActiveToken(nonComplete, false);
    }

    /**
     * Recursively scans the pattern structure to find the current active token. The groupOnly field determines
     * whether it is restricted to only GrammarGroup or can include GrammarValue and GrammarExpression tokens.
     * @param nonComplete Determines whether to return the active complete at the lowest level, or if this option is
     *                    enabled return further up the tree to the pattern token which is not complete but holds the
     *                    current complete token.
     * @param groupOnly If true then it will return the lowest GrammarGroup token containing the current active token.
     * @return Returns the current active token in the pattern
     */
    public Token<?> getActiveToken(boolean nonComplete, boolean groupOnly) {
        return getActiveToken(nonComplete, groupOnly, false);
    }

    /**
     * By default optionals are excluded from finding the active token. This overloaded method of the getActiveToken
     * function allows to include those as a response.
     * @param nonComplete Determines whether to return the active complete at the lowest level, or if this option is
     *                    enabled return further up the tree to the pattern token which is not complete but holds the
     *                    current complete token.
     * @param groupOnly If true then it will return the lowest GrammarGroup token containing the current active token.
     * @param includeOptionals Determines whether active optionals are returned
     * @return The active token selectively determined by the parameters
     */
    public Token<?> getActiveToken(boolean nonComplete, boolean groupOnly, boolean includeOptionals) {
        if (Objects.isNull(getPatternTokens()) || getPatternTokens().isEmpty() ||
                getTokenPosition() >= getPatternTokens().size()) return null;
        Token<?> current = getPatternTokens().get(getTokenPosition());
        if (current instanceof GrammarGroup && (includeOptionals || !((GrammarGroup) current).isOptional())) {
            if (!current.isComplete()) {
                Token<?> result = current.getActiveToken(nonComplete, groupOnly);
                if (result instanceof GrammarGroup && groupOnly) return result;
                return !(result instanceof GrammarGroup) && groupOnly ? current : result;
            } else {
                return current;
            }
        }
        return current;
    }

    /**
     * Scans the pattern tree for the current active grammar group (if one exists).
     * @return Returns the current active pattern group
     */
    public Token<?> getActiveGroup() {
        Token<?> current = getPatternTokens().get(getTokenPosition());
        if (current instanceof GrammarGroup && !((GrammarGroup) current).isOptional()) {
            Token<?> found = current.getActiveGroup();
            if (Objects.isNull(found)) {
                return current;
            } else {
                return found;
            }
        }
        return null;
    }

    /**
     * An overloaded version of the getContainedTokensOfType which allows an index to be provided instead of a
     * TokenGroup. This method searches the groups hierarchy for matching tokens with the provided name.
     * @param searchGroup The index of the group to search from the current token
     * @param name The name of the token to match within the current tokens group
     * @return Returns all found tokens matching that given name / type
     */
    public List<Token<?>> getContainedTokensOfType(int searchGroup, String name) {
        TokenGroup sourceGroup = getTokenGroups().get(searchGroup);
        return getContainedTokensOfType(sourceGroup, name);
    }

    /**
     * Searches the passed token group for all tokens found in its structure which match the tokens name. The name
     * of the token is a fixed static value and represents its type in the language. As such, this method can be
     * used to find all 'Integer' tokens within the structure. To find the name of a given token, please see that
     * tokens implementation or if not currently implemented, call the (name, value) constructor from that tokens
     * constructor.
     * @param sourceGroup The source group to scan for all matches of the given name / type
     * @param name The name / type of the tokens to find
     * @return The resulting list of all tokens matching that name / type
     */
    public List<Token<?>> getContainedTokensOfType(TokenGroup sourceGroup, String name) {
        List<Token<?>> result = new ArrayList<>();
        for (Token<?> current : sourceGroup.getTokens()) {
            if (current instanceof TokenGroup) {
                result.addAll(getContainedTokensOfType((TokenGroup)current, name));
            }
            if (current.isExpression()) {
                for (TokenGroup currentGroup : current.getTokenGroups()) {
                    result.addAll(getContainedTokensOfType(currentGroup, name));
                }
            }
            if (Objects.equals(current.getName(), name)) {
                result.add(current);
                continue;
            }
        }
        return result;
    }

    /**
     * When a value is trying to be resolved to a type, if multiple tokens exist which resolve to that type this
     * method is used to determine the default Token handler class to use. By default this is set to true and only
     * read when multiple token handlers are returned.
     * @return Returns true if the current token class should be used as the default if multiple other handlers
     * are found.
     */
    public boolean isDefault() {
        return false;
    }

    /**
     * This determines whether the current token is an expression in itself. The default for this is false so that
     * the parser will look for certain patterns and flag if missing operators for resolved values. In this case
     * though this notifies the parser that it is a separate executable instance and can be treated separately.
     * @return Determines whether to treat the token as it's own expression skipping traditional parser rules
     */
    public boolean isExpression() { return false; }

    /**
     * Used during the post-lexing stage to determine if the current token is still expecting a token to be specified.
     * This typically happens where a developer has left out a brace or some other character in the statement.
     * This only works for tokens which use the grammar pattern and is there, along with the ErrorHandler implementation
     * to provide a more readable error prior to it blowing up in the Parser. In this scenario we determine what token
     * value is missing (if any) and flag that up to the developer.
     * @param context The lexer context which determines whether the expected token is valid
     * @return Returns an optional missing token from the current statement if an expected value is missing
     */
    public Optional<Token<?>> getExpectedToken(List<Token<?>> context) {
        if (getTokenPosition() != getPatternTokens().size() - 1) {
            return getExpectedToken(this);
        } else if (getPatternTokens().get(getTokenPosition()) instanceof GrammarGroup &&
                !((GrammarGroup)getPatternTokens().get(getTokenPosition())).isOptional()) {
            Optional<Token<?>> found = findExpectedInCompleteGroups();
            if (found.isPresent()) return found;
            GrammarGroup group = (GrammarGroup) getPatternTokens().get(getTokenPosition());
            return getExpectedToken(context, group, group.getTokenPosition());
        }
        return Optional.empty();
    }

    private Optional<Token<?>> findExpectedInCompleteGroups() {
        for (Token<?> current : getPatternTokens()) {
            if (current instanceof GrammarGroup) {
                if (current.getTokenPosition() != current.getPatternTokens().size()) {
                    return getExpectedToken(current);
                }
            }
        }
        return Optional.empty();
    }

    /**
     * A recursive method to scan the pattern tokens and find the next valid expected token (if one exists)
     * @param token The token whose pattern tokens are being scanned
     * @return Returns an optional of the next expected token within the current group
     */
    public Optional<Token<?>> getExpectedToken(Token<?> token) {
        int counter = token.getTokenPosition() - 1;
        while (true) {
            if (++counter >= token.getPatternTokens().size()) return Optional.empty();
            Token<?> current = token.getPatternTokens().get(counter);
            if (((GrammarToken)current).isOptional()) continue;
            if (current instanceof GrammarGroup && ((GrammarGroup)current).isTechnicallyComplete()) continue;
            if (current instanceof GrammarExpression) continue;
            if (Objects.nonNull(current.getValue()) && current.getValue().toString().equalsIgnoreCase("val")) continue;
            if (current instanceof GrammarGroup)
                return getExpectedToken(current);
            return Optional.of(current);
        }
    }

    /**
     * In the case where the current group is the last one in the list of pattern tokens, this performs a scan
     * to try and find any token which is required but has not yet been reached. It may be the case that this
     * is a technically valid and complete token, as such an empty Optional will be returned.
     * @param context The context to determine whether any tokens have been left in short term storage which
     *                highlights a potential issue
     * @param group The group whose pattern tokens are being recursively scanned for an expected token
     * @param tokenPosition The current token position used to increment and scan the pattern tokens
     * @return Returns either an empty optional if the current token is valid and complete or the expected token
     */
    private Optional<Token<?>> getExpectedToken(List<Token<?>> context, GrammarGroup group, int tokenPosition) {
        tokenPosition = Math.min(tokenPosition, group.getPatternTokens().size() - 1);
        if (tokenPosition != group.getPatternTokens().size() - 1) {
            Token<?> suspect = group.getPatternTokens().get(tokenPosition + 1);
            if ((!(suspect instanceof GrammarGroup && ((GrammarGroup)suspect).isOptional()) && !group.isMultiple()) ||
                (!group.isOptional() && group.isMultiple() && !context.isEmpty() && countNonOptionals(group) > 1)) {
                return Optional.of(suspect);
            }
        }
        return Optional.empty();
    }

    /**
     * Finds the first token group with tokens within the current groups structure
     * @param ignoreExpressions Ignores expression tokens as a special case
     * @return Returns the first group with tokens
     */
    public Optional<TokenGroup> findGroupWithTokens(boolean ignoreExpressions) {
        if (!(this instanceof TokenGroup)) return Optional.empty();
        for (Token<?> current : ((TokenGroup)this).getTokens()) {
            if (ignoreExpressions && current.isExpression()) {
                for (TokenGroup group : current.getTokenGroups()) {
                    Optional<TokenGroup> subGroupResult = group.findGroupWithTokens(ignoreExpressions);
                    if (subGroupResult.isPresent())
                        return subGroupResult;
                }
            }
            if (!(current instanceof TokenGroup))
                return Optional.of((TokenGroup)this);
            Optional<TokenGroup> subGroupResult = current.findGroupWithTokens(ignoreExpressions);
            if (subGroupResult.isPresent())
                return subGroupResult;
        }
        return Optional.empty();
    }

    /**
     * Counts the number of required tokens in the current grammar group. This is so that if only one value / group is
     * required with a number of optionals, we don't want to return this as an expected if the current group is repeatable
     * @param group the current group to count the number of required pattern tokens
     * @return Returns the number of required tokens within the group
     */
    private int countNonOptionals(GrammarGroup group) {
        int counter = 0;
        for (Token<?> current : group.getPatternTokens()) {
            if (current instanceof GrammarToken) {
                if (!((GrammarToken<?>)current).isOptional()) counter++;
            }
        }
        return counter;
    }

    /**
     * Adds the ability to set the value referenced by the current token. The default response for this across all
     * tokens will be to throw an exception. For those tokens on which it is overridden, it allows values to be set
     * on what was originally immutable values / structures. This is how native support for variables was added
     * as they are represented by the default TokenValue class.
     * @param parser The parser used to resolve child token expressions
     * @param context The context for resolving object references
     * @param value The value which is intended to be set against the current token reference
     * @return Returns the value once it has been set
     */
    public Token<?> set(SLOPParser parser, SLOPContext context, Token<?> value) {
        throw new ParserException(String.format("Expected variable, but found immutable '%s' whilst trying to assign value '%s'",
                this.getClass().getSimpleName(), String.format("%s(%s)", value.getClass().getSimpleName(), value.getValue())));
    }

    /**
     * This can be overridden by individual tokens to make it easier to read the contents. This can be used in the syntax
     * checker to display tokens and for observing their contents in the debugger.
     * @return Returns a more simplistic version of the token serialised to String
     */
    public String toSimpleString() {
        return toString();
    }

    /**
     * Gets the index position of the last pattern token which isn't optional.
     * @return The index of the last required token
     */
    public int getLastNonOptionalPosition() {
        for (int i = getPatternTokens().size() - 1;i > -1;i--) {
            if (!((GrammarToken)getPatternTokens().get(i)).isOptional()) return i;
        }
        return 0;
    }

    /**
     * Determines whether to prioritise the evaluation of this token before other tokens when being evaluated. This
     * is so that in the case where we define a Function for example, all functions should be evaluated first so that
     * their references are on the heap regardless of whether a call to them is made before their definition.
     * @return Returns true if the parser needs to evaluate this token first
     */
    public boolean hasParserPriority() {
        return false;
    }

    /**
     * This allows tokens to exclude themselves from the scope applied to objects and variables. As such, if this is
     * disabled then any variable declared within it will be globally scoped, or if the child of another scoped token
     * within the scope of that.
     * @return Returns true if the token adheres to normal variable scoping
     */
    public boolean isScoped() { return true; }

    /**
     * This method can be overloaded by those tokens which are to be matched, but don't wish to be processed by the
     * lexer or the parser. This is useful for things like comments which match the content but are then discarded.
     * @return Returns true if the content matched by this token is not to be processed
     */
    public boolean isIgnored() { return false; }

    /**
     * Provides the ability for a token to modify how it represents itself to the parser. This a token which may have
     * one behaviour to exhibit another if desired. For example, a 'return' token may wish itself to be configurable
     * so that if it exists in a loop a flag can be passed so that it will return each value from each iteration can
     * be returned as opposed to its normal single break and return functionality.
     * @param flag The flag to set against the current token
     */
    protected void addParserFlag(ParserFlag flag) {
        parserFlags.add(flag);
    }

    /**
     * If overridden and specified, this allows a token type to restrict the token classes to which it can be a direct
     * descendant of. This instructs the lexer to throw an error if this is specified and the parent types do not match.
     * If none are specified or this method is not overridden, then the default is to allow all.
     * @return Returns the list of classes which are permitted to be the current tokens parent.
     */
    public List<Class<?>> getParentRestrictions() {
        return new ArrayList<>();
    }

    /**
     * Determines the depth to check for parents. By default this is 2 to represent the token of
     * the immediate parent and that parent. This is because body elements may also be included in
     * the hierarchy. This can be overridden depending on how strict or lax a specific token requires.
     * @return Returns the parent depth to check for the getParentRestrictions method
     */
    public int checkParentDepth() {
        return 2;
    }
}
