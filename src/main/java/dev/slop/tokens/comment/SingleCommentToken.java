package dev.slop.tokens.comment;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;

import java.util.List;
import java.util.Optional;

/**
 * A single line comment which is terminated by a new line character. This value is discarded by
 * the lexer during processing.
 */
public class SingleCommentToken extends Token<Void> {

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "^\\/\\/.*[\\r\\n]";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    protected List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return null;
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return new SingleCommentToken();
    }

    /**
     * See {@link Token#isIgnored() isIgnored}
     */
    @Override
    public boolean isIgnored() { return true; }
}
