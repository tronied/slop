package dev.slop.tokens.comment;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;

import java.util.List;
import java.util.Optional;

/**
 * Matches a multi-line comment using the traditional /* .. *\/ notation. This is useful when the
 * expression only exists on a single line (technically, but split String), but where commenting
 * is helpful. This value is discarded by the lexer during processing.
 */
public class MultiCommentToken extends Token<Void> {

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "^\\/\\*([^*]|[\\r\\n]|(\\*+([^*/]|[\\r\\n])))*\\*+\\/";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    protected List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return null;
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return new MultiCommentToken();
    }

    /**
     * See {@link Token#isIgnored() igIgnored}
     */
    @Override
    public boolean isIgnored() { return true; }
}
