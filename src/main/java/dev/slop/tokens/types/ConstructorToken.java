package dev.slop.tokens.types;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * The constructor acts as a way to define the fields within a class or at least initialise those values.
 * If no parameters have been defined in the type definition itself, then the type looks to these constructors
 * as a minimum set of requirements to be fulfilled when instantiating the type.
 */
public class ConstructorToken extends Token<Void> {

    public ConstructorToken() {
        super("Constructor", null);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new ConstructorToken());
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "'this' '(' ( val ',' )+ ')' [ singleLine, multiLine ]";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    protected List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return Collections.singletonList(parser.processExpression(getTokenGroups().get(0).getTokens(), context));
    }

    /**
     * See {@link Token#getParentRestrictions() getParentRestrictions}
     */
    @Override
    public List<Class<?>> getParentRestrictions() {
        return Collections.singletonList(TypeToken.class);
    }
}
