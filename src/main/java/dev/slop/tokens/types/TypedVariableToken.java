package dev.slop.tokens.types;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Given that all the default SLOP functionality is typeless, this is a proof of concept to show it working with
 * type restrictions on variables. To defined a typed variable this example uses a 'let' keyword along with a
 * type name, value and assignment expression. If the resulting type from the expression trying to be assigned
 * does not match the defined type name then an error is thrown. For example:
 *
 *      let Integer myInt = 1;
 *
 *  This would be fine, however when trying to use:
 *
 *      let Integer myInt = 'hello!';
 *
 *  This would throw an error as the two types do not match.
 */
public class TypedVariableToken extends Token<Void> {

    public TypedVariableToken() {
        super("TypedVariable", null);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new TypedVariableToken());
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "'let' val:String val:String '=' expr";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    protected List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        if (getTokenGroups().size() < 3) {
            throw new ParserException("Expected three token groups to be present <Type> <Variable Name> <Value>. Found " +
                    getTokenGroups().size());
        }
        String typeId = getTokenGroups().get(0).getFlatTokens().get(0).getValue().toString();
        //Check given type against all known types (literal or in context)
        List<String> availableTypeNames = config.getTokenHandlers().stream()
                .filter(th -> th.getPatternType() == PatternType.REGEX)
                .map(Token::getName)
                .collect(Collectors.toList());
        if (!availableTypeNames.contains(typeId) && !context.getPrototypes().contains(typeId)) {
            throw new ParserException(String.format("Type '%s' provided but it is not a registered literal or prototype",
                    typeId));
        }
        //Check given type against value being set against it
        if (!getTokenGroups().get(2).getFlatTokens().get(0).getName().equals(typeId)) {
            throw new ParserException(String.format("Could not assign type '%s' to '%s'!",
                    getTokenGroups().get(2).getFlatTokens().get(0).getName(), typeId));
        }
        return Collections.singletonList(getTokenGroups().get(2).getFlatTokens().get(0));
    }
}
