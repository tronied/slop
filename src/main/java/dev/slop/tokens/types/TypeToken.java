package dev.slop.tokens.types;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.enums.VariableType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenGroup;
import dev.slop.tokens.TokenInstance;
import dev.slop.tokens.TokenParameters;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.literals.NullToken;
import dev.slop.tokens.statements.reference.MultiLineToken;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A type defines a template with fields so that an instance can be created using the CreateTypeToken. When it is
 * initially called by the parser, all resources from its parent are added into its structure and it is added as
 * a prototype into memory. The result from the creation of an instance can be stored into a variable in memory.
 * Modifications to the FieldToken have been made to allow field references and functions to be referred to, but
 * could be expanded in the future to add other functionality such as types. A type can be defined using the following:
 *
 *      type MyType(a,b,c) &lt;- ParentType {
 *          ...
 *      }
 *
 * The parent type and list of parameters is optional. An alternative to using the type parameters is instead to
 * define them using the ConstructorToken:
 *
 *      type MyType {
 *          this(a,b,c);
 *      }
 *
 * This effectively does the same thing. What this allows you to do though is have overloaded constructors with
 * different fields.
 */
public class TypeToken extends Token<LinkedHashMap<String, Object>> implements TokenInstance {

    private HashMap<String, Object> resourceMap;

    public TypeToken(LinkedHashMap<String, Object> map) {
        super("TypeToken", map);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<LinkedHashMap<String, Object>> createToken(String value) {
        return cloneDefaultProperties(new TypeToken(new LinkedHashMap<>()));
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "'type' val:String ( '(' ( val:String ','? )+? ')' )? ( '<-' val:String )? [ multiLine ]";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    protected List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        resourceMap = new HashMap<>();
        while (getTokenGroups().size() < 4) getTokenGroups().add(new TokenGroup());
        if (!getTokenGroups().get(1).getFlatTokens().isEmpty() &&
                !getContainedTokensOfType(getTokenGroups().get(3), "Constructor").isEmpty()) {
            throw new ParserException("Found both type param definitions and constructors. The current type " +
                    "implementation only supports one at a time.");
        }
        if (getTokenGroups().get(0).getFlatTokens().size() != 1) {
            throw new ParserException(String.format("Expected a single TokenValue for the function name but found %d tokens provided",
                    getTokenGroups().get(0).getTokenPosition()));
        }
        String typeName = getTokenGroups().get(0).getFlatTokens().get(0).getValue().toString();
        List<Token<?>> dependencies = getDependencies().stream()
                .map(context::getContextObject)
                .map(t -> (Token<?>)t)
                .collect(Collectors.toList());
        //Add resources from parent to current prototype
        TokenGroup bodyGroup = getTokenGroups().get(3).findGroupWithTokens(true)
                .orElse(getTokenGroups().get(3));
        for (Token<?> dependency : dependencies) {
            if (!(dependency instanceof TypeToken)) {
                throw new ParserException(String.format("Expected parent to be of type 'TypeToken' but instead found '%s'",
                        dependency.getClass().getSimpleName()));
            }
            TypeToken parentType = (TypeToken) dependency;
            //Verify parent type requirements are satisfied
            verifyParentGroupParameters(parentType);
            TokenGroup parentTokenGroup = parentType.getTokenGroups().get(3);
            if (!parentTokenGroup.getTokens().isEmpty()) {
                Token<?> body = parentType.getTokenGroups().get(3).getTokens().get(0);
                if (!(body instanceof MultiLineToken)) {
                    throw new ParserException(String.format("Expected type to have a body section of type 'MultiLineToken' " +
                            "but instead found '%s'", body.getClass().getSimpleName()));
                }
                TokenGroup parentTokens = parentType.getTokenGroups().get(3).findGroupWithTokens(true)
                        .orElse(new TokenGroup());
                bodyGroup.getTokens().addAll(parentTokens.getTokens());
            }
        }
        for (Token<?> token : bodyGroup.getTokens()) {
            if (token instanceof TokenParameters) {
                resourceMap.put(((TokenParameters)token).getIdentifier(), token);
            }
        }
        context.set(typeName, this, VariableType.PROTOTYPE);
        return Collections.singletonList(new NullToken());
    }

    private void verifyParentGroupParameters(TypeToken parent) {
        List<Token<?>> parentConstructors = parent.getContainedTokensOfType(parent.getTokenGroups().get(3), "Constructor");
        if (parentConstructors.isEmpty()) {
            List<String> parentParams = parent.getTokenGroups().get(1).getFlatTokens().stream()
                    .map(Token::getValue)
                    .map(Object::toString)
                    .collect(Collectors.toList());
            //Bail as there are no requirements from the parent
            if (parentParams.isEmpty()) return;
            List<Token<?>> constructors = getContainedTokensOfType(parent.getTokenGroups().get(3), "Constructor");
            if (constructors.isEmpty()) {
                List<String> params = getTokenGroups().get(1).getFlatTokens().stream()
                        .map(Token::getValue)
                        .map(Object::toString)
                        .collect(Collectors.toList());
                String missingParams = parentParams.stream()
                        .filter(parentParam -> !params.contains(parentParam))
                        .collect(Collectors.joining(","));
                if (!missingParams.isEmpty()) {
                    throw new ParserException(String.format("'%s' has a dependency on '%s' which requires parameter(s) '%s' " +
                            "to be defined in the class definition or constructor. Please add this to the %s type definition.",
                            getTypeName(), parent.getTypeName(), missingParams, getTypeName()));
                }
            }
        } else {
            List<String> paramNames = getTokenGroups().get(1).getFlatTokens().stream()
                    .map(Token::getValue)
                    .map(Object::toString)
                    .collect(Collectors.toList());
            boolean foundMatching = false;
            for (Token<?> constructor : parentConstructors) {
                List<String> requiredParams = constructor.getTokenGroups().get(0).getFlatTokens().stream()
                        .map(Token::getValue)
                        .map(Object::toString)
                        .collect(Collectors.toList());
                if (new HashSet<>(paramNames).containsAll(requiredParams)) {
                    foundMatching = true;
                    break;
                }
            }
            if (!foundMatching) {
                throw new ParserException(String.format("Could not find at least one match for the parameters declared " +
                        "in parent (%s) constructor", parent.getTypeName()));
            }
        }
    }

    /**
     * See {@link Token#hasParserPriority() hasParserPriority}
     */
    @Override
    public boolean hasParserPriority() {
        return true;
    }

    /**
     * See {@link TokenInstance#process(SLOPParser, SLOPContext, SLOPConfig, String, List) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config, String target,
                                  List<Token<?>> params) {
        //Check for no defined body
        if (getTokenGroups().size() < 4) return Collections.singletonList(new NullToken());
        if (Objects.nonNull(getValue()) && Objects.isNull(getValue().get(target))) {
            Token<?> foundFunction = this.getContainedTokensOfType(getTokenGroups().get(3), "Function").stream()
                    .filter(t -> Objects.equals(t.getTokenGroups().get(0).getFlatTokens().get(0).getValue(), target))
                    .findFirst()
                    .orElseThrow(() -> new ParserException(String.format("Could not find function '%s' in object '%s'",
                            target, getName())));
            //TODO - local function variable handkling with up-stack scanning
            return foundFunction.processWithParams(parser, context, config, params);
        }
        return Collections.singletonList(new TokenValue(getValue().get(target)));
    }

    /**
     * See {@link TokenInstance#getTypeName() getTypeName}
     */
    @Override
    public String getTypeName() {
        return getTokenGroups().get(0).getFlatTokens().get(0).getValue(String.class);
    }

    /**
     * See {@link TokenInstance#getDependencies() getTypeName}
     */
    @Override
    public List<String> getDependencies() {
        List<Token<?>> parents = getTokenGroups().get(2).getFlatTokens();
        if (!parents.isEmpty()) {
            return Collections.singletonList(parents.get(0).getValue(String.class));
        }
        return new ArrayList<>();
    }

    /**
     * See {@link TokenInstance#getResourceMap() getTypeName}
     */
    @Override
    public Map<String, Object> getResourceMap() {
        Map<String, Object> result = new HashMap<>();
        result.putAll(resourceMap);
        if (Objects.nonNull(getValue())) {
            result.putAll(getValue());
        }
        return result;
    }
}
