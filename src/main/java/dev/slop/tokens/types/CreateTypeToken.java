package dev.slop.tokens.types;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.enums.VariableType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;

import java.util.*;

/**
 * This token allows a new instance of a type to be created and returned. It won't enter itself into memory
 * like a prototype, but instead clones the given type defined by the name and copies across all resources from
 * that and it's dependencies. It is then returned so that it can be used (or stored in a variable). The definition
 * uses the following format:
 *
 *      new DefinedType(param1,param2,param3,...);
 *
 * The optional list of parameters are there if there are fields required to be passed when it is created. This
 * also applies for it's parent types.
 */
public class CreateTypeToken extends Token<Token<?>> {

    public CreateTypeToken() {
        super("createType", null);
    }

    /**
     * See {@link Token#createToken(String value) createToken}
     */
    @Override
    public Token<Token<?>> createToken(String value) {
        return cloneDefaultProperties(new CreateTypeToken());
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "'new' val:String '(' ( val ',' )+ ')'";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        if (token.equalsIgnoreCase("("))
            return Optional.of("A new keyword requires an open bracket to be defined for the parameter list " +
                    "e.g. new MyType (<-- a, b, c )");
        if (token.equalsIgnoreCase(")"))
            return Optional.of("A new keyword requires a closing bracket to be defined for the parameter list " +
                    "e.g. new MyType ( a, b, c )<--");
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    protected List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        String name = getTokenGroups().get(0).getFlatTokens().get(0).getValue(String.class);
        if (!context.getPrototypes().contains(name)) {
            throw new ParserException("No registered type found with the name " + name);
        }
        Token<?> result = (Token<?>) context.getContextObject(name);
        if (!(result instanceof TypeToken)) {
            throw new ParserException(String.format("Target type '%s' is not a TypeToken!",
                    result.getClass().getSimpleName()));
        }
        TypeToken clonedToken = (TypeToken) result.clone();
        //Set name of Type into the token name for traceability
        clonedToken.setName(name);
        LinkedHashMap<String, Object> typeStore = new LinkedHashMap<>();
        if (getTokenGroups().size() > 1 && !getTokenGroups().get(1).getTokens().isEmpty()) {
            List<Token<?>> actualParams = getTokenGroups().get(1).getFlatTokens();
            List<Token<?>> expectedParams = result.getTokenGroups().get(1).getFlatTokens();
            if (expectedParams.isEmpty()) {
                List<Token<?>> matchedConstructors = result.getContainedTokensOfType(3, "Constructor");
                if (matchedConstructors.stream()
                        .allMatch(c -> c.getTokenGroups().get(0).getFlatTokens().size() != actualParams.size())) {
                    throw new ParserException(String.format("Found %d constructors but none had %d parameters",
                            matchedConstructors.size(), actualParams.size()));
                }
                Optional<Token<?>> foundConstructor = matchedConstructors.stream()
                        .filter(c -> c.getTokenGroups().get(0).getFlatTokens().size() == actualParams.size())
                        .findFirst();
                if (foundConstructor.isPresent()) {
                    expectedParams = foundConstructor.get().getTokenGroups().get(0).getFlatTokens();
                }
            } else if (actualParams.size() != expectedParams.size()) {
                throw new ParserException(String.format("Could not invoke default constructor parameter requirement " +
                        "not met. Found %d instead of expected %d", getTokenGroups().get(1).getTokens().size(),
                        result.getTokenGroups().get(1).getTokens().size()));
            }
            for (int i = 0;i < expectedParams.size();i++) {
                typeStore.put(expectedParams.get(i).getValue(String.class), actualParams.get(i).getValue());
            }
        }
        clonedToken.setValue(typeStore);
        clonedToken.setVariableType(VariableType.INSTANCE);
        return Collections.singletonList(clonedToken);
    }
}
