package dev.slop.tokens.literals;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * The integer token stores values containing numeric values 2,147,483,647. If a longer number is required then it
 * should be specified using the 'L' character to denote a Long.
 */
public class IntegerToken extends Token<Number> {

    public IntegerToken() { super("Integer", 0); }

    public IntegerToken(BigInteger value) {
        super("Integer", value);
    }

    public IntegerToken(Integer value) {
        super("Integer", BigInteger.valueOf(value));
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "^-?[0-9]+";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return Collections.singletonList(this);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Number> createToken(String value) {
        return new IntegerToken(new BigInteger(value));
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof IntegerToken)) return false;
        IntegerToken compare = (IntegerToken)o;
        return BigInteger.valueOf(compare.getValue().longValue())
                .compareTo(BigInteger.valueOf(getValue().longValue())) == 0;
    }

    /**
     * Overrides and handles specific types of conversion of the result. Because the internal type
     * is a Number, a custom mapping has to occur to Integer form.
     * @param clazz The class type to cast the value to
     * @return Returns the mapped object or alternatively an empty Optional
     * @param <L> The type to which to convert the value
     */
    @Override
    protected <L> Optional<L> convert(Class<L> clazz) {
        if (clazz.isInstance(Integer.class)) {
            return Optional.of((L) Integer.valueOf(getValue().intValue()));
        } else if (clazz.isInstance(BigInteger.class)) {
            return Optional.of((L) getValue());
        }
        return Optional.empty();
    }

    /**
     * Overrides the default getValue() method so that it maps the result to the correct type
     * based on it's size. For example, if the value is less than the maximum Integer size then
     * it is returned in that form. Alternatively it will be returned as a BigInteger.
     * @return
     */
    @Override
    public Number getValue() {
        if (Objects.isNull(super.getValue())) return null;
        if (super.getValue().longValue() > Integer.MAX_VALUE) {
            return BigInteger.valueOf(super.getValue().longValue());
        } else {
            return super.getValue().intValue();
        }
    }
}
