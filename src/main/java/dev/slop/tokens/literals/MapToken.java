package dev.slop.tokens.literals;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenGroup;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Maps can be defined by using curly braces with key / value pairs being separated by a '-&gt;' tag. Calculations
 * can be performed both in the key and values and it is not type specific. For example, you could define a Map
 * using the following:
 *
 *      {1-&gt;1,\"second\"-&gt;2,3-&gt;\"third\"}
 *
 * There are several supported operators including using the '+' with another map. For example, you can add new
 * values to a map by doing the following: {1-&gt;1,2-&gt;2} + {3-&gt;3,4-&gt;4} = {1-&gt;1,2-&gt;2,3-&gt;3,4-&gt;4}. It also supports
 * subtracting values using both a Map and List. With the Map variant both the key and value must match the
 * element in the original Map for it to be removed. For example:
 *
 *      {1-&gt;1,2-&gt;2,3-&gt;3} - {2-&gt;2} = {1-&gt;1,3-&gt;3}
 *      {1-&gt;1,2-&gt;2,3-&gt;3} - {2-&gt;3} = No Change as key / value pairs do not match
 *
 * Using a subtraction along with a list of values will remove those entries in the map based on the keys. For
 * example:
 *
 *      {1-&gt;1,2-&gt;2,3-&gt;3,4-&gt;4} - [2,4] = {1-&gt;1,3-&gt;3}
 *
 * As with many other data structures, native calls can be used to fetch values so long as unsafe operations is
 * disabled by using {1-&gt;"first",2-&gt;"second",3-&gt;"third"}.get(2) = "second".
 */
public class MapToken extends Token<Map<Token<?>, Token<?>>> {

    public MapToken() { super(); }

    public MapToken(Map<Token<?>, Token<?>> values) {
        super("Map", values);
    }

    @Override
    public Token<Map<Token<?>, Token<?>>> createToken(String value) {
        MapToken source = new MapToken(new HashMap<>());
        Token<Map<Token<?>, Token<?>>> result = cloneDefaultProperties(source);
        result.setValue(source.getValue());
        return result;
    }

    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    @Override
    public String getPattern() {
        return "'{' ( ( val '->' expr ','? ) )+ '}'";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        if (token.equalsIgnoreCase("}"))
            return Optional.of("A map definition requires key / value pairs to be wrapped in '{' and '}' " +
                    "characters e.g. {'Bob'->1,'Sally'->2'}");
        if (token.equalsIgnoreCase("->"))
            return Optional.of("A map item definition requires key / value pairs to be separated by '->' " +
                    "e.g. {'Bob'->1,...}");
        return Optional.empty();
    }

    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        Map<Token<?>, Token<?>> aMap = Objects.isNull(getValue()) ? new HashMap<>() : getValue();
        //Handle blank initializer
        if (!getTokenGroups().isEmpty()) {
            Token<?> mapStructure = getTokenGroups().get(0).unwrapKeepStructure(getTokenGroups().get(0));
            if (mapStructure instanceof TokenGroup) {
                /* Expect map structure to take form of a TokenGroup with child TokenGroups of Key / Value pairs.
                 * If this is not the case then wrap the result in another TokenGroup as the unwrap has gone too far */
                if (((TokenGroup) mapStructure).getTokens().stream().anyMatch(t -> !(t instanceof TokenGroup))) {
                    mapStructure = new TokenGroup(Collections.singletonList(mapStructure));
                }
                ((TokenGroup) mapStructure).getTokens()
                        .forEach(tg -> {
                            Token<?> key = ((TokenGroup) tg).getTokens().get(0);
                            key = parser.processExpression(key instanceof TokenGroup ?
                                    ((TokenGroup) key).getTokens() : Collections.singletonList(key), context);
                            Token<?> value = ((TokenGroup) tg).getTokens().get(1);
                            value = parser.processExpression(value instanceof TokenGroup ?
                                    ((TokenGroup) value).getTokens() : Collections.singletonList(value), context);
                            aMap.put(key, value);
                        });
            } else {
                throw new ParserException(String.format("Cannot initialize map with token of type '%s'",
                        mapStructure.getClass().getSimpleName()));
            }
        }
        if (Objects.isNull(getValue())) setValue(aMap);
        return Collections.singletonList(this);
    }

    @Override
    public String toString() {
        if (Objects.isNull(getValue())) {
            return "MapToken{}";
        }
        String result = getValue().entrySet().stream()
                .map(e -> e.getKey().getValue().toString() + " -> " + e.getValue().getValue().toString())
                .collect(Collectors.joining(", "));
        return "MapToken{values = [" +
                result +
                "]}";
    }
}
