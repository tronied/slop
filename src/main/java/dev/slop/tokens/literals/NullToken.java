package dev.slop.tokens.literals;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import lombok.Builder;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * The null token represents the lack of a value. This could occur if declared directly within the Expression String,
 * is a value taken from an object itself is null or the result from an operation evaluates to null. In any traditional
 * operation, if one side is null then an exception will be thrown (see NullOperation).
 */
@Builder
public class NullToken extends Token<Void> {

    public NullToken() {
        super("null", null);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "^null";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return Collections.singletonList(this);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return new NullToken();
    }
}
