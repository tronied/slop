package dev.slop.tokens.literals;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * The decimal literal token stores any number with a whole number and a fractional part separated by a decimal (.)
 */
public class DecimalToken extends Token<BigDecimal> {

    public DecimalToken() { super("Double", BigDecimal.ZERO); }

    public DecimalToken(BigDecimal value) {
        super("Double", value);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        /* TODO - Currently only supports UK / US decimal values and needs localisation support added for countries
         *        like Germany which use a comma. This should be easy to do using NumberFormat and Locale classes */
        return "^-?[0-9]+\\.[0-9]+";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return Collections.singletonList(this);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<BigDecimal> createToken(String value) {
        return new DecimalToken(new BigDecimal(value));
    }
}
