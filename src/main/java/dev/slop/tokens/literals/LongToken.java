package dev.slop.tokens.literals;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * The Long token can store values greater than the maximum Integer value (2,147,483,647). This is declared by specifing
 * a traditional number with the addition of an 'L'.
 */
public class LongToken extends Token<Long> {

    public LongToken() { super("Long", 0L); }

    public LongToken(Long value) {
        super("Long", value);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "^-?([0-9]+)L";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return Collections.singletonList(this);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Long> createToken(String value) {
        return new LongToken(Long.parseLong(value));
    }
}
