package dev.slop.tokens.literals;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.enums.TokenType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenGroup;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Arrays can be defined as a literal using square brackets and values separated by commas. Arrays are not restricted
 * to one specific type and therefore defining the following: [1 + 2, 5 &gt; 4, "test"] once parsed will result in
 * the following:
 *
 *      [3 (Integer), true (Boolean), "test" (String)]
 *
 * Arrays have standard operations including add e.g. [1,2] + [3,4] = [1,2,3,4] and subtract [1,2,3] - [2,3] = [1].
 * See the ArrayOperation class for more details.
 */
public class ArrayToken extends Token<List<Token<?>>> {

    public ArrayToken() { super("Array", null); }

    public ArrayToken(List<Token<?>> values) {
        super("Array", values);
    }

    public ArrayToken(List<Token<?>> values, boolean populateTokenGroups) {
        super("Array", values);
        if (populateTokenGroups) {
            setTokenGroups(values.stream()
                    .map(i -> new TokenGroup(Collections.singletonList(i)))
                    .collect(Collectors.toList()));
        }
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<List<Token<?>>> createToken(String value) {
        ArrayToken source = new ArrayToken(new ArrayList<>());
        Token<List<Token<?>>> result = cloneDefaultProperties(source);
        result.setValue(source.getValue());
        return result;
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "'[' ( expr ','? )+ ']'";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return (token.equalsIgnoreCase("]")) ?
                Optional.of("An array definition requires items to be wrapped in '[' and ']' characters e.g. [ 1,2 ]<--") :
                Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        if (!getTokenGroups().isEmpty()) {
            TokenGroup tokenGroup = getTokenGroups().get(0);
            setValue(tokenGroup.processTokens(parser, context));
        } else {
            setValue(new ArrayList<>());
        }
        return Collections.singletonList(this);
    }

    @Override
    public String toString() {
        return String.format("[%s]", getTokenGroups().isEmpty() ? getValue() : getTokenGroups().get(0));
    }

    /**
     * See {@link Token#isScoped() isScoped}
     */
    @Override
    public boolean isScoped() {
        return false;
    }
}
