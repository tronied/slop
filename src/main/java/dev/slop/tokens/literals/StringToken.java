package dev.slop.tokens.literals;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.model.Expression;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;

/**
 * The string literal token is defined by using any text defined between a set of speech marks. Speech marks can be
 * escaped e.g. "some text\" some more", which will result in everything between the two outliers being added to the
 * StringToken's value. Strings can be added to any other type and result in a new String containing the culmination
 * of all. For example using 123 + "4" + true, will result in "1234true" (String).
 */
public class StringToken extends Token<String> {

    public StringToken() { super("String", null); }

    public StringToken(String value) {
        super("String", value);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "^(\\\"(.*?[^\\\\])\\\"|'(.*?[^\\\\])')";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * Overrides the default Token matching algorithm given the String regex matches against two distinct variants
     * with their own groups. The first is speech marks and the second is single quotes. They can only be paired up
     * with the same type but can be escaped to have either a quote or single-quote within a String definition.
     * @param expression The current state of the expression String being parsed to match against the current token
     * @param dryRun If a match is found, this determines whether to remove it from the expression
     * @return Returns the group match based on the String type being matched
     */
    @Override
    public Optional<Token<String>> match(Expression expression, boolean dryRun) {
        Matcher m = getMatcher(expression.getValue());
        if (m.find()) {
            if (!dryRun) expression.cutTo(m.end(0));
            return Optional.of(createToken(Objects.isNull(m.group(2)) ? m.group(3) : m.group(2)));
        }
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return Collections.singletonList(this);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<String> createToken(String value) {
        return new StringToken(value);
    }

    @Override
    public String toString() {
        return String.format("'%s'", getValue());
    }

    /**
     * See {@link Token#isDefault()} isDefault}
     */
    @Override
    public boolean isDefault() {
        return true;
    }
}
