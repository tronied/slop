package dev.slop.tokens.base;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.enums.TokenType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * This class is one of the base token handlers. These are there as fallbacks or general token types if no other token
 * matches the beginning of the Expression String. Non-Tokens are typically used to structure statements / groups in
 * the syntax. These may be one or more non alpha-numeric characters and are important for tracking and progression
 * in each token which uses a Grammar Pattern.
 */
public class NonToken extends Token<String> {

    public NonToken() { super(); }

    public NonToken(String value) {
        super("Non-Token", value);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        //Scans for every character which is not alpha-numeric
        return "^[^a-zA-Z0-9\\s]";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig)} process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return Collections.singletonList(this);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<String> createToken(String value) {
        return new NonToken(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token<?> token = (Token<?>) o;
        return getValue().equals(token.getValue());
    }
}
