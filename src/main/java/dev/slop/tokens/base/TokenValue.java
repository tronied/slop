package dev.slop.tokens.base;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.enums.TokenType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * This class is one of the base token handlers. These are there as fallbacks or general token types if no other token
 * matches the beginning of the Expression String. TokenValue's represent all alpha-numeric characters with the additional
 * underscore character for naming functions i.e. NEGATE_AND_ADD(...). The pattern can be extended to match other values
 * though this should not affect the integrity of the other tokens and changes should be kept to a minimum if possible.
 * TokenValue's are widely used throughout SLOP and are used for capturing values in statements which aren't specified
 * directly in the Grammar Pattern.
 */
@NoArgsConstructor
public class TokenValue extends Token<Object> {

    public TokenValue(Object value) {
        super("Token", value);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    /**
     * See {@link Token#getPattern() process}
     */
    @Override
    public String getPattern() {
        return "^[a-zA-Z0-9_]+";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig)} process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return Collections.singletonList(this);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Object> createToken(String value) {
        return new TokenValue(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token<?> token = (Token<?>) o;
        if (token.getValue(String.class).equalsIgnoreCase("*") ||
            getValue(String.class).equalsIgnoreCase("*")) return true;
        return getValue().equals(token.getValue());
    }

    @Override
    public String toString() {
        return Objects.isNull(getValue()) ? "null" : getValue().toString();
    }

    /**
     * See {@link Token#set(SLOPParser, SLOPContext, Token) createToken}
     */
    @Override
    public Token<?> set(SLOPParser parser, SLOPContext context, Token<?> value) {
        if (Objects.isNull(getValue()))
            throw new ParserException("Cannot assign a value to a TokenValue with a null value!");
        context.set(getValue().toString(), value);
        return value;
    }
}
