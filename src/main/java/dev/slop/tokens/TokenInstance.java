package dev.slop.tokens;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.parser.SLOPParser;

import java.util.List;
import java.util.Map;

/**
 * A token instance is typically used to represent objects which are created from another Token defined within the
 * the language. They may have resources defined within them and may have one or more dependencies on other tokens.
 */
public interface TokenInstance {

    /**
     * Processes the current token with an optional list of parameters. This is used if the token instance has a list
     * of parameters that be must be passed on creation.
     * @param parser The parser used to resolve child token expressions
     * @param context The context for resolving object references
     * @param config The configuration for resolution of property values
     * @param params The list of passed parameters
     * @return Returns the result after the token has been processed
     */
    List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config, String target,
                           List<Token<?>> params);

    /**
     * The name of the type or object that this instance it was created from
     * @return Returns the name of the object
     */
    String getTypeName();

    /**
     * Gets a list of dependencies of the current token instance. This is useful if the object on which this is based
     * requires other resources to be pulled into the current instance during creation. This can also be used for
     * potential uses with polymorphism.
     * @return Returns a list of dependencies for the current object
     */
    List<String> getDependencies();

    /**
     * A map of contained resources found within the current object instance. This is useful when referring to fields
     * or functions or anything else within the body implementation.
     * @return Returns a list of resources contained within the current object
     */
    Map<String, Object> getResourceMap();
}
