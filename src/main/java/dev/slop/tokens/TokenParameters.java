package dev.slop.tokens;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.parser.SLOPParser;

import java.util.List;

/**
 * This can be implemented by a token and adds a method of processing in which parameters can be passed
 * to it. This is useful for things like functions which are called via another token and those parameters
 * need to be passed between implementations.
 */
public interface TokenParameters {

    /**
     * Processes the current token with an optional list of parameters. This is useful for tokens that require
     * values to be passed directly between tokens.
     * @param parser The parser used to resolve child token expressions
     * @param context The context for resolving object references
     * @param config The configuration for resolution of property values
     * @param params The list of passed parameters
     * @return Returns the result after the token has been processed
     */
    List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config, List<Token<?>> params);

    /**
     * Gets the identifying name for the token. In the case of a function for example, this would be the name
     * declared before (or after dependant on syntax) the parameter list.
     * @return Returns the identifier for the current token
     */
    String getIdentifier();
}
