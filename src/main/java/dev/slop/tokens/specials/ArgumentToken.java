package dev.slop.tokens.specials;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * The argument token is a special token used for relaying values between different parts of a statement. In itself
 * this token does not exist outside of the statement implementation and is therefore a placeholder. For example,
 * given the Switch statement:
 *
 *      switch(2,3)[$0 &gt; $1: "nope", $1 &gt; $0: "yep"]
 *
 * We can make reference to each parameter and include them in the conditions. These have to be managed by the
 * statement code and if a reference is made outside of this then it will simply evaluate to the tokens String value.
 */
public class ArgumentToken extends Token<String> {

    public ArgumentToken() { super(); }

    public ArgumentToken(String value) {
        super("Argument", value);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<String> createToken(String value) {
        return new ArgumentToken(value);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "^\\$([a-zA-Z0-9]+)";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return Collections.singletonList(this);
    }

    @Override
    public String toString() {
        return String.format("$%s", getValue());
    }
}
