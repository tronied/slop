package dev.slop.tokens.specials;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.exception.ParserException;
import dev.slop.exception.SLOPException;
import dev.slop.lexer.SLOPLexer;
import dev.slop.model.LexerResult;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.literals.NullToken;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * An import token allows one or more external source files or pre-compiled files to be imported and their contained
 * resources to be used and referred to in the current expression or file. Each import allows one or more files to be
 * defined using a '@' token prior to a source file to flag that it should first be compiled before processing it.
 * The definition of this token will take the following form:
 *
 *      import 'MyCompiledFile', @'UncompiledSource.slp'
 */
public class ImportToken extends Token<Void> {

    public ImportToken() {
        super("Import", null);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new ImportToken());
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "'import' ( '@'<? val:String ','? )+";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    protected List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        List<Token<?>> imports = getTokenGroups().get(0).getFlatTokens();
        if (imports.isEmpty()) {
            throw new ParserException("Must declare at least one source or compiled file to import!");
        }
        boolean nextUncompiled = false;
        for (int i = 0;i < imports.size();i++) {
            String filePath = imports.get(i).getValue(String.class);
            if (Objects.equals(filePath, "@")) {
                nextUncompiled = true;
                continue;
            }
            if (!nextUncompiled) {
                performStandardLoad(parser, context, filePath);
            } else {
                performLexAndLoad(filePath, parser, context, config);
                nextUncompiled = false;
            }
        }
        return Collections.singletonList(new NullToken());
    }

    /**
     * Performs a standard load of a pre-compiled source file. This simply loads the data into a LexerResult object
     * and runs it through the parser. No result is returned as it is just to load the resources found within the file
     * @param parser The parser from which to run the pre-compiled data
     * @param context The context used to hold the data referenced in the expression
     * @param filePath The path of the file to be loaded
     */
    @SuppressWarnings("unchecked")
    private void performStandardLoad(SLOPParser parser, SLOPContext context, String filePath) {
        if (!Files.exists(Paths.get(filePath))) {
            System.err.println(String.format("Could not find source or object file '%s' - ignoring.", filePath));
        }
        try (FileInputStream fileIn = new FileInputStream(filePath);
             ObjectInputStream in = new ObjectInputStream(fileIn)) {
            Object lexedResult = in.readObject();
            if (Objects.nonNull(lexedResult)) {
                if (!(lexedResult instanceof LexerResult<?>)) {
                    throw new SLOPException(String.format("Unexpected object '%s' when reading from pre-compiled file",
                            in.getClass().getSimpleName()));
                }
                parser.process((LexerResult<Token<?>>) lexedResult, context);
            }
        } catch (IOException | ClassNotFoundException ex) {
            throw new SLOPException("Unable to tokens to file: " + ex.getMessage());
        }
    }

    /**
     * Loads a source file from the given file path and passes it through a new instance of the Lexer created
     * using the config object. From there the result of that is passed into the parser. No result is returned
     * from this as it is simply to load the contained resources into memory.
     * @param filePath The path of the file where the source is to be loaded from
     * @param parser The parser from which to run the newly compiled data
     * @param context The context used to hold the data referenced in the expression
     * @param config The configuration used to create a new instance of the lexer
     */
    private void performLexAndLoad(String filePath, SLOPParser parser, SLOPContext context, SLOPConfig config) {
        SLOPLexer lexer = new SLOPLexer(config);
        String expression = readFileAsString(filePath);
        LexerResult<Token<?>> lexerResult = lexer.tokenize(expression);
        parser.process(lexerResult, context);
    }

    private String readFileAsString(String filePath) {
        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8)) {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        } catch (IOException e) {
            throw new SLOPException("Error reading token file: " + e.getMessage());
        }
        return contentBuilder.toString();
    }

    @Override
    public String toString() {
        String printable = getTokenGroups().stream().map(Object::toString).collect(Collectors.joining(" "));
        return "ImportToken{" + printable + "}";
    }

    /**
     * See {@link Token#hasParserPriority() hasParserPriority}
     */
    @Override
    public boolean hasParserPriority() {
        return true;
    }
}
