package dev.slop.tokens.specials;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.ParserFlag;
import dev.slop.enums.PatternType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.literals.NullToken;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * As with many languages, the return token is used to return a value from the current statement
 * or code block. It makes use of the parser flags to let the parser and parent tokens know how to
 * handle it correctly. The return has an optional '&amp;' flag which can be used in loops. This allows
 * multiple values to be returned from a loop rather than just a single. If no '&amp;' character is
 * specified in a loop then it will just break out and return that single value as a result.
 */
public class ReturnToken extends Token<Token<?>> {

    public ReturnToken(Token<?> token) {
        super("return", token);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Token<?>> createToken(String value) {
        return cloneDefaultProperties(new ReturnToken(null));
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "'return' '&'<? expr";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    protected List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        if (getTokenGroups().isEmpty()) {
            addParserFlag(ParserFlag.RETURN);
            setValue(new NullToken());
        } else {
            List<Token<?>> returnResult = getTokenGroups().get(0).getFlatTokens();
            if (returnResult.isEmpty()) return returnResult;
            if (Objects.nonNull(returnResult.get(0).getValue()) && returnResult.get(0).getValue().equals("&")) {
                returnResult.remove(0);
                addParserFlag(ParserFlag.RETURN_GROUP);
            } else {
                addParserFlag(ParserFlag.RETURN);
            }
            setValue(parser.processExpression(returnResult, context));
        }
        //Returns itself with the result contained within
        return Collections.singletonList(this);
    }

    @Override
    public String toString() {
        String printable = getTokenGroups().stream().map(Object::toString).collect(Collectors.joining(" "));
        return "ReturnToken{" + printable + ", value = " + getValue() + "}";
    }
}
