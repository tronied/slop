package dev.slop.tokens.specials;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.literals.ArrayToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Allows a range of characters or numbers to be specified. This will return an inclusive array of
 * the start and end values along with all values between. This uses the standard ASCII encoding by
 * converting a character into it's integer value and looping between the two values to return all
 * applicable values.
 */
public class RangeToken extends Token<Void> {

    public RangeToken() {
        super("range", null);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new RangeToken());
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "val '..' val";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    protected List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        if (getTokenGroups().size() < 2) {
            throw new ParserException("Expected an upper bound and a lower bound in a range!");
        }
        List<Token<?>> lowerTokens = getTokenGroups().get(0).getFlatTokens();
        List<Token<?>> upperTokens = getTokenGroups().get(1).getFlatTokens();
        if (lowerTokens.size() != 1 || upperTokens.size() != 1) {
            throw new ParserException("Expected only 1 token in both the lower and upper range specification!");
        }
        String lowerValue = lowerTokens.get(0).getValue().toString();
        String upperValue = upperTokens.get(0).getValue().toString();
        List<Token<?>> result = new ArrayList<>();
        try {
            int lower = Integer.parseInt(lowerValue);
            int upper = Integer.parseInt(upperValue);
            if (lower < upper) {
                for (int i = lower; i <= upper; i++) {
                    result.add(new TokenValue(i));
                }
            } else {
                for (int i = lower; i >= upper; i--) {
                    result.add(new TokenValue(i));
                }
            }
        } catch (NumberFormatException ex) {
            if (lowerValue.trim().length() != 1 || upperValue.trim().length() != 1) {
                throw new ParserException(String.format("Did not understand ranges %s to %s. Are you using a single character " +
                        "/ digit range?", lowerValue, upperValue));
            }
            char lowerChar = lowerValue.charAt(0);
            char upperChar = upperValue.charAt(0);
            if ((int) lowerChar < (int) upperChar) {
                for (int i = (int) lowerChar; i <= (int) upperChar; i++) {
                    result.add(new TokenValue(Character.toString((char)i)));
                }
            } else {
                for (int i = (int) lowerChar; i >= (int) upperChar; i--) {
                    result.add(new TokenValue(Character.toString((char)i)));
                }
            }
        }
        return Collections.singletonList(new ArrayToken(result));
    }
}
