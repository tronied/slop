package dev.slop.tokens.statements;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenGroup;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.literals.ArrayToken;
import dev.slop.tokens.literals.NullToken;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * The for each token is a statement which iterates over each object in a collection. They are common in most mainstream
 * languages and are useful by not having to concern yourself with indexes and assigning values. The grammar pattern
 * states that it starts with a 'foreach' word followed by a colon separated single capture value and an expression.
 * These represent the referencable variable to assign the object to in context and the collection.
 *
 * The body of the for each statement consists of one or more separate expressions which end with a semi-colon. Using
 * the following statement, we could do the following:
 *
 *      foreach (emp : acme.employees) hl = {?emp}.age / 2; result = {?emp}.name + "'s Half Age: " + {?hl};
 *
 * If there are two employees working for Acme named Mary (Age: 34), Bob (Age: 54) then we would get the following result:
 *
 *      [0] Mary's Half Age: 17
 *      [1] Bob's Half Age: 27
 *
 * A for each statement can have an unlimited number of expressions and later expressions can use stored variables
 * from earlier expressions (as above). The 'result' definition, although a normal variable, is handled specially by the
 * statement as that determines the result for the current iteration.
 *
 * @deprecated Please use the ForToken as a replacement
 */
@Deprecated
public class ForEachToken extends Token<Void> {

    public ForEachToken() {
        super("ForEach", null);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new ForEachToken());
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "'foreach' '(' ( val ':' expr ) ')' ( expr ';' )+";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        if (token.equalsIgnoreCase("("))
            return Optional.of("The for-each collection statement must be wrapped in braces e.g. " +
                    "foreach -->( obj : collection )");
        if (token.equalsIgnoreCase(")"))
            return Optional.of("The for-each collection statement must be wrapped in braces e.g. " +
                    "foreach ( obj : collection )<--");
        if (token.equalsIgnoreCase(";"))
            return Optional.of("Each for-each block must be terminated by a ';' character, even if there is only " +
                    "one e.g. foreach (...) result = 1 + 1 ;<--");
        if (groupsCount.contains(1)) {
            return Optional.of("Foreach statement requires at least one code block to be iterated e.g. " +
                    "foreach ( ... ) <code block1> ; <code block2> ; ...");
        }
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        List<Token<?>> arrayResult = new ArrayList<>();
        if (getTokenGroups().size() < 2) {
            throw new ParserException("Expected iteration arguments group and at least one expression to " +
                    "execute in the for each statement!");
        }
        TokenGroup iteratorArgs = getTokenGroups().get(0);
        if (iteratorArgs.getTokens().size() < 2) {
            throw new ParserException("Expected a variable and a collection provided in the for each iterator statement");
        }
        Token<?> varArg = iteratorArgs.getTokens().get(0);
        //Resolve the variable name to be used to store the iterated object in context
        String variableName = (varArg instanceof TokenGroup) ?
                ((TokenGroup)varArg).getFlatTokens().get(0).getValue(String.class) :
                varArg.getValue(String.class);
        Token<?> collectionArg = iteratorArgs.getTokens().get(1);
        Collection<?> collection = null;
        //Resolve the collection object
        if (collectionArg instanceof TokenGroup) {
            List<Token<?>> collectionArgs = ((TokenGroup)collectionArg).getFlatTokens();
            if (collectionArgs.size() == 1 && collectionArgs.get(0) instanceof TokenValue) {
                String collectionRef = collectionArgs.get(0).getValue(String.class);
                Object suspectedCollection = context.getVariable(collectionRef);
                if (!(suspectedCollection instanceof Collection)) {
                    throw new ParserException(String.format("The reference used in the for each loop (%s) did not result " +
                            "in a collection. Instead found '%s'", collectionRef, suspectedCollection.getClass().getSimpleName()));
                }
                collection = (Collection<?>)suspectedCollection;
            } else {
                collection = parser.processExpression(collectionArgs, context).getValue(Collection.class);
            }
        }
        List<Token<?>> loopExpressions = getTokenGroups().get(1).getTokens();
        for (Object current : collection) {
            //Set the current object to the defined variable
            context.set(variableName, current);

            //Execute contained expressions
            AtomicBoolean setVar = new AtomicBoolean(false);
            loopExpressions.forEach(e -> {
                if (!(e instanceof TokenGroup)) {
                    throw new ParserException("Repeat loop expression was not found to be a token group");
                }
                Token<?> result = parser.processExpression(((TokenGroup)e).getFlatTokens(), context);
                if (!result.getParserFlags().isEmpty()) {
                    setVar.set(true);
                    arrayResult.add((Token<?>)result.getValue());
                }
            });
            //Backwards compatibility
            Object result = context.getContextObject(RESULT_VARIABLE);
            if (Objects.nonNull(result) && !setVar.get()) {
                arrayResult.add((result instanceof Token<?>) ? (Token<?>)result : new TokenValue(result));
            }
            //Clear result after it's been added to avoid bleed-through
            context.removeContextObject(RESULT_VARIABLE);
        }
        return Collections.singletonList(new ArrayToken(arrayResult));
    }
}
