package dev.slop.tokens.statements;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * An operation is simply a wrapper for other statements or expressions. This allows a certain order of execution
 * to be used when resolving expressions. For example, take the two expressions:
 *
 *      1 + (3 + 4) * 5
 *      1 + 3 + 4 * 5
 *
 * Both of the above would result in different outcomes without brackets (Operation).
 */
public class OperationToken extends Token<Void> {

    public OperationToken() {
        super("Operation", null);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "'(' expr ')'";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        if (token.equalsIgnoreCase(")"))
            return Optional.of("An operation requires the contained logic to be wrapped in a pair of braces " +
                    "e.g. ( <logic> )<--");
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        //Fetch all inner tokens within operation to process
        List<Token<?>> tokens = getTokenGroups().get(0).getFlatTokens();
        return Collections.singletonList(parser.processExpression(tokens, context));
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new OperationToken());
    }

    @Override
    public String toString() {
        String printable = getTokenGroups().stream().map(Object::toString).collect(Collectors.joining(" "));
        return "OperationToken{" + printable + "}";
    }
}
