package dev.slop.tokens.statements;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.literals.NullToken;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SingleLineToken extends Token<Void> {

    public SingleLineToken() {
        super("singleLine", null);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "expr ';'";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        if (getTokenGroups().isEmpty())
            return Collections.singletonList(new NullToken());
        //Fetch all inner tokens within line to process
        List<Token<?>> tokens = getTokenGroups().get(0).getFlatTokens();
        return Collections.singletonList(parser.processExpression(tokens, context));
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new SingleLineToken());
    }

    @Override
    public String toString() {
        String printable = getTokenGroups().stream().map(Object::toString).collect(Collectors.joining(" "));
        return "SingleLineToken{" + printable + "}";
    }

    /**
     * See {@link Token#toSimpleString() toSimpleString}
     */
    @Override
    public String toSimpleString() {
        String printable = getTokenGroups().stream().map(Token::toSimpleString).collect(Collectors.joining(" "));
        return printable + ",;";
    }

    @Override
    public boolean isExpression() {
        return true;
    }
}
