package dev.slop.tokens.statements.reference;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.enums.TokenType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.literals.NullToken;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * This token acts as a reusable body portion of any segment of code executed within a statement.
 * It uses the standard '{' and '}' characters to start and end multiple lines of expressions.
 * This token even though secondary in nature also uses a grammar reference of one or more single
 * line references.
 */
public class MultiLineToken extends Token<Void> {

    public MultiLineToken() {
        super("multiLine", null, TokenType.SECONDARY);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "'{' ( [ singleLine ] )+ '}'";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        if (getTokenGroups().isEmpty()) return Collections.singletonList(new NullToken());
        return getTokenGroups().stream()
                .map(tg -> parser.processExpression(tg.getFlatTokens(), context))
                .collect(Collectors.toList());
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new MultiLineToken());
    }

    @Override
    public String toString() {
        String printable = getTokenGroups().stream().map(Object::toString).collect(Collectors.joining(" "));
        return "MultiLineToken{" + printable + "}";
    }

    /**
     * See {@link Token#toSimpleString() toSimpleString}
     */
    @Override
    public String toSimpleString() {
        String printable = getTokenGroups().stream().map(Token::toSimpleString).collect(Collectors.joining(" "));
        return printable + ",;";
    }

    @Override
    public boolean isExpression() {
        return true;
    }
}
