package dev.slop.tokens.statements.reference;

import dev.slop.callback.ReferenceCallback;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.ParserFlag;
import dev.slop.enums.PatternType;
import dev.slop.enums.TokenType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenCallback;
import dev.slop.tokens.TokenGroup;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.literals.ArrayToken;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This token acts as one of the potential secondary tokens referenced in the looping ForToken.
 * This acts as a variable iteration loop where each item is assigned to a named variable. This
 * matches other languages with their respective for-each loops. This is invoked during execution
 * by the parent ForToken along with a callback. On each iteration it calls the callback routine
 * and handles the result.
 */
public class VariableLoopToken extends Token<Void> implements TokenCallback {

    public VariableLoopToken() {
        super("variableLoop", null, TokenType.SECONDARY);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "val ':' expr";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        if (token.equalsIgnoreCase(":"))
            return Optional.of("A variable for-each loop define a variable and collection separated by a ':' character e.g. " +
                    "'for ( employee :<-- acme.employees ) <some logic>'");
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        throw new ParserException(String.format("Unexpected process invocation of process to %s token without callback. " +
                        "This is an implementation issue and if required would suggest changing token type to PRIMARY.",
                this.getClass().getSimpleName()));
    }

    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config, ReferenceCallback callback) {
        List<Token<?>> arrayResult = new ArrayList<>();
        if (getTokenGroups().size() < 2) {
            throw new ParserException("Expected a variable and a collection provided in the for each iterator statement");
        }
        Token<?> varArg = getTokenGroups().get(0).getFlatTokens().get(0);
        //Resolve the variable name to be used to store the iterated object in context
        String variableName = (varArg instanceof TokenGroup) ?
                ((TokenGroup)varArg).getFlatTokens().get(0).getValue(String.class) :
                varArg.getValue(String.class);
        List<Token<?>> collectionArgs = getTokenGroups().get(1).getFlatTokens();
        Collection<?> collection;
        //Resolve the collection object
        if (collectionArgs.size() == 1 && collectionArgs.get(0) instanceof TokenValue) {
            String collectionRef = collectionArgs.get(0).getValue(String.class);
            Object suspectedCollection = context.getContextObject(collectionRef);
            if (!(suspectedCollection instanceof Collection)) {
                throw new ParserException(String.format("The reference used in the for each loop (%s) did not result " +
                        "in a collection. Instead found '%s'", collectionRef, suspectedCollection.getClass().getSimpleName()));
            }
            collection = (Collection<?>)suspectedCollection;
        } else {
            collection = parser.processExpression(collectionArgs, context).getValue(Collection.class);
        }
        for (Object current : collection) {
            //Set the current object to the defined variable
            context.set(variableName, current);
            Optional<Token<?>> result = callback.call();
            if (result.isPresent()) {
                if (result.get().getParserFlags().contains(ParserFlag.RETURN_GROUP)) {
                    arrayResult.add((Token<?>) result.get().getValue());
                } else if (result.get().getParserFlags().contains(ParserFlag.RETURN)) {
                    return Collections.singletonList((Token<?>) result.get().getValue());
                }
            }
        }
        return Collections.singletonList(new ArrayToken(arrayResult));
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new VariableLoopToken());
    }

    @Override
    public String toString() {
        String printable = getTokenGroups().stream().map(Object::toString).collect(Collectors.joining(" "));
        return "VariableLoopToken{" + printable + "}";
    }

    /**
     * See {@link Token#toSimpleString() toSimpleString}
     */
    @Override
    public String toSimpleString() {
        String printable = getTokenGroups().stream().map(Token::toSimpleString).collect(Collectors.joining(" "));
        return printable + ",;";
    }

    @Override
    public boolean isExpression() {
        return false;
    }
}
