package dev.slop.tokens.statements.reference;

import dev.slop.callback.ReferenceCallback;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.OperatorType;
import dev.slop.enums.ParserFlag;
import dev.slop.enums.PatternType;
import dev.slop.enums.TokenType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenCallback;
import dev.slop.tokens.TokenGroup;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.literals.ArrayToken;
import dev.slop.tokens.operators.OperatorHandler;
import dev.slop.tokens.operators.OperatorToken;
import dev.slop.tokens.specials.ReturnToken;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This token acts as one of the potential secondary tokens referenced in the looping ForToken.
 * This acts as a fixed iteration loop where an integer variable is declared with steps, initial
 * value and rule to finish the loop. This matches other languages with their respective for loops.
 * This is invoked during execution by the parent ForToken along with a callback. On each iteration
 * it calls the callback routine and handles the result.
 */
public class FixedLoopToken extends Token<Void> implements TokenCallback {

    public FixedLoopToken() {
        super("fixedLoop", null, TokenType.SECONDARY);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "expr ';' expr ';' expr";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        if (token.equalsIgnoreCase(";"))
            return Optional.of("An iterative for loop must have three sections separated by a ';' character e.g. " +
                    "'for ( i++ ;<-- 0 ;<-- <10 ) <some logic> ;'");
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        throw new ParserException(String.format("Unexpected process invocation of process to %s token without callback. " +
                "This is an implementation issue and if required would suggest changing token type to PRIMARY.",
                this.getClass().getSimpleName()));
    }

    /**
     * See {@link TokenCallback#process(SLOPParser, SLOPContext, SLOPConfig, ReferenceCallback) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config, ReferenceCallback callback) {
        List<Token<?>> arrayResult = new ArrayList<>();
        if (Objects.isNull(getTokenGroups().get(0)) || getTokenGroups().get(0).getTokens().isEmpty()) {
            throw new ParserException("The first token must be a reference and operator combination! Example: " +
                    "repeat(i++,<start value>,<end value>) <expression>;");
        }
        TokenGroup varWithOperator = getTokenGroups().get(0);
        if (varWithOperator.getTokens().size() >= 2) {
            Token<?> variable = varWithOperator.containsTokenType(TokenValue.class)
                    .orElseThrow(() -> new ParserException("Repeat looping expression must contain at least one variable " +
                            "reference and an operator. Example: i++, i--, i = n etc"));
            String variableName = variable.getValue(String.class);
            Token<?> operator = varWithOperator.containsTokenType(OperatorToken.class)
                    .orElseThrow(() -> new ParserException("Repeat looping expression must contain at least one variable " +
                            "reference and an operator. Example: i++, i--, i + n etc"));
            Token<?> varIncrement = null;
            if (varWithOperator.getTokens().size() >= 3) {
                varIncrement = parser.processExpression(varWithOperator.getFlatTokens().subList(2,
                        varWithOperator.getFlatTokens().size()), context);
            }
            TokenGroup initializer = getTokenGroups().get(1);
            Token<?> initializerResult = parser.processExpression(initializer.getFlatTokens(), context);
            //Initialize the variable defined with initializer result
            context.set(variableName, initializerResult);
            while (meetsCondition(parser, context, context.getContextObject(variableName),
                    getTokenGroups().get(2), config)) {
                Optional<Token<?>> result = callback.call();
                if (result.isPresent()) {
                    if (result.get().getParserFlags().contains(ParserFlag.RETURN_GROUP)) {
                        arrayResult.add(result.get());
                    } else if (result.get().getParserFlags().contains(ParserFlag.RETURN)) {
                        return Collections.singletonList(result.get());
                    }
                }
                //Increment loop variable
                Object variableResult = context.getContextObject(variableName);
                if (variableResult instanceof Token<?>) {
                    List<Token<?>> varIncrementExpr = Objects.nonNull(varIncrement) ?
                            Arrays.asList((Token<?>)variableResult, operator, varIncrement) :
                            Arrays.asList((Token<?>)variableResult, operator);
                    context.set(variableName, parser.processExpression(varIncrementExpr, context));
                } else {
                    throw new ParserException("The repeat loop variable was not in an expected format!");
                }
            }
        }
        return Collections.singletonList(new ArrayToken(arrayResult));
    }

    private boolean meetsCondition(SLOPParser parser, SLOPContext context, Object variableState,
                                   TokenGroup conditionRule, SLOPConfig config) {
        if (!(variableState instanceof Token<?>)) {
            throw new ParserException("The repeat loop variable was not in an expected format!");
        }
        Token<?> currentValue = (Token<?>)variableState;
        List<Token<?>> ruleTokens = conditionRule.getFlatTokens();
        int operatorIndex = -1;
        //Determine index of the operator token
        for (int i = 0;i < ruleTokens.size();i++) {
            if (ruleTokens.get(i) instanceof OperatorToken) {
                operatorIndex = i;
                break;
            }
        }
        Token<?> conditionCompare = parser.processExpression(Collections.singletonList(
                ruleTokens.get(operatorIndex == -1 ? 0 : 1)), context);
        //Handle things like '< 1 + 2' by passing tokens to the parser
        if (operatorIndex == 0 && ruleTokens.size() > 2) {
            conditionCompare = parser.processExpression(ruleTokens.subList(1, ruleTokens.size() - 1), context);
        }
        OperatorHandler opEquals = config.getOperatorHandler().createOperator(OperatorType.EQUALS, config);
        //If no operator has been found then insert equals and evaluate
        Token<?> operator = operatorIndex != -1 ? ruleTokens.get(operatorIndex) : opEquals;
        Token<?> result = parser.processExpression(Arrays.asList(currentValue, operator,
                conditionCompare), context);
        if (result.is(Boolean.class)) {
            return result.getValue(Boolean.class);
        }
        throw new ParserException("Repeat loop condition did not evaluate to a boolean!");
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new FixedLoopToken());
    }

    @Override
    public String toString() {
        String printable = getTokenGroups().stream().map(Object::toString).collect(Collectors.joining(" "));
        return "FixedLoopToken{" + printable + "}";
    }

    /**
     * See {@link Token#toSimpleString() toSimpleString}
     */
    @Override
    public String toSimpleString() {
        String printable = getTokenGroups().stream().map(Token::toSimpleString).collect(Collectors.joining(" "));
        return printable + ",;";
    }

    @Override
    public boolean isExpression() {
        return false;
    }
}