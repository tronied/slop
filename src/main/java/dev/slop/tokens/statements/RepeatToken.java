package dev.slop.tokens.statements;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.OperatorType;
import dev.slop.enums.PatternType;
import dev.slop.exception.LexerException;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenGroup;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.literals.ArrayToken;
import dev.slop.tokens.operators.OperatorHandler;
import dev.slop.tokens.operators.OperatorToken;

import java.util.*;

/**
 * The repeat token is a statement which is similar to how loops work in most languages. The grammar pattern
 * states that it starts with a 'repeat' word followed by 3 separate comma separated expressions. These 3 are:
 *
 *      1) The declaration and operation by which a loop variable is changed
 *      2) An expression resulting in a starting value for the variable
 *      3) The condition which until met will keep executing until the contained expressions will be executed
 *
 * Using the declaration 'repeat(i++,0,&lt;10)' and using the above rules the following is true:
 *
 *      1) A variable 'i' is declared and will be incremented on each iteration
 *      2) The variable 'i' will be initialized to 0 before the loop starts
 *      3) The repeat statement will continue until 'i' is less than 10
 *
 * The body of the repeat statement consists of one or more separate expressions which end with a semi-colon.
 * For example, using the fibonacci sequence as an example and setting the context up with 2 values (first = 0 and
 * second = 1), we could define the following:
 *
 *      [0,1] + repeat(i++,0,&lt;10) result = {?first} + {?second}; first = {?second}; second = {?result};
 *
 * This would output the following:
 *
 *      [0,1,1,2,3,5,8,13,21,34,55,89]
 *
 * A repeat statement can have an unlimited number of expressions and later expressions can use stored variables
 * from earlier expressions. The 'result' definition above, although a normal variable, is handled specially by the
 * repeat statement as that determines the result for the current iteration.
 *
 * @deprecated Please use the ForToken as a replacement
 */
@Deprecated
public class RepeatToken extends Token<Void> {

    public RepeatToken() {
        super("Repeat", null);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new RepeatToken());
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "'repeat' '(' ( expr ',' expr ',' expr ) ')' ( expr ';' )+";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        if (token.equalsIgnoreCase("("))
            return Optional.of("A repeat statement requires the iteration definition to be wrapped in a " +
                    "pair of braces e.g. repeat -->( <variable> ; <condition> ; <step> ) ...");
        if (token.equalsIgnoreCase(")"))
            return Optional.of("A repeat statement requires the iteration definition to be wrapped in a " +
                    "pair of braces e.g. repeat ( <variable> ; <condition> ; <step> )<-- ...");
        if (token.equalsIgnoreCase(","))
            return Optional.of("A repeat statement requires 3 parts of the iteration to be defined and " +
                    "separated by a ',' character e.g. repeat ( i++, 0, <10 ) ...");
        if (token.equalsIgnoreCase(";"))
            return Optional.of("Repeat statement code blocks must be terminated with a ';' character e.g. " +
                    "repeat ( ... ) <some logic> ;<--");
        if (groupsCount.contains(1)) {
            return Optional.of("Repeat statement requires at least one code block to be iterated e.g. " +
                    "repeat ( ... ) <code block1> ; <code block2> ; ...");
        }
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        List<Token<?>> arrayResult = new ArrayList<>();
        TokenGroup loopCriteria = getTokenGroups().get(0);
        if (!(loopCriteria.getTokens().get(0) instanceof TokenGroup)) {
            throw new ParserException("The first token must be a reference and operator combination! Example: " +
                    "repeat(i++,<start value>,<end value>) <expression>;");
        }
        TokenGroup varWithOperator = (TokenGroup) loopCriteria.getTokens().get(0);
        if (varWithOperator.getTokens().size() >= 2) {
            Token<?> variable = varWithOperator.containsTokenType(TokenValue.class)
                    .orElseThrow(() -> new ParserException("Repeat looping expression must contain at least one variable " +
                            "reference and an operator. Example: i++, i--, i = n etc"));
            String variableName = variable.getValue(String.class);
            Token<?> operator = varWithOperator.containsTokenType(OperatorToken.class)
                    .orElseThrow(() -> new ParserException("Repeat looping expression must contain at least one variable " +
                            "reference and an operator. Example: i++, i--, i + n etc"));
            Token<?> varIncrement = null;
            if (varWithOperator.getTokens().size() >= 3) {
                varIncrement = parser.processExpression(varWithOperator.getFlatTokens().subList(2,
                        varWithOperator.getFlatTokens().size()), context);
            }
            TokenGroup initializer = (TokenGroup)loopCriteria.getTokens().get(1);
            Token<?> initializerResult = parser.processExpression(initializer.getFlatTokens(), context);
            //Initialize the variable defined with initializer result
            context.set(variableName, initializerResult);
            List<Token<?>> loopExpressions = getTokenGroups().get(1).getTokens();
            while (meetsCondition(parser, context, context.getContextObject(variableName),
                    (TokenGroup)loopCriteria.getTokens().get(2), config)) {
                //Execute contained expressions
                loopExpressions.forEach(e -> {
                    if (!(e instanceof TokenGroup)) {
                        throw new ParserException("Repeat loop expression was not found to be a token group");
                    }
                    parser.processExpression(((TokenGroup)e).getFlatTokens(), context);
                });
                Object result = context.getContextObject(RESULT_VARIABLE);
                if (Objects.nonNull(result)) {
                    arrayResult.add((result instanceof Token<?>) ? (Token<?>)result : new TokenValue(result));
                } else {
                    //TODO - Pointless loop which returns no result. Throw an exception?
                }
                //Increment loop variable
                Object variableResult = context.getContextObject(variableName);
                if (variableResult instanceof Token<?>) {
                    List<Token<?>> varIncrementExpr = Objects.nonNull(varIncrement) ?
                            Arrays.asList((Token<?>)variableResult, operator, varIncrement) :
                            Arrays.asList((Token<?>)variableResult, operator);
                    context.set(variableName, parser.processExpression(varIncrementExpr, context));
                } else {
                    throw new ParserException("The repeat loop variable was not in an expected format!");
                }
            }
            return Collections.singletonList(new ArrayToken(arrayResult));
        }
        throw new LexerException("Invalid repeat loop variable definition: " + varWithOperator);
    }

    private boolean meetsCondition(SLOPParser parser, SLOPContext context, Object variableState,
                                   TokenGroup conditionRule, SLOPConfig config) {
        if (!(variableState instanceof Token<?>)) {
            throw new ParserException("The repeat loop variable was not in an expected format!");
        }
        Token<?> currentValue = (Token<?>)variableState;
        List<Token<?>> ruleTokens = conditionRule.getFlatTokens();
        int operatorIndex = -1;
        //Determine index of the operator token
        for (int i = 0;i < ruleTokens.size();i++) {
            if (ruleTokens.get(i) instanceof OperatorToken) {
                operatorIndex = i;
                break;
            }
        }
        Token<?> conditionCompare = parser.processExpression(Collections.singletonList(
                ruleTokens.get(operatorIndex == -1 ? 0 : 1)), context);
        //Handle things like '< 1 + 2' by passing tokens to the parser
        if (operatorIndex == 0 && ruleTokens.size() > 2) {
            conditionCompare = parser.processExpression(ruleTokens.subList(1, ruleTokens.size() - 1), context);
        }
        OperatorHandler opEquals = config.getOperatorHandler().createOperator(OperatorType.EQUALS, config);
        //If no operator has been found then insert equals and evaluate
        Token<?> operator = operatorIndex != -1 ? ruleTokens.get(operatorIndex) : opEquals;
        Token<?> result = parser.processExpression(Arrays.asList(currentValue, operator,
                conditionCompare), context);
        if (result.is(Boolean.class)) {
            return result.getValue(Boolean.class);
        }
        throw new ParserException("Repeat loop condition did not evaluate to a boolean!");
    }

    @Override
    public String toString() {
        return "RepeatToken{" +
                "iterations=" + (!getTokenGroups().isEmpty() ? getTokenGroups().get(0) : null) +
                ", expression=" + (getTokenGroups().size() > 1 ? getTokenGroups().get(1) : null) +
                '}';
    }
}
