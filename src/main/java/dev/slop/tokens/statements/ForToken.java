package dev.slop.tokens.statements;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenGroup;
import dev.slop.tokens.specials.ReturnToken;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This replaces the RepeatToken and ForEachToken with a single token including functionality for
 * both. It does this by using grammar references so that when the expression is read, the decision
 * over the implementation is decided as it is being read and the appropriate set of pattern tokens
 * is used. For more information on each type of loop, please refer to the FixedLoopToken and
 * VariableLoopToken classes. These are SECONDARY tokens which means they can't be used on their
 * own but as a part of another token like this.
 */
public class ForToken extends Token<Void> {

    public ForToken() {
        super("ForEach", null);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new ForToken());
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "'for' '(' [ fixedLoop, variableLoop ] ')' [ singleLine, multiLine ]";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        if (token.equalsIgnoreCase("("))
            return Optional.of("The for looping statement must be wrapped in braces e.g. " +
                    "'for -->( obj : collection )' or 'for ( i++;0;<10 )<--'");
        if (token.equalsIgnoreCase(")"))
            return Optional.of("The for looping statement must be wrapped in braces e.g. " +
                    "'for ( obj : collection )<--' or 'for ( i++;0;<10 )<--'");
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        if (getTokenGroups().size() != 2) {
            String foundTokens = getTokenGroups().stream()
                    .flatMap(tg -> tg.getFlatTokens().stream())
                    .map(t -> t.getClass().getSimpleName())
                    .collect(Collectors.joining(", "));
            throw new ParserException("For token expects two tokens being the looping portion and the body. Instead got "
                    + foundTokens);
        }
        Token<?> loopToken = getTokenGroups().get(0).getFlatTokens().get(0);
        Token<?> bodyToken = getTokenGroups().get(1).getFlatTokens().get(0);
        return loopToken.processWithCallback(parser, context, config, () -> {
            List<Token<?>> result = bodyToken.processNoParams(parser, context, config);
            if (result.size() == 1 && result.get(0) instanceof ReturnToken) {
                return Optional.of(result.get(0));
            }
            return Optional.empty();
        });
    }

    @Override
    public String toString() {
        String print = getTokenGroups().stream().map(TokenGroup::toString).collect(Collectors.joining(", "));
        return String.format("ForToken{%s}", print);
    }
}
