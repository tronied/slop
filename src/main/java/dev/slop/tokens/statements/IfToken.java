package dev.slop.tokens.statements;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.literals.NullToken;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The If statement works as a single condition with a single result. If the condition is not true then
 * a null is returned as opposed to the single result. This works in the same way as the conditional
 * expr ? expr : null but avoids having to explicitly declare the null case.
 */
public class IfToken extends Token<Void> {

    public IfToken() { super("If", null); }

    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new IfToken());
    }

    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    @Override
    public String getPattern() {
        return "'if' '(' expr ')' [ singleLine, multiLine ] ( 'else' [ singleLine, multiLine ] )?";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        if (token.equalsIgnoreCase("("))
            return Optional.of("An if statement requires the condition to be wrapped in a pair of braces " +
                    "e.g. if -->( <condition> ) ...");
        if (token.equalsIgnoreCase(")"))
            return Optional.of("An if statement requires the condition to be wrapped in a pair of braces " +
                    "e.g. if ( <condition> )<-- ...");
        return Optional.empty();
    }

    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        Token<?> condition = parser.processExpression(getTokenGroups().get(0).getFlatTokens(), context);
        if (!condition.is(Boolean.class)) {
            throw new ParserException("If requires a boolean to be used as a the condition!");
        }
        if (Boolean.TRUE.equals(condition.getValue(Boolean.class))) {
            return Collections.singletonList(parser.processExpression(getTokenGroups().get(1).getFlatTokens(), context));
        } else if (getTokenGroups().size() == 3) {
            return Collections.singletonList(parser.processExpression(getTokenGroups().get(2).getFlatTokens(), context));
        }
        return Collections.singletonList(new NullToken());
    }

    @Override
    public String toString() {
        String printable = getTokenGroups().stream().map(Object::toString).collect(Collectors.joining(" "));
        return "IfToken{" + printable + "}";
    }
}
