package dev.slop.tokens.statements;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.literals.BooleanToken;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The conditional statement is greedy and consumes tokens both before and after where it's defined. Regarding the
 * first group, all previous tokens within scope or in short term memory are added to the first token group until the
 * next pattern token '?' is read. At which point the position within the pattern is progressed and the middle group
 * becomes active with tokens being added to that group. With the last group, all following tokens will be added to
 * the last group until either the end of the expression or a closing pattern token of a token further up the stack
 * is read. At which point the remaining tokens in short term memory can be read and added to the final group before
 * the conditional token is removed from the stack and added to the relevant group or long term storage.
 *
 * The actual implementation of a conditional is well known and has three sections which are the condition that
 * determines which result to execute. For example:
 *
 *      3 &gt; 4 ? "no" : "yes"
 *
 * In this case the condition is evaluated to false which is represented by the third grammar group. The tokens captured
 * within this group are then processed and the final result ("yes") is returned.
 */
public class ConditionalToken extends Token<Void> {

    public ConditionalToken() {
        super("Conditional", null);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        //condition ? trueResult : falseResult
        return "expr '?' expr ':' expr";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        if (token.equalsIgnoreCase(":"))
            return Optional.of("A conditional requires true / false outcomes to be separated by a ':' " +
                    "e.g. a > b ? a :<-- b");
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        //Expect that there are 3 tokens groups representing the condition and true / false token groups
        if (getTokenGroups().size() < 3) {
            String found = getTokenGroups().stream().map(Token::toSimpleString).collect(Collectors.joining(","));
            throw new ParserException(String.format("Condition does not have required arguments to execute. Expecting " +
                    "3 groups being condition, trueResult and falseResult. Found: [%s]", found));
        }
        //Evaluate the condition using the tokens found in the first token group
        Token<?> conditionResult = parser.processExpression(getTokenGroups().get(0).getTokens(), context);
        //If the condition is not a Boolean then throw an error i.e. "1 + 2 ? 3 : 4"
        if (!(conditionResult instanceof BooleanToken)) {
            throw new ParserException(String.format("Expected a boolean result from condition '%s'. Possible invalid " +
                            "condition specified", getTokenGroups().get(0)));
        }
        //Execute the relevant set of tokens based on the condition result
        return Collections.singletonList((((BooleanToken) conditionResult).getValue()) ?
                parser.processExpression(getTokenGroups().get(1).getFlatTokens(), context) :
                parser.processExpression(getTokenGroups().get(2).getFlatTokens(), context));
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new ConditionalToken());
    }

    @Override
    public String toString() {
        return "Conditional{" +
                "condition=" + (!getTokenGroups().isEmpty() ? getTokenGroups().get(0) : null) +
                ", trueResult=" + (getTokenGroups().size() > 1 ? getTokenGroups().get(1) : null) +
                ", falseResult=" + (getTokenGroups().size() > 2 ? getTokenGroups().get(2) : null) +
                '}';
    }
}
