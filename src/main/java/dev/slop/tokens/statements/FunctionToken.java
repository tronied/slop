package dev.slop.tokens.statements;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.enums.VariableType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenParameters;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.literals.NullToken;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This token allows a custom user function to be defined within code. As an example it has the
 * following format:
 *
 *      func myFunction(a,b,c) { return a + b + c; }
 *
 * This token class differs for a number of reasons. For a start the standard call to process does
 * not execute the contents but rather adds a reference of itself into context under the function
 * name. This can then be called by the now renamed InvocationToken using the parameters. This
 * token class implements another interface called TokenParameters which adds a method by which
 * those parameters can be passed and the functions content and be executed. This token also makes
 * use of another feature where it overrides the default hasParserPriority. What this does is to
 * tell the parser to give priority to execute this token before others so that it is entered into
 * memory prior to any others. This means that regardless of where it is defined (before or after
 * a call to the function) it works as expected.
 */
public class FunctionToken extends Token<Void> implements TokenParameters {

    public FunctionToken() {
        super("Function", null);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new FunctionToken());
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() createToken}
     */
    @Override
    public String getPattern() {
        return "'func' val '(' ( val ','? )+ ')' [ singleLine, multiLine ]";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * This gets called when the function is evaluated by the parser. It does not actually do anything at this stage
     * but instead simply places a reference of the token into the context (heap). This can then be called later
     * using the InvocationToken
     * @param parser The parser used to resolve child token expressions
     * @param context The context for resolving object references
     * @param config The configuration for resolution of property values
     * @return Returns null at this stage as there is no result to return
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        if (getTokenGroups().size() != 3) {
            throw new ParserException(String.format("Expected three groups being 1) Function Name 2) Parameters " +
                    "(if provided) 3) Implementation. Found only %d groups", getTokenGroups().size()));
        }
        if (getTokenGroups().get(0).getFlatTokens().size() != 1) {
            throw new ParserException(String.format("Expected a single TokenValue for the function name but found %d tokens provided",
                    getTokenGroups().get(0).getTokenPosition()));
        }
        String functionName = getTokenGroups().get(0).getFlatTokens().get(0).getValue().toString();
        context.set(functionName.concat(Integer.toString(getTokenGroups().get(1).getFlatTokens().size())),
                this, VariableType.SINGLETON);
        return Collections.singletonList(new NullToken());
    }

    /**
     * This is where the function is passed parameters by the InvocationToken which is being evaluated. That token
     * retrieved this one from the heap using a concatenation between the function name and the number of parameters.
     * The passed parameters are written into context and the contained tokens evaluated. The result is returned
     * from the evaluation of these.
     * @param parser The parser used to resolve child token expressions
     * @param context The context for resolving object references
     * @param config The configuration for resolution of property values
     * @param providedParams The parameters from the calling InvocationToken
     * @return Returns the resulting value from the function execution
     */
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config, List<Token<?>> providedParams) {
        String functionName = getTokenGroups().get(0).getFlatTokens().get(0).getValue().toString();
        List<Token<?>> paramVars = getTokenGroups().get(1).getFlatTokens();
        if (paramVars.size() != providedParams.size()) {
            throw new ParserException(String.format("Function %s expected %d parameters, but only called with %d",
                    functionName, paramVars.size(), providedParams.size()));
        }
        for (int i = 0;i < paramVars.size();i++) {
            context.set(paramVars.get(i).getValue(String.class), providedParams.get(i), VariableType.PARAMETER);
        }
        Token<?> result = parser.processExpression(getTokenGroups().get(2).getFlatTokens(), context);
        if (!result.getParserFlags().isEmpty()) {
            //Unwrap result as we don't want to return return token any further
            return Collections.singletonList(result.getValue() instanceof Token ?
                    (Token<?>)result.getValue() : new TokenValue(result.getValue()));
        }
        return Collections.singletonList(result);
    }

    /**
     * See {@link TokenParameters#getIdentifier() getIdentifier}
     */
    @Override
    public String getIdentifier() {
        return getTokenGroups().get(0).getFlatTokens().get(0).getValue(String.class)
                .concat(Integer.toString(getTokenGroups().get(1).getFlatTokens().size()));
    }

    /**
     * See {@link Token#hasParserPriority() hasParserPriority}
     */
    @Override
    public boolean hasParserPriority() {
        return true;
    }

    @Override
    public String toString() {
        String printable = getTokenGroups().stream().map(Object::toString).collect(Collectors.joining(" "));
        return "FunctionToken{" + printable + "}";
    }

    /**
     * Sets the type by which it is stored in context as a singleton. This means that no instances can be created from
     * it and there can be only one unique reference held directly in memory. Functions that are part of instances
     * are called in a different way which is not a singleton as they exist in the instances struture.
     * @return Returns the variable type
     */
    @Override
    public VariableType getVariableType() {
        return VariableType.SINGLETON;
    }
}
