package dev.slop.tokens.statements;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.exception.ParserException;
import dev.slop.exception.SLOPException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.specials.ArgumentToken;

import java.lang.reflect.Field;
import java.util.*;

public class JVMInvokeToken extends Token<Void> {

    public JVMInvokeToken() {
        super("JVMInvoke", null);
    }

    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new JVMInvokeToken());
    }

    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    @Override
    public String getPattern() {
        return "'#' ( val ( '[' ( expr ) ']' )? '.'? )+";
    }

    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        String classPath = config.getIncludeClasspath(getTokenGroups().get(0).getFlatTokens().get(0)
                .getValue(String.class));
        List<Token<?>> pathTokens = getTokenGroups().get(0).getFlatTokens();
        String innerEnumClass = null;
        String enumName;
        String key = null;
        if (pathTokens.get(1) instanceof ArgumentToken) {
            innerEnumClass = pathTokens.get(1).getValue(String.class);
            enumName = pathTokens.get(2).getValue(String.class);
            if (pathTokens.size() == 4) {
                key = pathTokens.get(3).getValue(String.class);
            }
        } else {
            enumName = pathTokens.get(1).getValue(String.class);
            if (getTokenGroups().get(0).getFlatTokens().size() == 3) {
                key = getTokenGroups().get(0).getFlatTokens().get(2).getValue(String.class);
            }
        }
        return Collections.singletonList(new TokenValue(getEnumValue(classPath, innerEnumClass, enumName, key)));
    }

    private Object getEnumValue(String classPath, String innerEnumClass, String enumName, String key) {
        try {
            Class<?> c = Class.forName(classPath);
            if (Objects.nonNull(innerEnumClass)) {
                c = Arrays.stream(c.getDeclaredClasses())
                        .filter(ic -> ic.getSimpleName().equalsIgnoreCase(innerEnumClass))
                        .findFirst()
                        .orElseThrow(() -> new ParserException(String.format("Could not find inner class '%s' in '%s'",
                                innerEnumClass, classPath)));
            }
            if (c.isEnum())
                return handleEnum(c, enumName, key);
        } catch (ClassNotFoundException ex) {
            throw new SLOPException(String.format("Could not invoke '%s' with '%s' as it does not exist in the class " +
                    "path", enumName, classPath));
        } catch (IllegalAccessException ex) {
            throw new SLOPException(String.format("Could not access '%s' from '%s': %s", enumName, classPath,
                    ex.getMessage()));
        } catch (NoSuchFieldException e) {
            throw new SLOPException(String.format("Attempt to read field '%s' from enum value '%s' with '%s' " +
                    "failed as it does not exist", key, enumName, classPath));
        }
        return null;
    }

    private Object handleEnum(Class<?> c, String enumName, String key) throws NoSuchFieldException,
            IllegalAccessException {
        Object[] objects = c.getEnumConstants();
        for (Object obj : objects) {
            if (!obj.toString().equalsIgnoreCase(enumName)) continue;
            if (Objects.isNull(key)) return obj;
            Field keyField = obj.getClass().getDeclaredField(key);
            keyField.setAccessible(true);
            return keyField.get(obj);
        }
        return null;
    }
}
