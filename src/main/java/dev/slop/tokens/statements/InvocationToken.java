package dev.slop.tokens.statements;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.exception.ParserException;
import dev.slop.functions.Function;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenGroup;
import dev.slop.tokens.base.TokenValue;

import java.util.*;

/**
 * The function token is a statement that can either act as a placeholder in another statement e.g. native calls
 * in FieldToken, or invoking a custom Function class within the source code. The pattern expects a single value
 * with brackets wrapping one or more comma separated expressions (@see com.slop.functions.Function#execute()).
 */
public class InvocationToken extends Token<Void> {

    public InvocationToken() { super(); }

    public InvocationToken(TokenValue value) {
        super("Function", null);
        getTokenGroups().add(new TokenGroup(Collections.singletonList(value)));
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new InvocationToken(new TokenValue(value)));
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "val:String '(' ( expr ','? )+ ')'";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        if (token.equalsIgnoreCase(")"))
            return Optional.of("A function definition requires parameters to be wrapped in a pair of braces " +
                    "e.g. FUNC( 1,2,3 )<--");
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        List<Token<?>> result = new ArrayList<>();
        if (getTokenGroups().isEmpty() || getTokenGroups().get(0).getTokens().isEmpty()) {
            throw new ParserException("Function has no name or parameters");
        }
        String methodName = getTokenGroups().get(0).getTokens().get(0).getValue(String.class);
        List<Token<?>> params = getTokenGroups().size() > 1 ?
                getTokenGroups().get(1).processTokens(parser, context) : new ArrayList<>();

        Object definedFunction = context.getContextObject(methodName.concat(Integer.toString(params.size())));
        if (Objects.nonNull(definedFunction)) {
            //Invoke a user defined function
            if (!(definedFunction instanceof FunctionToken)) {
                throw new ParserException(String.format("Found user defined function %s in the context but was not a FunctionToken",
                        methodName));
            }
            Token<?> token = (Token<?>)definedFunction;
            return token.processWithParams(parser, context, config, params);
        } else{
            //Attempt to find a system function by the name e.g. SUM(...)
            Function found = config.getFunctions().stream()
                    .filter(f -> f.isMatch(methodName))
                    .findFirst()
                    .orElseThrow(() -> new ParserException(
                            String.format("No user defined function or system operation found with name '%s'", methodName)));
            //Pass the params list to the function and add the result to the resulting operation array
            result.add(found.execute(config, params));
            if (config.isDebugMode())
                System.out.println(String.format("Function: %s(%s) -> %s", methodName, params, result.get(0)));
        }
        return result;
    }

    @Override
    public String toString() {
        return String.format("InvocationToken{name=%s, params=%s}", getTokenGroups().get(0),
                getTokenGroups().subList(1, getTokenGroups().size()));
    }
}
