package dev.slop.tokens.statements;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.OperatorType;
import dev.slop.enums.PatternType;
import dev.slop.exception.ParserException;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenGroup;
import dev.slop.tokens.base.NonToken;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.literals.NullToken;
import dev.slop.tokens.operators.OperatorToken;
import dev.slop.tokens.operators.UnaryOperator;
import dev.slop.tokens.specials.ArgumentToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The switch token is a statement that is common to most languages. The general idea is that a parameter is passed in
 * and that value is then compared against one or more cases with the relevant code block executed. The SLOP switch
 * is an extension on that idea where you can not only pass multiple parameters, but also have some flexibility over
 * the rules that you define in each case. Using the following example:
 *
 *      tax = switch(employee.salary)[&gt;= thresholdA: 0.05!;&gt;= thresholdB: 0.1; default: 0.023]
 *
 * In the above scenario an Employee salary is being evaluated against some thresholds defined in the context. In this
 * case the higher the salary is the more tax that person pays. As can be seen, SLOP switch cases are not restricted to
 * just being equal but we can do numeric operation conditions. There are a couple of things to note about the above
 * statement in that after the result is defined for the first case, a '!' is present. This informs that we will just
 * return that value instead of looking for other matches. This is similar to how the break keyword works in a language
 * like C or Java. In the next cases result you'll see that there is no exclamation mark, but that is because the only
 * other case is the fallback (default). As such, that token is not required as the default only gets triggered if no
 * other case is matched. Using another example, we can see how multiple parameters may be used in a single case
 * condition:
 *
 *      description = switch(product.price, vat[countryCode])[
 *          $0 * $1 &gt;= 1000: "Economy";
 *          $0 * $1 &gt;= 5000: "Standard";
 *          $0 * $1 &gt;= 10000: "Premium"]
 *
 * You can make reference to parameters by using the ArgumentToken. This is a special token which uses an index to
 * refer to the object desired. In this case $0 refers to the parameter declared at index 0. Moving on from this, you
 * can separate the parameter conditions using the following:
 *
 *      response = switch(query, locale)[
 *          GREETING, UK: "Hello";
 *          GREETING, DE: "Gutentag";
 *          INQUIRY, UK: "How are you?";
 *          INQUIRY, DE: "Wie Gehts?";
 *          ...]
 *
 * These are purely contrived examples and you would never do this in a real world situation, but this shows
 * separate condition can be used for each parameter. In this case we are using just the values and so each much be
 * equal. However, you could use any of the type operations and logic operators to expand this further. As one final
 * extension to this statement, you don't actually need to use a parameter at all and instead use it as another method
 * of chained conditional statements e.g.
 *
 *      switch(1)[4 &gt; 3 AND 3 * 9 == 27: "Match"; 45 &gt; 39: "Also Match"; 3 &gt; 4: "Nope"]
 *
 * In the above example we pass a 1 as the parameter, though since we're not going to use it we can ignore it. In this
 * scenario we have multiple conditions which do match and so the above result would be ["Match", "Also Match"] as
 * without using the exclamation mark, it will return all matching cases in an array. By not enforcing strict rules
 * upon the switch implementation, we provide greater flexibility over what can be done with it.
 */
public class SwitchToken extends Token<Void> {

    public SwitchToken() {
        super("Switch", null);
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "'switch' '(' ( expr ','? )+ ')' '[' ( ( expr ','? )+ ':' expr '!'<? ';'? )+ ( 'default' ':' expr )? ']'";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        if (token.equalsIgnoreCase("("))
            return Optional.of("A switch statement requires the evaluated value(s) to be wrapped in a pair of braces " +
                    "e.g. switch -->( <value> ) ...");
        if (token.equalsIgnoreCase(")"))
            return Optional.of("A switch statement requires the evaluated value(s) to be wrapped in a pair of braces " +
                    "e.g. switch ( <value> )<-- ...");
        if (token.equalsIgnoreCase("["))
            return Optional.of("The switch body requires cases to be wrapped in a pair of square brackets " +
                    "e.g. switch ( <value> ) -->[ <cases> ]");
        if (token.equalsIgnoreCase("]"))
            return Optional.of("The switch body requires cases to be wrapped in a pair of square brackets " +
                    "e.g. switch ( <value> ) [ <cases> ]<--");
        if (token.equalsIgnoreCase(";"))
            return Optional.of("Each switch case must be terminated by a ';' character e.g. switch ( age ) " +
                    "[ < 1 : 'Baby' ;<-- ... ]");
        if (token.equalsIgnoreCase(":"))
            return Optional.of("Each switch case condition and result must be separated by a ':' character e.g. " +
                    "switch ( age ) [ < 1 :<-- 'Baby' ; ... ]");
        if (groupsCount.contains(2))
            return Optional.of("At least one case must be specified in a switch statement e.g. " +
                    "switch ( ... )[ < 1 : 'Baby' ; ... ]");
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        //Fetch the token group containing the parameters
        TokenGroup paramGroup = getTokenGroups().get(0);
        //Fetch the token group containing the cases (each with a condition / result sub-group)
        List<Token<?>> cases = getTokenGroups().get(1).getTokens();
        for (Token<?> aCase : cases) {
            if (!(aCase instanceof TokenGroup)) {
                throw new ParserException(String.format("Unexpected token found in place of the expected switch case (%s)", aCase));
            }
            /* Loop through the cases and if the number of conditions in a case is not 1 and not equal to the size of
             * the number of parameters then throw an exception */
            TokenGroup tgCase = (TokenGroup)aCase;
            TokenGroup tgConditions = (TokenGroup) tgCase.getTokens().get(0);
            if (tgConditions.getTokens().size() != 1 && tgConditions.getTokens().size() != paramGroup.getTokens().size()) {
                throw new ParserException(String.format("Invalid number of arguments found in statement (%s). The number of case" +
                        "arguments should be one per case e.g. switch(1,2)[$0 > $1: ...] or one per parameter e.g. switch(1,2)" +
                        "[< 5, > 3: ...]", this));
            }
        }
        //Resolve the parameters as param tokens could be constructs e.g. switch(1 + 2, ...)
        List<Token<?>> resolvedParams = resolveParams(parser, context, paramGroup.getTokens());
        //Loop through the cases and return the result(s) in token form
        List<Token<?>> statementResult = resolveCases(parser, context, resolvedParams, cases, config);
        if (statementResult.isEmpty()) {
            //Handle scenario where no cases matched. Attempt to parse and return default case if specified and valid
            if (getTokenGroups().size() < 3) return Collections.singletonList(new NullToken());
            List<Token<?>> defaultToken = getTokenGroups().get(2).getFlatTokens();
            if (defaultToken.isEmpty() || defaultToken.get(0) instanceof TokenGroup &&
                    ((TokenGroup)defaultToken.get(0)).getTokens().isEmpty()) {
                return Collections.singletonList(new NullToken());
            }
            statementResult.add(parser.processExpression(defaultToken, context));
        }
        Token<?> result = statementResult.size() == 1 ? statementResult.get(0) : new TokenValue(statementResult);
        return Collections.singletonList(result);
    }

    /**
     * Processes each set of parameter tokens to return each as a single value in an array
     * @param parser The parser by which to process the tokens to resolve a parameter result
     * @param context The context from which to resolve referenced values within the tokens
     * @param params The set of parameter token groups
     * @return Returns a single list of resolved parameter tokens
     */
    private List<Token<?>> resolveParams(SLOPParser parser, SLOPContext context, List<Token<?>> params) {
        List<Token<?>> resolvedParams = new ArrayList<>();
        for (Token<?> param : params) {
            if (param instanceof TokenGroup) {
                resolvedParams.add(parser.processExpression(((TokenGroup) param).getFlatTokens(), context));
            } else {
                resolvedParams.add(parser.processExpression(Collections.singletonList(param), context));
            }
        }
        return resolvedParams;
    }

    /**
     * Loops through each case determining if each cases conditions have been met. If so, each set of result tokens
     * will be parsed and added to a list of result tokens to be returned. Each case will have the following group
     * construction as determined by the grammar pattern:
     *
     *      (Case) TokenGroup
     *          - (Conditions) TokenGroup
     *              - (Param 1 Condition) TokenGroup
     *                  - (Condition Tokens...)
     *              - (Optional - Param 2 Condition)
     *                  ...
     *              ...
     *      (Result) TokenGroup
     *          - (One or more result tokens)
     *
     * @param parser The parser to resolve both the case condition and result values
     * @param context The context from which to resolve referenced values within the tokens
     * @param resolvedParams The list of resolved parameter values which may be referenced in the cases by ArgumentToken
     * @param cases The list of case token groups
     * @param config The configuration used for access to properties and construction of operators
     * @return Returns a list of one or more case results
     */
    private List<Token<?>> resolveCases(SLOPParser parser, SLOPContext context, List<Token<?>> resolvedParams,
                                        List<Token<?>> cases, SLOPConfig config) {
        List<Token<?>> casesResult = new ArrayList<>();
        for (Token<?> aCase : cases) {
            TokenGroup theCase = (TokenGroup)aCase;
            //Get the token group containing the list of case conditions
            TokenGroup caseConditions = (TokenGroup) theCase.getTokens().get(0);
            boolean result = true;
            for (int i = 0;i < caseConditions.getTokens().size();i++) {
                TokenGroup condition = (TokenGroup) caseConditions.getTokens().get(i);
                List<Token<?>> caseCondition = condition.getTokens();
                caseCondition = caseCondition.stream().map(t -> {
                    /* Identify if the condition contains a reference to a parameter via an ArgumentToken. If so,
                     * resolve it's value and replace it with it's actual value from the resolved parameter list. */
                    if (t instanceof ArgumentToken) {
                        Object value = ((ArgumentToken) t).getValue();
                        try {
                            return resolvedParams.get(Integer.parseInt(value.toString()));
                        } catch (Exception ex) {
                            throw new ParserException(String.format("Found argument token but did not understand its " +
                                    "value (%s). A switch statement expects an integer ($0) token to be specified", value));
                        }
                    }
                    return t;
                }).collect(Collectors.toList());
                Token<?> resolvedArg = resolvedParams.get(i);
                if (condition.getTokens().size() == 2) {
                    //Handle case where there are only 2 tokens in condition i.e. '> 1' and insert param value
                    if (condition.getTokens().get(0) instanceof OperatorToken) {
                        caseCondition.add(0, resolvedArg);
                    } else {
                        caseCondition.add(resolvedArg);
                    }
                } else if (condition.getTokens().size() == 1) {
                    /* Handle case where only 1 token value is provided. In this case an equals operator is constructed
                     * using the configuration class and the relevant resolved parameter. */
                    Token<?> singleCondition = caseCondition.remove(0);
                    List<Token<?>> singleConditionResult = singleCondition instanceof TokenGroup ?
                            ((TokenGroup) singleCondition).getFlatTokens() : Collections.singletonList(singleCondition);
                    caseCondition.add(0, parser.processExpression(singleConditionResult, context));
                    caseCondition.add(0, config.getOperatorHandler().createOperator(OperatorType.EQUALS, config));
                    caseCondition.add(0, resolvedArg);
                }
                //Process the condition and return a boolean result
                Token<?> caseConditionResult = parser.processExpression(caseCondition, context);
                if (!caseConditionResult.getValue(Boolean.class)) {
                    //If one of the resolved condition results is false then set result to false and fail fast
                    result = false;
                    break;
                }
            }
            if (result) {
                Token<?> caseResultToken = theCase.getTokens().get(theCase.getTokens().size() - 1);
                if (caseResultToken instanceof TokenGroup) {
                    TokenGroup caseResult = (TokenGroup) caseResultToken;
                    /* If an exclamation token is found on the matched case condition(s) then break out and return
                     * with just this value. Alternatively, add the matched condition result to the list of results. */
                    if (caseResult.getTokens().size() > 1 && caseResult.getTokens().get(caseResult.getTokens().size() - 1)
                            instanceof NonToken) {
                        NonToken suspected = (NonToken)caseResult.getTokens().get(caseResult.getTokens().size() - 1);
                        if (suspected.getValue().equalsIgnoreCase("!")) {
                            casesResult.add(parser.processExpression(caseResult.getTokens().subList(0,
                                    caseResult.getTokens().size() - 1), context));
                            break;
                        }
                    } else {
                        casesResult.add(parser.processExpression(caseResult.getTokens(), context));
                    }
                } else {
                    casesResult.add(parser.processExpression(Collections.singletonList(caseResultToken), context));
                }
            }
        }
        return casesResult;
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new SwitchToken());
    }

    @Override
    public String toString() {
        return "SwitchToken{" +
                "params=" + (!getTokenGroups().isEmpty() ? getTokenGroups().get(0) : null) +
                ", cases=" + (getTokenGroups().size() > 1 ? getTokenGroups().get(1) : null) +
                ", defaultCase=" + (getTokenGroups().size() > 2 ? getTokenGroups().get(2) : null) +
                '}';
    }
}
