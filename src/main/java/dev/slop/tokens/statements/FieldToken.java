package dev.slop.tokens.statements;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.PatternType;
import dev.slop.enums.VariableType;
import dev.slop.exception.ParserException;
import dev.slop.lexer.SLOPLexer;
import dev.slop.model.Expression;
import dev.slop.model.LexerResult;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenGroup;
import dev.slop.tokens.TokenInstance;
import dev.slop.tokens.TokenParameters;
import dev.slop.tokens.base.NonToken;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.literals.ArrayToken;
import dev.slop.tokens.literals.BooleanToken;
import dev.slop.tokens.literals.NullToken;
import dev.slop.tokens.operators.LogicOperatorHandler;
import dev.slop.tokens.operators.OperatorToken;
import dev.slop.tokens.operators.UnaryOperator;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The field token class is used to resolve objects and their fields from context. It is one of the more complex
 * default statements within SLOP not least because of the amount field scenarios that are covered but also the
 * Grammar pattern itself. The pattern specifies that there are one or more single capture values, each with an
 * optional set of square brackets and an optional period character. Given it's complete generic nature, it has
 * the ability to trigger a match with just a single value. The Lexer (if multiple matches are detected) will
 * attempt its best to determine the best match based on the information (and past tokens) at hand, but this is
 * why it is important to keep tokens and their patterns unique to avoid ambiguity. In this case unfortunately
 * the object references are following most language interpretation for navigating an object and its fields and
 * therefore follows this pattern for consistency.
 *
 * With the FieldToken either an object name or index can be used as a reference in the first captured group. For
 * example, using an index may look like the following:
 *
 *      0.myArray[0].value
 *
 * This will return the item found at index 0 in the context. The order at which items are added to the context is
 * maintained to ensure this is enforced. The alternate method is a named context item e.g.
 *
 *      SLOPContext context = new SLOPContext();
 *      context.set("myObject", someObject);
 *      ...
 *      processor.process("myObject.myArray[0].intValue").getValue(Integer.class);
 *
 * Using the above two examples we can see the use of square brackets which are used for indexed Collections (above)
 * and String values for a key reference in a Map. Values read from objects using the FieldToken load the resulting
 * value(s) from the object into a Token so that they can potentially be used in follow-up statement operations.
 *
 * There are a number of special features that are provided by a FieldToken which are first the ability to extract
 * fields of an object in an array to a Collection. Typically in a language like Java you would have to iterate
 * through the Collection using a stream or loop and map the values into a separate collection. Using the FieldToken
 * you can simply omit the Collection index denoted by the square brackets and just refer to the field name e.g.
 *
 *      [Structure]
 *      Employee
 *      - (String) name
 *      - (Float) salary
 *
 *      Company
 *      - (List&lt;Employee&gt;) employees
 *
 *      [Code]
 *      List&lt;Employee&gt; employees = Arrays.asList(new Employee("Bob", 1234.99), new Employee("Mary", 4567.99));
 *      Company aCompany = new Company(employees);
 *      context.set("company", aCompany);
 *
 *      [Expression]
 *      company.employees.name
 *
 *      [Result]
 *      ["Bob", "Mary"]
 *
 * The second is that any object can call one of its native methods as long as the unsafe operations property is
 * enabled in the configuration properties. You can also chain these native calls together as you would do in the
 * original language. For example, using a String object you can use substring and concat with the following:
 *
 *      [Code]
 *      SLOPConfig config = new SLOPConfig();
 *      config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
 *      processor = new SLOPProcessor(config);
 *
 *      ...
 *
 *      SLOPContext context = new SLOPContext();
 *      context.set("myString", "caravan");
 *      String result = processor.process("myString.substring(1,3).concat(\" and trailer\")", context)
 *                          .getValue(String.class);
 *
 *      [Result]
 *      result = "car and trailer"
 *
 * When resolving any field value, it will use the safer method of invoking the Getter (if one exists). If that fails
 * and if the safe operations property in the configuration is disabled then it will fetch the value directly.
 */
public class FieldToken extends Token<Void> {

    public FieldToken() {
        super("Field", null);
    }

    /**
     * See {@link Token#createToken(String) createToken}
     */
    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new FieldToken());
    }

    /**
     * See {@link Token#getPatternType() getPatternType}
     */
    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    /**
     * See {@link Token#getPattern() getPattern}
     */
    @Override
    public String getPattern() {
        return "( val ( '[' expr ']' )? '.'? )+";
    }

    /**
     * See {@link Token#getGuidance(String, List) getGuidance}
     */
    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    /**
     * See {@link Token#process(SLOPParser, SLOPContext, SLOPConfig) process}
     */
    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        //For token re-evaluation (in loop for example), clone child tokens instead of removing permanently
        TokenGroup pathParts = new TokenGroup("1", getTokenGroups().get(0).getTokens());
        /* Each token representing an object or field will be in its own token group. As such we can simply
         * remove the first item, resolve it and iterate through the list. */
        Token<?> mainObject = pathParts.getTokens().remove(0);
        if (mainObject instanceof TokenGroup) {
            while (mainObject instanceof TokenGroup && ((TokenGroup) mainObject).getFlatTokens().isEmpty()) {
                mainObject = pathParts.getTokens().remove(0);
            }
        }
        Object found = null;
        if (!(mainObject instanceof TokenGroup) && mainObject.getValue() == null) {
            mainObject = parser.processExpression(Collections.singletonList(mainObject), context);
            found = mainObject.getValue();
        } else {
            //Resolve the main referenced object
            found = resolveMainObject(mainObject, context, parser, pathParts, config);
        }
        List<Token<?>> fieldParts = pathParts.getTokens();
        //Handle native call
        if (!pathParts.getTokens().isEmpty() && pathParts.getFlatTokens().get(0) instanceof InvocationToken)
            found = handleNativeCall(parser, context, config, fieldParts, found);
        if (fieldParts.isEmpty()) return Collections.singletonList(new TokenValue(found));
        //Make a call to recursive method to handle rest of field definition
        Optional<Token<?>> result = resolveField(parser, context, config, fieldParts, found,
                config.getProperty(DefaultProperty.SAFE_OPERATIONS, Boolean.class));
        if (result.isPresent()) {
            return Collections.singletonList(result.get());
        } else {
            throw new ParserException(String.format("Cannot find field(s) '%s' in class '%s'",
                    getNextFieldName(fieldParts), found.getClass().getSimpleName()));
        }
    }

    private Object resolveMainObject(Token<?> mainObject, SLOPContext context, SLOPParser parser, TokenGroup pathParts,
                                     SLOPConfig config) {
        String objectId = null;
        Object resolvedObject = null;
        Object result = null;
        int collectionRef = -1;
        //Break loop in case main object can be resolved early
        while (true) {
            if (mainObject instanceof TokenGroup) {
                TokenGroup tgFirst = ((TokenGroup) mainObject);
                //Attempt to resolve the object using the first token in the group
                Token<?> tokenValue = parser.processExpression(Collections.singletonList(
                        tgFirst.getFlatTokens().get(0)), context);
                if (tokenValue.getVariableType() == VariableType.INSTANCE) return tokenValue;
                boolean rawCollectionRef = pathParts.getTokens().isEmpty() && tokenValue.getValue() instanceof Collection;
                if (tgFirst.getFlatTokens().get(0) instanceof TokenValue || rawCollectionRef) {
                    result = tokenValue.getValue();
                    if (tgFirst.getFlatTokens().size() > 1 && tgFirst.getFlatTokens().get(1).is(Integer.class)) {
                        collectionRef = tgFirst.getFlatTokens().get(1).getValue(Integer.class);
                    } else {
                        if (((TokenGroup) mainObject).getTokens().size() > 1 && ((TokenGroup) mainObject).getTokens().get(1) instanceof TokenGroup &&
                                !((TokenGroup)((TokenGroup) mainObject).getTokens().get(1)).getFlatTokens().isEmpty()) {
                            List<Token<?>> ref = ((TokenGroup) ((TokenGroup) mainObject).getTokens().get(1)).getFlatTokens();
                            if (isCollectionSearch(ref)) {
                                result = handleCollectionSearch(ref, context, result, config, parser);
                            }
                        }
                    }
                    break;
                }
                //Regardless of whether an object is found, try and resolve an object ID as a fallback
                boolean failed = true;
                try {
                    tokenValue.getValue(String.class);
                    failed = false;
                } catch (ClassCastException ex) { }
                try {
                    tokenValue.getValue(Integer.class);
                    failed = false;
                } catch (ClassCastException ex) { }
                /* If the above break statement is hit then the object ID is neither a String or Integer. As such
                 * the ID the value will be cast to a String in the hope that it is a valid object. An error will be thrown
                 * later if no object can be found. */
                if (!failed) {
                    objectId = tgFirst.getFlatTokens().get(0).getValue().toString();
                    if (tgFirst.getFlatTokens().size() > 1 && tgFirst.getFlatTokens().get(1).is(Integer.class)) {
                        collectionRef = tgFirst.getFlatTokens().get(1).getValue(Integer.class);
                    } else {
                        if (((TokenGroup) mainObject).getTokens().size() > 1 && ((TokenGroup) mainObject).getTokens().get(1) instanceof TokenGroup) {
                            result = resolveObject(context, objectId);
                            if (((Optional)result).isPresent()) {
                                List<Token<?>> ref = ((TokenGroup) ((TokenGroup) mainObject).getTokens().get(1)).getFlatTokens();
                                if (isCollectionSearch(ref)) {
                                    result = handleCollectionSearch(ref, context, ((Optional)result).get(), config, parser);
                                }
                            }
                        }
                    }
                }
                //If an object was found from earlier then assign it to a variable
                resolvedObject = tokenValue.getValue();
            } else {
                objectId = mainObject.getValue(String.class);
            }
            break;
        }
        //Attempt to resolve the object using the object ID
        if (Objects.isNull(result)) {
            result = resolveObject(context, objectId).orElse(null);
        }
        //Use either the resolved object token based on whether the next token is a FunctionToken (unsafe call)
        result = Objects.isNull(result) && !pathParts.getFlatTokens().isEmpty() && pathParts.getFlatTokens().get(0) instanceof InvocationToken
                ? resolvedObject : result;
        if (Objects.isNull(result))
            throw new ParserException(String.format("Could not find object '%s' in SLOP context", objectId));
        if (result instanceof Collection) {
            Collection<?> collection = (Collection<?>)result;
            if (collectionRef == -1) return result;
            return collection.toArray(new Object[0])[collectionRef];
        }
        return result;
    }

    /**
     * Resolves the name of the next object / field based on the current list of expression Tokens. It handles token
     * groups and resolves the first token found within.
     * @param expression The current expression as a list of tokens
     * @return Returns the name of the next object or field
     */
    private String getNextFieldName(List<Token<?>> expression) {
        if (expression.isEmpty()) return "";
        Token<?> first = expression.get(0);
        if (first instanceof TokenGroup) {
            TokenGroup tokenGroup = (TokenGroup) first;
            return tokenGroup.getTokens().get(0).getValue(String.class);
        }
        return first.getValue(String.class);
    }

    /**
     * Attempts to fetch the object either by index first or alternatively name from the context
     * @param context The context where objects relating to references in the expression String are stored
     * @param objectId The ID to resolve the relevant object from context
     * @return An optional of the found object
     */
    private Optional<Object> resolveObject(SLOPContext context, String objectId) {
        try {
            int index = Integer.parseInt(objectId);
            return Optional.ofNullable(context.getContextObject(index));
        } catch (NumberFormatException ex) {
            return Optional.ofNullable(context.getVariable(objectId));
        }
    }

    /**
     * Native calls (calls to methods on context / scoped objects) by default do not run unless the
     * DefaultProperty.SAFE_OPERATIONS is disabled. This is because it may lead to unexpected consequences or even
     * security issues. If this is enabled then it does allow any scoped object method to be called.
     * @param parser The parser to resolve any token values
     * @param context The context containing objects referenced in expressions
     * @param config The SLOP configuration object
     * @param expression The field tokens currently being processed
     * @param object The object to which the native method relates
     * @return Returns any object returned from the native call. This allows them to be chained together
     */
    private Object handleNativeCall(SLOPParser parser, SLOPContext context, SLOPConfig config,
                                    List<Token<?>> expression, Object object) {
        Token<?> token = expression.remove(0);
        if (token instanceof TokenGroup) {
            TokenGroup functionGroup = (TokenGroup)token;
            InvocationToken invocationToken = (InvocationToken)functionGroup.getFlatTokens().get(0);
            //Throw exception if safe operations property is still enabled
            if (config.getProperty(DefaultProperty.SAFE_OPERATIONS, Boolean.class)) {
                throw new ParserException(String.format("Native call attempt to method '%s' on object type '%s' with safe " +
                                "operation mode enabled. Please disable safe operation mode in config properties and retry",
                        invocationToken.getTokenGroups().get(0).getFlatTokens().get(0).getValue(), object.getClass().getSimpleName()));
            }
            AtomicBoolean retry = new AtomicBoolean(false);
            //Fetch the method name of the native call from the function token
            String methodName = invocationToken.getTokenGroups().get(0).getFlatTokens().get(0).getValue().toString();
            while (true) {
                try {
                    //Construct the params and class types to invoke via reflection
                    List<Token<?>> params = new ArrayList<>();
                    if (invocationToken.getTokenGroups().size() > 1) {
                        TokenGroup paramsGroup = invocationToken.getTokenGroups().get(1);
                        for (Token<?> paramGroup : paramsGroup.getTokens()) {
                            params.add(parser.processExpression(
                                    paramGroup instanceof TokenGroup ?
                                            ((TokenGroup)paramGroup).getTokens() : Collections.singletonList(paramsGroup),
                                    context));
                        }
                    }
                    params = params.stream()
                            .map(p -> parser.processExpression(Collections.singletonList(p), context))
                            .collect(Collectors.toList());
                    if (object instanceof Token<?> && ((Token<?>)object).getVariableType() == VariableType.INSTANCE) {
                        if (!(object instanceof TokenInstance)) {
                            throw new ParserException(String.format("Could not invoke method on object '%s' as Token " +
                                    "class does not implement TokenParameters", object.getClass().getSimpleName()));
                        }
                        //TODO - Need to force this to avoid having to do it manually
                        parser.tokenStart((Token<?>)object);
                        //Invoke call to the instance class
                        Object result = ((TokenInstance)object).process(parser, context, config, methodName, params);
                        parser.tokenEnd((Token<?>)object);
                        if (result instanceof Collection && ((Collection<?>)result).size() == 1) {
                            return ((Collection<?>)result).toArray()[0];
                        }
                        return result;
                    } else {
                        final int paramsSize = params.size();
                        List<Method> found = Stream.of(object.getClass().getDeclaredMethods())
                                .filter(m -> m.getName().equalsIgnoreCase(methodName))
                                .filter(m -> m.getParameterCount() == paramsSize)
                                .collect(Collectors.toList());
                        for (Method method : found) {
                            try {
                                method.setAccessible(true);
                                boolean rawTypes = Stream.of(method.getParameterTypes())
                                        .noneMatch(t -> !Objects.equals(t, Object.class) && t.isAssignableFrom(Token.class));
                                Object result = method.invoke(object, rawTypes ?
                                        params.stream().map(Token::getValue).collect(Collectors.toList()).toArray() : params.toArray());
                                if (method.getReturnType().getName().equalsIgnoreCase("void")) return null;
                                return Objects.isNull(result) ? method.invoke(object, params.toArray()) : result;
                            } catch (IllegalArgumentException ex) {
                            }
                        }
                    }
                    throw new NoSuchMethodException("Failed to find suitable method for parameters");
                } catch (NoSuchMethodException ex) {
                    //If no method was found then retry using primitive equivalents if they exist
                    if (!retry.get()) {
                        retry.set(true);
                        continue;
                    }
                    //Throw an exception if default and primtiive equivalent classes have failed
                    throw new ParserException(String.format("Could not find native method '%s' in object type '%s'",
                            invocationToken.getTokenGroups().get(0).getFlatTokens().get(0).getValue(), object.getClass().getSimpleName()));
                } catch (InvocationTargetException | IllegalAccessException exc) {
                    throw new ParserException(String.format("An error occurred trying to invoke native method '%s' in object type '%s': %s",
                            invocationToken.getTokenGroups().get(0).getFlatTokens().get(0).getValue(), object.getClass().getSimpleName(),
                            exc.getMessage()));
                }
            }
        }
        throw new ParserException(String.format("Expected native call to be a TokenGroup, but instead found '%s'",
                token.getClass().getSimpleName()));
    }

    /**
     * Processes the remaining tokens in the FieldToken expression by recursively calling itself. It attempts to
     * resolve the provided field or object given the parent object.
     * @param parser The parser to resolve any token values
     * @param context The context where objects relating to references in the expression String are stored
     * @param config The SLOP configuration object used to retrieve stored properties
     * @param expression The tokens being processed from the field token
     * @param in The parent object relevant to the current field / token
     * @param safeOperations Determines whether the safe operations option is enabled
     * @return Returns the resolved value from referenced field
     */
    private Optional<Token<?>> resolveField(SLOPParser parser, SLOPContext context, SLOPConfig config,
                                            List<Token<?>> expression, Object in, boolean safeOperations) {
        //Extract the raw value contained within if it has previously been converted e.g. collection item
        if (in instanceof Token<?>) {
            if (((Token<?>)in).getVariableType() == VariableType.INSTANCE && in instanceof TokenInstance) {
                Token<?> found = findFirstNonEmptyGroup(expression);
                return Optional.of(((TokenInstance)in).process(parser, context, config, found.getValue().toString(),
                        expression.size() > 1 ? expression.subList(1, expression.size() - 1) : new ArrayList<>()).get(0));
            }
            in = ((Token<?>) in).getValue();
        }
        String fieldName;
        Object result = null;
        Token<?> current = expression.remove(0);
        List<Token<?>> collectionRef = null;
        //Resolve the field name and any collection reference (if specified)
        if (current instanceof TokenGroup) {
            TokenGroup tgFirst = (TokenGroup) current;
            List<Token<?>> refTokens = tgFirst.getFlatTokens();
            ArrayList<Token<?>> list = new ArrayList<>();
            list.add(new TokenGroup(refTokens));
            if (!refTokens.isEmpty() && refTokens.get(0) instanceof InvocationToken)
                result = handleNativeCall(parser, context, config, list, in);
            fieldName = refTokens.remove(0).getValue(String.class);
            collectionRef = !refTokens.isEmpty() ? refTokens : null;
        } else {
            fieldName = current.getValue(String.class);
        }
        try {
            if (Objects.isNull(result)) {
                Field field = null;
                if (!(in instanceof Collection)) {
                    if (in instanceof LinkedHashMap) {
                        LinkedHashMap<?,?> map = (LinkedHashMap<?,?>) in;
                        result = map.get(fieldName);
                        if (!expression.isEmpty() || (Objects.nonNull(collectionRef) && !collectionRef.isEmpty())) {
                            if (!Objects.isNull(collectionRef) && result instanceof Collection) {
                                if (isCollectionSearch(collectionRef)) {
                                    result = handleCollectionSearch(collectionRef, context, result, config, parser);
                                } else {
                                    int index = mapGenericIndex(parser.processExpression(collectionRef, context).getValue());
                                    result = ((Collection<?>) result).toArray()[index];
                                    if (expression.isEmpty()) return Optional.of(new TokenValue(result));
                                }
                            }
                            return resolveField(parser, context, config, expression, result, safeOperations);
                        }
                    } else {
                        //If the result hasn't previously been resolved via a native call, invoke a getter on the object
                        field = in.getClass().getDeclaredField(fieldName);
                        result = invokeGetter(in, fieldName);
                        if (Objects.isNull(result) && !safeOperations) {
                            try {
                                field.setAccessible(true);
                                result = field.get(in);
                            } catch (IllegalAccessException ex) {
                                throw new ParserException("Unsafe Field Operation Error: " + ex.getMessage());
                            }
                        }
                    }
                } else {
                    return handleMapCollection(parser, context, collectionRef, (Collection<LinkedHashMap<?, ?>>) in, fieldName,
                        expression, config, safeOperations);
                }
                //If the object is a collection, use the collection reference to fetch the relevant item
                if (result instanceof Collection) {
                    if (isCollectionSearch(collectionRef)) {
                        if (!expression.isEmpty() && ((TokenGroup)expression.get(0)).getFlatTokens().get(0) instanceof InvocationToken) {
                            collectionRef.add(expression.remove(0));
                        }
                        result = handleCollectionSearch(collectionRef, context, result, config, parser);
                        if (result instanceof Collection) {
                            return handleCollection(parser, context, config, expression, fieldName, field, result, -1,
                                    safeOperations);
                        } else {
                            if (expression.isEmpty()) return Optional.of(new TokenValue(result));
                            return resolveField(parser, context, config, expression, result, safeOperations);
                        }
                    } else {
                        int ref = Objects.isNull(collectionRef) ? -1 :
                                mapGenericIndex(parser.processExpression(collectionRef, context).getValue());
                        return handleCollection(parser, context, config, expression, fieldName, field, result, ref,
                                safeOperations);
                    }
                } else if (result instanceof Map) {
                    Token<?> keyRef = parser.processExpression(collectionRef, context);
                    if (keyRef.is(String.class)) {
                        return handleMap(fieldName, result, keyRef.getValue(String.class));
                    } else {
                        throw new ParserException(String.format("Invalid key (%s) result used in field map (%s) reference",
                                keyRef, fieldName));
                    }
                }
                if (!Objects.isNull(collectionRef)) {
                    throw new ParserException(String.format("Index argument specified [%s] on non-collection field '%s'",
                            collectionRef.stream().map(Object::toString).collect(Collectors.joining(" ")), fieldName));
                }
            }
            result = mapToToken(config, result);
            if (expression.isEmpty()) return Optional.of((Token<?>)result);
            return resolveField(parser, context, config, expression, result, safeOperations);
        } catch (NoSuchFieldException ex) {
            throw new ParserException(String.format("Cannot find field(s) '%s' in class '%s'",
                    fieldName, in.getClass().getSimpleName()));
        }
    }

    /**
     * Finds either the first non-empty token group or first non-group token found
     * @param expression The tokenised expression
     * @return Returns the first found token group or token
     */
    private Token<?> findFirstNonEmptyGroup(List<Token<?>> expression) {
        for (Token<?> current : expression) {
            if (current instanceof TokenGroup && !((TokenGroup)current).getFlatTokens().isEmpty()) {
                return ((TokenGroup)current).getFlatTokens().get(0);
            } else if (current instanceof TokenValue) {
                return current;
            }
        }
        throw new ParserException("Expected one object path variable, but found none!");
    }

    /**
     * Determines whether the collection of passed tokens is an inline collection search. This is usually
     * denoted by the first token being a NonToken with the value '^'.
     * @param collectionRef The collection of tokens contained within an index set of square brackets
     * @return Returns true if it is found to be a collection search
     */
    private boolean isCollectionSearch(List<Token<?>> collectionRef) {
        return Objects.nonNull(collectionRef) && ((ArrayList<?>)collectionRef).get(0) instanceof NonToken &&
            ((NonToken) ((ArrayList<?>)collectionRef).get(0)).getValue(String.class)
                .equalsIgnoreCase("^");
    }

    /**
     * Using the criteria specified in the collection search, it generates another SLOP statement to run a
     * for loop with an if statement to undertake the task and return the result. The collection search is
     * ultimates sugar coating, but it's less verbose and easier to define.
     * @param collectionRef The list of tokens containing criteria
     * @param context The context object which will be used when executing the generating statement
     * @param result The source collection on which the search will be run
     * @param config The configuration object to be used by the lexer when processing the statement
     * @param parser The parser to evaluate the generated statement
     * @return The result of the collction search
     */
    private Object handleCollectionSearch(List<Token<?>> collectionRef, SLOPContext context, Object result,
                                                      SLOPConfig config, SLOPParser parser) {
        ((ArrayList<?>)collectionRef).remove(0);
        context.set("fieldCollection", result);
        StringBuilder sb = new StringBuilder();
        formatCollectionSearchTokens(sb, collectionRef);
        String condition = sb.toString();
        SLOPLexer lexer = new SLOPLexer(config);
        String colCondition = String.format("for (obj : fieldCollection) if (%s) return & obj;",
                condition);
        LexerResult<Token<?>> lexerResult = lexer.tokenize(new Expression(colCondition));
        Object filterResult = parser.process(lexerResult, context).getValue();
        if (filterResult instanceof Collection) {
            if (((Collection<?>)filterResult).size() > 1) {
                return filterResult;
            } else if (!((Collection<?>)filterResult).isEmpty()) {
                return ((ArrayList<?>)filterResult).get(0);
            }
        }
        return filterResult;
    }

    /**
     * Formats the collection search tokens into String form so they can be added to the new expression
     * @param sb The string builder used to hold the new expression
     * @param collectionRef The list of tokens to be scanned and mapped to the new expression
     */
    private void formatCollectionSearchTokens(StringBuilder sb, List<Token<?>> collectionRef) {
        boolean collectionField = false;
        boolean aBreak = false;
        for (int i = 0;i < collectionRef.size();i++) {
            Token<?> token = collectionRef.get(i);
            while (token instanceof TokenGroup) token = ((TokenGroup) token).getTokens().get(0);
            if (Objects.nonNull(token.getValue()) && token.getValue().toString().equalsIgnoreCase("~")) {
                collectionField = true;
                continue;
            }
            if (collectionField) {
                if (token instanceof FieldToken) {
                    sb.append("obj.");
                    List<Token<?>> params = token.getTokenGroups().get(0).getFlatTokens();
                    formatCollectionSearchTokens(sb, params);
                } else {
                    sb.append(String.format("obj.%s", token));
                }
                collectionField = false;
            } else if (token instanceof InvocationToken) {
                List<Token<?>> params = token.getTokenGroups().size() > 1 ?
                        token.getTokenGroups().get(1).getFlatTokens() : new ArrayList<>();
                sb.append(String.format("%s(", token.getTokenGroups().get(0).getTokens().get(0).getValue()));
                formatCollectionSearchTokens(sb, params);
                sb.append(")");
            } else if (token instanceof UnaryOperator) {
                sb.append("!");
                List<Token<?>> params = token.getTokenGroups().get(0).getFlatTokens();
                formatCollectionSearchTokens(sb, params);
            } else if (token instanceof FieldToken) {
                List<Token<?>> params = token.getTokenGroups().get(0).getFlatTokens();
                formatCollectionSearchTokens(sb, params);
            } else if (token instanceof OperatorToken) {
                if (sb.toString().endsWith(".")) {
                    sb.replace(sb.length() - 1, sb.length(), "");
                }
                sb.append(String.format(" %s ", token.getValue()));
                aBreak = true;
            } else {
                if (token instanceof LogicOperatorHandler) {
                    if (sb.toString().endsWith(".")) {
                        sb.replace(sb.length() - 1, sb.length(), "");
                    }
                    sb.append(String.format(" %s ", token.getValue()));
                    aBreak = true;
                } else {
                    sb.append(token);
                }
            }
            if (i != collectionRef.size() - 1 && !aBreak) {
                sb.append(".");
            }
            aBreak = false;
        }
    }

    /**
     * Resolves a value from a LinkedHashMap inside a FieldToken reference.
     * @param parser The parser which may be required when resolving certain values like collection references
     * @param context The context from which stored values may be retrieved in the FieldToken reference
     * @param collectionRef An optional collection reference if one were specified
     * @param collectionMap The map from which the current reference relates
     * @param fieldName The name of the field to be fetched
     * @param expression The full expression list of tokens
     * @param config The configuration object required for native calls and collection reference searches
     * @param safeOperations Determines whether safe operations are enabled (no native calls)
     * @return Returns a resolved value from the map
     */
    private Optional<Token<?>> handleMapCollection(SLOPParser parser, SLOPContext context, List<Token<?>> collectionRef,
                                                   Collection<LinkedHashMap<?,?>> collectionMap, String fieldName,
                                                   List<Token<?>> expression, SLOPConfig config, boolean safeOperations) {
        List<Token<?>> resultArray = new ArrayList<>();
        for (LinkedHashMap<?,?> map : collectionMap) {
            Object result = map.get(fieldName);
            if (!expression.isEmpty()) {
                if (!Objects.isNull(collectionRef) && result instanceof Collection) {
                    int index = mapGenericIndex(parser.processExpression(collectionRef, context).getValue());
                    result = ((Collection<?>)result).toArray()[index];
                }
                return resolveField(parser, context, config, expression, result, safeOperations);
            }
            resultArray.add(mapToToken(config, result));
        }
        return resultArray.isEmpty() ? Optional.empty() : Optional.of(new ArrayToken(resultArray));
    }

    /**
     * Attempts to map the field type to an equivalent Token class. If no matching token handler is found then
     * the TokenValue default is used.
     * @param config The config to fetch a list of token handlers
     * @param result The field result to map to a Token
     * @return Returns the resulting Token class
     */
    private Token<?> mapToToken(SLOPConfig config, Object result) {
        if (Objects.isNull(result)) return new NullToken();
        List<Token<?>> found = config.getTokenHandlers().stream()
                .filter(th -> {
                    Type genType = th.getClass().getGenericSuperclass();
                    if (genType instanceof ParameterizedType) {
                        Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
                        Class<?> tokenHandlerClass;
                        try {
                            tokenHandlerClass = (Class<?>) params[0];
                        } catch (Exception ex) {
                            tokenHandlerClass = (Class<?>)((ParameterizedType) params[0]).getRawType();
                        }
                        return result.getClass().equals(tokenHandlerClass);
                    }
                    return false;
                })
                .collect(Collectors.toList());
        Token<?> selected;
        if (found.size() > 1) {
            //If multiple token handlers were found then use the default or throw an exception
            selected = found.stream()
                    .filter(Token::isDefault)
                    .findFirst()
                    .orElseThrow(() -> new ParserException(String.format("Multiple handlers found for type '%s' with " +
                            "no default set", result.getClass().getSimpleName())));
        } else if (found.size() == 1) {
            selected = found.get(0);
        } else {
            return new TokenValue(result);
        }
        //Return a new instance of the token class passing the object as a constructor parameter
        try {
            Constructor<?> constructor = selected.getClass().getConstructor(result.getClass());
            return (Token<?>)constructor.newInstance(result);
        } catch (NoSuchMethodException ex) {
            throw new ParserException(String.format("Could not find setValue method on Token Class '%s'",
                    found.getClass().getSimpleName()));
        } catch (Exception ex) {
            throw new ParserException(String.format("Token Creation Error: " + ex.getMessage()));
        }
    }

    /**
     * Attempts to map the collection reference to an integer equivalent
     * @param source The source value
     * @return Returns an integer index value
     */
    private int mapGenericIndex(Object source) {
        if (source instanceof BigDecimal) {
            return new BigDecimal(source.toString()).intValue();
        } else if (source instanceof Float) {
            return ((Float) source).intValue();
        } else if (source instanceof Double) {
            return ((Double) source).intValue();
        } else if (source instanceof Integer) {
            return (int)source;
        }
        throw new ParserException(String.format("Cannot map index reference '%s' to integer", source.toString()));
    }

    /**
     * Fetches either a single item from a collection or multiple based on a field reference.
     * @param parser The parser used to resolve token values
     * @param context The context where objects relating to references in the expression String are stored
     * @param config The SLOP configuration object used to retrieve stored properties
     * @param expression The tokens being processed from the field token
     * @param field The generic type for the given collection
     * @param fieldObject The field to fetch from the collection
     * @param object The collection object
     * @param index The index (-1 if none specified)
     * @param safeOperations Determines whether safe operation property is enabled
     * @return Returns one or more resolved values from the collection reference
     */
    private Optional<Token<?>> handleCollection(SLOPParser parser, SLOPContext context, SLOPConfig config,
                                                List<Token<?>> expression, String field, Field fieldObject, Object object,
                                                int index, boolean safeOperations) {
        if (Objects.isNull(fieldObject)) {
            //If no index has been specified and the field reference is null then return the current object
            if (index == -1) return Optional.of(new TokenValue(object));
            throw new ParserException(String.format("No generic type found for List object to retrieve '%s'", field));
        }
        Type type = fieldObject.getGenericType();
        //Check to see if we are using a parameterised type which we can resolve
        if (type instanceof ParameterizedType) {
            //We have previously checked it to be calculation so just cast and loop items
            Collection<?> collection = (Collection<?>)object;
            List<Object> values = new ArrayList<>();
            if (index != -1) {
                if (index > collection.size() - 1) {
                    throw new ParserException(String.format("Invalid array index specified. %d is greater than the " +
                            "collection size %d", index, collection.size()));
                }
                Object found = collection.toArray(new Object[0])[index];
                if (expression.isEmpty()) return Optional.of(new TokenValue(found));
                return resolveField(parser, context, config, expression, found, safeOperations);
            }
            Token<?> firstToken = !expression.isEmpty() ? (expression.get(0) instanceof TokenGroup) ?
                    ((TokenGroup)expression.get(0)).getFlatTokens().get(0) :
                    expression.get(0) : null;
            if (Objects.nonNull(firstToken) && firstToken instanceof InvocationToken) {
                Object nativeResult = handleNativeCall(parser, context, config, expression, collection);
                return Optional.of((nativeResult instanceof Boolean) ?
                        new BooleanToken((Boolean)nativeResult) : new TokenValue(nativeResult));
            } else {
                for (Object subObject : collection) {
                    Optional<Token<?>> result = Optional.empty();
                    //Pass sub-object field and current object from which to extract
                    if (expression.isEmpty()) {
                        values.add(subObject);
                    } else {
                        List<Token<?>> cloneExpression = new ArrayList<>();
                        /* Even though warning shown here, passing the expression via constructor shares tokens. As such
                         * we use addAll which clones different instances. */
                        cloneExpression.addAll(expression);
                        result = resolveField(parser, context, config, cloneExpression, subObject,
                                safeOperations);
                    }
                    result.ifPresent(values::add);
                }
            }
            return Optional.of(new TokenValue(values));
        }
        return Optional.empty();
    }

    /**
     * Fetches an object from a Map and returns the result as a token
     * @param field The values specified in the expression to construct the Map key
     * @param object The map object
     * @param ref The actual key value
     * @return Returns the resolved Map key value
     */
    private Optional<Token<?>> handleMap(String field, Object object, String ref) {
        Map<?,?> theMap = (Map<?,?>)object;
        Object result = Optional.ofNullable(theMap.get(ref)).orElseThrow(() ->
                new ParserException(String.format("The key '%s' for the Map '%s' does not exist",
                        ref, field.split("\\[")[0])));
        return Optional.of(new TokenValue(result));
    }

    /**
     * Attempts to resolve a field value in a safe way by invoking the getter method
     * @param obj The object on which to call the getter method for the field
     * @param variableName The String field name to fetch using a getter
     * @return Returns the resolved object
     */
    private Object invokeGetter(Object obj, String variableName) {
        try {
            PropertyDescriptor pd = new PropertyDescriptor(variableName, obj.getClass());
            Method getter = pd.getReadMethod();
            return getter.invoke(obj);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return "FieldToken{parts=" + getTokenGroups().get(0) + '}';
    }

    /**
     * See {@link Token#toSimpleString() toSimpleString}
     */
    @Override
    public String toSimpleString() {
        return getTokenGroups().get(0).getFlatTokens().stream()
                .map(Token::toSimpleString)
                .collect(Collectors.joining("."));
    }

    /**
     * See {@link Token#set(SLOPParser, SLOPContext, Token) createToken}
     */
    @Override
    public Token<?> set(SLOPParser parser, SLOPContext context, Token<?> value) {
        //TODO - Set value on object
        return super.set(parser, context, value);
    }
}
