package dev.slop.event;

import dev.slop.tokens.Token;

/**
 * This is implemented by any class which wishes to subscribe to parser notification events.
 * They would also need to be added to the subscriber list. The primary goal of this is to
 * notify any subscribing class of these events.
 */
public interface TokenEventSubscriber {
    void notify(String uid, Token<?> token, int depth, NotificationType type);
    void notify(String uid, Token<?> token, int depth, NotificationType type, String fallback, int fallbackDepth);
}
