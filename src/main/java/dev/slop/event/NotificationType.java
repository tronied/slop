package dev.slop.event;

/**
 * These are notification subscriber events from the parser. At the moment these are just to let
 * other classes know when a token has started / finished processing, but these could be expanded
 * in future to allow additional functionality.
 */
public enum NotificationType {
    ADDITION,
    REMOVAL
}
