package dev.slop.context;

import dev.slop.enums.VariableType;
import dev.slop.event.NotificationType;
import dev.slop.event.TokenEventSubscriber;
import dev.slop.tokens.Token;
import dev.slop.tokens.TokenInstance;
import dev.slop.tokens.literals.NullToken;

import java.util.*;

/**
 * The context class is used to store the objects which can be referenced within the Expressions read by SLOP. Objects
 * can either be added with or without a given name. If no name is provided then the index of the object (order in
 * which it is added) can be used e.g. 0.myField. The context object is used by the Parser for resolving referenced values.
 */
public class SLOPContext implements TokenEventSubscriber {
    private List<Object> objects;
    private Map<String, Integer> contextObjects;
    private Map<String, Object> valueCache;
    //Keeps track of which variables are instantiated with each token
    private Map<String, List<String>> stackVariables;
    //Stores references to each layer (recursive) that a variable is created / referenced
    private Map<String, Stack<String>> stackVarOccurrences;
    //Keeps track of all prototypes held in memory
    private List<String> prototypes;
    //A stack with all tokens currently being executed. The top is the most recent working backwards
    private Stack<Token<?>> tokenStack;
    //The token UID being processed
    private String activeToken;
    //The depth of the active token (if recursive)
    private int activeTokenDepth;
    private boolean isDebug;

    public SLOPContext() {
        objects = new ArrayList<>();
        contextObjects = new HashMap<>();
        valueCache = new HashMap<>();
        stackVariables = new HashMap<>();
        stackVarOccurrences = new HashMap<>();
        prototypes = new ArrayList<>();
        tokenStack = new Stack<>();
    }

    /**
     * Determines whether debug mode is enabled to output events in the context when they occur.
     * Ideally this should be handled by log4j or other framework which on the roadmap.
     * @param debug Enables / disables debug mode
     */
    public void setDebug(boolean debug) {
        this.isDebug = debug;
    }

    /**
     * Adds an object into context but instead of using a name, it is referenced using the index
     * of the object in the order it was added. For example, to reference you'd use 0.acme.employees
     * @param object The object to add so that it can be referenced by an index
     */
    public void add(Object object) {
        objects.add(object);
    }

    /**
     * Adds multiple objects into the context using the same index referencing as add(Object)
     * @param collection The collection of objects to add
     */
    public void addAll(Collection<?> collection) {
        objects.addAll(collection);
    }

    /**
     * Sets a variable using a reference name in context. This can then be used in expressions using
     * that name.
     * @param name The name under which the object is to be referenced
     * @param object The object to store
     */
    public void set(String name, Object object) {
        set(name, object, VariableType.DEFAULT);
    }

    /**
     * An overloaded version of the set(String, Object) method which allows an additional VariableType
     * parameter to be specified. This changes the way the variable is handled. For example, a normal
     * variable uses DEFAULT, but other types including PARAMETER (supports recursion) and SELF (only
     * one instance) can be specified.
     * @param name The name under which the object is to be referenced
     * @param object The object to store
     * @param type The type to determine how the variable is handled in context
     */
    public void set(String name, Object object, VariableType type) {
        if (Objects.isNull(object)) object = new NullToken();
        if (isDebug)
            System.out.println(String.format("Setting variable %s = %s", name, object));
        if (type == VariableType.PARAMETER || activeTokenDepth > 1) {
            setVariable(name, object, type);
        } else {
            setSingleVariable(name, object, type);
        }
    }

    /**
     * A simple method used to sets a variable with just the name and object without the need to maintain a stack
     * and history in the case of recursion. This is the step used on the first set of a variable, but will defer
     * to the setVariable method on subsequent calls if depth and history changes.
     * @param name The name of the variable
     * @param object The object to set as it's value
     * @param type The type of variable (see VariableType for more information)
     */
    private void setSingleVariable(String name, Object object, VariableType type) {
        if (type == VariableType.PROTOTYPE) prototypes.add(name);
        if (type != VariableType.SINGLETON && type != VariableType.PROTOTYPE &&
                !contextObjects.containsKey(name)) {
            setStackVariable(name);
        }
        if (contextObjects.containsKey(name)) {
            int index = contextObjects.get(name);
            objects.set(index, object);
        } else {
            objects.add(object);
            contextObjects.put(name, objects.size() - 1);
        }
    }

    /**
     * Fetches the value of an object stored in the context by name
     * @param name The name of the object to fetch
     * @return Returns the object or null if none found
     */
    public Object getContextObject(String name) {
        //Search up the stack for any instance reference of the resource being requested
        for (int i = tokenStack.size() - 1;i > -1;i--) {
            if (tokenStack.get(i) instanceof TokenInstance) {
                Object found = ((TokenInstance)tokenStack.get(i)).getResourceMap().get(name);
                if (Objects.nonNull(found))
                    return found;
            }
        }
        //Failing a match on the stack, look in the default heap
        Integer index = contextObjects.get(name);
        if (Objects.isNull(index)) return null;
        Object result = objects.get(index);
        if (isDebug)
            System.out.println(String.format("Reading variable %s = %s", name, result));
        return result;
    }

    /**
     * Removes an object from context using the reference name. This removes both from the object
     * store and the index reference.
     * @param name The name of the object to remove
     */
    public void removeContextObject(String name) {
        Integer index = contextObjects.get(name);
        if (Objects.isNull(index)) return;
        contextObjects.remove(name);
        contextObjects.entrySet().stream()
                .filter(e -> e.getValue() > index)
                .forEach(e -> e.setValue(e.getValue() - 1));
        objects.remove((int)index);
    }

    /**
     * Fetches an object using the index from context. This is used where an object has been added
     * without a name. This reference may be defined in an expression as &lt;index&gt;.myField
     * @param index The index of the object to fetch
     * @return Returns the object or null if none found
     */
    public Object getContextObject(int index) {
        return objects.get(index);
    }

    private void setVariable(String name, Object object, VariableType type) {
        setStackVariable(name);
        /* Count the number of occurrences of the variable to determine whether we are in the process
         * of recursion and set accordingly */
        if (countRootOccurrences(stackVarOccurrences.get(name)) > 1) {
            if (contextObjects.containsKey(name)) {
                int index = contextObjects.get(name);
                if (objects.get(index) instanceof Stack) {
                    Stack varStack = (Stack) objects.get(index);
                    varStack.push(object);
                } else {
                    Stack stack = new Stack<>();
                    stack.push(objects.get(index));
                    stack.push(object);
                    objects.add(stack);
                    contextObjects.put(name, objects.size() - 1);
                }
            } else {
                Stack stack = new Stack<>();
                stack.push(object);
                objects.add(stack);
                contextObjects.put(name, objects.size() - 1);
            }
        } else {
            setSingleVariable(name, object, type);
        }
    }

    /**
     * Uses the variables stack history to check it's root item (where it was defined) and count the
     * number of times it exists within the stack. If there is more than 1 then this means it should
     * maintain a state for each depth level which happens with recursion.
     * @param varOccurrences The current stack history for the current variable
     * @return The number of occurrences of the class where the current variable was defined
     */
    private int countRootOccurrences(Stack<String> varOccurrences) {
        int count = 0;
        String root = varOccurrences.get(0);
        for (String item : varOccurrences) {
            if (item.equals(root)) count++;
        }
        return count;
    }

    /**
     * Gets a variable by name from context. Variables are handled differently to standard context
     * objects in that they are added to a stack. This stack increases or decreases in size based
     * on the number of iterative recursions that occur. Take for example a function which accepts an
     * n parameter and calls itself with an incremented number whilst adding the result to the return
     * statement. Each value must be stored and as each repeat recursion occurs / completes it adds,
     * modifies and removes it from the stack.
     * @param name The name of the variable to add (may exist and in which case gets added to stack)
     * @return Returns the current value of that value
     */
    public Object getVariable(String name) {
        Integer index = contextObjects.get(name);
        if (Objects.isNull(index)) return null;
        Object result;
        if (!(getContextObject(name) instanceof Stack)) {
            result = getContextObject(name);
        } else {
            Stack varStack = (Stack) objects.get(index);
            result = varStack.peek();
        }
        if (isDebug)
            System.out.println(String.format("Reading variable %s = %s", name, result));
        return result;
    }

    public List<String> getPrototypes() {
        return prototypes;
    }

    /**
     * Called as part of internal garbage collection of old values during recursion. Once an iterative
     * cycle has finished and the token is no longer in scope, it is removes which either means removing
     * it from the stack or removing it entirely from the list of objects.
     * @param name The name of the variable to remove
     */
    private void removeVariable(String name) {
        if (name.equalsIgnoreCase("result")) {
            removeContextObject(name);
        } else {
            Integer index = contextObjects.get(name);
            if (Objects.isNull(index)) return;
            if (objects.get(index) instanceof Stack) {
                Stack varStack = (Stack) objects.get(index);
                varStack.pop();
                if (varStack.isEmpty()) removeContextObject(name);
            } else {
                removeContextObject(name);
            }
        }
    }

    /**
     * This updates two stacks to ensure variable scope is maintained for each level of recursion.
     * The first of these is the stackVarOccurrences which keeps the current state of each token
     * where the variable is either referenced or defined. The second stackVariables is a record
     * of each token and it's associated variables. This is so that when the token has finished
     * processing, all of those in-scope variables are removed from their respective stacks.
     * @param name The name of the variable
     */
    private void setStackVariable(String name) {
        String uniqueTokenId = String.format("%s:%s", activeToken, activeTokenDepth);
        if (stackVarOccurrences.containsKey(name)) {
            Stack<String> varOccurrences = stackVarOccurrences.get(name);
            if (!varOccurrences.isEmpty() && !varOccurrences.get(0).equals(activeToken)) return;
            stackVarOccurrences.get(name).push(activeToken);
        } else {
            Stack<String> stack = new Stack<>();
            stack.push(activeToken);
            stackVarOccurrences.put(name, stack);
        }
        if (stackVariables.containsKey(uniqueTokenId)) {
            stackVariables.get(uniqueTokenId).add(name);
        } else {
            List<String> tokenVars = new ArrayList<>();
            tokenVars.add(name);
            stackVariables.put(uniqueTokenId, tokenVars);
        }
        if (isDebug)
            System.out.println(String.format("Adding relation %s -> %s", uniqueTokenId, name));
    }

    @Override
    public void notify(String uid, Token<?> token, int depth, NotificationType type) {
        notify(uid, token, depth, type, null, 0);
    }

    /**
     * Handles a notification event from the parser that either a new token is in scope or has now
     * finished and cleanup needs to occur.
     * @param uid The UID of the token for which the event has occurred
     * @param token The current token on which the action is occurring
     * @param depth The depth at which the event occurred (used for recursion)
     * @param type The event type including ADDITION and REMOVAL
     * @param fallback If a token has finished processing, the UID of the token to return scope to
     * @param fallbackDepth The depth of the fallback token
     */
    @Override
    public void notify(String uid, Token<?> token, int depth, NotificationType type, String fallback,
                       int fallbackDepth) {
        String existingId = String.format("%s:%s", uid, depth);
        if (type == NotificationType.ADDITION) {
            activeToken = uid;
            activeTokenDepth = depth;
            tokenStack.push(token);
        } else if (stackVariables.containsKey(existingId)) {
            //Cleans up any scoped variables associated with the current token
            stackVariables.get(existingId).forEach(v -> {
                if (isDebug)
                    System.out.println("Cleaning " + v);
                if (stackVarOccurrences.get(v).equals(uid))
                    stackVarOccurrences.get(v).pop();
                removeVariable(v);
            });
            stackVariables.remove(existingId);
        }
        if (type == NotificationType.REMOVAL) {
            tokenStack.pop();
        }
        //Set the token / depth to fallback to
        if (Objects.nonNull(fallback)) {
            activeToken = fallback;
            activeTokenDepth = fallbackDepth;
        }
    }
}
