package dev.slop.exception;

/**
 * Parser exceptions may be thrown when the tokens and operations are resolved. Like the LexerException, these may
 * be thrown by the Parser itself or by the SLOPTokenHandler which handles token evaluation. An example of this
 * would be if the condition in a Conditional statement does not evaluate to a Boolean. Alternatively if an expression
 * has a hanging operator e.g. "3 + ".
 */
public class ParserException extends RuntimeException {

    public ParserException(String message) {
        super(message);
    }

    public ParserException(Exception exception) {
        super(exception);
    }
}
