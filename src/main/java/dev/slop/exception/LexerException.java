package dev.slop.exception;

/**
 * This exception can occur during the tokenization phase of expression evaluation. They can be thrown by the Lexer
 * itself or through one of the defined SLOPTokenHandler classes to which tokenization is deferred to when a pattern
 * match is found when scanning.
 */
public class LexerException extends RuntimeException {

    public LexerException(String message) {
        super(message);
    }

    public LexerException(Exception exception) {
        super(exception);
    }
}
