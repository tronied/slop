package dev.slop.exception;

/**
 * This is a general exception which can be thrown for anything outside of Lexer / Parser operations. This could
 * be handling errors with the processor, config or context.
 */
public class SLOPException extends RuntimeException {

    public SLOPException(String message) {
        super(message);
    }
}
