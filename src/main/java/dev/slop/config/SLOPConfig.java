package dev.slop.config;

import dev.slop.enums.PatternType;
import dev.slop.exception.SLOPException;
import dev.slop.functions.*;
import dev.slop.handler.ErrorHandler;
import dev.slop.handler.SyntaxChecker;
import dev.slop.lexer.Lexer;
import dev.slop.operations.*;
import dev.slop.parser.formatter.CollectionFormatter;
import dev.slop.parser.formatter.Formatter;
import dev.slop.parser.formatter.MapFormatter;
import dev.slop.tokens.base.NonToken;
import dev.slop.tokens.Token;
import dev.slop.tokens.base.TokenValue;
import dev.slop.tokens.comment.MultiCommentToken;
import dev.slop.tokens.comment.SingleCommentToken;
import dev.slop.tokens.grammar.GrammarGroup;
import dev.slop.tokens.grammar.GrammarValue;
import dev.slop.tokens.literals.*;
import dev.slop.tokens.operators.*;
import dev.slop.tokens.specials.*;
import dev.slop.tokens.statements.*;
import dev.slop.tokens.statements.reference.FixedLoopToken;
import dev.slop.tokens.statements.reference.MultiLineToken;
import dev.slop.tokens.statements.reference.VariableLoopToken;
import dev.slop.tokens.types.ConstructorToken;
import dev.slop.tokens.types.CreateTypeToken;
import dev.slop.tokens.types.TypeToken;
import dev.slop.tokens.types.TypedVariableToken;

import java.util.*;

/**
 * The configuration class defines everything that determines how the language behaves and operates. This includes
 * literals, shared properties, functions, grammar, statements and type operations. SLOP can be rewritten with
 * different behaviour and syntax by providing a custom config class with different token handlers. In addition, the
 * language can be extended by adding further literals and statements to the configured list below.
 */
public class SLOPConfig {

    private static final String DEFAULT_OR = "or";

    private Set<Function> functions;
    private Set<Token<?>> baseHandlers;
    private Set<Token<?>> tokenHandlers;
    private Set<Token<?>> grammarTokens;
    private Set<TypeOperation> typeOperations;
    private TypeOperation defaultTypeOperation;
    private LogicOperatorHandler logicOperator;
    private Set<Formatter<?>> parserFormatters;
    private Set<ErrorHandler> errorHandlers;
    private OperatorHandler operatorHandler;
    private Map<String, Object> properties;
    private Map<String, String> includeClasses;

    public SLOPConfig() {
        initStores();
        initFunctions();
        initProperties();
        initBaseTokens();
        initGrammarTokens();
        initTokenHandlers();
        initTypeOperations();
        initOperators();
        initParserFormatters();
        initErrorHandler();
    }

    private void initStores() {
        functions = new HashSet<>();
        properties = new HashMap<>();
        baseHandlers = new HashSet<>();
        grammarTokens = new HashSet<>();
        tokenHandlers = new HashSet<>();
        typeOperations = new HashSet<>();
        parserFormatters = new HashSet<>();
        errorHandlers = new HashSet<>();
        includeClasses = new HashMap<>();
    }

    /**
     * Functions are classes which represent normal Java functions which can be called from within a SLOP expression.
     * They are where extra actions are required that are either not supported directly or do not fit SLOPs single
     * line operation.
     */
    private void initFunctions() {
        functions.add(new PrintOp());
        functions.add(new SumOp());
        functions.add(new DateOp());
        functions.add(new RandomOp());
        functions.add(new NewFunction());
    }

    /**
     * This method initializes a default set of properties. These can be changed by using one of the setProperty methods
     * in this config class. These properties can alter output or behaviour of statements.
     */
    private void initProperties() {
        setProperty(DefaultProperty.DATE_FORMAT, "dd-MM-yyyy");
        setProperty(DefaultProperty.ESCAPE_CHARACTER, "\\");
        setProperty(DefaultProperty.OR_OPERATOR, DEFAULT_OR);
        setProperty(DefaultProperty.DEBUG_MODE, false);
        setProperty(DefaultProperty.SAFE_OPERATIONS, true);
    }

    private void initBaseTokens() {
        baseHandlers.add(new TokenValue(null));
        baseHandlers.add(new NonToken(null));
    }

    private void initGrammarTokens() {
        grammarTokens.add(new GrammarGroup("BASE", new ArrayList<>()));
        grammarTokens.add(new GrammarValue(null));
    }

    private void initTokenHandlers() {
        //Special handler for statement argument references i.e. switch parameter e.g. $0
        tokenHandlers.add(new ArgumentToken());

        //Language literals / supported types
        tokenHandlers.add(new BooleanToken());
        tokenHandlers.add(new FloatToken());
        tokenHandlers.add(new DecimalToken());
        tokenHandlers.add(new LongToken());
        tokenHandlers.add(new IntegerToken());
        tokenHandlers.add(new NullToken());
        tokenHandlers.add(new StringToken());
        tokenHandlers.add(new MapToken());
        tokenHandlers.add(new ArrayToken());

        //Discarded matched content i.e. comments
        tokenHandlers.add(new SingleCommentToken());
        tokenHandlers.add(new MultiCommentToken());

        //Logic Operators
        logicOperator = new LogicOperator();
        tokenHandlers.add((Token<?>)logicOperator);
        tokenHandlers.add(new UnaryOperator());
        tokenHandlers.add(new ElvisOperator());

        //Statements
        tokenHandlers.add(new SwitchToken());
        tokenHandlers.add(new ConditionalToken());
        tokenHandlers.add(new OperationToken());
        tokenHandlers.add(new FieldToken());
        tokenHandlers.add(new JVMInvokeToken());
        tokenHandlers.add(new RepeatToken()); //Deprecated
        tokenHandlers.add(new InvocationToken(null));
        tokenHandlers.add(new ForEachToken()); //Deprecated
        tokenHandlers.add(new IfToken());
        tokenHandlers.add(new ForToken());
        tokenHandlers.add(new FunctionToken());
        tokenHandlers.add(new ReturnToken(null));
        tokenHandlers.add(new RangeToken());
        tokenHandlers.add(new ImportToken());

        //Type handlers
        tokenHandlers.add(new TypeToken(null));
        tokenHandlers.add(new TypedVariableToken());
        tokenHandlers.add(new ConstructorToken());
        tokenHandlers.add(new CreateTypeToken());

        //Secondary
        tokenHandlers.add(new SingleLineToken());
        tokenHandlers.add(new MultiLineToken());
        tokenHandlers.add(new FixedLoopToken());
        tokenHandlers.add(new VariableLoopToken());
    }

    /**
     * Type operations determine the supported operators which can be used with a given type. For example, if a new
     * literal was added such as an IncomeToken (class containing salary, currency, review date etc), you could add
     * supported operators for adding, multiplying etc when comparing IncomeToken's or against other decimal values.
     * This way the SLOP can keep token specific code out of the parser and simply call the relevant type operation
     * class.
     */
    private void initTypeOperations() {
        typeOperations.add(new DateOperation());
        typeOperations.add(new EnumOperation());
        typeOperations.add(new StringOperation());
        typeOperations.add(new NumericOperation());
        typeOperations.add(new BooleanOperation());
        typeOperations.add(new ArrayOperation());
        typeOperations.add(new MapOperation());
        typeOperations.add(new NullOperation());
        //Fallback
        defaultTypeOperation = new ObjectOperation();
    }

    /**
     * The operator handler class defines the operators used within the language. The default class uses a mixture of
     * syntax from languages such as Java / C# / Pascal. A custom operator handler can be defined to override these
     * defaults using the setOperatorHandler method (see OperatorHandlerTest).
     */
    private void initOperators() {
        operatorHandler = new OperatorToken("");
    }

    /**
     * Parser formatters are used so that the internal token structure can be mapped to a native representative
     * of the object once a result has been returned. For example, a Collection may hold TokenGroup / Tokens
     * until their values are resolved. The Formatter is responsible for unwrapping these so that it is converted
     * from List&lt;Token&lt;?&gt;&gt; to List&lt;Integer&gt; for example.
     */
    private void initParserFormatters() {
        parserFormatters.add(new CollectionFormatter());
        parserFormatters.add(new MapFormatter());
    }

    private void initErrorHandler() {
        errorHandlers.add(new SyntaxChecker());
    }

    /**
     * This method is specific to the default SLOP config class but it scans and initializes all tokens which define
     * the GRAMMAR pattern type. This method populates a set of pattern tokens which are used by the language lexer
     * to keep track of the progression of each token / statement on the stack as the expression is read.
     * @param grammarLexer The lexer used to scan the tokens grammar String and create the resulting pattern tokens
     */
    public void mapGrammarTokens(Lexer<?> grammarLexer) {
        tokenHandlers.stream()
                .filter(th -> th.getPatternType() == PatternType.GRAMMAR)
                .forEach(th -> th.setPatternTokens(grammarLexer.tokenize(th.getPattern()).getResult()));
    }

    public Set<Token<?>> getBaseHandlers() {
        return baseHandlers;
    }

    public Set<Token<?>> getTokenHandlers() { return tokenHandlers; }

    public Set<Token<?>> getGrammarTokens() { return grammarTokens; }

    public Set<Function> getFunctions() { return functions; }

    public void setProperty(DefaultProperty property, Object value) {
        properties.put(property.getName(), value);
    }

    public void setProperty(String name, String value) {
        properties.put(name, value);
    }

    public <T> T getProperty(String name, Class<T> clazz) {
        return clazz.cast(properties.get(name));
    }

    public String getProperty(String name) {
        return properties.get(name).toString();
    }

    public <T> T getProperty(DefaultProperty property, Class<T> clazz) {
        return clazz.cast(properties.get(property.getName()));
    }

    public String getProperty(DefaultProperty property) {
        return properties.get(property.getName()).toString();
    }

    public Token<?> getOperatorToken() {
        return operatorHandler;
    }

    public boolean isDebugMode() {
        return getProperty(DefaultProperty.DEBUG_MODE, Boolean.class);
    }

    public OperatorHandler getOperatorHandler() {
        return this.operatorHandler;
    }

    public void setOperatorHandler(OperatorHandler operatorHandler) {
        this.operatorHandler = operatorHandler;
    }

    public Set<TypeOperation> getTypeOperations() { return this.typeOperations; }

    public TypeOperation getDefaultTypeOperation() { return this.defaultTypeOperation; }

    public void addFunction(Function function) {
        functions.add(function);
    }

    public Set<Formatter<?>> getParserFormatters() {
        return this.parserFormatters;
    }

    public Set<ErrorHandler> getErrorHandlers() { return this.errorHandlers; }

    public void setLogicOperatorHandler(LogicOperatorHandler logicOperator) {
        if (logicOperator instanceof Token<?>) {
            tokenHandlers.remove((Token<?>)this.logicOperator);
            this.logicOperator = logicOperator;
            tokenHandlers.add((Token<?>)logicOperator);
        }
    }

    public void include(String id, String classPath) {
        includeClasses.put(id, classPath);
    }

    public String getIncludeClasspath(String id) {
        if (!includeClasses.containsKey(id))
            throw new SLOPException(String.format("No class reference '%s' exists. Please use " +
                    "SLOPConfig.include(...) first", id));
        return includeClasses.get(id);
    }
}
