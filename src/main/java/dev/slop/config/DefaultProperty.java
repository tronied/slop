package dev.slop.config;

/**
 * Properties can alter the behaviour of Functions, Statements and Literals during tokenization and parsing. This
 * enum type stores names of common properties which can be used to modify their values within the config class.
 */
public enum DefaultProperty {
    DATE_FORMAT("dateFormat"),
    ESCAPE_CHARACTER("escapeCharacter"),
    OR_OPERATOR("orOperator"),
    DEBUG_MODE("debugMode"),
    SAFE_OPERATIONS("safeOperations");

    private String name;

    DefaultProperty(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}