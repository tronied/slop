package dev.slop.processor;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.exception.SLOPException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SLOPProcessorTest {

    private SLOPProcessor processor;

    @BeforeAll
    void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    void testTokenizeToFile_ReadInput_SimpleType() throws URISyntaxException {
        String expression = readFileAsString(getClass().getClassLoader().getResource(
                "source/SimpleType.slp").toURI());
        File file = new File("src/test/resources/processed/SimpleType");
        String absolutePath = file.getAbsolutePath();
        processor.tokenizeToFile(expression, absolutePath);
        assertEquals(10, processor.processFromFile(file.getAbsolutePath(), new SLOPContext()).getValue());
    }

    @Test
    void testTokenizeToFile_ReadInput_ATest() throws URISyntaxException {
        String expression = readFileAsString(getClass().getClassLoader().getResource(
                "source/ATest.slp").toURI());
        File file = new File("src/test/resources/processed/ATest");
        String absolutePath = file.getAbsolutePath();
        processor.tokenizeToFile(expression, absolutePath);
        assertEquals(5, processor.processFromFile(file.getAbsolutePath(), new SLOPContext()).getValue());
    }

    private String readFileAsString(URI uri) {
        StringBuilder contentBuilder = new StringBuilder();
        String mainPath = Paths.get(uri).toString();
        try (Stream<String> stream = Files.lines(Paths.get(mainPath), StandardCharsets.UTF_8)) {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        } catch (IOException e) {
            throw new SLOPException("Error reading token file: " + e.getMessage());
        }
        return contentBuilder.toString();
    }
}
