package dev.slop.template;

import dev.slop.model.Company;
import dev.slop.model.Employee;

public class TestTemplate {

    protected Company sampleCompany() {
        /**
         * acme.employees[^{?acme}.topEarners.contains(~payrollId) and ~name.startsWith('S')].name
         * acme.employees[^{?acme}.topEarners.contains(~payrollId) == false].name
         */
        Company acme = new Company();
        acme.getEmployees().add(Employee.builder().payrollId(123).name("Bob").build());
        acme.getEmployees().add(Employee.builder().payrollId(321).name("Sharon").build());
        acme.getEmployees().add(Employee.builder().payrollId(789).name("Mike").build());
        acme.getEmployees().add(Employee.builder().payrollId(987).name("Anna").build());
        acme.getTopEarners().add(321);
        acme.getTopEarners().add(987);
        return acme;
    }
}
