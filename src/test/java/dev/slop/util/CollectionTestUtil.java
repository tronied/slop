package dev.slop.util;

import dev.slop.context.SLOPContext;

import java.util.Arrays;
import java.util.List;

public class CollectionTestUtil {
    public static SLOPContext smallCollectionContext() {
        SLOPContext context = new SLOPContext();

        Employee employeeA = new Employee("Mary", 34);
        Employee employeeB = new Employee("Bob", 54);

        Company acmeCompany = new Company();
        acmeCompany.setEmployees(Arrays.asList(employeeA, employeeB));

        context.set("acme", acmeCompany);
        return context;
    }

    public static SLOPContext mediumCollectionContext() {
        SLOPContext context = new SLOPContext();

        //Employees
        Employee employeeA = new Employee("Mary", 34);
        Employee employeeB = new Employee("Bob", 54);
        Employee employeeC = new Employee("Susan", 64);
        Employee employeeD = new Employee("Jim", 23);

        //Company
        Company acmeCompany = new Company();
        acmeCompany.setName("ACME Company Plc");
        acmeCompany.setRevenue(129000000);
        acmeCompany.setEmployees(Arrays.asList(employeeA, employeeB, employeeC, employeeD));

        context.set("acme", acmeCompany);
        return context;
    }

    private static class Employee {
        private String name;
        private int age;

        public Employee(String name, int age) {
            this.name = name;
            this.age = age;
        }
    }

    private static class Company {
        private String name;
        private int revenue;
        private List<Employee> employees;

        public void setName(String name) {
            this.name = name;
        }

        public void setRevenue(int revenue) {
            this.revenue = revenue;
        }

        public void setEmployees(List<Employee> employees) {
            this.employees = employees;
        }
    }
}
