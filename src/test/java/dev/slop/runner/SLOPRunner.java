package dev.slop.runner;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.exception.LexerException;
import dev.slop.model.Company;
import dev.slop.model.Employee;
import dev.slop.processor.SLOPProcessor;
import dev.slop.tokens.model.SubObject;
import dev.slop.tokens.model.SubSubObject;
import dev.slop.tokens.model.TestEnum;
import dev.slop.tokens.model.TestObject;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class SLOPRunner {
    public static void main(String[] args) {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        SLOPContext context = new SLOPContext();
        context.set("testObject", sampleObject());
        context.set("acme", sampleCompany());
        SLOPProcessor experimentalProcessor = new SLOPProcessor(config);
        String input;
        System.out.println("\nSLOP Processor Test Utility");
        System.out.println("===================================");
        while ((input = readInput()) != null) {
            if (input.equalsIgnoreCase("exit")) {
                break;
            }
            try {
                long before = System.currentTimeMillis();
                System.out.println(String.format("Result: %s (Time taken: %dms)",
                        experimentalProcessor.process(input, context), System.currentTimeMillis() - before));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static String readInput() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private static TestObject sampleObject() {
        HashMap<String, String> testKeyValues = new HashMap<>();
        testKeyValues.put("bill", "ted");
        testKeyValues.put("bonnie", "clyde");
        return TestObject.builder()
                .componentA(BigDecimal.valueOf(12))
                .componentB(BigDecimal.valueOf(5))
                .componentC(BigDecimal.valueOf(99))
                .intValue(1)
                .dateA(LocalDateTime.of(2021, 01, 01, 12, 59, 00))
                .dateB(LocalDateTime.of(2020, 05, 01, 12, 59, 00))
                .dateC(Date.valueOf(LocalDate.of(1999, 01, 01)))
                .name("Jim")
                .subObjects(Arrays.asList(
                        SubObject.builder().value(12).subSubObject(
                                SubSubObject.builder().valueA(1).valueB(2).build()
                        ).build(),
                        SubObject.builder().value(54).subSubObject(
                                SubSubObject.builder().valueA(4).valueB(3).build()
                        ).build(),
                        SubObject.builder().value(74).subSubObject(
                                SubSubObject.builder().valueA(8).valueB(4).build()
                        ).build()
                ))
                .mapObjects(testKeyValues)
                .testEnum(TestEnum.VALUE_1)
                .build();
    }

    private static Company sampleCompany() {
        /**
         * acme.employees[^{?acme}.topEarners.contains(~payrollId) and ~name.startsWith('S')].name
         * acme.employees[^{?acme}.topEarners.contains(~payrollId) == false].name
         * (foreach(person : acme.employees) result = person.name.subString(1);)[0]
         */
        Company acme = new Company();
        acme.getEmployees().add(Employee.builder().payrollId(123).name("Bob").build());
        acme.getEmployees().add(Employee.builder().payrollId(321).name("Sharon").build());
        acme.getEmployees().add(Employee.builder().payrollId(789).name("Mike").build());
        acme.getEmployees().add(Employee.builder().payrollId(987).name("Anna").build());
        acme.getTopEarners().add(321);
        acme.getTopEarners().add(987);
        return acme;
    }
}