package dev.slop.handler;

import dev.slop.exception.LexerException;
import dev.slop.processor.SLOPProcessor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SyntaxCheckerTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        processor = new SLOPProcessor();
    }
    /**
     * repeat i
     * repeat(i++)
     * repeat(i++,0)
     * repeat(i++,0,<10)
     * repeat(i++,0,<10) result = 1 + 1
     * repeat(i++,0,<10) result = 1 + 1;
     * 3 > 4 ?
     * 3 > 4 ? 1
     * foreach
     * foreach(emp)
     * foreach(emp : employees) result = {?emp}.name
     * foreach(emp : employees) result = {?emp}.name;
     */

    @Test
    public void testRepeat_NoBrackets() {
        LexerException ex = Assertions.assertThrows(LexerException.class, () -> processor.process("repeat i"));
        String expected = createSyntaxError(
                "Expected token '(' in RepeatToken but expression ended prematurely. ",
                "A repeat statement requires the iteration definition to be wrapped in a pair of braces " +
                            "e.g. repeat -->( <variable> ; <condition> ; <step> ) ...",
                "'i'",
                "repeat i"
        );
        assertEquals(expected, ex.getMessage());
    }

    @Test
    public void testRepeat_NoIterationSectionComma() {
        LexerException ex = Assertions.assertThrows(LexerException.class, () -> processor.process("repeat(i++,0,<10)"));
        String expected = createSyntaxError(
                "The minimum statement requirements for RepeatToken were not met.",
                "Repeat statement requires at least one code block to be iterated e.g. repeat ( ... ) " +
                        "<code block1> ; <code block2> ; ...",
                "'i','++','0','<','10'",
                "repeat(i++,0,<10)"
        );
        assertEquals(expected, ex.getMessage());
    }

    @Test
    public void testRepeat_NoIterationSectionComma_NoClosingBrace() {
        LexerException ex = Assertions.assertThrows(LexerException.class, () -> processor.process("repeat(i++,0"));
        String expected = createSyntaxError(
                "Expected token ',' in RepeatToken but expression ended prematurely. ",
                "A repeat statement requires 3 parts of the iteration to be defined and separated by a ',' character " +
                        "e.g. repeat ( i++, 0, <10 ) ...",
                "'0'",
                "repeat(i++,0"
        );
        assertEquals(expected, ex.getMessage());
    }

    @Test
    public void testRepeat_Repeat_NoCodeBlockSemiColon() {
        LexerException ex = Assertions.assertThrows(LexerException.class, () -> processor.process("repeat(i++,0,<10) result = 1 + 1"));
        String expected = createSyntaxError(
                "Expected token ';' in RepeatToken but expression ended prematurely. ",
                "Repeat statement code blocks must be terminated with a ';' character e.g. repeat ( ... ) " +
                        "<some logic> ;<--",
                "'result','=','1','+','1'",
                "repeat(i++,0,<10) result = 1 + 1"
        );
        assertEquals(expected, ex.getMessage());
    }

    @Test
    public void testRepeat_Condition_NoTrueFalse() {
        LexerException ex = Assertions.assertThrows(LexerException.class, () -> processor.process("3 > 4 ?"));
        String expected = createSyntaxError(
                "Expected token ':' in ConditionalToken but expression ended prematurely. ",
                "A conditional requires true / false outcomes to be separated by a ':' e.g. a > b ? a :<-- b",
                "'3','>','4'",
                "3 > 4 ?"
        );
        assertEquals(expected, ex.getMessage());
    }

    @Test
    public void testRepeat_Condition_NoTrueFalseSplit() {
        LexerException ex = Assertions.assertThrows(LexerException.class, () -> processor.process("3 > 4 ? 1"));
        String expected = createSyntaxError(
                "Expected token ':' in ConditionalToken but expression ended prematurely. ",
                "A conditional requires true / false outcomes to be separated by a ':' e.g. a > b ? a :<-- b",
                "'1'",
                "3 > 4 ? 1"
        );
        assertEquals(expected, ex.getMessage());
    }

    @Test
    public void testRepeat_Foreach_NoIterationOpeningBrace() {
        LexerException ex = Assertions.assertThrows(LexerException.class, () -> processor.process("foreach"));
        String expected = createSyntaxError(
                "Expected token '(' in ForEachToken but expression ended prematurely. ",
                "The for-each collection statement must be wrapped in braces e.g. foreach -->( obj : collection )",
                "",
                "foreach"
        );
        assertEquals(expected, ex.getMessage());
    }

    @Test
    public void testRepeat_Foreach_NoCodeBlocks() {
        LexerException ex = Assertions.assertThrows(LexerException.class, () -> processor.process("foreach(emp)"));
        String expected = createSyntaxError(
                "Expected token ':' in ForEachToken but expression ended prematurely. ",
                "Foreach statement requires at least one code block to be iterated e.g. " +
                        "foreach ( ... ) <code block1> ; <code block2> ; ...",
                "'emp'",
                "foreach(emp)"
        );
        assertEquals(expected, ex.getMessage());
    }

    @Test
    public void testRepeat_Foreach_NoClosingIterationBrace() {
        LexerException ex = Assertions.assertThrows(LexerException.class, () -> processor.process("foreach(emp"));
        String expected = createSyntaxError(
                "Expected token ':' in ForEachToken but expression ended prematurely. ",
                "Foreach statement requires at least one code block to be iterated e.g. foreach ( ... ) " +
                        "<code block1> ; <code block2> ; ...",
                "'emp'",
                "foreach(emp"
        );
        assertEquals(expected, ex.getMessage());
    }

    private String createSyntaxError(String message, String guidance, String lastReadTokens, String expression) {
        return String.format("%s" + System.lineSeparator() +
                "----------------------------------------------------------------" + System.lineSeparator() +
                "- Guidance: %s" + System.lineSeparator() +
                "- Last Read Tokens: %s" + System.lineSeparator() +
                "- Expression: %s" + System.lineSeparator() +
                "----------------------------------------------------------------",
                message, guidance, lastReadTokens, expression);
    }
}
