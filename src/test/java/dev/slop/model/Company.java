package dev.slop.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Company {
    private List<Employee> employees;
    private List<Integer> topEarners;

    public Company() {
        this.employees = new ArrayList<>();
        this.topEarners = new ArrayList<>();
    }
}
