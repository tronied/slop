package dev.slop.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Employee {
    private int payrollId;
    private String name;
}
