package dev.slop.enums;

public enum TestEnum {
    VALUE_A(1),
    VALUE_B(2),
    VALUE_C(3);

    private int value;

    TestEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
