package dev.slop.lexer;

import dev.slop.config.SLOPConfig;
import dev.slop.model.Expression;
import dev.slop.tokens.grammar.*;
import org.junit.jupiter.api.*;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SLOPGrammarLexerTest {

    private SLOPGrammarLexer grammarLexer;

    @BeforeAll
    public void setUp() {
        grammarLexer = new SLOPGrammarLexer(new SLOPConfig());
    }

    @Test
    void testTokenize_Flat_StandardTokens() {
        String pattern = "'blah' '(' expr ')' expr";
        GrammarToken<?>[] expectedTokens = Arrays.asList(
                new GrammarValue("blah"),
                new GrammarValue("("),
                new GrammarExpression(),
                new GrammarValue(")"),
                new GrammarExpression()
        ).toArray(new GrammarToken[0]);
        GrammarToken[] resultTokens = grammarLexer.tokenize(
                new Expression(pattern)).getResult().toArray(new GrammarToken[0]);
        assertArrayEquals(expectedTokens, resultTokens);
    }

    @Test
    void testTokenize_EmbeddedGroup_StandardTokens() {
        String pattern = "'blah' '(' ( expr ',' )+ ')' expr";
        GrammarGroup expectedGroup = new GrammarGroup();
        expectedGroup.setGroupId("3");
        expectedGroup.setMultiple(true);
        expectedGroup.setPatternTokens(Arrays.asList(new GrammarExpression(), new GrammarValue(",")));
        GrammarToken<?>[] expectedTokens = Arrays.asList(
                new GrammarValue("blah"),
                new GrammarValue("("),
                expectedGroup,
                new GrammarValue(")"),
                new GrammarExpression()
        ).toArray(new GrammarToken[0]);
        assertArrayEquals(expectedTokens,
                grammarLexer.tokenize(new Expression(pattern)).getResult().toArray(new GrammarToken[0]));
    }

    @Test
    void testTokenize_TokenReferences() {
        String pattern = "'blah' '(' [ first, second, third ] ')' expr";
        GrammarToken<?>[] expectedTokens = Arrays.asList(
                new GrammarValue("blah"),
                new GrammarValue("("),
                new GrammarReference(Arrays.asList("first", "second", "third")),
                new GrammarValue(")"),
                new GrammarExpression()
        ).toArray(new GrammarToken[0]);
        assertArrayEquals(expectedTokens,
                grammarLexer.tokenize(new Expression(pattern)).getResult().toArray(new GrammarToken[0]));
    }

    @Test
    void testTokenize_MultipleSingleValueStyle() {
        String pattern = "'type' val:String '(' ( val:String ','? )+ ')' ( '<-' val:String )? [ multiLine ]";
        grammarLexer.tokenize(new Expression(pattern)).getResult().toArray(new GrammarToken[0]);
    }

    @Test
    void testTokenize_TypedTokenTest() {
        String pattern = "'let' val:String val:String '=' expr";
        grammarLexer.tokenize(new Expression(pattern)).getResult().toArray(new GrammarToken[0]);
    }

    @Test
    void testTokenize_TypeTokenTest() {
        String pattern = "'type' val:String ( '(' ( val:String ','? )+ ')' )? ( '<-' val:String )? [ multiLine ]";
        assertEquals(5, grammarLexer.tokenize(new Expression(pattern)).getResult().toArray(new GrammarToken[0]).length);
    }
}
