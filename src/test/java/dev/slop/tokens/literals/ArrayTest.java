package dev.slop.tokens.literals;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.exception.LexerException;
import dev.slop.operations.ArrayOperation;
import dev.slop.processor.SLOPProcessor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ArrayTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        processor = new SLOPProcessor();
    }

    @Test
    void testOperationInArray() {
        List<?> first = Arrays.asList(1, 2, 7);
        List<?> second = processor.process("[1,2 * 1,SUM(1,2,3) + 1]").getValue(List.class);
        assertArrayEquals(first.toArray(), second.toArray());
    }

    @Test
    void testArrayOperation_Subtract() {
        List<?> result = processor.process("[1,2,3] - [2,3]").getValue(List.class);
        assertArrayEquals(Arrays.asList(1).toArray(), result.toArray());
    }

    @Test
    void testArrayOperation_Add() {
        List<?> result = processor.process("[1,2,3] + [2,3]").getValue(List.class);
        assertArrayEquals(Arrays.asList(1,2,3,2,3).toArray(), result.toArray());
    }

    @Test
    void testArrayOperation_OR() {
        List<?> result = processor.process("[1,2,3] / [2,3]").getValue(List.class);
        assertArrayEquals(Arrays.asList(2, 3).toArray(), result.toArray());
    }

    @Test
    void testArrayOperation_ReverseIntersection() {
        List<?> result = processor.process("[1,2,3] * [2,3]").getValue(List.class);
        assertArrayEquals(Arrays.asList(1).toArray(), result.toArray());
    }

    @Test
    void testArrayOperation_UnclosedArray() {
        LexerException ex = assertThrows(LexerException.class,
                () -> processor.process("[1,2,3 + [1,2]").getValue(List.class));
        assertEquals("Expected token ']' in ArrayToken but expression ended prematurely. " +
                "----------------------------------------------------------------" +
                "- Guidance: An array definition requires items to be wrapped in '[' and ']' characters e.g. [ 1,2 ]<--" +
                "- Last Read Tokens: '1','2','3','+','[1, 2]'" +
                "- Expression: [1,2,3 + [1,2]" +
                "----------------------------------------------------------------",
                ex.getMessage().replace(System.lineSeparator(), ""));
    }

    @Test
    void testComplexArrayDef_WithDates() {
        List<?> result = processor.process("[1,2,DATE(\"23-05-1983\") > DATE(\"17-02-1981\"),99 + 1 > 25 * 4 ? 3 : (4 + 9)]")
                .getValue(List.class);
        assertArrayEquals(Arrays.asList(1, 2, true, 13).toArray(), result.toArray());
    }

    @Test
    void testRawArray_WithNativeCall() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        SLOPProcessor unsafeProcessor = new SLOPProcessor(config);
        assertEquals(5, unsafeProcessor.process("[1,2].size() + 3").getValue());
    }

    @Test
    void testVerifyVariableState_VariableUnchanged() {
        SLOPProcessor processor = new SLOPProcessor();
        assertTrue(processor.process(Arrays.asList(
            "myArray = []",
            "myArray + [1,2,3]",
            "myArray"
        )).getValue(List.class).isEmpty());
    }

    @Test
    void testVerifyVariableState_VariableChanged() {
        SLOPProcessor processor = new SLOPProcessor();
        assertArrayEquals(Arrays.asList(1,2,3).toArray(), processor.process(Arrays.asList(
                "myArray = []",
                "myArray = myArray + [1,2,3]",
                "myArray"
        )).getValue(List.class).toArray());
    }
}
