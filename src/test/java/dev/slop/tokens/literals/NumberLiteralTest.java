package dev.slop.tokens.literals;

import dev.slop.context.SLOPContext;
import dev.slop.processor.SLOPProcessor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class NumberLiteralTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        processor = new SLOPProcessor();
    }

    @Test
    void testContextValue_Multiply() {
        SLOPContext context = new SLOPContext();
        context.set("myInt", 4);
        assertEquals(16, processor.process("myInt * 4", context).getValue());
    }
}
