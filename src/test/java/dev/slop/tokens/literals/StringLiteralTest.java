package dev.slop.tokens.literals;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.processor.SLOPProcessor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class StringLiteralTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    void testContextValue_ChainedNativeCalls() {
        SLOPContext context = new SLOPContext();
        context.set("myString", "hello");
        assertEquals("ellhello", processor.process("myString.substring(1,4).concat(\"hello\")",
                context).getValue());
    }

    @Test
    void testRawString_WithNativeCall() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        SLOPProcessor unsafeProcessor = new SLOPProcessor(config);
        //Test primitives with native call
        assertEquals("ell", unsafeProcessor.process("\"hello\".substring(1,4)").getValue());
    }
}
