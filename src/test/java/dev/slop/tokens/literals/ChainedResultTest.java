package dev.slop.tokens.literals;

import dev.slop.processor.SLOPProcessor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ChainedResultTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        processor = new SLOPProcessor();
    }

    @Test
    public void testSimpleChainedResults() {
        assertEquals(2, processor.process(Arrays.asList(
                "4 * 4",
                "last / 4",
                "last - 2"
        )).getValue());
    }

    @Test
    public void testAssignNameChainedExpression() {
        assertEquals(12, processor.process(Arrays.asList(
                "resultA = 4 * 4",
                "resultB = resultA / 4",
                "resultA - resultB"
        )).getValue());
    }
}
