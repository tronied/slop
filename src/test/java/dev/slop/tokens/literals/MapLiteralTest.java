package dev.slop.tokens.literals;

import com.google.common.collect.Maps;
import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.model.Person;
import dev.slop.processor.SLOPProcessor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MapLiteralTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    public void testAddTwoMaps_SingleObject() {
        Map<Integer, Integer> expected = new HashMap<>();
        for (int i = 1;i < 5;i++) {
            expected.put(i, i);
        }
        //Result should be {1->1,2->2,3->3,4->4}
        assertEquals(processor.process("{1->1,2->2,3->3} + {4->4}").getValue(Map.class), expected);
    }

    @Test
    public void testAddTwoMaps_MultiObject() {
        Map<Integer, Integer> expected = new HashMap<>();
        for (int i = 1;i < 6;i++) {
            expected.put(i, i);
        }
        //Result should be {1->1,2->2,3->3,4->4,5->5}
        assertEquals(processor.process("{1->1,2->2,3->3} + {4->4,5->5}").getValue(Map.class), expected);
    }

    @Test
    public void testAddTwoMaps_OverwriteValue() {
        Map<Integer, Integer> expected = new HashMap<>();
        for (int i = 1;i < 4;i++) {
            expected.put(i, i);
        }
        expected.put(2,4);
        //Result should be {1->1,2->4,3->3}
        assertEquals(processor.process("{1->1,2->2,3->3} + {2->4}").getValue(Map.class), expected);
    }

    @Test
    public void testAddTwoMaps_DifferentTypes() {
        Map<Object, Object> expected = new HashMap<>();
        expected.put(1,1);
        expected.put("second",2);
        expected.put(3,"third");
        expected.put("fourth","4th");
        assertEquals(processor.process("{1->1,\"second\"->2,3->\"third\"} + {\"fourth\"->\"4th\"}")
                .getValue(Map.class), expected);
    }

    @Test
    public void testSubtractTwoMaps_ExactingValue() {
        Map<Integer, Integer> expected = new HashMap<>();
        expected.put(1,1);
        expected.put(3,3);
        assertEquals(processor.process("{1->1,2->2,3->3} - {2->2}")
                .getValue(Map.class), expected);
    }

    @Test
    public void testSubtractTwoMaps_NonExactingValue_ExpectingNoChange() {
        Map<Integer, Integer> expected = new HashMap<>();
        for (int i = 1;i < 4;i++) {
            expected.put(i, i);
        }
        assertEquals(processor.process("{1->1,2->2,3->3} - {2->3}")
                .getValue(Map.class), expected);
    }

    @Test
    public void testSubtractKeyListFromMap() {
        Map<Integer, Integer> expected = new HashMap<>();
        expected.put(1,1);
        assertEquals(processor.process("{1->1,2->2,3->3} - [2,3]")
                .getValue(Map.class), expected);
    }

    @Test
    public void testDeclareMap_NativeCall() {
        assertEquals(processor.process("{1->\"first\",2->\"second\",3->\"third\"}.get(2)")
                .getValue(String.class), "second");
    }

    @Test
    public void testDeclareMap_NativeCall_AssignedToVariable() {
        assertEquals(processor.process("myVar = {1->\"first\",2->\"second\",3->\"third\"}.remove(2)")
                .getValue(String.class), "second");
    }

    @Test
    public void testMapWithContext_AddAndLoopElements_NativeCalls() {
        testMapLoopWithElements_WithCustomExpression("resultMap = resultMap + " +
                "{person.getUsername() -> person.getFirstName() + ' ' + person.getLastName()};");
    }

    @Test
    public void testMapWithContext_AddAndLoopElements_DirectFieldCalls() {
        testMapLoopWithElements_WithCustomExpression("resultMap = resultMap + " +
                "{person.username -> person.firstName + \" \" + person.lastName};");
    }

    private void testMapLoopWithElements_WithCustomExpression(String expression) {
        List<Person> people = Arrays.asList(
                new Person("user1", "Bob", "Andrews", 23),
                new Person("user2", "Sarah", "Matthews", 45),
                new Person("user3", "John", "Barrow", 33),
                new Person("user4", "Julianne", "Murray", 29));
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        SLOPContext context = new SLOPContext();
        context.set("people", people);
        SLOPProcessor processorb = new SLOPProcessor(config, context);
        Map<String, Object> expected = people.stream()
                .collect(Collectors.toMap(Person::getUsername,
                        p -> String.format("%s %s", p.getFirstName(), p.getLastName())));
        assertEquals(expected, processorb.process(
                Arrays.asList(
                        "resultMap = {}",
                        "foreach(person : people) " +
                                expression,
                        "resultMap")).getValue(Map.class));
    }

    @Test
    void testVerifyVariableState_VariableUnchanged() {
        SLOPProcessor processor = new SLOPProcessor();
        assertTrue(processor.process(Arrays.asList(
                "myMap = {}",
                "myMap + {1->1,2->2,3->3}",
                "myMap"
        )).getValue(Map.class).isEmpty());
    }

    @Test
    void testVerifyVariableState_VariableChanged() {
        SLOPProcessor processor = new SLOPProcessor();
        HashMap<Integer, Integer> expected = new HashMap<>();
        for (int i = 1;i < 4;i++) {
            expected.put(i, i);
        }
        Map<?,?> result = processor.process(Arrays.asList(
                "myMap = {}",
                "myMap = myMap + {1->1,2->2,3->3}",
                "myMap"
        )).getValue(Map.class);
        assertTrue(Maps.difference(expected, result).areEqual());
    }
}
