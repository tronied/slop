package dev.slop.tokens.functions;

import dev.slop.config.SLOPConfig;

public class TestFunctionConfig extends SLOPConfig {

    public TestFunctionConfig() {
        super();
        addFunction(new TestFunction());
    }
}
