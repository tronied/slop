package dev.slop.tokens.functions;

import dev.slop.config.SLOPConfig;
import dev.slop.exception.ParserException;
import dev.slop.functions.Function;
import dev.slop.tokens.Token;
import dev.slop.tokens.base.TokenValue;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

/**
 * Pointless test function which causes all positive numbers to become negative and vice
 * versa. The results of these are all added together to form the result.
 */
public class TestFunction implements Function {

    @Override
    public boolean isMatch(String name) {
        return name.equalsIgnoreCase("NEGATE_AND_ADD");
    }

    @Override
    public Token<?> execute(SLOPConfig config, List<Token<?>> tokens) {
        Object current = null;
        BigDecimal result = BigDecimal.ZERO;
        if (tokens.isEmpty()) {
            throw new ParserException("Must have at least one value to work!");
        }
        try {
            for (Token<?> token : tokens) {
                if (token.is(Collection.class)) {
                    Collection<?> collection = token.getValue(Collection.class);
                    for (Object item : collection) {
                        if (item instanceof Token<?>) {
                            current = ((Token<?>) item).getValue();
                            item = ((Token<?>) item).getValue();
                        }
                        result = result.add(new BigDecimal(item.toString())
                                .subtract(new BigDecimal(item.toString()).multiply(BigDecimal.valueOf(2))));
                    }
                } else {
                    current = token.getValue();
                    result = result.add(new BigDecimal(token.getValue().toString())
                            .subtract(new BigDecimal(token.getValue().toString()).multiply(BigDecimal.valueOf(2))));
                }
            }
        } catch (NumberFormatException ex) {
            throw new ParserException(String.format("Could not operate numerically with value '%s'", current));
        }
        return new TokenValue(result);
    }
}
