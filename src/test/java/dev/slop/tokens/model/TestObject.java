package dev.slop.tokens.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TestObject {
    private BigDecimal componentA;
    private BigDecimal componentB;
    private BigDecimal componentC;
    private boolean something;
    private int intValue;
    private LocalDateTime dateA;
    private LocalDateTime dateB;
    private Date dateC;
    private String name;
    private List<SubObject> subObjects;
    private Map<String, String> mapObjects;
    private TestEnum testEnum;
}
