package dev.slop.tokens;

import dev.slop.config.SLOPConfig;
import dev.slop.tokens.handler.VerboseOperator;

public class TestOperationConfig extends SLOPConfig {

    public TestOperationConfig() {
        super();
        setOperatorHandler(new VerboseOperator(""));
    }
}
