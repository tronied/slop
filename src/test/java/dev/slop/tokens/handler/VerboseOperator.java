package dev.slop.tokens.handler;

import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.enums.OperatorType;
import dev.slop.enums.PatternType;
import dev.slop.parser.SLOPParser;
import dev.slop.tokens.Token;
import dev.slop.tokens.operators.OperatorToken;

import java.util.Collections;
import java.util.List;

public class VerboseOperator extends OperatorToken {

    public VerboseOperator(String value) {
        super(value);
        initOperators();
    }

    private void initOperators() {
        addOperator("add", OperatorType.ADD);
        addOperator("sub", OperatorType.SUBTRACT);
        addOperator("div", OperatorType.DIVIDE);
        addOperator("mul", OperatorType.MULTIPLY);
        addOperator("gte", OperatorType.GTE);
        addOperator("lte", OperatorType.LTE);
        addOperator("lt", OperatorType.LT);
        addOperator("gt", OperatorType.GT);
        addOperator("mod", OperatorType.MOD);
        addOperator("ne", OperatorType.NOT_EQUALS);
        addOperator("eq", OperatorType.EQUALS);
        addOperator("inc", OperatorType.INCREMENT);
        addOperator("dec", OperatorType.DECREMENT);
    }

    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    @Override
    public String getPattern() {
        return "^(inc|dec|add|sub|div|mul|gte|lte|lt|gt|mod|ne|eq)";
    }

    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return Collections.singletonList(this);
    }

    @Override
    public Token<String> createToken(String value) {
        return new VerboseOperator(value);
    }

    @Override
    public String[][] getOperatorOrder() {
        return new String[][] {
                {"mul", "div", "mod"},
                {"add", "sub"},
                {"gte", "lte", "gt", "lt"},
                {"eq", "ne"}
        };
    }
}
