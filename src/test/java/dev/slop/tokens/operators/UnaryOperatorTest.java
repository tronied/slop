package dev.slop.tokens.operators;

import dev.slop.config.DefaultProperty;
import dev.slop.processor.SLOPProcessor;
import dev.slop.tokens.functions.TestFunctionConfig;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UnaryOperatorTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        TestFunctionConfig testSLOPConfig = new TestFunctionConfig();
        testSLOPConfig.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(testSLOPConfig);
    }

    @Test
    void testSimpleNegation() {
        assertFalse(processor.process("!true").getValue(Boolean.class));
    }

    @Test
    void testDoubleNegation() {
        assertTrue(processor.process("!!true").getValue(Boolean.class));
    }

    @Test
    void testSimpleOperationNegation() {
        assertTrue(processor.process("!(3 > 4)").getValue(Boolean.class));
    }

    @Test
    void testComplexNegation() {
        assertTrue(processor.process("!(3 > 4) == 4 > 3").getValue(Boolean.class));
    }
}
