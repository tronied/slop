package dev.slop.tokens.statements;

import dev.slop.context.SLOPContext;
import dev.slop.exception.ParserException;
import dev.slop.processor.SLOPProcessor;
import dev.slop.tokens.model.TestObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SwitchTokenTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        //Use caching feature since we have multiple expressions using fields
        SLOPContext context = new SLOPContext();
        context.add(sampleObject());
        processor = new SLOPProcessor(context);
    }

    @Test
    void testSingleParam_NumericConditions() {
        assertEquals("third",
            processor.process("switch(0.componentA)[< 5:\"first\";< 10: \"second\";< 15: \"third\"]").getValue());
    }

    @Test
    void testSingleParam_SimpleStringCondition() {
        assertEquals("first",
            processor.process("switch(0.name)[\"Jim\":\"first\";\"Sam\": \"second\"]").getValue());
    }

    @Test
    void testMultiParam_VariableRefComparison() {
        /* This expression would be better as a conditional, but this is verify that the parameter references
         * are working correctly */
        assertEquals("second",
            processor.process("switch (0.componentA,0.componentB)[$0 < $1: \"first\";$0 > $1: \"second\"]")
                    .getValue());
    }

    @Test
    void testSingleParam() {
        assertEquals("less than 15",
                processor.process("switch (0.componentA)[$0 < 5:\"less than 5\";$0 < 10:\"less than 10\";" +
                        "$0 < 15:\"less than 15\"]").getValue());
    }

    @Test
    void testMultiParam_MultiSimpleCriteria() {
        assertEquals("third",
            processor.process("switch(0.componentA,0.componentB)[5,4:\"first\";12,2:\"second\";12,5:\"third\"]")
                    .getValue());
    }

    @Test
    void testMultiParam_MultiNumericCriteria() {
        assertEquals("yay",
            processor.process("switch (0.componentA,0.componentB)[$0 > 5,$1 < 10:\"yay\";" +
                    "$0 < 10,$1 > 100:\"boo\"]").getValue());
    }

    @Test
    void testMultiParam_MultiNumericCriteria_Simplified() {
        assertEquals("yay",
                processor.process("switch (0.componentA,0.componentB)[> 5,< 10:\"yay\";< 10,> 100:\"boo\"]")
                        .getValue());
    }

    @Test
    void testMultiParam_MultiNumericCriteria_MixedTypes() {
        assertEquals("yay",
                processor.process("switch (0.componentA,0.componentB,0.name)[> 5,< 10,\"Jim\": \"yay\";" +
                                "< 10,> 100,\"Sam\": \"boo\"]").getValue());
    }

    @Test
    void testMultiParam_MultiNumericCriteria_Literal_MixedMatching() {
        assertEquals("Match",
                processor.process("switch (5,10)[5,<= 10:\"Match\";< 10,> 100:\"No match\"]").getValue());
    }

    @Test
    void testMultiParam_MultiNumericCriteria_Literal_BooleanResult() {
        assertEquals(true,
                processor.process("switch (5,10)[5,10:true;< 10,> 100:false]").getValue());
    }

    @Test
    void testSingleParam_ResultUsedInOperation() {
        assertEquals(9, processor.process("3 * switch (5)[< 5:1.5;< 10:3;> 15:4.5]").getValue());
    }

    @Test
    void testNoSpacesConditionAndResult() {
        assertEquals("Yes", processor.process("switch(4)[3:\"Nope\";3>4?3:4:\"Yes\"]").getValue());
    }

    @Test
    void testMultipleMatches() {
        assertArrayEquals(new String[] {"Yep", "Yep too"},
                processor.process("switch(4)[<5:\"Yep\";>3:\"Yep too\"]").getValue(List.class).toArray());
    }

    @Test
    void testMultipleMatches_UntilStopNotation() {
        assertEquals("Yep",
                processor.process("switch(4)[<5:\"Yep\"!;>3:\"Yep too\";4:\"no\"]").getValue());
        assertArrayEquals(new String[] {"Yep", "Yep too"},
                processor.process("switch(4)[<5:\"Yep\";>3:\"Yep too\"!;4:\"no\"]").getValue(List.class).toArray());
    }

    @Test
    void testNoMatches_DefaultCaseUsed() {
        assertEquals("ha",
                processor.process("switch (4)[<1:\"blah\";<3:\"hello\";<4:\"tra\";default:\"ha\"]").getValue());
    }

    @Test
    void testMatch_WithDefaultCase() {
        assertEquals("blah",
                processor.process("switch (4)[4:'blah';<3:\"hello\";<4:\"tra\";default:\"ha\"]").getValue());
    }

    @Test
    void testBothMatch_WithStop() {
        assertEquals(12,
                processor.process("switch(1,2,3)[$0==1,==2,==3: 3 * 4!;>=1,>=2,>=3: \"wrong answer\";default: \"fail\"]").getValue());
    }

    @Test
    void testComplexParam() {
        assertEquals("Its 6",
                processor.process("switch(2 / (9 - 3), 11 - 2)[<1, ==9: \"Its 6\"!;< 10,==9: \"Less than 10\";" +
                        "default: \"Couldnt match\"]").getValue());
    }

    @Test
    void testConditionalInCase() {
        assertEquals(3, processor.process("switch(4)[3>4?3:4:3;>4: false;default: SUM(1,2,3) + 2]").getValue());
    }

    private static TestObject sampleObject() {
        return TestObject.builder()
                .componentA(BigDecimal.valueOf(12))
                .componentB(BigDecimal.valueOf(5))
                .name("Jim")
                .build();
    }
}
