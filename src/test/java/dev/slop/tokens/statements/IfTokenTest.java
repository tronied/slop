package dev.slop.tokens.statements;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.processor.SLOPProcessor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class IfTokenTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    public void testIfCondition_WithNativeCallAndEvaluation() {
        assertEquals(4, processor.process("if ([1,2].size() + 3 == 5) result = 4;").getValue(Integer.class));
    }

    @Test
    public void testIfCondition_Random() {
        String expression =
                "if (2 > 1) {" +
                "   a = 1;" +
                "   b = 2;" +
                "   return a + b;" +
                "}";
        assertEquals(3, processor.process(expression).getValue(Integer.class));
    }

    @Test
    public void testIfCondition_Random_NegativeCondition() {
        String expression =
                "a = 0;b = 0;" +
                "if (1 > 2) {" +
                "   a = 1;" +
                "   b = 2;" +
                "} else {" +
                "   a = 3;" +
                "   b = 5;" +
                "}" +
                "result = a + b;";
        assertEquals(8, processor.process(expression).getValue(Integer.class));
    }
}
