package dev.slop.tokens.statements;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.processor.SLOPProcessor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class JVMInvokeTokenTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.include("testEnum", "dev.slop.enums.TestEnum");
        config.include("testClass", "dev.slop.model.TestClass");
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    public void testWorks() {
        assertEquals(31, processor.process("(#testEnum.VALUE_A.value + 123) / 4")
                .getValue(Integer.class));
    }

    @Test
    public void testWorks2() {
        assertEquals(31, processor.process("(#testClass.$TestEnum.VALUE_A.value + 123) / 4")
                .getValue(Integer.class));
    }
}
