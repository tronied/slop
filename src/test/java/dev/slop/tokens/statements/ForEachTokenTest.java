package dev.slop.tokens.statements;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.exception.LexerException;
import dev.slop.model.Company;
import dev.slop.model.Employee;
import dev.slop.processor.SLOPProcessor;
import dev.slop.template.TestTemplate;
import dev.slop.util.CollectionTestUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ForEachTokenTest extends TestTemplate {
    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    void testForEach_ObjectRef_FieldExtraction() {
        SLOPContext context = CollectionTestUtil.smallCollectionContext();
        Assertions.assertArrayEquals(
                Arrays.asList("Mary", "Bob").toArray(),
                processor.process("foreach(emp : acme.employees) result = emp.name;",
                    context).getValue(List.class).toArray());
    }

    @Test
    void testForEach_ObjectRef_FieldExtraction_Filter() {
        SLOPContext context = CollectionTestUtil.mediumCollectionContext();
        Assertions.assertArrayEquals(
                Arrays.asList("Bob", "Susan").toArray(),
                processor.process("foreach(emp : acme.employees) if (emp.age > 40) return & emp.name;",
                        context).getValue(List.class).toArray());
    }

    @Test
    void testMultiNumericCondition_MultiExpr_Result() {
        SLOPContext context = CollectionTestUtil.smallCollectionContext();
        Assertions.assertArrayEquals(
                Arrays.asList("Mary's Half Age: 17", "Bob's Half Age: 27").toArray(),
                processor.process(
                        "foreach (emp : acme.employees) hl = emp.age / 2; result = emp.name + \"'s Half Age: \" + hl;",
                        context).getValue(List.class).toArray());
    }

    @Test
    void testGeneratedExpression_WithComplexCondition() {
        Company company = sampleCompany();
        SLOPContext context = new SLOPContext();
        context.set("acme", company);
        String expression = "foreach(obj : acme.employees) " +
                "if (acme.topEarners.contains(obj.payrollId) and obj.name.startsWith('A')) return obj.name;";
        assertArrayEquals(Collections.singletonList("Anna").toArray(),
                processor.process(expression, context).getValue(List.class).toArray());
    }

    @Test
    void testMultiNumericCondition_MultiExpr_IncompleteStatement_SemiColon() {
        SLOPContext context = CollectionTestUtil.smallCollectionContext();
        LexerException ex = Assertions.assertThrows(LexerException.class, () ->
                processor.process(
                        "foreach (emp : acme.employees) hl = emp.age / 2; result = emp.name + \"'s Half Age: \" + hl",
                        context).getValue(List.class));
        assertEquals("Expected token ';' in ForEachToken but expression ended prematurely. " +
                "----------------------------------------------------------------" +
                "- Guidance: Each for-each block must be terminated by a ';' character, even if there is only one e.g. foreach (...) result = 1 + 1 ;<--" +
                "- Last Read Tokens: '+','s Half Age: ','+','hl'" +
                "- Expression: foreach (emp : acme.employees) hl = emp.age / 2; result = emp.name + \"'s Half Age: \" + hl" +
                "----------------------------------------------------------------",
                ex.getMessage().replace(System.lineSeparator(), ""));
    }

    @Test
    void testMultiNumericCondition_MultiExpr_IncompleteStatement_ConditionClosingBrace() {
        SLOPContext context = CollectionTestUtil.smallCollectionContext();
        LexerException ex = Assertions.assertThrows(LexerException.class, () ->
                processor.process(
                        "foreach (emp : acme.employees hl = emp.age / 2; result = emp.name + \"'s Half Age: \" + hl;",
                        context).getValue(List.class));
        assertEquals("Expected token ')' in ForEachToken but expression ended prematurely. " +
                "----------------------------------------------------------------" +
                "- Guidance: The for-each collection statement must be wrapped in braces e.g. foreach ( obj : collection )<--" +
                "- Last Read Tokens: 'emp','acme.employees','hl','=','emp.age','/','2'" +
                "- Expression: foreach (emp : acme.employees hl = emp.age / 2; result = emp.name + \"'s Half Age: \" + hl;" +
                "----------------------------------------------------------------",
                ex.getMessage().replace(System.lineSeparator(), ""));
    }
}
