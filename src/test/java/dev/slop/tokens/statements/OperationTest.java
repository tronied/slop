package dev.slop.tokens.statements;

import dev.slop.config.DefaultProperty;
import dev.slop.exception.ParserException;
import dev.slop.processor.SLOPProcessor;
import dev.slop.tokens.Token;
import dev.slop.tokens.functions.TestFunctionConfig;
import dev.slop.tokens.literals.IntegerToken;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class OperationTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        TestFunctionConfig testSLOPConfig = new TestFunctionConfig();
        testSLOPConfig.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(testSLOPConfig);
    }

    @Test
    void testBracketedOperation() {
        assertEquals(36, processor.process("3 * (3 + 9)").getValue());
    }

    @Test
    void testMultiBracketedOperation() {
        assertEquals(BigDecimal.valueOf(22.5), processor.process("(15 / 2) * (12 - 9)").getValue());
    }

    @Test
    void testComplexBracketedOperation() {
        assertEquals(BigDecimal.valueOf(1.005681),
                processor.process("(3 * 4 - 2 * (3 / 4) + 9 / (12 + 4)) / (9 + 2)").getValue());
    }

    @Test
    void testOperationWithSwitch() {
        assertEquals("falseBlah",
                processor.process("(3 * 4 - 12 >= 49) + switch(4)[>3 : \"Blah\"]").getValue());
    }

    @Test
    void testFunction_CustomConfig_SingleLiterals() {
        assertEquals(new BigDecimal(-15), processor.process("NEGATE_AND_ADD(1,2,3,4,5)").getValue());
    }

    @Test
    void testFunction_CustomConfig_TokenValueCollection() {
        assertEquals(new BigDecimal(-16), processor.process("NEGATE_AND_ADD(0.testCollection)",
                new TokenValueCollection()).getValue());
    }

    @Test
    void testFunction_CustomConfig_TokenValueCollection_NegativeAdditionals() {
        assertEquals(new BigDecimal(-1), processor.process("NEGATE_AND_ADD(0.testCollection,-5,-10)",
                new TokenValueCollection()).getValue());
    }

    private static class TokenValueCollection {
        private List<Token<?>> testCollection;

        public TokenValueCollection() {
            testCollection = Arrays.asList(
                    new IntegerToken(1),
                    new IntegerToken(3),
                    new IntegerToken(5),
                    new IntegerToken(7)
            );
        }
    }

    @Test
    void testFunction_CustomConfig_RawNumberCollection() {
        assertEquals(new BigDecimal(9), processor.process("NEGATE_AND_ADD(0.testCollection)",
                new RawValueCollection()).getValue());
    }

    private static class RawValueCollection {
        private List<BigDecimal> testCollection;

        public RawValueCollection() {
            //-1 + 4 + -10 + 15 = -7
            testCollection = Arrays.asList(
                    BigDecimal.valueOf(1),
                    BigDecimal.valueOf(-5),
                    BigDecimal.valueOf(10),
                    BigDecimal.valueOf(-15)
            );
        }
    }

    @Test
    void testFunction_InvalidParamValue() {
        ParserException exception = assertThrows(ParserException.class,
                () -> processor.process("NEGATE_AND_ADD(1,2,\"hello\",4,5)"));
        assertEquals("Could not operate numerically with value 'hello'", exception.getMessage());
    }
}
