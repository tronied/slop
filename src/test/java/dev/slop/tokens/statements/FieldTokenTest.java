package dev.slop.tokens.statements;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.model.Company;
import dev.slop.model.Person;
import dev.slop.processor.SLOPProcessor;
import dev.slop.template.TestTemplate;
import dev.slop.util.CollectionTestUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FieldTokenTest extends TestTemplate {

    private SLOPProcessor processor;

    @BeforeAll
    void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    void testFieldToken_ExtractNames_UsingRepeat() {
        SLOPContext context = CollectionTestUtil.smallCollectionContext();
        List<String> names = Arrays.asList("Mary", "Bob");
        Assertions.assertArrayEquals(names.toArray(), processor.process(
                "repeat(index++,0,<acme.employees.size()) result = acme.employees[index].name;", context)
                    .getValue(List.class).toArray());

    }

    @Test
    void testFieldToken_ExtractNames_CollectionField() {
        SLOPContext context = CollectionTestUtil.smallCollectionContext();
        List<String> names = Arrays.asList("Mary", "Bob");
        Assertions.assertArrayEquals(names.toArray(), processor.process("acme.employees.name", context)
            .getValue(List.class).toArray());
    }

    @Test
    void testFieldToken_CalculateRevenue_PerEmployee_AsInteger() {
        SLOPContext context = CollectionTestUtil.mediumCollectionContext();
        //Use Native call to get Integer value otherwise it will return as 3.225E...
        assertEquals(32250000, processor.process("(acme.revenue / acme.employees.size()).intValue()",
                context).getValue(Integer.class));
    }

    @Test
    void testObjectConcatenation() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        SLOPContext context = new SLOPContext();
        context.set("p", new Person("user1", "Bob", "Andrews", 24));
        SLOPProcessor processor = new SLOPProcessor(config, context);
        assertEquals("Bob Andrews - Active", processor.process(
                "p.getFirstName() + ' ' + p.getLastName() + ' - Active'").getValue(String.class));
    }

    @Test
    void testContextArrayItems() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Bob", 25));
        employees.add(new Employee("Sarah", 35));
        SLOPContext context = new SLOPContext();
        context.set("employees", employees);
        assertEquals("Bob", processor.process("employees[0].name", context).getValue(String.class));
    }

    @Test
    void testContextArrayItems_NoFieldName() {
        List<Employee> employees = new ArrayList<>();
        Employee first = new Employee("Bob", 25);
        employees.add(first);
        employees.add(new Employee("Sarah", 35));
        SLOPContext context = new SLOPContext();
        context.set("employees", employees);
        assertEquals(first, processor.process("employees[0]", context).getValue(Employee.class));
    }

    @Test
    void testContextArrayItemsWithWrapper() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Bob", 25));
        employees.add(new Employee("Sarah", 35));
        WrapperClass wrapperClass = new WrapperClass(employees);
        SLOPContext context = new SLOPContext();
        context.set("employees", wrapperClass);
        assertEquals("Bob", processor.process("employees.people[0].name", context)
                .getValue(String.class));
    }

    @Test
    void testContextArrayItems_WithSearchIndexDef() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Bob", 25));
        employees.add(new Employee("Sarah", 35));
        WrapperClass wrapperClass = new WrapperClass(employees);
        SLOPContext context = new SLOPContext();
        context.set("employees", wrapperClass);
        assertEquals(25, processor.process("employees.people[^~name == 'Bob'].age", context)
                .getValue(Integer.class));
    }

    @Test
    void testContextArrayItems_WithSearchIndexDef_MultipleResults() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Bob", 25));
        employees.add(new Employee("Sarah", 35));
        employees.add(new Employee("Sally", 59));
        WrapperClass wrapperClass = new WrapperClass(employees);
        SLOPContext context = new SLOPContext();
        context.set("employees", wrapperClass);
        List<String> expected = Arrays.asList("Bob", "Sarah");
        assertArrayEquals(expected.toArray(), processor.process("employees.people[^~age < 40].name", context)
                .getValue(List.class).toArray());
    }

    @Test
    void testContextArrayItems_WithSearchIndexDef_LinkedHashMap() {
        SLOPContext context = initLinkedHashMapEmployees();
        assertEquals(25, processor.process("employees.people[^~name == 'Bob'].age", context)
                .getValue(Integer.class));
    }

    @Test
    void testContextArrayItems_IntegerIndex_LinkedHashMap() throws Exception {
        String json = "{\"name\": \"Acme Corp\",\"employees\": [{\"name\": \"Bob\",\"age\": 23}," +
                "{\"name\": \"Sally\",\"age\": 49},{\"name\": \"Terrence\",\"age\": 60}," +
                "{\"name\": \"Anna\",\"age\": 25}]}";
        LinkedHashMap<String, Object> result = new ObjectMapper().readValue(json, LinkedHashMap.class);
        SLOPContext context = new SLOPContext();
        context.set("Acme", result);
        assertEquals("Bob Sally", processor.process("Acme.employees[0].name + ' ' + Acme.employees[1].name", context)
                .getValue(String.class));
        assertEquals(false, processor.process("Acme.employees[0] == Acme.employees[1]", context)
                .getValue(Boolean.class));
        assertEquals(true, processor.process("Acme.employees[0] == Acme.employees[0]", context)
                .getValue(Boolean.class));
    }

    @Test
    void testContextArrayItems_WithSearchIndexDef_MultipleResults_LinkedHashMap() {
        SLOPContext context = initLinkedHashMapEmployees();
        List<String> expected = Arrays.asList("Bob", "Sarah");
        assertArrayEquals(expected.toArray(), processor.process("employees.people[^~age < 40].name", context)
                .getValue(List.class).toArray());
    }

    @Test
    void testContextArrayItems_WithSearchIndexDef_MultipleResults_ArrayLinkedHashMap() {
        SLOPContext context = initRawArrayLinkedHashMapEmployees();
        List<String> expected = Arrays.asList("Bob", "Sarah");
        assertArrayEquals(expected.toArray(), processor.process("employees[^~age < 40].name", context)
                .getValue(List.class).toArray());
    }

    @Test
    void testContextArrayItems_NoIndex_LinkedHashMap() {
        SLOPContext context = initLinkedHashMapEmployees();
        List<String> expected = Arrays.asList("Bob", "Sarah", "Sally");
        assertArrayEquals(expected.toArray(), processor.process("employees.people.name", context)
                .getValue(List.class).toArray());
    }

    @Test
    void testGetPlainArrayByReference() {
        SLOPContext context = new SLOPContext();
        LinkedHashMap<String, Object> rootObject = new LinkedHashMap<>();
        LinkedHashMap<String, Object> employee1 = new LinkedHashMap<>();
        employee1.put("id", 1);
        employee1.put("name", "Bob");
        LinkedHashMap<String, Object> employee2 = new LinkedHashMap<>();
        employee2.put("id", 2);
        employee2.put("name", "Sally");
        rootObject.put("employees", Arrays.asList(employee1, employee2));
        context.set("source", rootObject);
        assertArrayEquals(Arrays.asList(employee1, employee2).toArray(),
                processor.process("source.employees", context).getValue(List.class).toArray());
    }

    @Test
    void testCollectionFilter_WithExistingContextMatch() {
        SLOPContext context = new SLOPContext();
        String expr = "salaries[^~employeeId == e.employeeId]";
        context.set("e", Salary.builder().employeeId(25).build());
        Salary expected = Salary.builder().employeeId(25).name("Mary").build();
        context.set("salaries", Arrays.asList(
            Salary.builder().employeeId(10).name("Bob").build(), expected,
            Salary.builder().employeeId(30).name("Sue").build()));
        assertEquals(expected, processor.process(expr, context).getValue(Salary.class));
    }

    @Test
    void testCollectionFilter_FieldWithNativeCallCompared() {
        String expression = "acme.employees[^acme.topEarners.contains(~payrollId)].name";
        SLOPContext context = new SLOPContext();
        Company company = sampleCompany();
        context.set("acme", company);
        assertArrayEquals(Arrays.asList("Sharon", "Anna").toArray(),
                processor.process(expression, context).getValue(List.class).toArray());
    }

    @Test
    void testCollectionFilter_FieldWithNativeCallComparedWithBoolean() {
        String expression = "acme.employees[^acme.topEarners.contains(~payrollId) == false].name";
        SLOPContext context = new SLOPContext();
        Company company = sampleCompany();
        context.set("acme", company);
        assertArrayEquals(Arrays.asList("Bob", "Mike").toArray(),
                processor.process(expression, context).getValue(List.class).toArray());
    }

    @Test
    void testCollectionFilter_FieldWithNativeCallComparedWithUnaryOperator() {
        String expression = "acme.employees[^!acme.topEarners.contains(~payrollId)].name";
        SLOPContext context = new SLOPContext();
        Company company = sampleCompany();
        context.set("acme", company);
        assertArrayEquals(Arrays.asList("Bob", "Mike").toArray(),
                processor.process(expression, context).getValue(List.class).toArray());
    }

    @Test
    void testCollectionFilter_MultipleConditionsInFilter() {
        String expression = "acme.employees[^acme.topEarners.contains(~payrollId) and ~name.startsWith('S')].name";
        SLOPContext context = new SLOPContext();
        Company company = sampleCompany();
        context.set("acme", company);
        assertEquals("Sharon", processor.process(expression, context).getValue());
    }

    @Test
    void testCollectionFilter_StringLengthNativeCall() {
        String expression = "acme.employees[^~name.length() == 4].name";
        SLOPContext context = new SLOPContext();
        Company company = sampleCompany();
        context.set("acme", company);
        assertArrayEquals(Arrays.asList("Mike", "Anna").toArray(),
                processor.process(expression, context).getValue(List.class).toArray());
    }
    
    @Test
    void testArrayReference_InArrayResult() {
        String expression = "(foreach(person : acme.employees) result = person.name.subString(1);)[0]";
        SLOPContext context = new SLOPContext();
        Company company = sampleCompany();
        context.set("acme", company);
        assertEquals("ob", processor.process(expression, context).getValue(String.class));
    }

    @Test
    void testTwoFieldToken_ObjectEvaluation_NoMatch() {
        String expression = "acme.employees[0] == acme.employees[1]";
        SLOPContext context = new SLOPContext();
        Company company = sampleCompany();
        context.set("acme", company);
        assertEquals(false, processor.process(expression, context).getValue(Boolean.class));
    }

    @Test
    void testTwoFieldToken_ObjectEvaluation_Match() {
        String expression = "acme.employees[1] == acme.employees[1]";
        SLOPContext context = new SLOPContext();
        Company company = sampleCompany();
        context.set("acme", company);
        assertEquals(true, processor.process(expression, context).getValue(Boolean.class));
    }

    @Test
    void testAddition_NumberWithFunction() {
        String expression = "20 + SUM(acme.employees.payrollId)";
        SLOPContext context = new SLOPContext();
        Company company = sampleCompany();
        context.set("acme", company);
        assertEquals(2240, processor.process(expression, context).getValue(Integer.class));
    }

    @Test
    void testAddition_FieldRefWithFunction() {
        String expression = "random.employeeId + SUM(acme.employees.payrollId)";
        SLOPContext context = new SLOPContext();
        Company company = sampleCompany();
        context.set("acme", company);
        context.set("random", Salary.builder().employeeId(123).build());
        assertEquals(2343, processor.process(expression, context).getValue(Integer.class));
    }

    @Test
    void testAddition_FieldRefWithFunction_WithinOperation() {
        String expression = "(random.employeeId + SUM(acme.employees.payrollId))";
        SLOPContext context = new SLOPContext();
        Company company = sampleCompany();
        context.set("acme", company);
        context.set("random", Salary.builder().employeeId(123).build());
        assertEquals(2343, processor.process(expression, context).getValue(Integer.class));
    }

    @Data
    @Builder
    private static class Salary {
        private int employeeId;
        private String name;
    }

    private SLOPContext initLinkedHashMapEmployees() {
        LinkedHashMap<String, Object> people = new LinkedHashMap<>();
        LinkedHashMap<String, Object> bob = new LinkedHashMap<>();
        bob.put("name", "Bob");
        bob.put("age", 25);
        LinkedHashMap<String, Object> sarah = new LinkedHashMap<>();
        sarah.put("name", "Sarah");
        sarah.put("age", 35);
        LinkedHashMap<String, Object> sally = new LinkedHashMap<>();
        sally.put("name", "Sally");
        sally.put("age", 59);
        people.put("people", Arrays.asList(bob, sarah, sally));
        SLOPContext context = new SLOPContext();
        context.set("employees", people);
        return context;
    }

    private SLOPContext initRawArrayLinkedHashMapEmployees() {
        LinkedHashMap<String, Object> bob = new LinkedHashMap<>();
        bob.put("name", "Bob");
        bob.put("age", 25);
        LinkedHashMap<String, Object> sarah = new LinkedHashMap<>();
        sarah.put("name", "Sarah");
        sarah.put("age", 35);
        LinkedHashMap<String, Object> sally = new LinkedHashMap<>();
        sally.put("name", "Sally");
        sally.put("age", 59);
        SLOPContext context = new SLOPContext();
        context.set("employees", Arrays.asList(bob, sarah, sally));
        return context;
    }

    @Data
    @AllArgsConstructor
    private class WrapperClass {
        private List<Employee> people;
    }

    @Data
    @AllArgsConstructor
    private class Employee {
        private String name;
        private int age;
    }
}
