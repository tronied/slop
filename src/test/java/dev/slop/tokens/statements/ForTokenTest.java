package dev.slop.tokens.statements;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.exception.ParserException;
import dev.slop.model.Company;
import dev.slop.processor.SLOPProcessor;
import dev.slop.template.TestTemplate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ForTokenTest extends TestTemplate {
    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    void testFixedLoop_SingleLine() {
        String expression = "for (i++;0;<10) return & 'hello';";
        processor.process(expression);
    }

    @Test
    void testFixedLoop_MultiLine() {
        String expression = "for (i++;0;<10) { return & 'hello' + (i + a); a = i + 2; }";
        processor.process(expression);
    }

    @Test
    void testFixedLoop_SingleLine_NoContent() {
        String expression = "for (i++;0;<10) ;";
        processor.process(expression);
    }

    @Test
    void testFixedLoop_MultiLine_NoContent() {
        String expression = "for (i++;0;<10) {}";
        processor.process(expression);
    }

    @Test
    void testVariableLoop_Implementation() {
        Company company = sampleCompany();
        SLOPContext context = new SLOPContext();
        context.set("acme", company);
        String expression = "for (emp : acme.employees) { a = 123; return & emp.name + a; }";
        processor.process(expression, context);
    }

    @Test
    void testSecondaryToken_Error() {
        String expression = "{ a = 123; result = 'hello'; }";
        //Will actually lex to tokens though incorrect. The map will throw an exception during execution by parser
        Assertions.assertThrows(ParserException.class, () -> processor.process(expression));
    }

    @Test
    void testLoopWithIfCondition() {
        Company company = sampleCompany();
        SLOPContext context = new SLOPContext();
        context.set("acme", company);
        String expression = "for (emp : acme.employees) if (3 > 2) result = obj;";
        processor.process(expression, context);
    }

    @Test
    void testGeneratedExpression_WithComplexCondition() {
        Company company = sampleCompany();
        SLOPContext context = new SLOPContext();
        context.set("acme", company);
        String expression =
                "for (obj : acme.employees) {" +
                "   if (acme.topEarners.contains(obj.payrollId) and obj.name.startsWith('A')) " +
                "       return & obj.name; " +
                "}";
        assertArrayEquals(Collections.singletonList("Anna").toArray(),
                processor.process(expression, context).getValue(List.class).toArray());
    }
}
