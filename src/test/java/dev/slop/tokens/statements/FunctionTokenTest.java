package dev.slop.tokens.statements;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.processor.SLOPProcessor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigInteger;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FunctionTokenTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.DEBUG_MODE, false);
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    void testFunction_SimpleDefinition() {
        String expression =
                "func myFunction(a,b) { " +
                    "result = a + b; " +
                "}" +
                "result = myFunction(1,2);";
        assertEquals(3, processor.process(expression).getValue(Integer.class));
    }

    @Test
    void testFunction_Fibonacci_Single() {
        String expression =
                "func fibonacci(size) { " +
                "   fib = [a = 0, b = 1];" +
                "   for (i++;0;<size) {" +
                "       fib = fib + [c = a + b];" +
                "       a = b;" +
                "       b = c;" +
                "   }"+
                "   return fib;" +
                "}" +
                "result = fibonacci(10);";
        Integer[] expected = {0,1,1,2,3,5,8,13,21,34,55,89};
        assertArrayEquals(expected, processor.process(expression).getValue(List.class).toArray());
    }

    @Test
    void testFunction_Fibonacci_AddEqualsSign() {
        String expression =
                "func fibonacci(size) { " +
                "   fib = [a = 0, b = 1];" +
                "   for (i++;0;<size) {" +
                "       fib += [c = a + b];" +
                "       a = b;" +
                "       b = c;" +
                "   }"+
                "   return fib;" +
                "}" +
                "result = fibonacci(10);";
        Integer[] expected = {0,1,1,2,3,5,8,13,21,34,55,89};
        assertArrayEquals(expected, processor.process(expression).getValue(List.class).toArray());
    }

    @Test
    void testFunction_Fibonacci_Multi() {
        String expression =
                "func initArray() {" +
                "   result = [0,1];" +
                "}" +
                "" +
                "func fibonacci(size) { " +
                "   fib = [a = 0, b = 1];" +
                "   for (i++;0;<size) {" +
                "       fib = fib + [c = a + b];" +
                "       a = b;" +
                "       b = c;" +
                "   }"+
                "   return fib;" +
                "}" +
                "return initArray() + fibonacci(10);";
        //Odd test, but the array is being appended by the initArray unnecessarily, hence we get the below
        Integer[] expected = {0,1,0,1,1,2,3,5,8,13,21,34,55,89};
        assertArrayEquals(expected, processor.process(expression).getValue(List.class).toArray());
    }

    @Test
    void testFunction_EmbeddedMethodCall_CallDownstream() {
        String expression =
                "result = myFunctionA(10);" +
                "func myFunctionA(a) {" +
                "   result = myFunctionB(a);" +
                "}" +
                "" +
                "func myFunctionB(b) { " +
                "   result = b + 10;" +
                "}";
        assertEquals(20, processor.process(expression).getValue(Integer.class));
    }

    @Test
    void testFunction_FibonacciDefinition_Recursive() {
        String expression =
                "func fibonacci(n) { " +
                "   if (n == 0 or n == 1) {" +
                "       return n;" +
                "   }" +
                "   return fibonacci(n - 1) + fibonacci(n - 2);"+
                "}" +
                "result = fibonacci(10);";
        assertEquals(55, processor.process(expression).getValue(Integer.class));
    }

    @Test
    void testFunction_FibonacciDefinition_Recursive_OperationCondition_Streamlined() {
        String expression =
                "func fibonacci(n) { " +
                "   return (n <= 1) ? n : fibonacci(n - 1) + fibonacci(n - 2);"+
                "}" +
                "result = fibonacci(10);";
        assertEquals(55, processor.process(expression).getValue(Integer.class));
    }

    @Test
    void testFunction_FibonacciDefinition_Recursive_Streamlined() {
        String expression =
                "func fibonacci(n) { " +
                "   return n <= 1 ? n : fibonacci(n - 1) + fibonacci(n - 2);"+
                "}" +
                "result = fibonacci(10);";
        assertEquals(55, processor.process(expression).getValue(Integer.class));
    }

    @Test
    void testFunction_FibonacciDefinition_Recursive_ElseIf() {
        String expression =
                "func fibonacci(n) { " +
                "   if (n == 0 or n == 1) {" +
                "       return n;" +
                "   } else {" +
                "       return fibonacci(n - 1) + fibonacci(n - 2);"+
                "   }" +
                "}" +
                "result = fibonacci(10);";
        assertEquals(55, processor.process(expression).getValue(Integer.class));
    }

    @Test
    void testFunction_ScopedVariables() {
        String expression =
                "func myFunctionA() {" +
                "   a = 123;" +
                "   b = 987;" +
                "}" +
                "myFunctionA();" +
                "result = a + b;";
        assertEquals("ab", processor.process(expression).getValue(String.class));
    }
    
    @Test
    void testFibonacci_Recursive_Fast_MemMap() {
        String expression = createFastMemMapExpression("fastFib(47)");
        assertEquals(BigInteger.valueOf(2971215073L), processor.process(expression).getValue());
    }

    @Test
    void testFibonacci_Recursive_Fast_MemMap_ReturnOperation() {
        String expression = createFastMemMapExpression("(fastFib(47).longValue())");
        assertEquals(2971215073L, processor.process(expression).getValue());
    }

    @Test
    void testFibonacci_Recursive_Fast_MemMap_NativeMapping() {
        String expression = createFastMemMapExpression("fastFib(47).longValue()");
        assertEquals(2971215073L, processor.process(expression).getValue());
    }

    private String createFastMemMapExpression(String returnStatement) {
        return String.format(
               "memMap = {};" +
               "func fastFib(n) {" +
               "    if (n <= 1) return n;" +
               "    first = memMap.get(n - 1);" +
               "    second = memMap.get(n - 2);" +
               "    result = (first == null ? fastFib(n - 1) : first) + (second == null ? fastFib(n - 2) : second);" +
               "    memMap += {n -> result};" +
               "    return result;" +
               "}" +
               "return %s;", returnStatement);
    }

    @Test
    void testFibonacci_Recursive_Fast_ElvisOperator() {
        String expression =
                "memMap = {};" +
                "func fastFib(n) {" +
                "    if (n <= 1) return n;" +
                "    /* If the results exist use that, alternatively perform recursive call */" +
                "    result = memMap.get(n - 1) ?: fastFib(n - 1) + memMap.get(n - 2) ?: fastFib(n - 2);" +
                "    memMap += {n -> result};" +
                "    return result;" +
                "}" +
                "return fastFib(47);";
        assertEquals(BigInteger.valueOf(2971215073L), processor.process(expression).getValue());
    }
}
