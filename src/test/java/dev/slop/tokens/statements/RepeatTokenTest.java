package dev.slop.tokens.statements;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.processor.SLOPProcessor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class RepeatTokenTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    public void testRepeat_FibonacciSequence() {
        SLOPContext context = new SLOPContext();
        context.set("first", 0);
        context.set("second", 1);
        assertArrayEquals(Arrays.asList(0,1,1,2,3,5,8,13,21,34,55,89).toArray(),
            processor.process(
                    "[0,1] + repeat(i++,0,<10) " +
                            "       result = first + second;" +
                            "       first = second;" +
                            "       second = result;",
                    context).getValue(List.class).toArray());
    }

    @Test
    public void testRepeat_FibonacciSequence_ArrayVariableInitialiser_NoContext() {
        assertArrayEquals(Arrays.asList(0,1,1,2,3,5,8,13,21,34,55,89).toArray(),
                processor.process(
                        "[a = 0,b = 1] + repeat(i++,0,<10) result = a + b; a = b; b = result;")
                        .getValue(List.class).toArray());
    }

    @Test
    public void testRepeatResult_AddArray() {
        assertArrayEquals(Arrays.asList(0,1,2,3).toArray(),
                processor.process("repeat(i++,0,<2) result = i; + [2,3]").getValue(List.class).toArray());
    }

    @Test
    public void testRepeatResult_AddArray_NativeCallGet() {
        assertEquals(1, processor.process("(repeat(i++,0,<2) result = i; + [2,3]).get(1)").getValue());
    }
}
