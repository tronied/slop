package dev.slop.tokens.statements;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.exception.LexerException;
import dev.slop.processor.SLOPProcessor;
import dev.slop.tokens.model.SubObject;
import dev.slop.tokens.model.SubSubObject;
import dev.slop.tokens.model.TestObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ConditionalTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    void testNumericCondition_NumberResult() {
        assertEquals(2, processor.process("3 > 4 ? 1 : 2").getValue());
    }

    @Test
    void testMultiNumericCondition_LogicalOR_NumberResult() {
        assertEquals(1, processor.process("3 > 4 or 2 > 1 ? 1 : 2").getValue());
    }

    @Test
    void testComplexCondition_ComplexResult() {
        assertEquals(20, processor.process(
                "0.componentA * 0.componentB + 6 <= (SUM(0.subObjects.value,4) - 0.componentC / 3) + 30 ? 9 * " +
                        "0.subObjects[0].subSubObject.valueB + 2 : \"hello\"", sampleObject()).getValue());
    }

    @Test
    void testComplexCondition_ComplexResult_NamedContext() {
        SLOPContext context = new SLOPContext();
        context.set("sampleObject", sampleObject());
        assertEquals(20, processor.process(
                "sampleObject.componentA * sampleObject.componentB + 6 <= (SUM(sampleObject.subObjects.value,4) - " +
                        "sampleObject.componentC / 3) + 30 ? 9 * sampleObject.subObjects[0].subSubObject.valueB + 2 : " +
                        "\"hello\"", context).getValue());
    }

    @Test
    void testHandlingGreedyPattern() {
        assertEquals(BigDecimal.valueOf(3), processor.process("SUM(1,SUM(1,2) > 4 ? 1 : 2)", new SLOPContext()).getValue());
    }

    @Test
    void testHandlingGreedyPattern_MultipleEmbedded() {
        SLOPContext context = new SLOPContext();
        context.set("testObject", sampleObject());
        assertEquals("Hooray",
                processor.process("switch(testObject.subObjects[3 > 4 ? 0 : 1].value)[54: \"Hooray\";12: \"Boo\"]",
                        context).getValue());
    }

    /* Test Generator found issues + fixes */

    @Test
    void testConditionalSwitch_Boundaries() {
        assertEquals("actual", processor.process("-1454641597 >= 1697518649 ? (270132229) : switch(-1006807977)" +
                "[-1042937622 >= -573657634 : -674395793;default: \"actual\"]").getValue());
    }

    @Test
    void testConditionalSwitch_ConditionalOperation() {
        assertEquals(5, processor.process("((1 + 2) / 3 > 2 ? 1 : 2) + 3").getValue());
    }

    @Test
    void testConditionalSwitch_UnfinishedStatement() {
        LexerException ex = assertThrows(LexerException.class,
                () -> processor.process("((1 + 2) / 3 > 2 ? 1) + 3").getValue());
        assertEquals("Expected token ':' in ConditionalToken but expression ended prematurely. " +
                "----------------------------------------------------------------" +
                "- Guidance: A conditional requires true / false outcomes to be separated by a ':' e.g. a > b ? a :<-- b" +
                "- Last Read Tokens: '1',')','+','3'" +
                "- Expression: ((1 + 2) / 3 > 2 ? 1) + 3" +
                "----------------------------------------------------------------",
                ex.getMessage().replace(System.lineSeparator(), ""));
    }

    @Test
    void testConditional_Extensive() {
        assertEquals(1039972447, processor.process(
                "(-1187437359 >= -1769692242 ? -1471417678 <= -535759796 ? 1994481996 >= 2145601754 ? " +
                "switch(1313532929)[-1790569574 > -905058706 : 1071265614;default: \"no\"] : 1914136872 >= 673357921 ? " +
                "657516292 > -386909990 ? (1039972447) : -185513997 >= -1593308893 ? (1050355491) : " +
                "switch(765285209)[-1433157028 <= 582341618 : 1881008235;default: \"no\"] : -2031837165 <= -1343248879 ? " +
                "switch(-31847026)[-1287409390 > -1894163833 : 980356953;default: \"no\"] : " +
                "switch(1109476370)[973857669 >= 1456236177 : 1730587778;default: \"no\"] : -1057573624 >= -521341230 ? " +
                "429517875 <= 1613652713 ? 1629357163 <= -1291805840 ? -48710900 <= 279310149 ? -1454641597 >= 1697518649 ? " +
                "(270132229) : switch(-1006807977)[-1042937622 >= -573657634 : -674395793;default: \"no\"] : " +
                "switch(-1412896987)[-40681625 >= -20564261 : 131719176;default: \"no\"] : " +
                "switch(-690722827)[-1016845861 > 2082276916 : 2145732301;default: \"no\"] : " +
                "switch(-626486094)[933947492 <= -1071008706 : -1745796812;default: \"no\"] : (861420854) : " +
                "switch(-1953001549)[278963667 >= -1154072668 : 1292779038;default: \"no\"])").getValue());
    }

    @Test
    void testConditional_Arbitrary() {
        assertEquals(980356953, processor.process("-2031837165 <= -1343248879 ? " +
                "switch(-31847026)[-1287409390 > -1894163833 : 980356953;default: \"no\"] : " +
                "switch(1109476370)[973857669 >= 1456236177 : 1730587778;default: \"no\"]").getValue());
    }

    @Test
    void testConditional_WithNativeCall() {
        SLOPContext context = new SLOPContext();
        context.set("testObject", sampleObject());
        assertEquals("Correct", processor.process(
                "testObject.subObjects.size() == 2 ? 'Correct' : 'False'", context).getResult());
    }

    @Test
    void testConditional_WithNativeCall_InsideOtherToken() {
        SLOPContext context = new SLOPContext();
        context.set("testObject", sampleObject());
        assertEquals("Correct", processor.process(
                "(testObject.subObjects.size() == 2 ? 'Correct' : 'False')", context).getResult());
    }

    @Test
    void testConditional_EmbeddedConditional() {
        assertEquals("true", processor.process(
                "2 > 1 ? 1 > 2 ? 'false' : 'true' : 'false'").getValue(String.class));
    }

    private static TestObject sampleObject() {
        return TestObject.builder()
                .componentA(BigDecimal.valueOf(12))
                .componentB(BigDecimal.valueOf(5))
                .componentC(BigDecimal.valueOf(99))
                .dateA(LocalDateTime.of(2021, 01, 01, 12, 59, 00))
                .dateB(LocalDateTime.of(2020, 05, 01, 12, 59, 00))
                .dateC(Date.valueOf(LocalDate.of(1999, 01, 01)))
                .name("Jim")
                .subObjects(Arrays.asList(
                        SubObject.builder().value(12).subSubObject(
                                SubSubObject.builder().valueA(1).valueB(2).build()
                        ).build(),
                        SubObject.builder().value(54).subSubObject(
                                SubSubObject.builder().valueA(4).valueB(3).build()
                        ).build()
                ))
                .build();
    }
}
