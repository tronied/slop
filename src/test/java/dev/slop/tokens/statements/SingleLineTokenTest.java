package dev.slop.tokens.statements;

import dev.slop.config.DefaultProperty;
import dev.slop.processor.SLOPProcessor;
import dev.slop.tokens.functions.TestFunctionConfig;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SingleLineTokenTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        TestFunctionConfig testSLOPConfig = new TestFunctionConfig();
        testSLOPConfig.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(testSLOPConfig);
    }

    @Test
    void testSimple_MultiLineExpression() {
        assertEquals(6, processor.process(
                "val1 = 1 + 1;\n" +
                        "val2 = 2 + 2;\n" +
                        "result = val1 + val2;").getValue(Integer.class));
    }

    @Test
    void testSimple_MultiLineExpression_EmbeddedIfToken() {
        /* This scenario is odd, but basically if you add an integer to an unknown token, it concatenates the two values
         * together. Here the condition is not met which means val1 is never assigned to, but val 2 is. As such, val1
         * (string value) is added to the value of val2 which is 4. Hence we get val14 */
        assertEquals("val14", processor.process(
                "if (3 > 4) val1 = 4;" +
                        "val2 = 2 + 2;" +
                        "result = val1 + val2;").getValue());
    }

    @Test
    void testSimple_MultiLineExpression_ConditionalWithVariable() {
        assertEquals(6, processor.process(
                "val1 = 3 > 4 ? 1 : 2;" +
                        "val2 = 2 + 2;" +
                        "result = val1 + val2;").getValue());
    }

    @Test
    void testSimple_MultiLineExpression_EmbeddedConditional() {
        assertEquals(7, processor.process(
                "val1 = switch(3)[<3:1!;<4:2] > 4 ? 4 : 3;" +
                         "val2 = 2 + 2;" +
                         "result = val1 + val2;").getValue(Integer.class));
    }

    @Test
    void testSimple_MultiLineExpression_ArrayAddition() {
        assertEquals(5, processor.process(
                "[val1 = 1,val3 = 4];" +
                        "val2 = 2 + 2;" +
                        "result = val1 + val2;").getValue(Integer.class));
    }

    @Test
    void testSimple_MultiLineExpression_MapAddition() {
        assertEquals(6, processor.process(
                "myMap = {val1->1,val3->2};" +
                        "val2 = 2 + 2;" +
                        "result = myMap.get(val3) + val2;").getValue(Integer.class));
    }
}
