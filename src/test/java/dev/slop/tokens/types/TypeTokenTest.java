package dev.slop.tokens.types;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.exception.ParserException;
import dev.slop.processor.SLOPProcessor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TypeTokenTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    void testSimpleType() {
        String expression =
                "type MyType(a,b,c) {" +
                "   func doSomething() {" +
                "       return a + b;" +
                "   }" +
                "}" +
                "test = new MyType(1,2,3);" +
                "return test.doSomething();";
        assertEquals(3, processor.process(expression).getValue());
    }

    @Test
    void testSimpleType_TypeProperties() {
        String expression =
            "type MyType(a,b,c) {" +
            "   func doSomething() {" +
            "       return a + b + c;" +
            "   }" +
            "}" +
            "" +
            "test = new MyType(1,2,3);" +
            "return test.a + test.b;";
        assertEquals(3, processor.process(expression).getValue());
    }

    @Test
    void testSimpleType_Inherits() {
        String expression =
            "type MyType(a,b,c,d) <- ParentType {" +
            "   func doSomething() {" +
            "       return a + b + c + somethingElse();" +
            "   }" +
            "}" +
            "" +
            "type ParentType(d) {" +
            "   func somethingElse() {" +
            "       return d;" +
            "   }" +
            "}" +
            "" +
            "test = new MyType(1,2,3,4);" +
            "return test.doSomething();";
        assertEquals(10, processor.process(expression).getValue());
    }

    @Test
    void testSimpleType_Inherits_DoesNotMeetParentRequirements() {
        String expression =
            "type MyType(a,b,c) <- ParentType {" +
            "   func doSomething() {" +
            "       return a + b;" +
            "   }" +
            "}" +
            "" +
            "type ParentType(d) {" +
            "   func somethingElse() {" +
            "       return d;" +
            "   }" +
            "}" +
            "" +
            "test = new MyType(1,2,3);";
        ParserException ex = Assertions.assertThrows(ParserException.class, () -> processor.process(expression));
        assertEquals("'MyType' has a dependency on 'ParentType' which requires parameter(s) 'd' to be defined " +
                "in the class definition or constructor. Please add this to the MyType type definition.", ex.getMessage());
    }

    @Test
    void testSimpleType_Inherits_ParentCall() {
        String expression =
            "type MyType <- ParentType {" +
            "   func doSomething() {" +
            "       return 'Child';" +
            "   }" +
            "}" +
            "" +
            "type ParentType {" +
            "   func somethingElse() {" +
            "       return 'Parent';" +
            "   }" +
            "}" +
            "" +
            "test = new MyType();" +
            "return [test.doSomething(),test.somethingElse()];";
        assertEquals(Arrays.asList("Child", "Parent"), processor.process(expression).getValue(List.class));
    }

    @Test
    void testSimpleType_WithConstructor() {
        String expression =
                "type MyType {" +
                "   this(a,b,c);" +
                "}" +
                "test = new MyType(1,2,3);" +
                "test.doSomething();";
        ParserException ex = Assertions.assertThrows(ParserException.class, () -> processor.process(expression));
        assertEquals("Could not find function 'doSomething' in object 'test'", ex.getMessage());
    }
}
