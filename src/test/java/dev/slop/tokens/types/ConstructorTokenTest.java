package dev.slop.tokens.types;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.exception.LexerException;
import dev.slop.processor.SLOPProcessor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ConstructorTokenTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    void testConstructor_Restriction_ParentTypeMismatch() {
        String expression =
                "func myFunction(a,b,c) {" +
                "   this(a,b,c) {" +
                "   }" +
                "}";
        LexerException ex = Assertions.assertThrows(LexerException.class,
                () -> processor.process(expression));
        assertEquals("ConstructorToken is required to be a child of one of the following token types " +
                        "[TypeToken]. Instead found following class(es) [MultiLineToken,FunctionToken] on the stack",
                ex.getMessage());
    }

    @Test
    void testConstructor_Restriction_ParentAccepted() {
        String expression =
                "type MyType() {" +
                "   this(a,b,c) {" +
                "   }" +
                "}";
        Assertions.assertDoesNotThrow(() -> processor.process(expression));
    }
}
