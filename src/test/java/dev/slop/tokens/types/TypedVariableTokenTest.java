package dev.slop.tokens.types;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.exception.ParserException;
import dev.slop.processor.SLOPProcessor;
import dev.slop.template.TestTemplate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TypedVariableTokenTest extends TestTemplate {
    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    void testTypedVariable_Assignment_Integer() {
        assertEquals(123, processor.process("let Integer myInt = 123;").getValue(Integer.class));
    }

    @Test
    void testTypedVariable_Assignment_String_To_Integer_Fail() {
        ParserException ex = Assertions.assertThrows(ParserException.class,
                () -> processor.process("let Integer myInt = 'hello';"));
        assertEquals("Could not assign type 'String' to 'Integer'!", ex.getMessage());
    }
}
