package dev.slop.tokens.specials;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.processor.SLOPProcessor;
import dev.slop.template.TestTemplate;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RangeTokenTest extends TestTemplate {
    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    void testRangeToken_Uppercase_Incremental() {
        List<String> expected = new ArrayList<>();
        for (int i = (int)'A';i <= (int)'Z';i++) {
            expected.add(Character.toString((char)i));
        }
        assertArrayEquals(expected.toArray(), processor.process("for(char : A..Z) return & char;")
                .getValue(List.class).toArray());
    }

    @Test
    void testRangeToken_Uppercase_Decremental() {
        List<String> expected = new ArrayList<>();
        for (int i = (int)'Z';i >= (int)'A';i--) {
            expected.add(Character.toString((char)i));
        }
        assertArrayEquals(expected.toArray(), processor.process("for(char : Z..A) return & char;")
                .getValue(List.class).toArray());
    }

    @Test
    void testRangeToken_Numbers_Incremental() {
        List<Integer> expected = new ArrayList<>();
        for (int i = 0;i <= 20;i++) {
            expected.add(i);
        }
        assertArrayEquals(expected.toArray(), processor.process("for(int : 0..20) return & int;")
                .getValue(List.class).toArray());
    }
}
