package dev.slop.tokens.specials;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.context.SLOPContext;
import dev.slop.processor.SLOPProcessor;
import dev.slop.template.TestTemplate;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ReturnTokenTest extends TestTemplate {
    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    void testReturnToken_ForLoop_ReturnSingular() {
        SLOPContext context = new SLOPContext();
        context.set("acme", sampleCompany());
        processor.process("for(emp : acme.employees) return emp.name;", context);
    }

    @Test
    void testReturnToken_Type2() {
        processor.process("return & 1 + 1;");
    }

    @Test
    void testReturnToken_Type1() {
        assertEquals(2, processor.process("return 1 + 1;").getValue(Integer.class));
    }
}
