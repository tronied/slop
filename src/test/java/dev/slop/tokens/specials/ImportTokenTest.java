package dev.slop.tokens.specials;

import dev.slop.config.DefaultProperty;
import dev.slop.config.SLOPConfig;
import dev.slop.processor.SLOPProcessor;
import dev.slop.template.TestTemplate;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ImportTokenTest extends TestTemplate {
    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        SLOPConfig config = new SLOPConfig();
        config.setProperty(DefaultProperty.SAFE_OPERATIONS, false);
        processor = new SLOPProcessor(config);
    }

    @Test
    void testSingleImport() {
        String singleImport = testImport("processed/HelloWorld");
        String expression =
                String.format("import '%s';", singleImport) +
                "return helloWorld();";
        assertEquals("Hello World!", processor.process(expression).getValue(String.class));
    }

    @Test
    void testMultiImport_One_Unprocessed() {
        String helloWorld = testImport("processed/HelloWorld");
        String unprocessed = testImport("source/Unprocessed.slp");
        String expression =
                String.format("import '%s', @'%s';", helloWorld, unprocessed) +
                "return helloWorld() + ' ' + unprocessed()";
        assertEquals("Hello World! I had to be compiled to run!", processor.process(expression).getValue());
    }

    private String testImport(String path) {
        return Paths.get(String.format("src/test/resources/%s", path)).toAbsolutePath().toString();
    }
}
