package dev.slop.tokens;

import dev.slop.processor.SLOPProcessor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class OperationHandlerTest {

    private SLOPProcessor processor;

    @BeforeAll
    public void setUp() {
        TestOperationConfig testSLOPConfig = new TestOperationConfig();
        processor = new SLOPProcessor(testSLOPConfig);
    }

    @Test
    void testOperation_Operation_CustomOperators() {
        assertEquals(4, processor.process("(3 add 5) div 2").getValue());
    }

    @Test
    void testOperation_Switch_CustomOperators() {
        assertEquals("yes",
                processor.process("switch (5)[lte 3: \"No\";lte 4: \"Still no\";lte 5: \"yes\"]").getValue());
    }
}
