# Single Line Operations Processor (SLOP)
SLOP is an extensible expression language which uses its own grammar system. It provides a default set of literals, 
operations and statements and has been written from the ground for extensibility. It attempts to free developers 
from having to hardcode every scenario and instead provide dynamic configuration by outsourcing that logic onto 
database records, objects, documents, annotations or even transmission over REST / SOAP calls. The following
guide helps you get an understanding on where and how to use it to suit your individual / group needs. If you find 
SLOP doesn't meet your needs, please first browse the [Functions](#functions) and [Extending](#extending) sections or 
alternatively the [Questions / Issues?](#questions--issues) section for help.

## Table of Contents
1. [Quick Start](#quick-start)
2. [Why?](#why)
2. [Example Projects](#example-projects)
    - [State Engine](#state-engine)
    - [Conditional Messaging Queue](#conditional-messaging-queue)
    - [Document Generator](#document-generator)
    - [Employee Checks](#employee-checks)
    - [Form Validation](#form-validation)
3. [Language](#language-features)
    - [Overview](#overview)
         - [Processor](#processor)
         - [Configuration Class](#configuration-class)
         - [Lexer](#lexer)
         - [Parser](#parser)
         - [Context](#context)
    - [Literals](#literals)
    - [Collections](#collections)
        - [Array](#array)
    - [Functions](#functions)
    - [Operators](#operators)
    - [Statements](#statements)
         - [Conditional](#conditional)
         - [Operation](#operation)
         - [Repeat](#repeat-loop)
         - [For Each](#for-each-loop)
         - [Switch](#switch)
         - [Field](#field)
         - [Variables](#variables)
    - [Extra Features](#extra-features)
         - [Saving / Loading Lexer Output](#saving--loading-lexer-output)
         - [Chained Expressions](#chained-expressions)
4. [Extending](#extending)
    - [Design Approach](#design-approach)
    - [Grammar](#grammar)   
    - [Pattern Tokens](#pattern-tokens)   
    - [Type Operations](#type-operations)
    - [Custom Operators](#operators)
    - [Statements](#statements)
4. [Questions / Issues?](#questions--issues)

## Quick Start
Create a new project declaring the following dependency in maven:
```xml
<dependency>
  <groupId>dev.slop</groupId>
  <artifactId>slop-core</artifactId>
  <version>1.50</version>
</dependency>
``` 
or gradle:
```groovy
implementation 'dev.slop:slop-core:1.50'
```
Add a new Java class with the following:
```java
public static void main(String[] args) {
    int result = SLOPProcessor.processStatic("(1 + 1) * 4").getValue(Integer.class);
    System.out.println("Result: " + result);
}
```
Run the application:
```bash
Result: 8
```
Alternatively for something a bit more ambitious:
```java
public static void main(String[] args) {
    SLOPProcessor processor = new SLOPProcessor();
    SLOPContext context = new SLOPContext();
    context.set("first", 0);
    context.set("second", 1);
    List<?> result = processor.process(
        "[0,1] + repeat(i++,0,<10) result = {?first} + {?second}; first = {?second}; second = {?result};", 
            context).getValue(List.class);
    System.out.println(String.format("Result: [%s]",
            result.stream().map(Object::toString).collect(Collectors.joining(", "))));
}
```
This will print the fibonacci sequence up to 12 places:
```bash
Result: [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
```
...and that's it! These are simple examples to get you started, but please take a look at the following sections to 
discover more of SLOP's features, example projects and even how to extend it yourself. If however you're new to expression 
languages or have a nagging question running through your head, please check out the [why?](#why) section.

## Why
If you've read through the Quick Start section or are a newcomer to expression languages, you may be asking yourself 
that question. Looking around for an answer on the web is not that easy. There are well established expression languages (EL) 
out there including [Jexl](https://commons.apache.org/proper/commons-jexl/) and [SpeL](https://docs.spring.io/spring-integration/docs/5.3.0.RELEASE/reference/html/spel.html), but most sites seem to dive into how to use them rather than 
explain why you'd actually want to do so. They mention something about dynamic configuration / scripting and then move on. 
As such, complaints like these usually follow:
1. No IDE support to help write it
2. No perceived type safety
3. Why use it when you can just write native code?

Some of these like the lack of IDE support are genuine, but that's not to say it won't happen. The advantage with IDE's 
like [IntelliJ](https://www.jetbrains.com/idea/) is that plugins can be written to provide that support. The main issue as 
with most things is finding the time to do it. It is on the roadmap and depending on how the initial release goes, it will 
be prioritised accordingly.

As for type safety, SLOP adheres to the same ruleset as the underlying language (Java). When values are read from an 
expression they are assigned to an typed literal token class. If a value is defined as one type but is not in the expected 
format, an error is thrown. Likewise type operations (whilst configurable) must also adhere to the specified types that 
are compatible or be rejected by the Parser. (see [Type Operations](#type-operations) for more information). 

On the last point, why would you choose this over writing actual code? Typically when writing back-end API's or 
applications, you add functionality to handle the predicted use model. For example, take a bank where customers accounts 
are being managed. You can write code to handle common things like payments, transfers, account information etc. All of 
these are well defined but what about something a bit more open-ended like notifications? Say for example the customer 
wants to be informed when their account goes below a certain threshold. Most banks can support this, but what happens 
when a customer wants to be notified if they have less than 250 in the bank prior to the 11th of every month when they 
pay their energy bill? A system can be written to handle most scenarios, but as coverage increases so too does the 
complexity.

This is where expression languages come in so that you don't have to try and predict what to handle. Code can be written 
on-the-fly to handle any scenario that arises if provided the necessary context and can run instantly. Keep in mind that 
this is just one example, but by opening up your system to dynamic configuration you could start to remove the shackles 
of constant code deployments / hotfixes. For more examples of how this can be used, please see the next section.

## Example Projects
If you've read through the quick start guide, you're probably now asking yourself "...but when would I ever use this?!".
The following section will take you through some example projects to hopefully not just answer that question but also 
make you think diffierently about software development as strict code releases and patch cycles.

**NOTE**: All of these projects are a work in progress and below are some small descriptions of what they will be when
finished.

### State Machine (TBD)
Stores each step as a database record with routing rules and behaviour stored as a field. A separate table can be used 
to record the location of each element traversing the model and the step they are at.

### Conditional Messaging Queue (TBD)
Using Kafka / RabbitMQ, custom rules can be applied for routing traffic based on the messages contents. These rules
can be updated dynamically using a REST endpoint so that there is no downtime.

### Document Generator (TBD)
Using Apache POI and template document that contain SLOP expressions, it would have the following features: 
- Tables can be generated from a Collection (context object or referenced field)
- Conditionals can be used to add / omit sections or watermarks
- When a template file is uploaded, the document can be run through the lexer and the output saved to file. This can
then be loaded to improve performance on multi-document creation.
- Template files would be stored in *.docx format and output to the same or PDF

### Customer Checks (TBD)
Custom rules could be stored against a customer record. For example, a customer could call up a bank and provide a list 
of requirements where they would like to be notified of such an event. By adding these dynamic rules directly to the
customers record (or another table) and providing the relevant resources, events can be triggered to handle such scenarios.

### Form Validation (TBD)
By using a custom annotation on DTO object fields and by using a REST endpoint which accepts a payload from the F/E, complex
validation rules could be enforced. For example, you could have the following:

```java
@Data
public class Employee {
    
    private JobType title;

    @Validation("emp.dept != null and emp.dept == HUMAN_RESOURCES and " +
                "(emp.title != CONSTRUCTION_WORKER or emp.title != BUILDING_MAINTENANCE)")
    private Department dept;
}
```
This is a crude example, but you can validate the currently payload as it is entered into the F/E and if any of the rules 
evaluate to false, it could flag up the issue. We could even return a detailed message as to why it is not valid.

## Language
### Overview
SLOP includes five main components:

1) [Processor](#processor): _Evaluates expressions, ability to save / import lexer output and override other SLOP components._
2) [Config Class](#config-class): _Declaration of token classes (literals, operators, statements etc) and management of properties._
3) [Lexer](#lexer): _Converts the String expression into a hierarchical set of tokens that can be read by the parser._
4) [Parser](#parser): _Reads tokens and defers responsibility to each handler and type operation classes to resolve values_
5) [Context](#context): _Contains objects that are referenced in the expression. Used to store variables._

#### Processor
The processor class is the starting point for anyone wishing to use or configure SLOP. It can evaluate static expressions by
 using:
```java
SLOPProcessor.processStatic("3 > 4 ? \"no\" : \"yes\"").getValue(String.class);
```
or by creating an instance and calling the process method:
```java
SLOPProcessor processor = new SLOPProcessor();
processor.process("3 > 4 ? \"no\" : \"yes\"").getValue(String.class);
```
Each accepts a context object and each has its own advantages over the other. The static version is quick to call 
without the need to create an instance. It is meant for fast lightweight evaluation of expressions, though this 
does impact performance making multiple requests. Also, unlike the instanced version it does not provide the ability to
serialize the lexer output, chain expressions (See [Extra Features](#extra-features)), or modify the configuration / 
override other SLOP components.

#### Configuration Class
The configuration class can be ignored by most users but is useful to those wishing to add / modify existing functionality.
It provides methods to add literals, type operations, statements and customize operators. For example, by creating 
a default configuration class, setting the setOperatorHandler method to the following, you can change the default operators 
to use a different set:
```java
SLOPConfig config = new SLOPConfig();
config.setOperatorHandler(new VerboseOperator(""));
SLOPProcessor processor = SLOPProcessor(config);
```
Now when defining an expression, instead of declaring:
```bash
1 + 3 / (5 * 9) % 2
```
You can use:
```bash
1 add 3 div (5 mul 9) mod 2
```
Why would you do this? I have no idea, but it shows that every aspect of SLOP can be changed if you so wish. If you're 
curious, please take a look at the default OperatorToken class compared with the VerboseOperator class to see how this can 
be done.

Another important aspect of the configuration class is defining and setting custom properties. The tokens use these 
properties to alter their behaviour or end result. At this stage (and to avoid confusion), everything that is
read from an expression String is mapped to a token class (literals, statements etc). Although I will go into this
later in greater detail (See [Extending](#extending)), each token class contains the pattern and code to process a 
result. As such, when I refer to tokens I am referring to them in their abstract form using this context.

There are some general properties which have been defined in a DefaultProperties class. These are:
```java
 DATE_FORMAT("dateFormat"),
 ESCAPE_CHARACTER("escapeCharacter"),
 OR_OPERATOR("orOperator"),
 DEBUG_MODE("debugMode"),
 SAFE_OPERATIONS("safeOperations");
```
As a quick run through of each:
- The date format determines the default format used when using the DATE function e.g. dd-MM-yyyy
- Determines which character can be used as an escape character e.g. "Test String \\"Inner String\\"". In this case 
it's using the backslash character.
- The keyword used as an OR operator. This is a legacy entry and would recommend looking to the LogicOperator class instead.
- Debug mode enables all debug messages to see how values were derived.
- Safe operations is enabled by default. Disabling allows native calls (via reflection) to be called on objects. For example:
  ```java
  SLOPConfig config = new SLOPConfig();
  config.setProperty(DefaultProperties.SAFE_OPERATIONS, false)
  SLOPProcessor processor = new SLOPProcessor(config);
  processor.process("\"testing\".substring(1,4).concat(\"run\")"); //Results in "testrun"
  ```
  Using the above example, attempting a native call without setting that property will throw a ParserException.

The above properties are the default set included with SLOP, however you can add your own and use them within your own
statements. Please see the [Adding Statements](#adding-statements) section for more information.

#### Lexer
Lexers are used to transform different types of expressions into a series of tokens. SLOP uses two lexers which are the 
SLOPGrammarLexer and SLOPLexer. As per it's name, the first is dedicated to translating token grammar Strings into
pattern tokens. Pattern tokens are used by the second lexer to match the current token (read from an expression) to an 
equivalent token type as either a new match or existing item in the stack. Please see the [Design](#design) section for
more information on how the Lexer processes an Expression String.

#### Parser
The parser takes the tokens mapped by the lexer and resolves them into one or more resulting values. It does this by 
deferring responsibility to each token for resolution of it's value. For example, given the following:
```bash
(1 + 2) / 2
```
This would equate to the following token structure from the Lexer:
```bash
TokenGroup[
   OperationToken[
      IntegerToken(1),
      OperatorToken("+"),
      IntegerToken(2)
   ],
   OperatorToken("/"),
   IntegerToken(2)
]
```
The Parser will loop through each token calling the process method to resolve their values. With most literal 
tokens (Integer / Operator / String), when invoked it simply returns its own value e.g. String("hello") 
would be "hello". With a token like the OperationToken (itself a statement) that has three child tokens, these will have to be 
calculated into a result. It is the responsibility of each token to resolve its own child tokens. Don't think though 
that the token is left to fend for itself and start performing adding or subtracting calculations as that responsibility 
can be passed back to the Parser. The Parser is a parameter in the process call and has methods to resolve a 
list of tokens into a result. If this is confusing then I can appreciate that! For more information please see the 
[Design](#design) section for more information on how the Parser and Tokens work together to resolve values.

#### Context
The context instance can be used to pass objects so that they can be evaluated and be referenced in Expression Strings.
For example, given the following classes:
```java
class Company {
    private String name;
    private BigDecimal revenue;
    private List<Employee> employees;
    
    //...
}

class Employee {
    private String name;
    private int age;
    
    //All args constructor
}
```
We could create a context object and set the following:
```java
SLOPContext context = new SLOPContext();

//Employees
Employee employeeA = new Employee("Mary", 34);
Employee employeeB = new Employee("Bob", 54);
Employee employeeC = new Employee("Susan", 64);
Employee employeeD = new Employee("Jim", 23);

//Company
Company acmeCompany = new Company();
acmeCompany.setName("ACME Company Plc");
acmeCompany.setRevenue(BigDecimal.valueOf(129000000));
acmeCompany.setEmployees(Arrays.asList(employeeA, employeeB, employeeC, employeeD));

context.set("acme", acmeCompany);
```
Now using the above, we could write the following to calculate revenue divided by the number of employees:
```java
SLOPConfig config = new SLOPConfig();
config.setProperty(DefaultProperties.SAFE_OPERATIONS, false);
SLOPProcessor processor = new SLOPProcessor(config);
System.out.println("Result: " + processor.process("(acme.revenue / acme.employees.size()).intValue()"
            .getValue(Integer.class));
```
**NOTE**: Because the default result from this would be a BigDecimal (3.225E..), we use the native call intValue() on 
the result to get a more understandable figure. As such, this results in:
```bash
Result: 32250000
```
Using another example with the employee collection, let's say we wanted to extract each name into a collection. 
In most languages you would iterate through each object adding the relevant field. This would be similar to the following:
```bash
repeat(index++;0;<acme.employees.size()) result = acme.employees[{?index}].name;
```
or when using the for-each statement:
```bash
foreach(emp : acme.employees) result = {?emp}.name;
```
You can however get the same result by skipping looping entirely and just specifying the collection and target field:
```bash
acme.employees.name
```
All three above examples would result in the following:
```bash
["Mary", "Bob", "Susan", "Jim"]
```
For more information on the repeat or field token, please see the [Statements](#statements) section for
more information.

### Literals
Literals are fixed values but can represent different types. The default set of types are listed below:

| Type | Pattern | Example | Description |
|:-----|:--------|:--------|:------------|
| Boolean | N/A | myVal = true | True / false value |
| Decimal | #.# | 39.4 | Stored as a big decimal |
| Float | #.#F | 39.4F | Floating point equivalent of decimal |
| Long | ###L | 123456789L | A long is used where a value would exceed the maximum integer length |
| Integer | ## | 12345 | A standard non-decimal number |
| Null | N/A | null | Used where a result can be compared to the lack of any value |
| String | "..." | "This is a string" | A text value |
| Array | [ ... , ... ] | [1,2,3,4,5] | A collection which stores any values e.g. [1,true,3.5F] |

As they have fixed values they cannot be changed, however they can be used in type operations to create new literals.
For example, adding the following two literals results in the creation of a new literal:
```bash
addString = "First" + "Second"
```
Evaluating by using {?addString} would result in "FirstSecond", likewise performing an addition on two arrays:
```bash
[1,2] + [3,4]
```
Would result in:
```bash
[1,2,3,4]
```
Although it may look like we're modifying the first by adding the second to get the result, in actuality a new array 
literal is created. Literals are the building blocks of all expressions and without them, we could not perform any 
operations. Even resolved field values are read into literal types. Only variables can have their values modified and
updated.

### Functions
SLOP has been designed to provide as much functionality as possible, but it may be the case where the right results 
cannot come by using expressions alone. This is where custom functions come into play. Functions can be written in 
the native language but referenced within expression Strings. Only a limited set of functions have been provided in 
the core SLOP package, but writing your own is very easy. Let's take a look at creating a random function:

```java
public class RandomOp implements Function {

    @Override
    public String getName() {
        return "RAND";
    }

    @Override
    public Token<?> execute(SLOPConfig config, List<Token<?>> values) {
        Random random = new Random();
        if (values.size() < 1 || values.size() > 2) {
            throw new ParserException("Expected two arguments e.g. 'RAND(\"integer\", 1000)'");
        }
        if (!(values.get(0).getValue() instanceof String)) {
            throw new ParserException("Expected random type (param 0) to be a String value. Valid values are: " +
                    "\"integer\", \"double\", \"float\", \"long\", \"boolean\"");
        }
        String type = values.get(0).getValue(String.class);
        if (values.size() == 2) {
            if (!type.equalsIgnoreCase("integer")) {
                throw new ParserException("Bound only supported when using the integer type");
            }
            if (values.get(1).getValue() instanceof Integer) {
                throw new ParserException("The bound parameter must be specified as an integer");
            }
        }
        switch (type) {
            case "integer": return new TokenValue(values.size() == 1 ? random.nextInt() :
                random.nextInt(values.get(1).getValue(Integer.class)));
            case "float": return new TokenValue(random.nextFloat());
            case "double": return new TokenValue(random.nextDouble());
            case "boolean": return new TokenValue(random.nextBoolean());
            case "long": return new TokenValue(random.nextLong());
        }
        throw new ParserException(String.format("Unexpected type provided in random function '%s'", type));
    }
}
```
Functions are simple from a code standpoint as it simply requires implementing the Function interface and methods.
The above function will provide the ability to create random numbers using a type parameter. It is important when
creating functions to check and throw errors if the parameter list is not provided or in the expected format. The
code itself is fairly self-explanatory and will allow it to be called by using the RAND name and two parameters
(the second being restricted on the type). Next we need to add it to our config:
```java
SLOPConfig config = new SLOPConfig();
config.addFunction(new RandomOp());
SLOPProcessor processor = new SLOPProcessor(config);
```
Finally we are ready to test it out. It can be useful to test any SLOP changes by using the SLOPRunner class in the 
test package. This allows expressions to typed in and evaluated after the enter key is pressed. Here is some output 
for our new function:
```bash
> RAND("integer")
Result: 13788106 (Time taken: 3ms)
> RAND("integer",1000)
Result: 730 (Time taken: 5ms)
> RAND("integer",100)
Result: 55 (Time taken: 2ms)
> RAND("boolean")
Result: true (Time taken: 2ms)
> RAND("boolean")
Result: false (Time taken: 2ms)
> RAND("float")
Result: 0.5708346 (Time taken: 2ms)
> RAND("long")
Result: -7999691372481956837 (Time taken: 1ms)
```
Although this is just an example, it shows that we can leverage the underlying native language to fulfill either
shortfalls in what is possible with the included literals and statements or to avoid overly complex expressions. 
Functions can be used for anything and as an example could be used to make a REST call. Using Spring Boot for 
example we could pass an autowired adapter class into the function constructor. The functions process method 
could then use the parameters to make one or more calls based on the criteria in the expression String held on 
a database. For example if in a bank a custom wanted them to notify him if his balance is lower than a certain
figure and there are more than 10 days left in a certain month, the expression which could be located on the
customers database record could trigger an event to handle this.

### Operators
SLOP uses the same operators as C / C++ / Java languages out of the box. Below a list of the supported operators
with examples:

| Name | Notation | Example | Result |
|:-----|:---------|:--------|:-------|
| Add  | +        | 1 + 1   | 2      |
| Subtract | -    | 4 - 2   | 2      |
| Divide | /      | 4 / 2   | 2      |
| Multiply | *    | 2 * 2   | 4      |
| Greater or Equal To | >= | 5 >= 4 | true |
| Less than or Equal To | <= | 5 <= 4 | false |
| Less than | <   |  3 < 4   | true   |
| Greater than | &gt; | 3 &gt; 4   | false  |
| Modulus | %     | 8 % 3   | 2      |
| Not Equal | !=  | 1 != 2  | true   |
| Equals | ==     | 2 == 2  | true   |

The default set of operators follow the standard BODMAS order of mathematical operations. This along with modification or 
addition of new operators is supported. Please see the [Custom Operators](#custom-operators) section for more information. 

**NOTE**: There is a limitation where operators that modify left-side values i.e. increment (++), decrement (--)
are only supported for special cases in specific statements. This is because at present the Parser does not
support modification of variables directly, but have to be reassigned using the 'val = expr' pattern. An issue has
been logged about this and will look to included in upcoming versions. Until that time to increment a variable
please use 'myVar = {?myVar} + 1'.

### Statements
Statements facilitate the bulk of the logic happens within expressions. They can take an input and filtering, 
fetch or modify data into a desired output. SLOP comes with a set of basic statements included but has been designed to
support many more. Please see the [Extending](#extending) section for more information. The following section will
run through each of these statements providing examples of how they can be used either individually or together.

#### Conditional
The conditional or ternary statement is the easiest way to evaluate a condition and select a single value as a result.
It is made up of three parts (condition, true result, false result) and if you're familiar with programming works in the 
same way most languages use them. Conditionals can be simple:
```bash
4 > 3 ? "yes" : "no"
```
or complex:
```bash
12 % (sampleObject.componentA * sampleObject.componentB + 6 <= (SUM(sampleObject.subObjects.value,4) -
    sampleObject.componentC / 3) + 30 ? 9 * sampleObject.subObjects[0].subSubObject.valueB + 2 : 3)
```
Conditionals are greedy by nature meaning that they eat preceding or following tokens until a parent statement token
is found. This can make them difficult to read as it is not always clear where they begin and end (the above is a
good example of this). They can be chained together to evaluate multiple conditions with results:
```bash
person.age < 2 ? "Infant" : person.age < 9 ? "Child" : person.age < 19 ? "Adolescent" : ...
```
For most scenarios this would not be the recommended as it is not easy to read. Please look to the [Switch](#switch) 
statement for a better alternative.

#### Operation
An operation does not provide any specific outcome / logic on its own. It can be used to isolate sections of an expression
either to group logic together or to affect the order in which it is evaluated. It can be defined by wrapping a section
in brackets. Using the previous example taken from the [Context](#context) section for dividing the number of employees
by company revenue:
```bash
(acme.revenue / acme.employees.size()).intValue()
```
In this situation if we did not use an operation, we would not be able to map the resulting value to an integer using a 
native call. Another example of where using operations is beneficial is to ensure correct evaluation with operators. 
For example:
```
1 - 2 * 3 / 4
```
In this scenario, the result may be different from what is expected. The default result for this is -0.5, however
if we want it to calculate from left to right, we would need to do the following to override the default operator order:
```
((1 - 2) * 3) / 4
```
In this case the result would be 0.75 which matches what we're expecting.

**NOTE**: The default operator order is BODMAS but this can be changed. Please see [Custom Operators](#custom-operators)
for more information.

#### Repeat (Loop)
The repeat statement is a collection of one or more expressions that is repeated until a condition is met. This is
similar to the for loop in most languages which consist of three parts. These are:

1) The loop variable definition and method of progression
2) A value to initialize the loop variable
3) The condition that will be checked every iteration. If it is met then the loop will exit.

The body of the repeat statement is a series of separate expression Strings that are separated by semi-colons. Let's look
at the simplest example:
```
repeat(i++,1,<11) result = {?i};
```
The result of this is:
```
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
```
We can reverse that by doing the following:
```
repeat(i--,10,>0) result = {?i};
```
Which will result in:
```
[10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
```
Each individual expression String in the body operate in exactly the same way as a standard expression. As such, you 
can use other statements or assign values to variables. The 'result' variable though is a special case where the value 
assigned is used in the resulting array for that iteration. The order of expression evaluation goes from left to right, 
much like a standard language if you added a new line character between each e.g.:
```
repeat(index++;0;<acme.employees.size())
   salaryAge = acme.employees[{?index}].salary / acme.employees[{?index}.age; 
   revCalculation = acme.revenue / {?salaryAge}; 
   result = switch({?revCalculation})[< 2000: {?salaryAge} * 0.15;< 4000: {?salaryAge} * 0.10; ... ]
```

#### For Each (Loop)
The foreach loop is a simplified version of the repeat statement. It simplifies the loop criteria
by iterating over a collection of objects assigning each value to a scoped variable. In the
following example each employee in a collection is assigned to the variable 'emp':
```bash
foreach(emp : acme.employees) result = "Name: " + {?emp}.name + ", Age: " + {?emp}.age;
```
The result from this would be an array of Strings:
```bash
["Name: Jennifer Smith, Age: 39", "Name: Bob Seinfeld, Age: 23", ...]
```
The primary reason for this loop type is to simplify the definition so we don't have to reference
each object by using the collection and index reference. If you are just looking to extract a specific
field from a collection then you can simply use the following without the loop:
```bash
acme.employees.name
```
Getting back to the for each, we can also filter the results back from the loop. In the following
example we are filtering the names returned by age:
```bash
foreach(emp : acme.employees) {?emp}.age > 40 ? result = {?emp}.name : null;
```
This would just return the names of only those individuals older than 40. This use case is not perfect
as you see we are only setting the result by using a conditional where the false case return an unused
'null' value. This could be fixed by adding a single conditional 'if' statement, but this exceeds the 
scope for this simple scenario. Please see [Adding Statements](#adding-statements) for more information.

#### Switch
The switch statement has been so far the most ambitious of the project. Being frustrated by the lack of flexibility in
the Java version, I attempted to overcome these shortfalls by adding many of the features I would have liked to be
present as possible. Whether all of these changes somehow removes the basic idea of a simple switch is up for debate,
but below I will list and explain these features below.

At it's core the idea of a switch is simple. You evaluate a single value against a set of other values (or cases) and 
execute code paired with the match. At the basic level the SLOP statement does not differ from that, you can still
use that formula:
```java
switch(emp.name)["Mary": "Banker";"Bob": "Coder"; "Jim": "Marketing"; default: "I don't know"]
```
In this case our value is passed as a parameter and cases are separated by semi-colons with the condition present before
the colon and the result after. So far so simple, but one of the restrictive things about this model is the limited
cases (no pun intended) where you can use this. What happens if you want to evaluate integers for example? Say we have
multiple conditions where we want to check whether a value falls within some set boundaries. In Java this is not currently
possible at the time of writing this. In SLOP though we have no such restriction and therefore can use the following:
```java
switch(person.age)[< 1: "Baby";< 3: "Toddler";< 12: "Adolescent"; ...]
```
Firstly there is an issue with the above statement. If you run it and the person declared in the parameter is 2 for example, 
the result would be a collection of multiple results being ["Toddler", "Adolescent", ...] and every case after. This is 
because the switch statement does not stop when it finds a match by default and will return all matching condition results 
in the set. The avoid this we can use the '!' flag following the case definition:
```java
switch(person.age)[< 1: "Baby"!;< 3: "Toddler"!;< 12: "Adolescent"!; ...]
```
The above would now return the correct result of just "Toddler". You may notice that we are defining all case evaluations 
without specifying the value we're comparing it to. This is typical for a switch, but should this be required (especially 
when declaring multiple parameters - more on this later!), you can reference each parameter by doing the following:
```java
switch(person.age)[$0 < 1: "Baby";$0 < 3: "Toddler";$0 < 12: "Adolescent"; ...]
```
This is unnnecessary or the most part so you can ditch that just in favour of using the desired operator and comparison
value. It is also not restricted to using single value evaluations. For example, we could do the following:
```java
switch(person.salary)[> acme.revenue / acme.averageSalary: "High Earner"; ...]
```
Much like the C / C++ / Java equivalents, the SLOP switch has a default case which acts as a fallback if none of the
other cases match. The condition is replaced by the keyword 'default', but follows the same pattern with the result
being provided after the semi-colon. An example of this can be seen in the first example on this page.

##### Multiple Parameters
A typical switch has one parameter by which it is used to compare against the cases. What would happen though if we 
wanted to pass in multiple values or use those in a condition? You can provide multiple parameters by defining them
separated by a comma. There are two methods of evaluating these parameters in each switch case. The first is using
them in a single condition. For example:
```java
switch(acme.outgoings, acme.revenue)[$0 > $1: "Deficit";$0 < $1: "Profit";$0 == $1: "Holding Even"]
```
Here we are using the earlier method of referencing parmeters. It could be argued that the values could be simply referenced 
without passing them as parameters which is true, however by using the parameter reference it makes it a lot more concise.
Using this same method you can also evaluate both parameters against some value:
```java
switch(acme.outgoings, acme.revenue)[
    $0 > 500000 AND $1 - $0 > 100000: "We're Ok";
    $0 > 750000 AND $1 - $0 > 50000: "Cause for concern"; 
    $0 > 750000 AND $1 - $0 <= 10000: "Uh oh"; 
    ...
]
```
As one final feature of the SLOP switch, parameters can be contained within their own conditions separated by a comma
character. For example:
```java
switch(a.intA, a.intB, a.intC)[>5,>10,>15: "First";>10,<12,20: "Second"]
```
When defining parameter conditions like this, you must ensure that the number of comma separated conditions matches
the number of parameters otherwise an exception will be thrown.

##### Future Improvements
These are the features I will also be looking to add in the near future:
1. Evaluation against arrays of objects. This would be similar to how a contains() works against the parameter variable.
For example, you could use:
  ```java
  switch(emp.name)[["Mary","Bob","Susan"]: "Board Member"; default: "Employee"]
  ```
2. Similar to the multiple expressions suppported by the repeat and for-each, add the ability to add groups of expressions
per case. As the current case syntax separator is already a semi-colon, an optional grouping could be added e.g.:
  ```java
  switch(emp.age)[<30: {empAgeCompanyAge = $0 * acme.age; result = "R = " + {?empAgeCompanyAge}}]
  ```

#### Field
The field statement is a simple method of structuring objects and fields to access values. Similar to how this would
be done in Java, field or object references are separated by a period character (.). Depending on how many levels
deep the statement goes within sub-objects or collections it can vary in size. Collections or Maps can use square
brackets to specify an index or key to extract a particular value. Firstly though, let's look at a simple example:
```
acme.revenue / acme.employees.size()
```
In the above we have two field statements. The first is an Integer field whilst the second uses the native call
'size()' on the employee's collection*. Fields can be used in calculations or statements so long as the returned
value is compatible e.g. ``employee.isActive / employee.dateJoined`` would throw a Parser exception. Moving on
from this we can access specific items in collections using the following:
```
acme.employees[0].name
```
Alternative you can specify the following if the employees object was a Map:
```
acme.employees["#E0001354"].name
```
The Field statement also has a useful shortcut to extract values. Typically you would need to iterate through
collections with an incrementing index, but in this case that can be done automatically to return the value:
```
acme.employees.firstName + " " + acme.employees.lastName
```
This would return an array of every employee's full name. As SLOP is open with regards to adding new types, you
could get creative with the above to form new structures or filter / extract data with little to no effort.

*NOTE: Native calls can only be used if the SAFE_OPERATIONS property in the configuration object is set to true.
Please see the [Overview - Configuration Class](#configuration-class) for more information.

#### Variables
Variables at present are classified as statements because they are matched and processed using the Grammar system.
They currently have limited ability aside from being set and read from the context. To set a value you simply define
a name with an equals and the value e.g.
```
myValue = 12
```
To reference that value, you wrap it in curly brackets with a question mark prefix before the name e.g.
```
result = {?myValue} * 4
```
At previously mentioned, the increment (++) and decrement (--) operators are not currently supported for modifying
variable values. This is because it needs Parser support to be added to modify context values directly. Support will
be added for this in a future release. If you do want to increment the value of a variable, you will need to reference
it in the operation and store it back to itself e.g.
```
myValue = {?myValue} + 1
```
Variables can be used in Chained Expressions or statements which support multiple expressions. For example, when
using the repeat statement you can share values between them in a left-to-right direction. Using the fibonacci
example again, we can see this in use:
```
[0,1] + repeat(i++,0,<10) result = {?first} + {?second}; first = {?second}; second = {?result};
```

### Extra Features

#### Saving / Loading Lexer Output
#### Chained Expressions

## Extending
### Design Approach
All literals and statements must extend the Token class in order for it to be used by SLOP. Let's take a look at an 
example with the LongToken class:
```java
@NoArgsConstructor
public class LongToken extends Token<Long> {
    public LongToken(Long value) {
        super("Long", value);
    }

    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    @Override
    public String getPattern() {
        return "^([0-9]+)L";
    }

    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        return Collections.singletonList(this);
    }

    @Override
    public Token<Long> createToken(String value) {
        return new LongToken(Long.parseLong(value));
    }
}
```
The first thing to notice is that the Token class takes a type. This represents the type that the token is going to 
store. For literals this will be the underlying language type matching the one we want to represent e.g. Long,
Integer, String, Float, Double etc. In this case we pass it 'Long' which will determine the type of the value to
which a value (read from the expression String) gets stored. This can be retrieved at any time using the Token.getValue()
method.

Each token class must implement several methods which are getPatternType, getPattern, process and createToken. These
are described below:
- **getPatternType**: Returns a PatternType to determine whether to match against a regular expression or a Grammar 
  expression pattern.
- **getPattern**: Defines the pattern as determined by the value given in the getPatternType method. This could either 
  be a regular expression or Grammar expression pattern. For more information on grammar patterns please see the 
  [Grammar](#grammar) section for more information.
- **process**: This method returns the resulting value for the token which is returned to the Parser. In the case of a 
  literal this is simply the token itself as all values must be returned in a Collection of Token<?>. For statements, 
  this method would contain code to evaluate conditions, process child tokens and calculate the result to return (See
  [Adding Statements](#adding-statements)).
- **createToken**: This method gets invoked by the Lexer when the pattern (either regular expression or grammar) is
  matched. The method parameter value extracted directly from the expression String and will need to be cast to the 
  relevant type. This is then passed to the new instance of the Token via the constructor.

Another important aspect is the definition of the Token's constructor which must call the superclass constructor 
with the passed parameter value. This is so that upon creation, the internal value variable is set and all relevant 
resources are initialised. 

**NOTE**: This documentation will not go into any detail on how to write regular expressions. There are however many 
helpful guides and tools on the web to get started. One I can recommend and use frequently myself is 
[regexr](https://www.regexr.com 'Learn Regular Expressions') but am not in any way affiliated with this site.

### Grammar
The grammar pattern determines not only the structure of the statement and how it can be defined, but also structure 
of how the tokens are organised into groups for processing. To start there are two basic grammar tokens which cause 
other values to be captured. These are 'val' (single) and 'expr' (multiple). Around this you can group tokens together 
using brackets or define syntax by wrapping values in single quotes. For example, the grammar for a FunctionToken is 
the following:
```bash
val '(' ( expr ','? )+ ')'
```
Breaking this down we get the following:
```
val   = Captures single value
'('   = Denotes an opening bracket in the syntax
(     = Start of a grammar group
expr  = Captures one or more values
','?  = An optional comma value. '?' marks the preceding token as optional
)+    = End of the grammar group. The '+' denotes that the contents of this group can be repeated one or more times
')'   = A closing bracket in the syntax
```
Using this we can pass the following values in an expression which would be a valid match:
```bash
DATE("14-02-1983 15:49:03", "dd-MM-yyyy hh:mm:ss")
SUM(45 * 15 + 1, 2 / 3, 3)
```
The first match would result in the following token output:
```
FunctionToken( tokenGroups = [
    TokenGroup( tokens = [
        StringToken( value = "DATE" )
    ],
    TokenGroup( tokens = [
        TokenGroup ( tokens = [ StringToken( value = "14-02-1983 15:49:03" ) ] ),
        TokenGroup ( tokens = [ StringToken( value = "dd-MM-yyyy hh:mm:ss" ) ] ),
    ] )
] )
```
In the above example, the 'val' capture group even though it is a single token gets added into its own TokenGroup. A
second TokenGroup gets added to contain the one or more 'expr' capture groups that are expected. Expressions 
representative of one or more tokens always get added into their own TokenGroups so that there is a clear split between
subsequent parameters (in this case) and the tokens which go to constructing that single parameter.

For the more complicated second example, it would be the following:
```
FunctionToken( tokenGroups = [
    TokenGroup( tokens = [
        StringToken( value = "SUM" )
    ],
    TokenGroup( tokens = [
        TokenGroup ( tokens = [
            IntegerToken( value = 45 ),
            OperatorToken( value = * ),
            IntegerToken( value = 15 ),
            OperatorToken( value = + ),
            IntegerToken( value = 1 )
        ] ),
        TokenGroup ( tokens = [
            IntegerToken( value = 2 ),
            OperatorToken( value = / ),
            IntegerToken( value = 3 ),
        ] ),
        TokenGroup ( tokens = [ IntegerToken( value = 3 ) ] )
    ] )
] )
```
When defining a grammar pattern you should be aware of the structure of other tokens defined within SLOP. Should
one of the existing Tokens conflict with the new Token and the Lexer is unsure of which match to use, it will
throw a LexerException to highlight the issue.

There is one final token which can be used when specifying grammar which is the '<' flag. This can be seen in
the SwitchToken:

     ... ( expr ','? )+ ':' expr '!'<? ';'? )+ ...

This can follow a syntax token and tells the Lexer not to discard it but instead add it to the context as it would
with a normal token as a TokenValue or NonToken. This can be useful to pass flags to the token processing code
(triggered by the Parser) to denote a certain situation. In the above example if an exclamation mark is found in a
switch case then it will stop looking at any other cases (similar to how a break works in the Java switch statement).

If constructing a new Grammar expression string is still confusing and how it relates to the resulting TokenGroup
structure, please follow the example provided in the [Adding Statements](#adding-statements) section.

### Pattern Tokens
Although this section is not strictly necessary for extending SLOP, it describes the mechanism through which more 
complex tokens (statements) are identified and updated. As opposed to the standard literal tokens which use regular 
expressions to match and have their values set, statements are added to a stack and appended to until the token is 
marked as complete and added to the list of resulting tokens.

Without wanting to skip over how literals are scanned, a loop scans through the expression String and each token's
pattern is then evaluated against it to see if there is a match. Take the following:
```
12345.99 + 100 / 50
```
Using the following token patterns:
```
DecimalToken = ^-?[0-9]+\.[0-9]+
IntegerToken = ^-?[0-9]+
OperatorToken = ^(\+\+|--|\+|-|\/|\*|>=|<=|<|>|%|!=|==)
```
We can perform a loop now on the expression, removing each match until it is empty. The String is trimmed after each 
iteration which matches other languages where whitespace is ignored. Using this we see the following iteration events:
```
1) DecimalToken matched with value '12345.99'
2) OperatorToken matched with the value '+'
3) IntegerToken matched with the value '100'
4) OperatorToken matched with value '/'
5) IntegerToken matched with value '50'
```
This is a very simple example, but the same basic premise is used for statements. The only difference here is that the
grammar pattern is mapped into a series of hierarchical pattern tokens. This is used to keep track of the location of 
the last read token and what we are expecting the next token to be. For example, using the OperatorToken as an example
which has the following pattern:
```
'(' expr ')'
```
This is mapped to the following pattern:
```
GrammarGroup(position = 0, tokens = [
    GrammarValue('('),
    GrammarExpression(),
    GrammarValue(')')
])
```
All pattern tokens (even if they are not part of a defined group in the pattern) will be part of a GrammarGroup. This 
is because the GrammarGroup is used to record and track the current position within the token. If we assume that we 
only have this statement in the list of token handlers, with the following expression:
```
1 + (2 + 3)
```
We first read the Integer and Operator tokens and add them to the resulting token list. However, when we get to 
the '(', none of the literals trigger a match. A check of the topmost stack item is made first to ensure that it is 
not the next logical token in that. As our stack is empty however, we look for a new match in the token handlers 
list and find a match with the OperatorToken. A new instance of that token is created and added to the top of the stack 
and the parent GrammarGroup pattern token's position is updated to 1. The position always points to the next expected 
token that the token is expecting. In this case the positions are 0-indexed hence 1 pointing to the GrammarExpression.

The Lexer then reads three more tokens which are an IntegerToken, OperatorToken and a final IntegerToken. As the next
token after the GrammarExpression is a closing bracket ')', all of these get added to the child token list of the 
OperatorToken. Finally the closing brace is read, the tokens position is updated and marked as complete and removed 
from the top of the stack and added to the resulting token list:
```
[== Resulting Tokens ==]
IntegerToken(1),
OperatorToken(+),
OperationToken(tokens = [
    TokenGroup(tokens = [      
        IntegerToken(2),
        OperatorToken(+),
        IntegerToken(3)
    ])    
])
```
This is a very simple example, but what happens if we have embedded statements? This is where the stack comes more 
into play. Let's take the following expression as an example:
```
((1 - 2) * 3) / 4 < -1 ? "no" : "yes"
```
In the above although it may look simple, we have 3 levels of statements being processed. For this example I will
describe each stage and provide the state of the stack. In the beginning our stack is empty and our first read
token is an open bracket. Like the last example we have a match in the OperationToken which gets created and added
to the top of the stack. Unlike last time though, the next read token is also an open bracket. As our top-most
stack item is not expecting a closing bracket without at least some content, we look to the token handler list 
and again match the OperationToken which again gets created and added to the stack.

After the first two tokens have been evaluated, this is the state of our stack:
```
[== Stack ==]
OperationToken(position = 1, tokens = []) <-- Stack Top
OperationToken(position = 1, tokens = [])
```
The next three tokens are read and as they are two literals and an operator they are added to the top-most stack item. 
The next token is a closing bracket which matches the next logical token in our top-most stack item. As such, it's 
position is increased to the next logical token. In this case though as there are only 3 pattern tokens that form the
OperatorToken, the position actually exceeds the number of tokens. This triggers the token to be flagged as complete.
The current state of the stack matches the following:
```
[== Stack ==]
OperationToken(position = 3, tokens = [
    TokenGroup(tokens = [      
        IntegerToken(1),
        OperatorToken(-),
        IntegerToken(2)
    ])
])
OperationToken(position = 1, tokens = [])
```
Prior to the next token being read, a check is made to check the state of the current top-most stack item. In this case
it is found to be complete and as such it is removed from the stack and because the stack is not empty added as a child
of the next stack item:
```
[== Stack ==]
OperationToken(position = 1, tokens = [
    TokenGroup(tokens = [      
        OperationToken(position = 3, tokens = [
            TokenGroup(tokens = [                  
                IntegerToken(1),
                OperatorToken(-),
                IntegerToken(2)
            ])
        ])
    ])    
])
```
Two more tokens are then read which are an OperatorToken(*) and an IntegerToken(4). These are added to the top-most stack
item and a final closing brace is recorded which causes the last remaining item on the stack to be marked as complete:
```
[== Stack ==]
OperationToken(position = 3, tokens = [
    TokenGroup(tokens = [      
        OperationToken(position = 3, tokens = [
            TokenGroup(tokens = [                  
                IntegerToken(1),
                OperatorToken(-),
                IntegerToken(2)
            ]),            
        ]),
        OperatorToken(*),
        IntegerToken(3)
    ]),    
])
```
The OperationToken as the last remaining token is popped from the top of the stack and added to the resulting list of tokens.
Four more tokens are read and added to the token list which are the OperatorToken(/), IntegerToken(4), OperatorToken(<) and
finally an IntegerToken(-1). Without moving into the job of the Parser, if the expression ended here this would result in
a condition which would result in a Boolean. As it is, the list of tokens looks like the following:
```
[== Resulting Tokens ==]
OperationToken(position = 3, tokens = [
    TokenGroup(tokens = [  
        OperationToken(position = 3, tokens = [
            TokenGroup(tokens = [              
                IntegerToken(1),
                OperatorToken(-),
                IntegerToken(2)
            ]),            
        ]),
        OperatorToken(*),
        IntegerToken(3)
    ]),    
]),
OperatorToken(/),
IntegerToken(4),
OperatorToken(<),
IntegerToken(-1)
```
Something then unexpected happens where we find a '?' token. This is part of a token called a Conditional which has the
following grammar pattern:
```
expr '?' expr ':' expr
```
What is strange about this is that we had no prior warning that the previously lexed tokens were part of a statement. You
may also notice that the '?' is not the first token in the pattern. Since that was an expression capture group, the first which 
can be matched is actually the second token which says something about the statement's nature. The conditional is greedy and 
scoops up tokens immediately before and after as it has no strict opening and closing syantax tags. The start and end of the
statement can be identified by looking at those which share the same level / scope as the statement itself. For this 
example a match is made against the ConditionalToken and a new instance added onto the stack with all prior tokens added. 
The new state of the stack looks like the following:
```
[== Stack ==]
ConditionalToken(position = 2, tokens = [
    TokenGroup(tokens = [    
        OperationToken(position = 3, tokens = [
            TokenGroup(tokens = [
                OperationToken(position = 3, tokens = [
                    IntegerToken(1),
                    OperatorToken(-),
                    IntegerToken(2)
                ])
            ]),
            OperatorToken(*),
            IntegerToken(3)
        ]),
        OperatorToken(/),
        IntegerToken(4),
        OperatorToken(<),
        IntegerToken(-1)
    ]),    
])
```
As the active token is now a different grammar expression, all captured tokens will be written to a new token group. As
there is only a single String token, that is added to that. The next token is a ':' which matches the next logical token 
of the head of the stack in the Conditional statement. The position is incremented again to the position past the last
token so that it now points to position 4 which is the last 'expr' capture group. As the last remaining token is a
solitary StringToken as well, the final picture of the stack prior to the ConditionalToken being removed looks like the
following:
```
[== Stack ==]
ConditionalToken(position = 4, tokens = [
    TokenGroup(tokens = [    
        OperationToken(position = 3, tokens = [
            ...
        ]),
        ...
        IntegerToken(-1)        
    ]),
    TokenGroup(tokens = [    
        StringToken("no")
    ]),
    TokenGroup(tokens = [    
        StringToken("yes")
    ]),        
])
```
As the lexer has now reached the end of the expression and there are no further tokens to be added, a finalizeExpression
method is called which removes any tokens still on the stack (in this case the ConditionalToken) and adds it to the 
resulting token list. These are then returned to the SLOPProcessor and can either be serialized to a String or File or
passed to the Parser for evaluation and resolution.

## Additional Grammar / Pattern Flags
In the above section we have covered how pattern tokens are used by the Lexer to keep track of where in the statement 
read tokens belong. There are several grammar flags which cause this flow to be disrupted or change. The first of these
is the '+' flag which can be applied to any Grammar Group in a String. For example, taking the FieldToken pattern we
can see this being used to represent a repeating pattern:
```bash
( val ( '[' expr ']' )? '.'? )+
```
The entire pattern is wrapped in a parent grammar group which can be repeated one or more times (denoted by the following 
'+' character). Inside is a 'val' representing a single value capture group with an optional following grammar group 
(denoted by the following '?') with a set of square brackets surrounding an expression capture group. Finally this is 
followed by an optional syntax period character ('.'). If we provide some examples it should become clear as to the
values it is trying to capture:
```
object.field
object.collection[0].field
object.map["value" + 1]
```
As can be seen some of the examples have index / key references whilst others do not. This is where the '?' tokens allows
optional values to be captured. Going further we can split the middle example up to the following:
```
object.
collection[0].
field
```
Each one represents an individual capture event of the repeated grammar group pattern defined above. Likewise, each one
will be captured in its own TokenGroup. Let's then start running over the above example and see how the pattern tokens
are structured and see how the Lexer handles them. The grammar lexer will output the following pattern tokens onto the
FieldToken when SLOP is initialised:
```
GrammarGroup(position = 0, multiple = true, tokens = [
    GrammarValue(capture = true),
    GrammarGroup(position = 0, optional = true, tokens = [
        GrammarValue('['),
        GrammarExpression(),
        GrammarValue(']'),
    ]),
    GrammarValue(value = '.', optional = true)
])
```
Reading the expression String 'object.collection[0].field', the first read token is a TokenValue('object') and so far
no match occurs is found in our pattern as it is too generic. Keep in mind that a match only happens if an identifying 
tag (typically syntax) is read. The next token however is a match for the '.' found following the optional grammar group.
This may seem a bit odd as we have skipped the entire second GrammarGroup, but keep in mind the Lexer will only apply 
strict matching if it is required. As this is optional, it skipped ahead and looked for a matching token which it found 
in the optional GrammarValue('.'). Upon finding this match, a new instance of the FieldToken is added to the top of the
stack and TokenValue('object') is added into a TokenGroup.

The position of the group is then set to the position after the '.' which in this case exceeds the number of pattern 
tokens in the group. A check is triggered and since the group supports multiple occurances, it's position and all
contained group positions are reset back to their initial positions. At this stage the state of the stack looks like
the following:
```
[== Stack ==]
FieldToken(tokens = [
    TokenGroup(tokens = [    
        TokenValue('object')  
    ])  
])
```
The next token is another TokenValue('collection') which gets added to the short term memory list of tokens. This is used
to store recently read tokens which we're not quite sure what to do with yet. The next token is a TokenValue('[') which
triggers a match in the FieldToken on the stack but for a different reason than the previous. On this occasion instead 
of skipping over the optional group it has found a match in the starting syntax token. Looking at the pattern tokens
again we can see how the positions are updated to reflect this:
```
GrammarGroup(position = 1, multiple = true, tokens = [
    GrammarValue(capture = true),
    GrammarGroup(position = 1, optional = true, tokens = [
        GrammarValue('['),
        GrammarExpression(),
        GrammarValue(']'),
    ]),
    GrammarValue(value = '.', optional = true)
])
```
The current active pattern token is a grammar expression and will be used to capture any tokens found within the square
brackets. In our case it is just a single IntegerToken(0) and after the subsequent closing TokenValue(']') is read, the
stack looks like the following:
```
[== Stack ==]
FieldToken(tokens = [
    TokenGroup(tokens = [
        TokenGroup(tokens = [    
            TokenValue('object')  
        ]),
        TokenGroup(tokens = [    
            TokenValue('collection'),
            TokenGroup(tokens = [
                IntegerToken(0)
            ])  
        ])
    ])        
])
```
The pattern tokens with their positions are the following:
```
[== Pattern Tokens ==]
GrammarGroup(position = 2, multiple = true, tokens = [
    GrammarValue(capture = true),
    GrammarGroup(position = 3, optional = true, tokens = [
        GrammarValue('['),
        GrammarExpression(),
        GrammarValue(']'),
    ]),
    GrammarValue(value = '.', optional = true)
])
```
The current active token is pointing to the GrammarValue('.') which is the next token read by the Lexer. Again the
position is updated and the group is marked as complete, a check is performed to determine whether it can be captured
multiple times (which it can) and again the position of the group reset.

Finally we are on the last iteration through the FieldToken with the last remaining token being read which is a 
GrammarValue('field'). In this case though we have no clear closing syntax as fields are not followed by a '.'. This
is where the Lexer has to use a bit of deductive reasoning which differs depending on the scenario. In our case we
have a single value and the end of the expression was met. As such, it is a safe bet to add it to the top stack 
item. If we had the following scenario however:
```
object.collection[0].field * 40
```
The Lexer would then look at the top item in the stack and the following tokens to make a best guess. In this situation
the FieldToken does support a single TokenValue and is a valid match against the tokens pattern. Since that has a higher
probability of a match rather than existing as a single TokenValue between the statement and an Operator / Integer token, 
again it is safe to assume that it can be added to that item.

This covers the process by which the Lexer matches tokens against statements using pattern tokens and how it deals with 
the different types of grammar flags. As mentioned previously, this information is not strictly required for writing your
own statements, but it is useful to understand why tokens are organised into the groups they are and the mechanism used
to facilitate this.

**NOTE**: Although not covered here, when a grammar group is marked to capture multiple sets of tokens (as above), if 
it is found that the group is in its reset state (complete) but a following token after that group triggers a match, 
the token position will be moved to that location to avoid being stuck in an endless cycle of repetitions. The lexer 
uses intelligent look ahead in this scenario.

### Type Operations
If you've read the section [Design Approach](#design-approach), you should now know how to add your own types.
You should now be able to define a value using declared syntax and see that type reflected in its tokenized form. If 
you're unsure of how to do this, the best way to do this would be to stick a breakpoint in your IDE in the 
``YourType.process(...)`` method. The next stage is to add type operations so that when you use an operator with the 
new type, an appropriate action occurs and a result returned. For example, say I want to be able to define
a new map type I would declare a class with the following*:
```java
@NoArgsConstructor
public class MapToken extends Token<Map<Token<?>, Token<?>>> {

    public MapToken(Map<Token<?>, Token<?>> values) {
        super("Map", values);
    }

    @Override
    public Token<Map<Token<?>, Token<?>>> createToken(String value) {
        MapToken source = new MapToken(new HashMap<>());
        Token<Map<Token<?>, Token<?>>> result = cloneDefaultProperties(source);
        result.setValue(source.getValue());
        return result;
    }

    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    @Override
    public String getPattern() {
        return "'{' ( ( val '->' expr ','? ) )+ '}'";
    }

    @Override
    public List<Token<?>> process(SLOPParser parser, SLOPContext context, SLOPConfig config) {
        if (Objects.isNull(getValue())) {
            HashMap<Token<?>, Token<?>> aMap = new HashMap<>();
            //Unwrap to bare-bones groups and tokens
            Token<?> mapStructure = getTokenGroups().get(0).unwrapKeepStructure(getTokenGroups().get(0), 1);
            if (!(mapStructure instanceof TokenGroup)) {
                throw new ParserException("Expected map to simplify to TokenGroup but got " +
                        mapStructure.getClass().getSimpleName());
            }
            //Iterate through map structure and resolve all contained token values
            ((TokenGroup) mapStructure).getTokens()
                    .forEach(tg -> {
                        if (!(tg instanceof TokenGroup)) {
                            throw new ParserException("Expected mapping to be TokenGroup but got " +
                                    tg.getClass().getSimpleName());
                        }
                        Token<?> key = ((TokenGroup) tg).getTokens().get(0);
                        key = parser.processExpression(key instanceof TokenGroup ?
                                ((TokenGroup)key).getTokens() : Collections.singletonList(key), context);
                        Token<?> value = ((TokenGroup) tg).getTokens().get(1);
                        value = parser.processExpression(value instanceof TokenGroup ?
                                ((TokenGroup)value).getTokens() : Collections.singletonList(value), context);
                        aMap.put(key, value);
                    });
            setValue(aMap);
        }
        return Collections.singletonList(this);
    }

    @Override
    public String toString() {
        if (Objects.isNull(getValue())) {
            throw new ParserException("Map values have not been processed yet");
        }
        String result = getValue().entrySet().stream()
                .map(e -> e.getKey().getValue().toString() + " -> " + e.getValue().getValue().toString())
                .collect(Collectors.joining(", "));
        return "MapToken{values = [" +
                result +
                "]}";
    }
}
```
This allows us to now define a Map using ``{Key->Value,Key->Value,...}``. If you enable unsafe operations 
(See [Configuration Class](#configuration-class)), you can add, remove or fetch values as the core class
is simply a Map and those functions can be performed out of the box e.g.
```bash
> {1->"first",2->"second"}.get(1)
Result: "first" (Time taken: 1ms)
```
The maps are not specific on type and can be defined using different types:
```bash
> myVar = {"first"->1,2->"second"}
> {?myVar}.put("third",3)
> {?myVar}
Result: {"first"=1, 2="second", "third"=3} (Time taken: 1ms)
> {?myVar}.remove("third")
> {?myVar}
Result: {"first"=1, 2="second"} (Time taken: 1ms)
```
Type operations can perform the same operations but slightly allow more customization. For example, by defining the
following in a MapOperations class and registering it in the config:
```java
public class MapOperation implements TypeOperation {
    
    @Override
    public boolean canHandle(Token<?> first, OperatorToken operator, Token<?> second) {
        return first.is(Map.class) && (second.is(Map.class) || second.is(List.class));
    }

    @Override
    @SuppressWarnings("unchecked")
    public Token<?> process(SLOPConfig config, Token<?> first, OperatorToken operator, Token<?> second) {
        OperatorHandler handler = config.getOperatorHandler();
        //We can suppress warnings here as we've already checked the contained token
        Map<Token<?>,Token<?>> original = first.getValue(Map.class);
        if (second.is(Map.class)) {
            Map<Token<?>,Token<?>> compare = second.getValue(Map.class);
            return handleMapOperations(handler, original, compare, operator,
                    () -> handleCustomOperator(first, operator, second));
        } else {
            List<Token<?>> compare = second.getValue(List.class);
            return handleListOperations(handler, original, compare, operator,
                    () -> handleCustomOperator(first, operator, second));
        }
    }

    @SuppressWarnings("unchecked")
    private Token<?> handleMapOperations(OperatorHandler handler, Map<Token<?>,Token<?>> original,
                                         Map<Token<?>,Token<?>> compare, OperatorToken operator,
                                         CustomOperation customOp) {
        Map<Token<?>, Token<?>> result;
        switch (handler.getOpType(operator)) {
            /* Adds two maps together e.g. {1:1,2:2} + {3:3,4:4} = {1:1,2:2,3:3,4:4}. Note that if there is a key
             * which already exists in the left-side map then it will be overwritten by that on the right e.g.
             * {1:1,2:2} + {1:2,3:3} = {1:2,2:2,3:3} */
            case ADD:
                original.putAll(compare);
                result = original;
                break;
            /* Subtracts a map from the other e.g. [1:1,2:2,3:3] - [2:2,3:3] = [1:1]. Please note items are only removed
             * if their key and values match. There if you did [1:1] - [1:3], you'd still result in the original [1:1] */
            case SUBTRACT:
                List<Token<?>> keysToRemove = original.keySet().stream()
                        .filter(compare.keySet()::contains)
                        .collect(Collectors.toList());
                keysToRemove.forEach(kr -> {
                    //Only remove those with matching values too
                    if (original.get(kr).equalsValue(compare.get(kr))) {
                        original.remove(kr);
                    }
                });
                result = original;
                break;
            default: result = (Map<Token<?>, Token<?>>) customOp.handleCustomOp();
        }
        return new MapToken(result);
    }

    @SuppressWarnings("unchecked")
    private Token<?> handleListOperations(OperatorHandler handler, Map<Token<?>,Token<?>> original,
                                         List<Token<?>> compare, OperatorToken operator,
                                         CustomOperation customOp) {
        Map<Token<?>, Token<?>> result;
        if (handler.getOpType(operator) == OperatorType.SUBTRACT) {
            /* Subtracts the matching keys present in the right-side list from the left-side map */
            List<Token<?>> keysToRemove = original.keySet().stream()
                    .filter(compare::contains)
                    .collect(Collectors.toList());
            keysToRemove.forEach(original::remove);
            result = original;
        } else {
            result = (Map<Token<?>, Token<?>>) customOp.handleCustomOp();
        }
        return new MapToken(result);
    }

    @Override
    public <T> T handleCustomOperator(Token<?> first, OperatorToken operator, Token<?> second) {
        throw new ParserException(String.format("Unable to handle Map operation with operator '%s'",
                operator.getValue()));
    }

    /**
     * Cuts down on the amount of parameters needing to be passed to each operation handler method
     */
    @FunctionalInterface
    private interface CustomOperation {
        Token<?> handleCustomOp();
    }
}

```
The following operations can now be performed:
```
{1->1,2->2} + {3->3,4->4}
Result: {1=1, 2=2, 3=3, 4=4} (Time taken: 46ms)
{1->1,2->2,3->3} - {2->2}
Result: {1=1, 3=3} (Time taken: 14ms)
{1->1,2->2,3->3,4->4} - [1,3]
Result: {2=2, 4=4} (Time taken: 12ms)
```
To explain this, the ``canHandle`` method in the TypeOperation interface determines which types the operations class can 
handle from the Parser. For most types this would be restricted to the same type. However, it provides the flexibility 
to control extra types to provide more functionality or for ease of use. In the above case the MapOperations class 
will trigger if the first token is a Map but the second could be either a Map or List. This is purely because whilst 
the ``+`` operation is handy for adding elements to a Map, the ``-`` is a bit overkill to specify both a key and value 
to remove the entry. As such, we can utilise the List to represent the keys to simplify things.

*NOTE: This code is now included as part of the core SLOP functionality.

### Custom Operators
### Adding Statements

## Questions / Issues?
